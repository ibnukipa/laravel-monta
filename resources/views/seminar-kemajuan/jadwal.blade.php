
@extends('app')

@section('template_title')
	Jadwal Seminar Kemajuan TA {{ Auth::user()->name }}
@endsection

@section('content')
<div class="wadah" style="">
	<div class="row">
		<div class="col s3" style="width: 19%; padding: 0; position: fixed; left: 0; z-index: 9">
			<div class="card" style="box-shadow: none; margin: 0">
				<section id="menu-user">
					@if(Auth::user()->hasRole('mahasiswa'))
						@include('partials.menu-mahasiswa')
					@elseif(Auth::user()->hasRole('koordinator') || Auth::user()->hasRole('kaprodi'))
						@include('partials.menu-koordinator')
					@elseif(Auth::user()->hasRole('operator'))
						@include('partials.menu-operator')
					@elseif(Auth::user()->hasRole('administrator'))
						@include('partials.menu-admin')
					@endif
				</section>
			</div>
		</div>
		<section id="content-container">
			<div class="col s10 right" style="width: 81%; padding: 0;">
				<div class="card bayangan_2dp" style="margin: 0">
			        <div class="status-content" style="background: #364150 ; color: white; font-weight: 500">
					{!! Breadcrumbs::render('pendaftaran-seminar-kemajuan-jadwal') !!}
			        </div>
			        <div class="progress" style="margin: 0; background-color: #B3E5FC;">
					    <div class="indeterminate" style="background-color: #0288D1"></div>
					</div>
					<section id="content" style="border-bottom: 3px solid {{Auth::user()->warna['utama']}} ; padding-top: 20px; color: red ?>">
						<h4 class="head-form">
							Jadwal Seminar Kemajuan TA
						</h4>

						<h6 class="center" style="margin-bottom: 20px; color: {{Auth::user()->warna['utama']}}; opacity: .5">Teknik Lingkungan ITS</h6>
						<div class="divider"></div>

						<div class="row">
							<div class="col s12">
								@include('partials.form-status')
							</div>
							@if(Auth::user()->proposal->status_id >= 10)
							<div class="col s12 mr-top1" style="text-align: left;">
								<ul>
									<li class="btn-hov">
										<a data-url="{{ route('jadwal.file', ['seminar-kemajuan']) }}" data-bread="{{Breadcrumbs::render('pendaftaran-seminar-kemajuan-file-jadwal')}}" onclick="load_content(this)" style="padding: 0 15px;" class="btn bayangan_2dp blue">
											<i class="material-icons left" style="line-height: inherit; margin-right: 5px; font-size: 1.3em">collections</i>
											<span class="font_button">File Jadwal</span>
										</a>
									</li>
									{{-- <li class="btn-hov pull-right">
										<a target="_blank" href="{{ route('berkas', [Auth::user()->id, 'fta04']) }}" style="padding: 0 15px;" class="btn bayangan_2dp grey-dark">
											<i class="material-icons left" style="line-height: inherit; margin-right: 5px; font-size: 1.3em">print</i>
											<span class="font_button">Form FTA-04</span>
										</a>
									</li> --}}
								</ul>
							</div>
							<div class="col s12 mr-top1">
								<div class="form-blue">
									<div class="row no-margin">
										
										@if( !$jadwal )
										Maaf! Jadwal online Anda belum tersedia.
										@else
											<div class="input-field col s6 mr-top2">
												<i class="material-icons prefix">date_range</i>
												<?php $waktu = new Waktu ?>
												{!! Form::text('waktu_seminar',
													$waktu->hari[date('N', strtotime($jadwal->tanggal))] .
													date(', d M Y', strtotime($jadwal->tanggal)) ." (". date('H:i', strtotime($jadwal->jadwal_jam['mulai'])) ." - ".date('H:i', strtotime($jadwal->jadwal_jam['selesai'])) .")"
												, array('style' => 'font-size: 1.3rem; font-weight: 400','id' => 'waktu_seminar', 'class' => '', 'required' => 'required', 'readonly' => 'readonly')) !!}
												{!! Form::label('waktu_seminar', 'Waktu Seminar : ' , array('class' => 'active')); !!}
											</div>

											<div class="input-field col s6 mr-top2">
												<i class="material-icons prefix">room</i>
												{!! Form::text('ruang', $jadwal->jadwal_ruang->description, array('style' => 'font-size: 1.3rem; font-weight: 400','id' => 'ruang', 'class' => '', 'required' => 'required', 'readonly' => 'readonly')) !!}
												{!! Form::label('ruang', 'Ruang Seminar Proposal TA: ' , array('class' => 'active')); !!}
											</div>

											<div class="input-field col s6 mr-top2">
												<i class="material-icons prefix">person</i>
												{!! Form::text('dosen_penguji1', $jadwal->dosen_penguji[0]->dosen->full_name, array('style' => 'font-size: 1.3rem; font-weight: 400','id' => 'dosen_penguji1', 'class' => '', 'required' => 'required', 'readonly' => 'readonly')) !!}
												{!! Form::label('dosen_penguji1', 'Dosen Penguji 1: ' , array('class' => 'active')); !!}
											</div>

											<div class="input-field col s6 mr-top2">
												<i class="material-icons prefix">person</i>
												{!! Form::text('dosen_penguji2', $jadwal->dosen_penguji[1]->dosen->full_name, array('style' => 'font-size: 1.3rem; font-weight: 400','id' => 'dosen_penguji2', 'class' => '', 'required' => 'required', 'readonly' => 'readonly')) !!}
												{!! Form::label('dosen_penguji2', 'Dosen Penguji 2: ' , array('class' => 'active')); !!}
											</div>
											<div class="input-field col s6 mr-top2">
												<i class="material-icons prefix">person</i>
												{!! Form::text('dosen_penguji3', $jadwal->dosen_penguji[2]->dosen->full_name, array('style' => 'font-size: 1.3rem; font-weight: 400','id' => 'dosen_penguji3', 'class' => '', 'required' => 'required', 'readonly' => 'readonly')) !!}
												{!! Form::label('dosen_penguji3', 'Dosen Penguji 3: ' , array('class' => 'active')); !!}
											</div>

											<div class="input-field col s6 mr-top2">
												<i class="material-icons prefix">donut_small</i>
												{!! Form::text('dosen_penguji3', $jadwal->jadwal_kategori->description, array('style' => 'font-size: 1.3rem; font-weight: 400','id' => 'dosen_penguji3', 'class' => '', 'required' => 'required', 'readonly' => 'readonly')) !!}
												{!! Form::label('dosen_penguji3', 'Tipe Seminar: ' , array('class' => 'active')); !!}
											</div>
										@endif
									</div>
								</div>
							</div>
							@endif
						</div>
					</section>
				</div>
			</div>
		</section>
	</div>
</div>

@endsection

@section('template_scripts')
	<script src="{{URL::asset('js/dash.js')}}"></script>
	@include('scripts.load-content-script')
@endsection

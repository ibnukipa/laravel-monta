<h4 class="head-form">
	TAMBAH KEGIATAN ASISTENSI
</h4>

<h6 class="center" style="margin-bottom: 20px; color: {{Auth::user()->warna['utama']}}; opacity: .5">Teknik Lingkungan ITS</h6>
<div class="divider"></div>

{!! Form::open(array('url' => 'asistensi', 'method' => 'POST', 'class' => ' form-blue lockscreen-credentials form-horizontal', 'role' => 'form', 'files'=>true)) !!}
	<div class="row no-margin">
        <div class="col s12" style="margin-bottom: 1rem">
			@include('partials.form-status')
        </div>
		<div class="input-field col s4 mr-top3">	
            <i class="material-icons prefix" style="margin-top: -0.5rem">date_range</i>
            {!! Form::input('date', 'tanggal_asistensi', null, ['id' =>'tanggal_asistensi', 'class' => 'datepicker', 'placeholder' => 'Pilih tanggal...', 'style' => 'font-size: 1.3rem']); !!}
            {!! Form::label('tanggal_asistensi', 'Tanggal Kegiatan / Pembahasan Asistensi' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
        </div>

		<div class="input-field col s12 mr-top2">
			<i class="material-icons prefix">import_contacts</i>
			{!! Form::textarea('keterangan', null , array('id' => 'keterangan', 'class' => 'form-control materialize-textarea', 'required' => 'required')) !!}
   			{!! Form::label('keterangan', 'Keterangan Kegiatan / Pembahasan' , array('class' => 'active')); !!}
		</div>
		
         <div class="col s12 mr-top2" style="text-align: right; border-top: 1px solid rgba(0, 0, 0, .1); padding-top: 1rem" >
            <ul>
                <li class="btn-hov">
                    {!! Form::button("<i class='material-icons left'' style='line-height: inherit; margin-right: 5px; font-size: 1.3em'>save</i><span class='font_button'>Simpan</span>", array('class' => 'btn bayangan_2dp green','type' => 'submit')) !!}
                </li>
            </ul>
        </div>

	</div>
{!! Form::close() !!}
<script src="{{URL::asset('js/dash.js')}}"></script>
<script type="text/javascript">
    $('.datepicker').pickadate({
            selectMonths: true,
            selectYears: 15,
            format: 'dd-mm-yyyy'
        });
</script>

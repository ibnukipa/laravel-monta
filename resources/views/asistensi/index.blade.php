
@extends('app')

@section('template_title')
	Dashboard {{ Auth::user()->name }}
@endsection

@section('template_linked_css')
    
@endsection

@section('content')
<div class="wadah" style="">
	<div class="row">
		<div class="col s3" style="width: 19%; padding: 0; position: fixed; left: 0; z-index: 9">
			<div class="card" style="box-shadow: none; margin: 0">
				<section id="menu-user">
					@include('partials.menu-mahasiswa')
				</section>
			</div>
		</div>
		<section id="content-container">
			<div class="col s10 right" style="width: 81%; padding: 0;">
				<div class="card bayangan_2dp" style="margin: 0">
			        <div class="status-content" style="background: #364150 ; color: white; font-weight: 500">
					{!! Breadcrumbs::render('asistensi-kegiatan') !!}
			        </div>
			        <div class="progress" style="margin: 0; background-color: #B3E5FC;">
					    <div class="indeterminate" style="background-color: #0288D1"></div>
					</div>
					<section id="content" style="border-bottom: 3px solid {{Auth::user()->warna['utama']}} ; padding-top: 20px; color: red ?>">
						<h4 class="head-form">
							Kegiatan Asistensi Tugas Akhir
						</h4>

						<h6 class="center" style="margin-bottom: 20px; color: {{Auth::user()->warna['utama']}}; opacity: .5">Teknik Lingkungan ITS</h6>
						<div class="divider"></div>

						<div class="row">
							<div class="col s12">
								@include('partials.form-status')
							</div>
                            <div class="col s12 mr-top1" style="text-align: left;">
								<ul>
									<li class="btn-hov">
										<a data-url="{{ route('asistensi.create') }}" data-bread="{{Breadcrumbs::render('asistensi-kegiatan-create')}}" onclick="load_content(this)" style="padding: 0 15px;" class="btn bayangan_2dp green">
											<i class="material-icons left" style="line-height: inherit; margin-right: 5px; font-size: 1.3em">add</i>
											<span class="font_button">Buat Kegiatan Asistensi Baru</span>
										</a>
									</li>
									
									{{-- <li class="btn-hov pull-right">
										<a target="_blank" href="{{ route('berkas', [Auth::user()->id, 'fta03']) }}" style="padding: 0 15px;" class="btn bayangan_2dp grey-dark">
											<i class="material-icons left" style="line-height: inherit; margin-right: 5px; font-size: 1.3em">print</i>
											<span class="font_button">Form FTA-03</span>
										</a>
									</li>
									
									<li class="btn-hov pull-right" style="margin-right: 1rem">
										<a target="_blank" href="{{ route('file.download', ['single-cek-format', Auth::user()->id]) }}" style="padding: 0 15px;" class="btn bayangan_2dp grey-dark">
											<i class="material-icons left" style="line-height: inherit; margin-right: 5px; font-size: 1.3em">file_download</i>
											<span class="font_button">Borang Cek Format</span>
										</a>
									</li> --}}
								</ul>
							</div>
							<div class="col s12" style="padding: 1rem">
                                <table id="users_table" class="highlight" style="margin-bottom: 2rem">
                                    <thead>
                                        <tr class="grey-light">
											<td width="7%">No</td>
                                            <th width="15%">Tanggal</th>
                                            <th width="65%">Keterangan</th>
                                            <th width="10%">Tindakan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($kegiatans as $key => $kegiatan)
                                            <tr>
												<td>{{ $key+1 }}.</td>
                                                <td>{{ date("d-m-Y",strtotime($kegiatan->tanggal)) }}</td>
                                                <td style="text-align: justify" >{{ $kegiatan->description }}</td>
                                                <td class="center">
                                                    <ul class="no-margin">
                                                        <li style="display: inline-block">
                                                            {!! Form::open(array('url' => url('asistensi/' . $kegiatan->id))) !!}
																{!! Form::hidden('_method', 'DELETE') !!}
																<a onclick="doDelete(this)" data-target="confirmDelete" data-judul="Apakah Anda yakin akan menghapus Data Asistensi ini?" style="border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto" class=" red">
											                        <span class="font_button" style="text-transform: none">Hapus</span>
																</a>
															{!! Form::close() !!}
                                                        </li>
                                                    </ul>
												</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div> 
							</div>
						</div>
					</section>
				</div>
			</div>
		</section>
	</div>
</div>
@include('modals.modal-delete')
@endsection

@section('template_scripts')
	<script src="{{URL::asset('js/dash.js')}}"></script>
	
	{{-- Data Table --}}
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
	@include('scripts.load-content-script')
	@include('scripts.modal-delete-script')
    <script type="text/javascript">
		$(function () {
			$(document).ready(function() {
			    // DataTable
			    var table = $('#users_table').DataTable({
			    	language: {
			    		searchPlaceholder: "Cari Data...",
                        "zeroRecords": "Tidak ada data Kegiatan Asistensi.",
			    	},
			    	"columnDefs": [
				    	{
				    		"targets"	: [3],
				    		"searchable": false,
				    		'bSortable': false,
				    	}
			    	],
                    "searching": true,
			    	"paging": true,
			    	"autoWidth": false,
			    });
			 
			   
			} );
		});
    </script>
@endsection

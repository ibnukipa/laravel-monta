@if(session()->get('status'))
	<div class="chip alert {{ session()->get('status')['color'] }} mr-top1" style="font-size: 1rem">
		{{ session()->get('status')['content'] }}
		<i class="close material-icons">close</i>
	</div>
@endif

@if(count($errors) == 1)
	<div class="chip alert red-light mr-top1">
		@foreach ($errors->all() as $error)
			{{ $error }}
		@endforeach
		<i class="close material-icons">close</i>
	</div>
@endif

@if(isset($status))
	<div class="chip alert {{ $status['color'] }} mr-top1" style="font-size: 1rem">
		{!! $status['content'] !!}
		{{-- <i class="close material-icons">close</i> --}}
	</div>
@endif

@if((isset($lewat) && $lewat) || (isset($belum)) && $belum)
	@if(isset($lewat))
		<div class="chip alert red-dark mr-top1" style="font-size: 1rem">
			Mohon Maaf! Portal sudah ditutup.
			{{-- <i class="close material-icons">close</i> --}}
		</div>
	@endif

	@if(isset($belum))
		<div class="chip alert grey-dark mr-top1" style="font-size: 1rem">
			Mohon Maaf! Portal {{ $tipeSeminar }} pada semester ini belum tersedia.
			{{-- <i class="close material-icons">close</i> --}}
		</div>
	@endif
@endif

{{-- @if(Auth::user() && Auth::user()->has('catatan') && Auth::user()->catatan->count() > 0 )
	<div class="chip alert grey-dark mr-top1" style="font-size: 1rem; height: auto; line-height: 1rem; padding: 12px; font-weight: 400">
		Catatan: 
		<br>
	@foreach (Auth::user()->catatan as $catatan)
		@if ($catatan->tipe == 'proposal')
			- {{ $catatan->name }} : {{ $catatan->description }}
			<br>
		@endif
	@endforeach
	</div>
@endif --}}
@if($breadcrumbs)
<div class='row' style='margin-bottom: 0px'><div class='col s8' style='padding: 0; display: table'><i class='material-icons' style='vertical-align: middle; color: white; font-size: 25px'>dashboard</i><span style='vertical-align: middle; font-size: 1.2rem;'><a href='{{ url('/') }}' style='color: white; opacity: .9'> Dashboard </a></span>@foreach($breadcrumbs as $breadcrumb)<i class='material-icons' style='font-size: 20px; vertical-align: middle; ccolor: white; opacity: .8'>chevron_right</i><span style='vertical-align: middle; font-size: 15px;'><a href='#' style='color: white; opacity:.7'></a> {{ $breadcrumb->title }} </span>@endforeach</div><div class='col s4' style='vertical-align: middle; padding: 0 10px; text-align: right;'><span style='vertical-align: middle; font-size: 13px; font-weight: 400'><strong style='font-weight: 700'>Status: </strong>
@if(Auth::user()->hasRole('koordinator')) koordinator
@elseif (Auth::user()->hasRole('administrator')) admin
@elseif (Auth::user()->hasRole('operator')) operator 
@elseif (Auth::user()->hasRole('kaprodi')) kaprodi
@elseif (!Auth::user()->proposal) Belum ada pendaftaran proposal 
@elseif (Auth::user()->proposal->status_id <= 15) {{Auth::user()->proposal->status->name}}
@elseif (Auth::user()->tugas_akhir) {{ Auth::user()->tugas_akhir->status->name }}
@endif</span></div></div>@endif
<ul class="collapsible bayangan_2dp" data-collapsible="expandable" style="border: none; min-height: 100vh">
    <li class="nama-menu">
        <div class="collapsible-header" style="border: none; background-color: {{Auth::user()->warna['ketiga']}} ; color: white; font-weight: 300; font-size: 1.2rem">
            <!-- <i class="material-icons left" style="color: white; font-size: 2rem">menu</i> -->
            <?php  $tahunAjar = new TahunAjaran ?> 
            <div style="opacity: .9; font-weight: 300; text-transform: capitalize;">Semester {{ $tahunAjar->getSemester() }} {{ $tahunAjar->getTahunAjaran() }}</div>
        </div>
    </li>

    <!-- Start Proposal TA -->
    <li style="padding: 0">
        <div class="collapsible-header" style="border: none">
            <a href="{{ route('akun-pegawai.showAll') }}" style="display: block">
                <i class="material-icons left" style="color: {{Auth::user()->warna['ketiga']}}; font-size: 1.4rem">people</i>
                <i class="material-icons right detail" style="font-size: 1rem; margin: 0; position: absolute;">play_circle_outline</i>
                <span style="vertical-align: middle; text-align: left; display: inline-block">
                    <span style="display: block; position: relative; line-height: 1; font-size: 18px">
                    Akun Pegawai
                    </span>
                    <span style="opacity: .5; text-align: left; display: block; position: relative; line-height: 1; font-size: 13px">
                    MonTA Teknik Lingkungan
                    </span>
                </span>
            </a>
        </div>
    </li>

    <li style="padding: 0">
        <div class="collapsible-header" style="border: none">
            <a href="{{ route('akun-mahasiswa.showAll') }}" style="display: block">
                <i class="material-icons left" style="color: {{Auth::user()->warna['ketiga']}}; font-size: 1.4rem">people</i>
                <i class="material-icons right detail" style="font-size: 1rem; margin: 0; position: absolute;">play_circle_outline</i>
                <span style="vertical-align: middle; text-align: left; display: inline-block">
                    <span style="display: block; position: relative; line-height: 1; font-size: 18px">
                    Akun Mahasiswa
                    </span>
                    <span style="opacity: .5; text-align: left; display: block; position: relative; line-height: 1; font-size: 13px">
                    MonTA Teknik Lingkungan
                    </span>
                </span>
            </a>
        </div>
    </li>

    <li style="padding: 0">
        <div class="collapsible-header" style="border: none">
            <a href="{{ route('data-dosen.showAll') }}" style="display: block">
                <i class="material-icons left" style="color: {{Auth::user()->warna['ketiga']}}; font-size: 1.4rem">toc</i>
                <i class="material-icons right detail" style="font-size: 1rem; margin: 0; position: absolute;">play_circle_outline</i>
                <span style="vertical-align: middle; text-align: left; display: inline-block">
                    <span style="display: block; position: relative; line-height: 1; font-size: 18px">
                    Data Dosen
                    </span>
                    <span style="opacity: .5; text-align: left; display: block; position: relative; line-height: 1; font-size: 13px">
                    MonTA Teknik Lingkungan
                    </span>
                </span>
            </a>
        </div>
    </li>

    <li style="padding: 0">
        <div class="collapsible-header" style="border: none">
            <a href="{{ route('data-jam.showAll') }}" style="display: block">
                <i class="material-icons left" style="color: {{Auth::user()->warna['ketiga']}}; font-size: 1.4rem">toc</i>
                <i class="material-icons right detail" style="font-size: 1rem; margin: 0; position: absolute;">play_circle_outline</i>
                <span style="vertical-align: middle; text-align: left; display: inline-block">
                    <span style="display: block; position: relative; line-height: 1; font-size: 18px">
                    Data Jam Seminar
                    </span>
                    <span style="opacity: .5; text-align: left; display: block; position: relative; line-height: 1; font-size: 13px">
                    MonTA Teknik Lingkungan
                    </span>
                </span>
            </a>
        </div>
    </li>

    <li style="padding: 0">
        <div class="collapsible-header" style="border: none">
            <a href="{{ route('admin.berkas') }}" style="display: block">
                <i class="material-icons left" style="color: {{Auth::user()->warna['ketiga']}}; font-size: 1.4rem">perm_media</i>
                <i class="material-icons right detail" style="font-size: 1rem; margin: 0; position: absolute;">play_circle_outline</i>
                <span style="vertical-align: middle; text-align: left; display: inline-block">
                    <span style="display: block; position: relative; line-height: 1; font-size: 18px">
                    Pengaturan Berkas
                    </span>
                    <span style="opacity: .5; text-align: left; display: block; position: relative; line-height: 1; font-size: 13px">
                    MonTA Teknik Lingkungan
                    </span>
                </span>
            </a>
        </div>
    </li>
    
</ul>

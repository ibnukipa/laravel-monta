<nav style="background-color: #2b3643">
	<div class="nav-wrapper fixed">
	    <!-- <a href="#" class="show-on-large disable_select" style="padding: 0 15px; float: left; position: relative; z-index: 1;"><i class="material-icons" style="font-size: 30px">menu</i></a> -->
	    <div style="line-height: 0; display: inline-block; vertical-align: middle; margin-top: -4px">
	        <span class="judul_utama disable_select">Monitoring Tugas Akhir</span>
	        <span class="judul_kedua disable_select">MonTA - Teknik Lingkungan ITS</span>
	    </div>
	    <ul style="float: right">
			<li class="btn-hov">
				<a class="waves-effect waves-light">
	                <span class="font_button">
						<div id="date_real" style="color: rgba(255, 255, 255, .5); text-transform: none"></div>
					</span>
	            </a>
	            <a class="waves-effect waves-light">
	                <span class="font_button">
						<div id="time_real" style="color: rgba(255, 255, 255, .5); text-transform: none"></div>
					</span>
	            </a>
	        </li>
			<li class="btn-hov">
	            <a href="{{url('umum/tugas-akhir')}}" class="waves-effect waves-light btn bayangan_2dp grey-dark">
	                <i class="material-icons left" style="line-height: inherit; margin-right: 5px; font-size: 1.3em">class</i>
	                <span class="font_button">TUGAS AKHIR</span>
	            </a>
	        </li>
			@if(Auth::guest())
	        <li class="btn-hov">
	            <a href="{{url('home')}}" style="padding: 0 15px; background-image: -webkit-linear-gradient(top,#8BC34A,#7EB83D);" class="waves-effect waves-light btn bayangan_2dp">
	                <i class="material-icons left" style="line-height: inherit; margin-right: 5px; font-size: 1.3em">lock</i>
	                <span class="font_button">MASUK</span>
	            </a>
	        </li>
			
			@else
	        <li class="hover-menu" style="min-width: 200px">
	            <a class="dropdown-button" data-activates="drop_account" data-warna="kedua" style="padding-right: 0">
	                <i class="material-icons left">account_circle</i>
	                <i class="material-icons right hide-on-small-only">arrow_drop_down</i>
	                <span class="hide-on-small-only" style="vertical-align: middle; text-align: right; display: inline-block">
	                    
	                    <span style="display: block; position: relative; line-height: 1; font-size: 20px">
	                    	{{ Auth::user()->username }}
	                    </span>
	                    <span style="text-transform: capitalize; opacity: .5; text-align: left; display: block; position: relative; line-height: 1; font-size: 15px">
	                        @foreach(Auth::user()->roles as $role)
	                        	{{$role->name}}
	                        @endforeach
	                    </span>
	                </span>
	            </a>
	            <ul id='drop_account' class='dropdown-content'>
	                <li>
	                    <a style="color: #3e464c" href="{{ url('home') }}">
	                        <i class="material-icons left hide-on-small-only">dashboard</i>
	                        <span>Dashboard</span>
	                    </a>
	                </li>
	                <li>
	                    <a style="color: #3e464c" href="{{ route('reset') }}">
	                        <i class="material-icons left hide-on-small-only">lock</i>
	                        <span>Ganti Password</span>
	                    </a>
	                </li>
	                <li class="divider"></li>
	                <li>
	                    <a style="color: #3e464c" href="{{ url('auth/logout') }}">
	                        <i class="material-icons left hide-on-small-only">exit_to_app</i>
	                        <span>Logout</span>
	                    </a>
	                </li>
	            </ul>
	            
	        </li>
			@endif
	    </ul>
	 
	</div>
</nav>


<ul class="collapsible bayangan_2dp" data-collapsible="expandable" style="border: none; min-height: 100vh">
    <li class="nama-menu">
        <div class="collapsible-header" style="border: none; background-color: {{Auth::user()->warna['ketiga']}} ; color: white; font-weight: 300; font-size: 1.2rem">
            <!-- <i class="material-icons left" style="color: white; font-size: 2rem">menu</i> -->
            <?php  $tahunAjar = new TahunAjaran ?> 
            <div style="opacity: .9; font-weight: 300; text-transform: capitalize;">Semester {{ $tahunAjar->getSemester() }} {{ $tahunAjar->getTahunAjaran() }}</div>
        </div>
    </li>

    <!-- Start Proposal TA -->
    <li style="padding: 0">
        <div class="collapsible-header" style="border: none">
            <a href="{{ route('jadwal.index', ['seminar-proposal']) }}" style="display: block">
                <i class="material-icons left" style="color: {{Auth::user()->warna['ketiga']}}; font-size: 1.4rem">date_range</i>
                <i class="material-icons right detail" style="font-size: 1rem; margin: 0; position: absolute;">play_circle_outline</i>
                <span style="vertical-align: middle; text-align: left; display: inline-block">
                    <span style="display: block; position: relative; line-height: 1; font-size: 18px">
                    Jadwal Seminar
                    </span>
                    <span style="opacity: .5; text-align: left; display: block; position: relative; line-height: 1; font-size: 13px">
                    Proposal TA
                    </span>
                </span>
            </a>
        </div>
    </li>

    <li style="padding: 0">
        <div class="collapsible-header" style="border: none">
            <a href="{{ route('jadwal.index', ['seminar-kemajuan']) }}" style="display: block">
                <i class="material-icons left" style="color: {{Auth::user()->warna['ketiga']}}; font-size: 1.4rem">date_range</i>
                <i class="material-icons right detail" style="font-size: 1rem; margin: 0; position: absolute;">play_circle_outline</i>
                <span style="vertical-align: middle; text-align: left; display: inline-block">
                    <span style="display: block; position: relative; line-height: 1; font-size: 18px">
                    Jadwal Seminar
                    </span>
                    <span style="opacity: .5; text-align: left; display: block; position: relative; line-height: 1; font-size: 13px">
                    Kemajuan TA
                    </span>
                </span>
            </a>
        </div>
    </li>

    <li style="padding: 0">
        <div class="collapsible-header" style="border: none">
            <a href="{{ route('jadwal.index', ['sidang']) }}" style="display: block">
                <i class="material-icons left" style="color: {{Auth::user()->warna['ketiga']}}; font-size: 1.4rem">date_range</i>
                <i class="material-icons right detail" style="font-size: 1rem; margin: 0; position: absolute;">play_circle_outline</i>
                <span style="vertical-align: middle; text-align: left; display: inline-block">
                    <span style="display: block; position: relative; line-height: 1; font-size: 18px">
                    Jadwal Sidang
                    </span>
                    <span style="opacity: .5; text-align: left; display: block; position: relative; line-height: 1; font-size: 13px">
                    Tugas Akhir
                    </span>
                </span>
            </a>
        </div>
    </li>

    <li style="padding: 0">
        <div class="collapsible-header @if(isset($mahasiswaMenu)) active click @endif" style="border: none">
            <a href="{{ route('mahasiswa.index') }}" style="display: block">
                <i class="material-icons left" style="color: {{Auth::user()->warna['ketiga']}}; font-size: 1.4rem">view_list</i>
                <i class="material-icons right detail" style="font-size: 1rem; margin: 0; position: absolute;">play_circle_outline</i>
                <span style="vertical-align: middle; text-align: left; display: inline-block">
                    <span style="display: block; position: relative; line-height: 1; font-size: 18px">
                    Daftar Mahasiswa
                    </span>
                    <span style="opacity: .5; text-align: left; display: block; position: relative; line-height: 1; font-size: 13px">
                    Teknik Lingkungan
                    </span>
                </span>
            </a>
        </div>
    </li>
    
</ul>

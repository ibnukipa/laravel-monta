<ul class="collapsible bayangan_2dp" data-collapsible="accordion" style="border: none; min-height: 100vh">
    <li class="nama-menu">
        <div class="collapsible-header" style="border: none; background-color: {{Auth::user()->warna['ketiga']}} ; color: white; font-weight: 300; font-size: 1.2rem">
            <!-- <i class="material-icons left" style="color: white; font-size: 2rem">menu</i> -->
            <?php  $tahunAjar = new TahunAjaran ?> 
            <div style="opacity: .9; font-weight: 300; text-transform: capitalize;">Semester {{ $tahunAjar->getSemester() }} {{ $tahunAjar->getTahunAjaran() }}</div>
        </div>
    </li>

    <li>
        <div class="collapsible-header @if(isset($m_proposal_ta) ) active @endif">
            <a style="color: {{Auth::user()->warna['utama']}}" href="#!">
                <i class="material-icons left">book</i>
                <i class="material-icons right change_icon" style="margin: 0"></i>
                <span style="font-weight: 500">Proposal TA</span>
            </a>
        </div>
        <div class="collapsible-body">
            <ul>
                <li style="padding: 0">
                    <div class="collapsible-header" style="border: none">
                        <a href="{{ route('proposal.index') }}" style="display: block">
                            <i class="material-icons left" style="color: {{Auth::user()->warna['ketiga']}}; font-size: 1.4rem">note_add</i>
                            <i class="material-icons right detail" style="font-size: 1rem; margin: 0; position: absolute;">play_circle_outline</i>
                            <span style="vertical-align: middle; text-align: left; display: inline-block">
                                <span style="display: block; position: relative; line-height: 1; font-size: 17px">
                                Pendaftaran
                                </span>
                                <span style="opacity: .5; text-align: left; display: block; position: relative; line-height: 1; font-size: 13px">
                                Proposal TA
                                </span>
                            </span>
                        </a>
                    </div>
                </li>
                @if(Auth::user()->proposal && Auth::user()->proposal->status_id >= 5 && Auth::user()->proposal->status_id != 15)
                <li style="padding: 0">
                    <div class="collapsible-header" style="border: none">
                        <a href="{{ route('seminar-proposal.index') }}" style="display: block">
                            <i class="material-icons left" style="color: {{Auth::user()->warna['ketiga']}}; font-size: 1.4rem">note_add</i>
                            <i class="material-icons right detail" style="font-size: 1rem; margin: 0; position: absolute;">play_circle_outline</i>
                            <span style="vertical-align: middle; text-align: left; display: inline-block">
                                <span style="display: block; position: relative; line-height: 1; font-size: 17px">
                                Pengajuan Seminar 
                                </span>
                                <span style="opacity: .5; text-align: left; display: block; position: relative; line-height: 1; font-size: 13px">
                                Proposal TA
                                </span>
                            </span>
                        </a>
                    </div>
                </li>
                @endif

                @if(Auth::user()->proposal && (Auth::user()->proposal->status_id >= 10 && Auth::user()->proposal->status_id != 15))
                <li style="padding: 0">
                    <div class="collapsible-header" style="border: none">
                        <a href="{{ route('seminar-proposal.jadwal') }}" style="display: block">
                            <i class="material-icons left" style="color: {{Auth::user()->warna['ketiga']}}; font-size: 1.4rem">date_range</i>
                            <i class="material-icons right detail" style="font-size: 1rem; margin: 0; position: absolute;">play_circle_outline</i>
                            <span style="vertical-align: middle; text-align: left; display: inline-block">
                                <span style="display: block; position: relative; line-height: 1; font-size: 17px">
                                Jadwal Seminar
                                </span>
                                <span style="opacity: .5; text-align: left; display: block; position: relative; line-height: 1; font-size: 13px">
                                Proposal TA
                                </span>
                            </span>
                        </a>
                    </div>
                </li>
                @endif
            </ul>
        </div>
    </li>
    @if(Auth::user()->proposal && (Auth::user()->proposal->status_id == 12 || Auth::user()->proposal->status_id == 14 || Auth::user()->proposal->status_id >= 16) && Auth::user()->tugas_akhir->status_id >= 1)
    <li>
        <div class="collapsible-header @if(isset($m_asistensi_ta) ) active @endif ">
            <a style="color: {{Auth::user()->warna['utama']}}" href="#!">
                <i class="material-icons left">book</i>
                <i class="material-icons right change_icon" style="margin: 0"></i>
                <span style="font-weight: 500">Asistensi TA</span>
            </a>
        </div>
        <div class="collapsible-body">
            <ul>
                <li style="padding: 0">
                    <div class="collapsible-header" style="border: none">
                        <a href="{{ route('asistensi.index') }}" style="display: block">
                            <i class="material-icons left" style="color: {{Auth::user()->warna['ketiga']}}; font-size: 1.4rem">trending_up</i>
                            <i class="material-icons right detail" style="font-size: 1rem; margin: 0; position: absolute;">play_circle_outline</i>
                            <span style="vertical-align: middle; text-align: left; display: inline-block">
                                <span style="display: block; position: relative; line-height: 1; font-size: 17px">
                                Kegiatan
                                </span>
                                <span style="opacity: .5; text-align: left; display: block; position: relative; line-height: 1; font-size: 13px">
                                Asistensi TA
                                </span>
                            </span>
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </li>

    <li>
        <div class="collapsible-header @if(isset($m_kemajuan_ta) ) active @endif">
            <a style="color: {{Auth::user()->warna['utama']}}" href="#!">
                <i class="material-icons left">book</i>
                <i class="material-icons right change_icon" style="margin: 0"></i>
                <span style="font-weight: 500">Seminar Kemajuan TA</span>
            </a>
        </div>
        <div class="collapsible-body">
            <ul>
                @if(Auth::user()->tugas_akhir)
                <li style="padding: 0">
                    <div class="collapsible-header" style="border: none">
                        <a href="{{ route('seminar-kemajuan.index') }}" style="display: block">
                            <i class="material-icons left" style="color: {{Auth::user()->warna['ketiga']}}; font-size: 1.4rem">note_add</i>
                            <i class="material-icons right detail" style="font-size: 1rem; margin: 0; position: absolute;">play_circle_outline</i>
                            <span style="vertical-align: middle; text-align: left; display: inline-block">
                                <span style="display: block; position: relative; line-height: 1; font-size: 17px">
                                Pengajuan  
                                </span>
                                <span style="opacity: .5; text-align: left; display: block; position: relative; line-height: 1; font-size: 13px">
                                Seminar Kemajuan TA
                                </span>
                            </span>
                        </a>
                    </div>
                </li>
                @endif

                @if(Auth::user()->tugas_akhir && Auth::user()->tugas_akhir->status_id >= 5 && Auth::user()->tugas_akhir->status_id != 9)
                <li style="padding: 0">
                    <div class="collapsible-header" style="border: none">
                        <a href="{{ route('seminar-kemajuan.jadwal') }}" style="display: block">
                            <i class="material-icons left" style="color: {{Auth::user()->warna['ketiga']}}; font-size: 1.4rem">date_range</i>
                            <i class="material-icons right detail" style="font-size: 1rem; margin: 0; position: absolute;">play_circle_outline</i>
                            <span style="vertical-align: middle; text-align: left; display: inline-block">
                                <span style="display: block; position: relative; line-height: 1; font-size: 17px">
                                Jadwal Seminar
                                </span>
                                <span style="opacity: .5; text-align: left; display: block; position: relative; line-height: 1; font-size: 13px">
                                Kemajuan TA
                                </span>
                            </span>
                        </a>
                    </div>
                </li>
                @endif
            </ul>
        </div>
    </li>
    @endif

    @if(Auth::user()->tugas_akhir && (Auth::user()->tugas_akhir->status_id > 9 || Auth::user()->tugas_akhir->status_id == 8))
    <li>
        <div class="collapsible-header @if(isset($m_sidang_ta) ) active @endif">
            <a style="color: {{Auth::user()->warna['utama']}}" href="#!">
                <i class="material-icons left">book</i>
                <i class="material-icons right change_icon" style="margin: 0"></i>
                <span style="font-weight: 500">Sidang TA</span>
            </a>
        </div>
        <div class="collapsible-body">
            <ul>
                @if(Auth::user()->tugas_akhir)
                <li style="padding: 0">
                    <div class="collapsible-header" style="border: none">
                        <a href="{{ route('sidang.index') }}" style="display: block">
                            <i class="material-icons left" style="color: {{Auth::user()->warna['ketiga']}}; font-size: 1.4rem">note_add</i>
                            <i class="material-icons right detail" style="font-size: 1rem; margin: 0; position: absolute;">play_circle_outline</i>
                            <span style="vertical-align: middle; text-align: left; display: inline-block">
                                <span style="display: block; position: relative; line-height: 1; font-size: 17px">
                                Pengajuan Sidang
                                </span>
                                <span style="opacity: .5; text-align: left; display: block; position: relative; line-height: 1; font-size: 13px">
                                Tugas Akhir
                                </span>
                            </span>
                        </a>
                    </div>
                </li>
                @endif

                @if(Auth::user()->tugas_akhir && (Auth::user()->tugas_akhir->status_id >= 14) && Auth::user()->tugas_akhir->status_id != 18)
                <li style="padding: 0">
                    <div class="collapsible-header" style="border: none">
                        <a href="{{ route('sidang.jadwal') }}" style="display: block">
                            <i class="material-icons left" style="color: {{Auth::user()->warna['ketiga']}}; font-size: 1.4rem">date_range</i>
                            <i class="material-icons right detail" style="font-size: 1rem; margin: 0; position: absolute;">play_circle_outline</i>
                            <span style="vertical-align: middle; text-align: left; display: inline-block">
                                <span style="display: block; position: relative; line-height: 1; font-size: 17px">
                                Jadwal Sidang
                                </span>
                                <span style="opacity: .5; text-align: left; display: block; position: relative; line-height: 1; font-size: 13px">
                                Tugas Akhir
                                </span>
                            </span>
                        </a>
                    </div>
                </li>
                @endif
            </ul>
        </div>
    </li>
    @endif

</ul>

{!! Form::open(array('url' => route('akun.update', [$user->id]), 'method' => 'PATCH', 'class' => ' form-blue lockscreen-credentials form-horizontal', 'role' => 'form', 'files'=>true)) !!}
	<div class="row no-margin">

		<div class="input-field col s12 ">
            <div style="line-height: 0; display: inline-block; vertical-align: middle; margin-top: -4px; width: 100%">
                <span class="judul_kedua disable_select center">Hak Akses</span>
                <span class="judul_utama disable_select center" style="padding: 5px; text-transform: capitalize">
					{{$user->roles[0]->name}}
                </span>
            </div>
        </div>


		<div class="input-field col s6 mr-top3">
			<i class="material-icons prefix active">account_box</i>
            {!! Form::text('nama_lengkap', $user->full_name, array('id' => 'nama_lengkap', 'class' => 'validate valid', '' => '')) !!}
   			{!! Form::label('nama_lengkap', 'Nama Lengkap' , array('class' => 'active')); !!}
			<div style="margin-top: -.5rem; text-align: right; color: rgba(0, 0, 0, .3); font-size: .8rem; font-weight: 600">
				<span>tuliskan nama selengkap-lengkapnya (jabatan/gelar jika perlu)</span>
			</div>
		</div>

		<div class="input-field col s6 mr-top3">
			<i class="material-icons prefix active">chrome_reader_mode</i>
			{!! Form::text('username', $user->username, array('id' => 'username', 'class' => 'validate valid', '' => '')) !!}
   			{!! Form::label('username', 'Username (NRP/NIP)' , array('class' => 'active')); !!}
			<div style="margin-top: -.5rem; text-align: right; color: rgba(0, 0, 0, .3); font-size: .8rem; font-weight: 600">
				<span>username yang akan digunakan saat login</span>
			</div>
		</div>

        <div class="input-field col s6 mr-top3">
			<i class="material-icons prefix active">https</i>
			{!! Form::password('password_baru', array('placeholder' => '*********', 'id' => 'password_baru', 'class' => '', '' => '')) !!}
   			{!! Form::label('password_baru', 'Password' , array('class' => 'active')); !!}
			<div style="margin-top: -.5rem; text-align: right; color: rgba(0, 0, 0, .3); font-size: .8rem; font-weight: 600">
				<span>kosongkan jika tidak berubah</span>
			</div>
		</div>

		<div class="input-field col s6 mr-top3">
			<i class="material-icons prefix active">https</i>
			{!! Form::password('password_baru_k', array('placeholder' => '*********', 'id' => 'password_baru_k', 'class' => '', '' => '')) !!}
   			{!! Form::label('password_baru_k', 'Konfirmasi Password' , array('class' => 'active')); !!}
			<div style="margin-top: -.5rem; text-align: right; color: rgba(0, 0, 0, .3); font-size: .8rem; font-weight: 600">
				<span>ulangi password</span>
			</div>
		</div>

		@if($user->roles[0]->name != 'administrator')
		<div class="input-field col s12 mr-top3">
			<div class="switch center" style="margin-bottom: 3rem">
				<label style="position: relative">
					Akun tidak Aktif
					<input name="active" type="checkbox" @if($user->active) checked @endif>
					<span class="lever"></span>
					Akun Aktif
				</label>
			</div>
		</div>
		@endif
         <div class="col s12 mr-top2" style="text-align: right; border-top: 1px solid rgba(0, 0, 0, .1); padding-top: 1rem" >
            <ul>
                <li class="btn-hov">
                    {!! Form::button("<i class='material-icons left'' style='line-height: inherit; margin-right: 5px; font-size: 1.3em'>save</i><span class='font_button'>Simpan Perubahan</span>", array('class' => 'btn bayangan_2dp blue','type' => 'submit')) !!}
                </li>
            </ul>
        </div>

	</div>
{!! Form::close() !!}

<script>
	$('#role').material_select();
</script>
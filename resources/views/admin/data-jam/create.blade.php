<h4 class="head-form">
    Tambah Data Jam
</h4>

<h6 class="center" style="margin-bottom: 20px; color: {{Auth::user()->warna['utama']}}; opacity: .5">Teknik Lingkungan ITS</h6>
<div class="divider"></div>

{!! Form::open(array('url' => route('data-jam.store'), 'method' => 'POST', 'class' => ' form-blue lockscreen-credentials form-horizontal', 'role' => 'form', 'files'=>true)) !!}
	<div class="row no-margin">

        <div class="input-field col s8 mr-top3">
			<i class="material-icons prefix" style="margin-top: -0.5rem">donut_small</i>
            {!! Form::select('kategori', $kategori, null, array('id' => 'kategori', 'style' => 'width: 90%', 'required' => 'required')) !!}
            {!! Form::label('kategori', 'Kategori Seminar : ' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
		</div>

		<div class="input-field col s6">
			<i class="material-icons prefix active">account_box</i>
            {!! Form::text('mulai', null, array('id' => 'mulai', 'class' => 'validate valid', 'required' => 'required')) !!}
   			{!! Form::label('mulai', 'Jam Mulai: ' , array('class' => '')); !!}
			<div style="margin-top: -.5rem; text-align: right; color: rgba(0, 0, 0, .3); font-size: .8rem; font-weight: 600">
				<span>Jam mulai seminar. Contoh: 08:00:00</span>
			</div>
		</div>

		<div class="input-field col s6">
			<i class="material-icons prefix active">chrome_reader_mode</i>
			{!! Form::text('selesai', null, array('id' => 'selesai', 'class' => 'validate valid', 'required' => 'required')) !!}
   			{!! Form::label('selesai', 'Jam Selesai: ' , array('class' => '')); !!}
			<div style="margin-top: -.5rem; text-align: right; color: rgba(0, 0, 0, .3); font-size: .8rem; font-weight: 600">
				<span>Jam selesai seminar. Contoh: 08:00:00</span>
			</div>
		</div>

         <div class="col s12 mr-top2" style="text-align: right; border-top: 1px solid rgba(0, 0, 0, .1); padding-top: 1rem" >
            <ul>
                <li class="btn-hov">
                    {!! Form::button("<i class='material-icons left' style='line-height: inherit; margin-right: 5px; font-size: 1.3em'>save</i><span class='font_button'>Simpan</span>", array('class' => 'btn bayangan_2dp green','type' => 'submit')) !!}
                </li>
            </ul>
        </div>

	</div>
{!! Form::close() !!}

<script>
	$('#kategori').material_select();
</script>
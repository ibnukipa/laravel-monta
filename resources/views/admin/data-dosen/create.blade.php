<h4 class="head-form">
    Tambah Data Dosen
</h4>

<h6 class="center" style="margin-bottom: 20px; color: {{Auth::user()->warna['utama']}}; opacity: .5">Teknik Lingkungan ITS</h6>
<div class="divider"></div>

{!! Form::open(array('url' => route('data-dosen.store'), 'method' => 'POST', 'class' => ' form-blue lockscreen-credentials form-horizontal', 'role' => 'form', 'files'=>true)) !!}
	<div class="row no-margin">

		<div class="input-field col s6">
			<i class="material-icons prefix active">account_box</i>
            {!! Form::text('nama_lengkap', null, array('id' => 'nama_lengkap', 'class' => 'validate valid', '' => '')) !!}
   			{!! Form::label('nama_lengkap', 'Nama Lengkap' , array('class' => '')); !!}
			<div style="margin-top: -.5rem; text-align: right; color: rgba(0, 0, 0, .3); font-size: .8rem; font-weight: 600">
				<span>tuliskan nama selengkap-lengkapnya (jabatan/gelar jika perlu)</span>
			</div>
		</div>

		<div class="input-field col s6">
			<i class="material-icons prefix active">chrome_reader_mode</i>
			{!! Form::text('nip', null, array('id' => 'nip', 'class' => 'validate valid', '' => '')) !!}
   			{!! Form::label('nip', 'NIP' , array('class' => '')); !!}
			<div style="margin-top: -.5rem; text-align: right; color: rgba(0, 0, 0, .3); font-size: .8rem; font-weight: 600">
				<span>NIP dengan spasi. contoh: 19500114 197903 1 001</span>
			</div>
		</div>

         <div class="col s12 mr-top2" style="text-align: right; border-top: 1px solid rgba(0, 0, 0, .1); padding-top: 1rem" >
            <ul>
                <li class="btn-hov">
                    {!! Form::button("<i class='material-icons left'' style='line-height: inherit; margin-right: 5px; font-size: 1.3em'>save</i><span class='font_button'>Simpan</span>", array('class' => 'btn bayangan_2dp green','type' => 'submit')) !!}
                </li>
            </ul>
        </div>

	</div>
{!! Form::close() !!}

<script>
	$('#role').material_select();
</script>
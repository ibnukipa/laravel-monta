<h4 class="head-form">
    Tambah Akun
</h4>

<h6 class="center" style="margin-bottom: 20px; color: {{Auth::user()->warna['utama']}}; opacity: .5">Teknik Lingkungan ITS</h6>
<div class="divider"></div>

{!! Form::open(array('url' => route('akun.store'), 'method' => 'POST', 'class' => ' form-blue lockscreen-credentials form-horizontal', 'role' => 'form', 'files'=>true)) !!}
	<div class="row no-margin">

        <div class="input-field col s6">
			<i class="material-icons prefix active">chrome_reader_mode</i>
			{!! Form::text('username', null, array('id' => 'username', 'class' => 'validate valid', 'required' => 'required')) !!}
   			{!! Form::label('username', 'Username (NIP/NRP)' , array('class' => '')); !!}
			<div style="margin-top: -.5rem; text-align: right; color: rgba(0, 0, 0, .3); font-size: .8rem; font-weight: 600">
				<span>Username yang akan digunakan untuk LOGIN.</span>
			</div>
		</div>

		<div class="input-field col s6">
			<i class="material-icons prefix active">account_box</i>
            {!! Form::text('full_name', null, array('id' => 'full_name', 'class' => 'validate valid', 'required' => 'required')) !!}
   			{!! Form::label('full_name', 'Nama Lengkap' , array('class' => '')); !!}
			<div style="margin-top: -.5rem; text-align: right; color: rgba(0, 0, 0, .3); font-size: .8rem; font-weight: 600">
				<span>tuliskan nama selengkap-lengkapnya (jabatan/gelar jika perlu)</span>
			</div>
		</div>

        <div class="input-field col s6 mr-top3">
			<i class="material-icons prefix active">https</i>
			{!! Form::password('password_baru', array('placeholder' => '*********', 'id' => 'password_baru', 'class' => '', 'required' => 'required')) !!}
   			{!! Form::label('password_baru', 'Password' , array('class' => 'active')); !!}
			<div style="margin-top: -.5rem; text-align: right; color: rgba(0, 0, 0, .3); font-size: .8rem; font-weight: 600">
				<span>minimal 6 karakter dan maksimal 20 karakter.</span>
			</div>
		</div>

        <div class="input-field col s6 mr-top3">
			<i class="material-icons prefix" style="margin-top: -0.5rem">donut_small</i>
			{!! Form::select('role', $roles , null, array('id' => 'role', 'style' => 'width: 90%', 'required' => 'required')) !!}
            {!! Form::label('role', 'Hak Akses : ' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
		</div>

         <div class="col s12 mr-top2" style="text-align: right; border-top: 1px solid rgba(0, 0, 0, .1); padding-top: 1rem" >
            <ul>
                <li class="btn-hov">
                    {!! Form::button("<i class='material-icons left'' style='line-height: inherit; margin-right: 5px; font-size: 1.3em'>save</i><span class='font_button'>Simpan</span>", array('class' => 'btn bayangan_2dp green','type' => 'submit')) !!}
                </li>
            </ul>
        </div>

	</div>
{!! Form::close() !!}

<script>
	$('#role').material_select();
</script>
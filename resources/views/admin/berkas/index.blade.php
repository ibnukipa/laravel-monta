
@extends('app')

@section('template_title')
	Pengaturan Berkas
@endsection

@section('content')
<div class="wadah" style="">
	<div class="row">
		<div class="col s3" style="width: 19%; padding: 0; position: fixed; left: 0; z-index: 9">
			<div class="card" style="box-shadow: none; margin: 0">
				<section id="menu-user">
					@if(Auth::user()->hasRole('mahasiswa'))
						@include('partials.menu-mahasiswa')
					@elseif(Auth::user()->hasRole('koordinator') || Auth::user()->hasRole('kaprodi'))
						@include('partials.menu-koordinator')
					@elseif(Auth::user()->hasRole('operator'))
						@include('partials.menu-operator')
					@elseif(Auth::user()->hasRole('administrator'))
						@include('partials.menu-admin')
					@endif
				</section>
			</div>
		</div>
		<section id="content-container">
			<div class="col s10 right" style="width: 81%; padding: 0;">
				<div class="card bayangan_2dp" style="margin: 0">
			        <div class="status-content" style="background: #364150 ; color: white; font-weight: 500">
					{{-- {!! Breadcrumbs::render('pendaftaran-seminar-proposal-on') !!} --}}
			        </div>
			        <div class="progress" style="margin: 0; background-color: #B3E5FC;">
					    <div class="indeterminate" style="background-color: #0288D1"></div>
					</div>
					<section id="content" style="border-bottom: 3px solid {{Auth::user()->warna['utama']}} ; padding-top: 20px; color: red ?>">
						<h4 class="head-form">
							Pengaturan Berkas
						</h4>

						<h6 class="center" style="margin-bottom: 20px; color: {{Auth::user()->warna['utama']}}; opacity: .5">Teknik Lingkungan ITS</h6>
						<div class="divider"></div>

						<div class="row">
							<div class="col s12">
								@include('partials.form-status')
							</div>
						</div>
                        <div class="row">
                            <div class="col s12">
                                {!! Form::open(array('url' => 'admin/berkas/update', 'method' => 'POST', 'class' => ' form-blue lockscreen-credentials form-horizontal', 'role' => 'form', 'files'=>true)) !!}
                                    <div class="row no-margin">
                                        <div class="input-field col s4 mr-top3">
                                            <i class="material-icons prefix" style="margin-top: -0.5rem">insert_drive_file</i>
                                            {!! Form::select('tipe_berkas', array(
                                                    'PTA02' => 'PTA-02', 
                                                    'PTA03' => 'PTA-03', 
                                                    'PTA04' => 'PTA-04',
                                                    'KTA01' => 'KTA-01', 
                                                    'KTA02' => 'KTA-02',
                                                    'KTA03' => 'KTA-03',
                                                    'UTA02' => 'UTA-02',
                                                    'UTA03' => 'UTA-03',
                                                    'FTA03' => 'FTA-03',
                                                    'FTA04' => 'FTA-04'), old('tipe_berkas'), array('id' => 'tipe_berkas', 'style' => 'width: 90%;', 'required' => 'required')) !!}
                                            {!! Form::label('tipe_berkas', 'Tipe Berkas' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
                                        </div>

                                        <div class="input-field col s8 mr-top2" style="margin-bottom: 10px; margin-top: 2.5rem;">
                                            <i class="material-icons prefix">attachment</i>
                                            {!! Form::file('file_berkas', array('id' => 'file_berkas', 'required' => 'required', 'accept' => 'application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document')) !!}
                                            {!! Form::label('file_berkas', 'Upload Berkas yang bersangkutan :' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
                                            {!! Form::label('file_berkas', "(.docx ) (max: 10MB)" , array('class' => 'active', 'style' => 'font-size: .8rem;top: 1rem; color: rgba("0, 0, 0, .5")', 'required' => 'required')); !!}			            			
                                        </div>
                                        
                                        <div class="col s12 mr-top2" style="text-align: right; border-top: 1px solid rgba(0, 0, 0, .1); padding-top: 1rem" >
                                            <ul>
                                                <li class="btn-hov">
                                                    {!! Form::button("<i class='material-icons left' style='line-height: inherit; margin-right: 5px; font-size: 1.3em'>update</i><span class='font_button'>Update</span>", array('class' => 'btn bayangan_2dp green','type' => 'submit')) !!}
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <script>

                                        $('#tipe_berkas').material_select();
                                        $('#file_berkas').filer({
                                            changeInput: '<a data-warna="biru" class="jFiler-input-button bayangan_2dp" style="margin-top: 1rem">Cari berkas...</a>',
                                            limit: 1,
                                            maxSize: 10,
                                            extensions: ["docx"],
                                            showThumbs: true,
                                            captions : {
                                                errors: {
                                                    filesType: "Maaf! file yang Anda pilih tidak didukung.",
                                                }
                                            }
                                        });
                                    </script>
                                {!! Form::close() !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s4">
                                <div style="padding: 1rem; border: 1px solid #e0e0e0; display: block">
                                    <h4 class="head-form" style="text-align: left">
                                        PTA-02
                                    </h4>
                                    <h6 style="margin-bottom: 1rem; color: {{Auth::user()->warna['utama']}}; opacity: .5">
                                        File verifikasi Dosen Pembimbing
                                    </h6>
                                    <h6 style="margin-bottom: 1.5rem; color: {{Auth::user()->warna['utama']}}; opacity: .5">
                                        - ${nama}
                                        <br>
                                        - ${nrp}
                                        <br>
                                        - ${dosen_pembimbing}
                                    </h6>
                                    <a class="green" href="{{ route('admin.berkas.download', ['tipe' => 'PTA02']) }}" style="border-radius: 3px; cursor: pointer; padding: 2px 10px;">
                                        <span class="font_button" style="text-transform: none">Download</span>
                                    </a>
                                </div>
                                <div style="padding: 1rem; border: 1px solid #e0e0e0; display: block">
                                    <h4 class="head-form" style="text-align: left">
                                        PTA-03
                                    </h4>
                                    <h6 style="margin-bottom: 1rem; color: {{Auth::user()->warna['utama']}}; opacity: .5">
                                        Berita Acara Seminar Porposal TA
                                    </h6>
                                    <h6 style="margin-bottom: 1.5rem; color: {{Auth::user()->warna['utama']}}; opacity: .5">
                                        - ${semester}
                                        <br>
                                        - ${hari_tanggal}
                                        <br>
                                        - ${jam}
                                        <br>
                                        - ${accepted}
                                        <br>
                                        - ${toefl}
                                        <br>
                                        - ${tempat}
                                        <br>
                                        - ${judul_ta}
                                        <br>
                                        - ${user_fullname}
                                        <br>
                                        - ${user_nrp}
                                        <br>
                                        - ${user_bidang_ta}
                                        <br>
                                        - ${user_pembimbing}
                                    </h6>
                                    <a class="green" href="{{ route('admin.berkas.download', ['tipe' => 'PTA03']) }}" style="border-radius: 3px; cursor: pointer; padding: 2px 10px;">
                                        <span class="font_button" style="text-transform: none">Download</span>
                                    </a>
                                </div>
                                <div style="padding: 1rem; border: 1px solid #e0e0e0; display: block">
                                    <h4 class="head-form" style="text-align: left">
                                        PTA-04
                                    </h4>
                                    <h6 style="margin-bottom: 1rem; color: {{Auth::user()->warna['utama']}}; opacity: .5">
                                        Hasil Evaluasi Seminar Porposal TA
                                    </h6>
                                    <h6 style="margin-bottom: 1.5rem; color: {{Auth::user()->warna['utama']}}; opacity: .5">
                                        - ${judul_ta}
                                        <br>
                                        - ${accepted}
                                        <br>
                                        - ${toefl}
                                        <br>
                                        - ${user_fullname}
                                        <br>
                                        - ${user_nrp}
                                        <br>
                                        - ${user_bidang_ta}
                                        <br>
                                        - ${user_pembimbing}
                                        <br>
                                        - ${tgl_ttd}
                                        <br>
                                        - ${name_koordinator}
                                    </h6>
                                    <a class="green" href="{{ route('admin.berkas.download', ['tipe' => 'PTA04']) }}" style="border-radius: 3px; cursor: pointer; padding: 2px 10px;">
                                        <span class="font_button" style="text-transform: none">Download</span>
                                    </a>
                                </div>
                            </div>
                            <div class="col s4">
                                <div style="padding: 1rem; border: 1px solid #e0e0e0; display: block">
                                    <h4 class="head-form" style="text-align: left">
                                        KTA-01
                                    </h4>
                                    <h6 style="margin-bottom: 1rem; color: {{Auth::user()->warna['utama']}}; opacity: .5">
                                        Formulir Seminar Kemajuan TA
                                    </h6>
                                    <h6 style="margin-bottom: 1.5rem; color: {{Auth::user()->warna['utama']}}; opacity: .5">
                                        - ${nama}
                                        <br>
                                        - ${nrp}
                                        <br>
                                        - ${judul}
                                        <br>
                                        - ${lab}
                                        <br>
                                        - ${kategori}
                                        <br>
                                        - ${koor}
                                        <br>
                                        - ${dosen_pembimbing}
                                    </h6>
                                    <a class="green" href="{{ route('admin.berkas.download', ['tipe' => 'KTA01']) }}" style="border-radius: 3px; cursor: pointer; padding: 2px 10px;">
                                        <span class="font_button" style="text-transform: none">Download</span>
                                    </a>
                                </div>
                                <div style="padding: 1rem; border: 1px solid #e0e0e0; display: block">
                                    <h4 class="head-form" style="text-align: left">
                                        KTA-02
                                    </h4>
                                    <h6 style="margin-bottom: 1rem; color: {{Auth::user()->warna['utama']}}; opacity: .5">
                                        Berita Acara Seminar Kemajuan TA
                                    </h6>
                                    <h6 style="margin-bottom: 1.5rem; color: {{Auth::user()->warna['utama']}}; opacity: .5">
                                        - ${semester}
                                        <br>
                                        - ${hari_tanggal}
                                        <br>
                                        - ${jam}
                                        <br>
                                        - ${accepted}
                                        <br>
                                        - ${toefl}
                                        <br>
                                        - ${tempat}
                                        <br>
                                        - ${judul_ta}
                                        <br>
                                        - ${user_fullname}
                                        <br>
                                        - ${user_nrp}
                                        <br>
                                        - ${user_bidang_ta}
                                        <br>
                                        - ${user_pembimbing}
                                    </h6>
                                    <a class="green" href="{{ route('admin.berkas.download', ['tipe' => 'KTA02']) }}" style="border-radius: 3px; cursor: pointer; padding: 2px 10px;">
                                        <span class="font_button" style="text-transform: none">Download</span>
                                    </a>
                                </div>
                                <div style="padding: 1rem; border: 1px solid #e0e0e0; display: block">
                                    <h4 class="head-form" style="text-align: left">
                                        KTA-03
                                    </h4>
                                    <h6 style="margin-bottom: 1rem; color: {{Auth::user()->warna['utama']}}; opacity: .5">
                                        Hasil Evaluasi Seminar Kemajuan TA
                                    </h6>
                                    <h6 style="margin-bottom: 1.5rem; color: {{Auth::user()->warna['utama']}}; opacity: .5">
                                        - ${judul_ta}
                                        <br>
                                        - ${accepted}
                                        <br>
                                        - ${toefl}
                                        <br>
                                        - ${user_fullname}
                                        <br>
                                        - ${user_nrp}
                                        <br>
                                        - ${user_bidang_ta}
                                        <br>
                                        - ${user_pembimbing}
                                        <br>
                                        - ${tgl_ttd}
                                        <br>
                                        - ${name_koordinator}
                                    </h6>
                                    <a class="green" href="{{ route('admin.berkas.download', ['tipe' => 'KTA03']) }}" style="border-radius: 3px; cursor: pointer; padding: 2px 10px;">
                                        <span class="font_button" style="text-transform: none">Download</span>
                                    </a>
                                </div>
                            </div>
                            <div class="col s4">
                                <div style="padding: 1rem; border: 1px solid #e0e0e0; display: block">
                                    <h4 class="head-form" style="text-align: left">
                                        FTA-03
                                    </h4>
                                    <h6 style="margin-bottom: 1rem; color: {{Auth::user()->warna['utama']}}; opacity: .5">
                                        Kegiatan Asistensi TA
                                    </h6>
                                    <h6 style="margin-bottom: 1.5rem; color: {{Auth::user()->warna['utama']}}; opacity: .5">
                                        - ${nama}
                                        <br>
                                        - ${nrp}
                                        <br>
                                        - ${judul}
                                        <br>
                                        - ${tgl}
                                        <br>
                                        - ${rowNumber}
                                        <br>
                                        - ${value_tanggal}
                                        <br>
                                        - ${value_description}
                                        <br>
                                        - ${dosen_pembimbing}
                                    </h6>
                                    <a class="green" href="{{ route('admin.berkas.download', ['tipe' => 'FTA03']) }}" style="border-radius: 3px; cursor: pointer; padding: 2px 10px;">
                                        <span class="font_button" style="text-transform: none">Download</span>
                                    </a>
                                </div>
                                <div style="padding: 1rem; border: 1px solid #e0e0e0; display: block">
                                    <h4 class="head-form" style="text-align: left">
                                        UTA-02
                                    </h4>
                                    <h6 style="margin-bottom: 1rem; color: {{Auth::user()->warna['utama']}}; opacity: .5">
                                        Berita Acara Ujian/Sidang TA
                                    </h6>
                                    <h6 style="margin-bottom: 1.5rem; color: {{Auth::user()->warna['utama']}}; opacity: .5">
                                        - ${semester}
                                        <br>
                                        - ${hari_tanggal}
                                        <br>
                                        - ${jam}
                                        <br>
                                        - ${accepted}
                                        <br>
                                        - ${toefl}
                                        <br>
                                        - ${tempat}
                                        <br>
                                        - ${judul_ta}
                                        <br>
                                        - ${user_fullname}
                                        <br>
                                        - ${user_nrp}
                                        <br>
                                        - ${user_bidang_ta}
                                        <br>
                                        - ${user_pembimbing}
                                    </h6>
                                    <a class="green" href="{{ route('admin.berkas.download', ['tipe' => 'UTA02']) }}" style="border-radius: 3px; cursor: pointer; padding: 2px 10px;">
                                        <span class="font_button" style="text-transform: none">Download</span>
                                    </a>
                                </div>
                                <div style="padding: 1rem; border: 1px solid #e0e0e0; display: block">
                                    <h4 class="head-form" style="text-align: left">
                                        UTA-03
                                    </h4>
                                    <h6 style="margin-bottom: 1rem; color: {{Auth::user()->warna['utama']}}; opacity: .5">
                                        Hasil Evaluasi Ujian/Sidang TA
                                    </h6>
                                    <h6 style="margin-bottom: 1.5rem; color: {{Auth::user()->warna['utama']}}; opacity: .5">
                                        - ${judul_ta}
                                        <br>
                                        - ${accepted}
                                        <br>
                                        - ${toefl}
                                        <br>
                                        - ${user_fullname}
                                        <br>
                                        - ${user_nrp}
                                        <br>
                                        - ${user_bidang_ta}
                                        <br>
                                        - ${user_pembimbing}
                                        <br>
                                        - ${tgl_ttd}
                                        <br>
                                        - ${name_koordinator}
                                    </h6>
                                    <a class="green" href="{{ route('admin.berkas.download', ['tipe' => 'UTA03']) }}" style="border-radius: 3px; cursor: pointer; padding: 2px 10px;">
                                        <span class="font_button" style="text-transform: none">Download</span>
                                    </a>
                                </div>
                            </div>
                        </div>
					</section>
				</div>
			</div>
		</section>
	</div>
</div>
@endsection
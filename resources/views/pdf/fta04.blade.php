</div><title>Form FTA-04</title>
<style>
.right {
	text-align: right;
}

.left {
	text-align: left;
}

.center {
	text-align: center;
}

h2 {
    font-size: 2rem;
}

.container{
	margin-right: -30px;
	margin-left: 30px;
	margin: 40px 50px;
}
@page {
  margin-top: 0.1in;
  margin-left: 0.5in;
  margin-right: 0.5in;
  margin-bottom: 0.02in;
}

.row {
	display: inline-block;
}

table {
	display: table;
	width: 100%;
    font-size: 1.6rem;
}

.table-container {
    padding: 0 3rem;
}

tbody tr td {
    padding: 5px;
}

table.bordered {
    margin: 5px;
}

table.bordered tr td {
    border: 1px solid black;
}

td, th {
	padding:3px 5px 3px 5px; 
	padding-right:10px;
}

.form-box {
	padding:3px; 
	border:1px solid black;
}
</style>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<br>
{{-- <div class="table-container">
    <p>
        <img src="https://its.ac.id/files/images/logo-its-biru-transparan.png" alt="" width="139" height="90" />&nbsp;
    </p>
</div> --}}

<div class="table-container">
    {{-- <p style="font-size: 2rem; margin-left: 5px;">Surabaya, 09-09-2016</p>
    <p style="font-size: 2rem; margin-left: 5px">Mengetahui: </p> --}}
    <table>
        <tbody>
            <tr>
                <td style="width: 50%">
                    <img src="https://its.ac.id/files/images/logo-its-biru-transparan.png" alt="" width="139" height="90" />&nbsp;
                </td>
                <td class="right">
                    <span style="font-size: 2rem"><strong>JURUSAN TEKNIK LINGKUNGAN</strong></span>
                    <br>
                    <span style="font-size: 1.5rem; font-weight: 500">FAKULTAS TEKNIK SIPIL DAN PERENCANAAN</span>
                    <br>
                    <span style="font-size: 1.4rem; font-weight: 600">Kampus ITS Sukolilo, Surabaya 60111</span>
                    <br>
                    <span style="font-size: 1.4rem; font-weight: 600">Telp: 031-5948886, Fax: 031-5928387</span>
                </td>
            <tr>
                <td><br></td>
            </tr>
            </tr>
                <td></td>
                <td class="right">
                    <span style="border: 1px solid black; padding: 5px">
                        <strong>FORM FTA-04</strong>
                    </span>
                </td>
            </tr>
        </tbody>
    </table>
</div>

<h2 style="font-size: 2.5rem; text-align: center; margin-bottom: 0;">
    <strong>FORMULIR PERBAIKAN LAPORAN TUGAS AKHIR</strong>
</h2>
{{-- <h2 style="font-size: 2.5rem;text-align: center; margin-top: 0;">
    <strong>SEMINAR KEMAJUAN TUGAS AKHIR</strong>
</h2> --}}
<p style="text-align: center; margin-top: 0;">&nbsp;</p>
<div class="table-container">
    <table>
        <tbody>
            
            <tr>
                <td>Nama Mahasiswa</td>
                <td>:</td>
                <td>{{ $user->full_name }}</td>
            </tr>
            <tr>
                <td>NRP&nbsp;</td>
                <td>:&nbsp;</td>
                <td>{{ $user->username }}</td>
            </tr>
            <tr>
                <td style="width: 25%; vertical-align: top;">Judul</td>
                <td style="width: 1%; vertical-align: top;">:</td>
                <td>{!! $user->tugas_akhir->judul !!}</td>
            </tr>
        </tbody>
    </table>
</div>

<br>
<div class="table-container">
    <table class="bordered" style="height: 105px; border-color: 0, 0, 0; border: 1px solid black" width="620">
        <tbody>
            <tr class="center">
                <td width="2%"><strong>No</strong></td>
                <td width="37%"><strong>Saran Perbaikan <br> (sesuai Form KTA-02)</strong></td>
                <td width="57%"><strong>Tanggapan / Perbaikan <br> (bila perlu, sebutkan halaman)</strong></td>
            </tr>
            <tr>
                <td>
                    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                </td>
                <td><br></td>
                <td><br></td>
            </tr>
        </tbody>
    </table>
</div>
<br>
<br>
<br>
<div class="table-container">
    {{-- <p style="font-size: 2rem; margin-left: 5px;">Surabaya, 09-09-2016</p>
    <p style="font-size: 2rem; margin-left: 5px">Mengetahui: </p> --}}
    <table>
        <tbody>
            <tr>
                <td>
                    Dosen Pembimbing,
                </td>
                <td style="width: 50%">
                    Mahasiswa Ybs.
                    <span> 
                        {{ date(' d ') }}
                        {{ $waktu->bulan[date('n')] }}
                        {{ date(' Y') }}
                    </span>
                </td>
            </tr>
            <tr>
                <td><br></td>
            </tr>
            <tr>
                <td><br></td>
            </tr>
            <tr>
                <td><br></td>
            </tr>
            <tr>
                <td>{{ $user->tugas_akhir->dosen_pembimbing->full_name }}</td>
                <td>{{ $user->full_name }}</td>                
            </tr>
            <tr>
                <td><br></td>
            </tr>
        </tbody>
    </table>
</div>

<script>
    window.onload = function() {
        window.print();
    }
</script>
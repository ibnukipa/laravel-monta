<title>{{ $judul }}</title>
<style>
.right {
	text-align: right;
}

.left {
	text-align: left;
}

.center {
	text-align: center;
}

h2 {
    font-size: 2rem;
}

.container{
	margin-right: -30px;
	margin-left: 30px;
	margin: 40px 50px;
}
@page {
  margin-top: 0.1in;
  margin-left: 0.5in;
  margin-right: 0.5in;
  margin-bottom: 0.02in;
}

.row {
	display: inline-block;
}

table {
	display: table;
	width: 100%;
    font-size: 1.6rem;
}

.table-container {
    padding: 0 3rem;
}

tbody tr td {
    padding: 5px;
}

table.bordered {
    margin: 5px;
}

table.bordered tr td {
    border: 1px solid black;
}

td, th {
	padding:3px 5px 3px 5px; 
	padding-right:10px;
}

.form-box {
	padding:3px; 
	border:1px solid black;
}
td.black {
        background-color: black !important;
        -webkit-print-color-adjust:exact;
    }

@media print {
    black {
        background-color: black !important;
    }
}

</style>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<br>
{{-- <div class="table-container">
    <p>
        <img src="https://its.ac.id/files/images/logo-its-biru-transparan.png" alt="" width="139" height="90" />&nbsp;
    </p>
</div> --}}

<div class="table-container">
    {{-- <p style="font-size: 2rem; margin-left: 5px;">Surabaya, 09-09-2016</p>
    <p style="font-size: 2rem; margin-left: 5px">Mengetahui: </p> --}}
    <table>
        <tbody>
            <tr>
                <td style="width: 50%">
                    <img src="https://its.ac.id/files/images/logo-its-biru-transparan.png" alt="" width="139" height="90" />&nbsp;
                </td>
                <td class="right">
                    <span style="font-size: 2rem"><strong>JURUSAN TEKNIK LINGKUNGAN</strong></span>
                    <br>
                    <span style="font-size: 1.5rem; font-weight: 500">FAKULTAS TEKNIK SIPIL DAN PERENCANAAN</span>
                    <br>
                    <span style="font-size: 1.4rem; font-weight: 600">Kampus ITS Sukolilo, Surabaya 60111</span>
                    <br>
                    <span style="font-size: 1.4rem; font-weight: 600">Telp: 031-5948886, Fax: 031-5928387</span>
                </td>
            <tr>
                <td><br></td>
            </tr>
            </tr>
                <td></td>
                <td class="right">
                    <span style="border: 1px solid black; padding: 5px">
                        <strong>FORM UTA-03</strong>
                    </span>
                </td>
            </tr>
        </tbody>
    </table>
</div>

<h2 style="font-size: 2.5rem; text-align: center; margin-bottom: 0;">
    <strong>HASIL EVALUASI</strong>
</h2>
<h2 style="font-size: 2.5rem;text-align: center; margin-top: 0;">
    <strong>UJIAN/SIDANG TUGAS AKHIR</strong>
</h2>
<p style="text-align: center; margin-top: 0;">&nbsp;</p>
<div class="table-container">
    <table style="height: 106px;" width="622">
        <tbody>
            <tr>
                <td colspan="4" class="right">
                    TOEFL: {{ $user->proposal->nilai_toefl }} | IPK: .....
                </td>
            </tr>
            <tr>
                <td style="width: 25%; vertical-align: top;">Judul</td>
                <td style="width: 1%; vertical-align: top;">:</td>
                <td>{!! $user->tugas_akhir->judul !!}</td>
            </tr>
            <tr>
                <td>Nama Mahasiswa</td>
                <td>:</td>
                <td>{{ $user->full_name }}</td>
            </tr>
            <tr>
                <td>NRP&nbsp;</td>
                <td>:&nbsp;</td>
                <td>{{ $user->username }}</td>
            </tr>
            <tr>
                <td>Program Studi&nbsp;</td>
                <td>:&nbsp;</td>
                <td>S-1 Teknik Lingkungan&nbsp;</td>
            </tr>
            <tr>
                <td>Bidang Tugas Akhir&nbsp;</td>
                <td>:&nbsp;</td>
                <td>{{ $user->tugas_akhir->kategori_ta->description }}</td>
            </tr>
        </tbody>
    </table>
</div>

<br>
<div class="table-container">
    <p style="font-size: 1.8rem; margin-left: 5px">Dengan Nilai :</p>
    <table class="bordered" style="font-size: 1.3rem; height: 105px; border-color: 0, 0, 0; border: 1px solid black" width="620">
        <tbody>
            <tr class="center">
                <td><strong>Nilai</strong></td>
                <td><strong>Penguji I</strong></td>
                <td><strong>Penguji II</strong></td>
                <td><strong>Penguji III</strong></td>
                <td><strong>Pembimbing</strong></td>
                <td><strong>RATA-RATA</strong></td>
                <td><strong>Presentase</strong></td>
                <td><strong>Jumlah</strong></td>
            </tr>
            <tr>
                <td><strong>Nilai Proposal</strong></td>
                <td class="black"></td>
                <td class="black"></td>
                <td class="black"></td>
                <td class="black"></td>
                <td>&nbsp;</td>
                <td class="center">10%</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td><strong>Nilai Kemajuan</strong></td>
                <td class="black"></td>
                <td class="black"></td>
                <td class="black"></td>
                <td class="black"></td>
                <td>&nbsp;</td>
                <td class="center">10%</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td><strong>Nilai Ujian</strong></td>
                <td class="black"></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td class="center">45%</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td><strong>Nilai Ujian Pembimbing</strong></td>
                <td>&nbsp;</td>
                <td class="black"></td>
                <td class="black"></td>
                <td class="black"></td>
                <td class="black"></td>
                <td class="center">15%</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
            <td><strong>Nilai Bimbingan TA</strong></td>
                <td>&nbsp;</td>
                <td class="black"></td>
                <td class="black"></td>
                <td class="black"></td>
                <td class="black"></td>
                <td class="center">20%</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center;" colspan="5"><strong>Nilai Total</strong></td>
                <td class="black"></td>
                <td class="black"></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center;" colspan="5"><strong>Nilai Huruf</strong></td>
                <td class="black"></td>
                <td class="black"></td>
                <td>&nbsp;</td>
            </tr>
        </tbody>
    </table>
</div>
<br>
<div class="table-container">
    <p style="font-size: 1.8rem; margin-left: 5px">Tim Penguji :</p>
    <table class="bordered" style="width: 70%; border: 1px solid black; font-size: 1.2rem">
        <tbody>
            <tr class="center">
                <td style="width: 5%"><strong>No</strong></td>
                <td style="width: 50%"><strong>Nama Dosen</strong></td>
                <td style="width: 15%"><strong>Tanda Tangan</strong></td>
            </tr>
            <tr>
                <td class="center">1</td>
                <td></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="center">2</td>
                <td></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="center">3</td>
                <td></td>
                <td>&nbsp;</td>
            </tr>
        </tbody>
    </table>
</div>

<br>
<br>
<div class="table-container">
    {{-- <p style="font-size: 2rem; margin-left: 5px;">Surabaya, 09-09-2016</p>
    <p style="font-size: 2rem; margin-left: 5px">Mengetahui: </p> --}}
    <table>
        <tbody>
            <tr>
                <td style="width: 50%">
                    <br>
                    <span>Mengetahui: </span>
                    <br>
                    Koordinator Tugas Akhir,
                </td>
                <td>
                    <span>Surabaya, 
                        {{ date(' d ', strtotime($jadwal->tanggal)) }}
                        {{ $waktu->bulan[date('n', strtotime($jadwal->tanggal))] }}
                        {{ date(' Y', strtotime($jadwal->tanggal)) }}
                    </span>
                    <br>
                    <br>
                    Pembimbing Tugas Akhir,
                </td>
            </tr>
            <tr>
                <td><br></td>
            </tr>
            <tr>
                <td><br></td>
            </tr>
            <tr>
                <td><br></td>
            </tr>
            <tr>
                <td>{{ $koorta->full_name }}</td>
                <td>{{ $user->tugas_akhir->dosen_pembimbing->full_name }}</td>
            </tr>
            <tr>
                <td><br></td>
            </tr>
        </tbody>
    </table>
</div>

<script>
    window.onload = function() {
        window.print();
    }
</script>
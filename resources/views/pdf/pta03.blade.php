<title>{{ $judul }}</title>
<style>
.right {
	text-align: right;
}

.left {
	text-align: left;
}

.center {
	text-align: center;
}

h2 {
    font-size: 2rem;
}

.container{
	margin-right: -30px;
	margin-left: 30px;
	margin: 40px 50px;
}
@page {
  margin-top: 0.1in;
  margin-left: 0.5in;
  margin-right: 0.5in;
  margin-bottom: 0.02in;
}

.row {
	display: inline-block;
}

table {
	display: table;
	width: 100%;
    font-size: 1.6rem;
}

.table-container {
    padding: 0 3rem;
}

tbody tr td {
    padding: 2px;
}

table.bordered {
    margin: 2px;
}

table.bordered tr td {
    border: 1px solid black;
}

</style>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<br>
{{-- <div class="table-container">
    <p>
        <img src="https://its.ac.id/files/images/logo-its-biru-transparan.png" alt="" width="139" height="90" />&nbsp;
    </p>
</div> --}}

<div class="table-container">
    {{-- <p style="font-size: 2rem; margin-left: 5px;">Surabaya, 09-09-2016</p>
    <p style="font-size: 2rem; margin-left: 5px">Mengetahui: </p> --}}
    <table>
        <tbody>
            <tr>
                <td style="width: 50%">
                    <img src="https://its.ac.id/files/images/logo-its-biru-transparan.png" alt="" width="139" height="90" />&nbsp;
                </td>
                <td class="right">
                    <span style="font-size: 2rem"><strong>JURUSAN TEKNIK LINGKUNGAN</strong></span>
                    <br>
                    <span style="font-size: 1.5rem; font-weight: 500">FAKULTAS TEKNIK SIPIL DAN PERENCANAAN</span>
                    <br>
                    <span style="font-size: 1.4rem; font-weight: 600">Kampus ITS Sukolilo, Surabaya 60111</span>
                    <br>
                    <span style="font-size: 1.4rem; font-weight: 600">Telp: 031-5948886, Fax: 031-5928387</span>
                </td>
            <tr>
                <td><br></td>
            </tr>
            </tr>
                <td></td>
                <td class="right">
                    <span style="border: 1px solid black; padding: 5px">
                        <strong>FORM PTA-03</strong>
                    </span>
                </td>
            </tr>
        </tbody>
    </table>
</div>

<h2 style="font-size: 2.5rem; text-align: center; margin-bottom: 0;">
    <strong>BERITA ACARA</strong>
</h2>
<h2 style="font-size: 2.5rem;text-align: center; margin-bottom: 0;; margin-top: 0;">
    <strong>SEMINAR PROPOSAL TUGAS AKHIR</strong>
</h2>
<h2 style="font-size: 2.2rem;text-align: center; margin-top: 0;">
    <i><strong>{{$semester }}</strong></i>
</h2>

<p style="text-align: center; margin-top: 0;">&nbsp;</p>
<div class="table-container">
    <table width="622">
        <tbody>
            <tr>
                <td colspan="4" class="right">
                    TOEFL: {{ $user->proposal->nilai_toefl }} | IPK: .....
                </td>
            </tr>
            <tr>
                <td style="width: 1%; vertical-align: top;" colspan="4">Pada :</td>
            </tr>
            <tr>
                <td style="width: 1%"></td>
                <td style="width: 10%; vertical-align: top;">Hari, Tanggal</td>
                <td style="width: 1%; vertical-align: top;">:</td>
                <td style="width: 25%; vertical-align: top;">
                    {{ $waktu->hari[date('N', strtotime($jadwal->tanggal))] }}
                    {{ date(', d ', strtotime($jadwal->tanggal)) }}
                    {{ $waktu->bulan[date('n', strtotime($jadwal->tanggal))] }}
                    {{ date(' Y', strtotime($jadwal->tanggal)) }}
                </td>
            </tr>
            <tr>
                <td ></td>
                <td >Jam</td>
                <td >:</td>
                <td >{{ date('H:i', strtotime($jadwal->jadwal_jam->mulai)) }} - {{ date('H:i', strtotime($jadwal->jadwal_jam->selesai)) }}</td>
            </tr>
            <tr>
                <td ></td>
                <td >Tempat</td>
                <td >:</td>
                <td >{{ $jadwal->jadwal_ruang->description }}</td>
            </tr>
        </tbody>
    </table>
</div>
<br>
<div class="table-container">
    <table width="622">
        <tbody>
            <tr>
                <td style="width: 1%; vertical-align: top;" colspan="4">telah dilaksanakan Seminar Proposal Tugas Akhir :</td>
            </tr>
            <tr>
                <td style="width: 1%; vertical-align: top;"></td>
                <td style="width: 10%; vertical-align: top;">Judul Tugas Akhir</td>
                <td style="width: 1%; vertical-align: top;">:</td>
                <td style="width: 25%; vertical-align: top;">{!! $user->tugas_akhir->judul !!}</td>
            </tr>
            <tr>
                <td ></td>
                <td >Nama Mahasiswa</td>
                <td >:</td>
                <td >{{ $user->full_name }}</td>
            </tr>
            <tr>
                <td ></td>
                <td >NRP</td>
                <td >:</td>
                <td >{{ $user->username }}</td>
            </tr>
            <tr>
                <td ></td>
                <td >Program Studi</td>
                <td >:</td>
                <td >S-1 Teknik Lingkungan</td>
            </tr>
            <tr>
                <td ></td>
                <td >Bidang Tugas Akhir</td>
                <td >:</td>
                <td >{{ $user->tugas_akhir->kategori_ta->description }}</td>
            </tr>
            <tr>
                <td><br></td>
            </tr>
            <tr>
                <td ></td>
                <td >Tanda Tangan</td>
                <td >:</td>
                <td >....................</td>
            </tr>
        </tbody>
    </table>
</div>

<br>
<div class="table-container">
    <table width="622">
        <tbody>
            <tr>
                <td style="width: 1%; vertical-align: top;" colspan="4">Berdasarkan hasil evaluasi penguji, dinyatakan bahwa proposal tersebut :</td>
            </tr>
            <tr>
                <td style="width: 1%; vertical-align: top;"></td>
                <td style="width: 10%; vertical-align: top;">1. diterima</td>
                <td style="width: 1%; vertical-align: top;"></td>
                <td style="width: 25%; vertical-align: top;"></td>
            </tr>
            <tr>
                <td ></td>
                <td >2. ditolak</td>
                <td ></td>
                <td ></td>
            </tr>
            <tr>
                <td ></td>
                <td >3. seminar ulang</td>
                <td ></td>
                <td ></td>
            </tr>
        </tbody>
    </table>
</div>

<br>
<div class="table-container">
    <p style="font-size: 1.8rem; margin-left: 5px"><strong>Saran-saran dan perbaikan :</strong></p>
    <table>
        <p style="font-size: 1.8rem; margin-left: 5px; line-height: 1.7"><strong>
            .................................................................................................................................................
            .................................................................................................................................................
            .................................................................................................................................................
            .................................................................................................................................................
            .................................................................................................................................................
            .................................................................................................................................................
            .................................................................................................................................................
            .................................................................................................................................................
            .................................................................................................................................................
            .................................................................................................................................................
            .................................................................................................................................................
        </strong></p>
    </table>
</div>
<br>
<div class="table-container">
    <table>
        <tbody>
            <tr>
                <td style="width: 50%">
                    
                </td>
                <td class="center">
                    <strong>Pembimbing</strong>
                </td>
            </tr>
            <tr>
                <td><br></td>
            </tr>
            <tr>
                <td><br></td>
            </tr>
            <tr>
                <td></td>
                <td class="center">_______________________________________</td>
            </tr>
            <tr>
                <td><br></td>
            </tr>
        </tbody>
    </table>
</div>

<div class="table-container">
    <p style="font-size: 1.8rem; margin-left: 5px">Tim Penguji :</p>
    <table class="bordered" style="width: 70%; border: 1px solid black; font-size: 1.2rem">
        <tbody>
            <tr class="center">
                <td style="width: 5%"><strong>No</strong></td>
                <td style="width: 50%"><strong>Nama Dosen</strong></td>
                <td style="width: 15%"><strong>Tanda Tangan</strong></td>
            </tr>
            <tr>
                <td class="center">1</td>
                <td></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="center">2</td>
                <td></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="center">3</td>
                <td></td>
                <td>&nbsp;</td>
            </tr>
        </tbody>
    </table>
</div>

<script>
    window.onload = function() {
        window.print();
    }
</script>
<html>
    <tr>
        <td colspan="5" style="font-size: 18pt; text-align: center">{{ $judul }}</td>
    </tr>
    <tr></tr>
    <tr>
        <td style="font-weight: 700;border: 5px solid #212121">No</td>
        <td style="font-weight: 700;border: 1px solid #212121">NRP</td>
        <td style="font-weight: 700;border: 1px solid #212121">Nama Lengkap</td>
        <td style="font-weight: 700;border: 1px solid #212121">Judul</td>
        <td style="font-weight: 700;border: 1px solid #212121">Dosen Pembimbing</td>
    </tr>
    
    @foreach($users as $key => $value)
    <tr>
        <td style="border: 1px solid rgba(33,33,33, .5); text-align: center">{{$key + 1}}</td> 
        <td style="border: 1px solid #212121;">{{ $value->username }}</td>
        <td style="border: 1px solid #212121;">{{ $value->full_name }}</td>
        <td style="border: 1px solid #212121;">{{ $value->proposal->usulan_judul }}</td>
        <td style="border: 1px solid #212121;">{{ $value->tugas_akhir->dosen_pembimbing->full_name }}</td>
    </tr>
    @endforeach
</html>
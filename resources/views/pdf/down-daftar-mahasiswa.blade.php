<html>
    <tr>
        <td colspan="5" style="font-size: 18pt; text-align: center">{{ $judul }}</td>
    </tr>
    <tr></tr>
    <tr>
        <td style="font-weight: 700;border: 5px solid #212121">No</td>
        <td style="font-weight: 700;border: 1px solid #212121">NRP</td>
        <td style="font-weight: 700;border: 1px solid #212121">Nama Lengkap</td>
        <td style="font-weight: 700;border: 1px solid #212121">Judul TA</td>
        <td style="font-weight: 700;border: 1px solid #212121">Dosen Pembimbing</td>
    </tr>
    
    <?php $i=1 ?>
    @foreach($labTA as $labkey => $lab)
        <tr>
            <td style="text-align: left; font-size: 1.2rem" colspan="5"></td>
        </tr>
        <tr>
            <td style="text-align: left; font-size: 1.2rem" colspan="5"><strong>{{$lab->description}}</strong></td>
        </tr>
        <?php $j=0 ?>
        @foreach($users as $userkey => $value)
            @if($value->tugas_akhir->lab_ta_id == $lab->id)
            <tr>
                <td style="border: 1px solid rgba(33,33,33, .5); text-align: center">{{$i}}</td> 
                <td style="border: 1px solid #212121;">{{ $value->username }}</td>
                <td style="border: 1px solid #212121;">{{ $value->full_name }}</td>
                <td style="border: 1px solid #212121;">{!! strip_tags($value->tugas_akhir->judul) !!}</td>
                <td style="border: 1px solid #212121;">{{ $value->tugas_akhir->dosen_pembimbing->full_name }}</td>
            </tr>
            <?php $j++ ?>
            <?php $i++ ?>
            @endif
        @endforeach
        @if($j == 0)
            <tr>
                <td class="center" >-</td>
                <td class="center">-</td>
                <td class="center">-</td>
                <td class="center">-</td>
                <td class="center">-</td>
            </tr>
        @endif

    @endforeach
    
</html>
<html>
    <tr>
        <td colspan="6" style="font-size: 18pt; text-align: center">{{ $judul }}</td>
    </tr>
    <tr></tr>
    <tr>
        <td style="font-weight: 700;border: 5px solid #212121">No</td>
        <td style="font-weight: 700;border: 1px solid #212121">NRP</td>
        <td style="font-weight: 700;border: 1px solid #212121">Nama Lengkap</td>
        <td style="font-weight: 700;border: 1px solid #212121">Usulan Judul</td>
        <td style="font-weight: 700;border: 1px solid #212121">Usulan Dosen Pembimbing 1</td>
        <td style="font-weight: 700;border: 1px solid #212121">Usulan Dosen Pembimbing 2</td>
    </tr>
    
    @foreach($proposal as $key => $value)
    <tr>
        <td style="border: 1px solid rgba(33,33,33, .5); text-align: center">{{$key + 1}}</td> 
        <td style="border: 1px solid #212121;">{{ $value->user->username }}</td>
        <td style="border: 1px solid #212121;">{{ $value->user->full_name }}</td>
        <td style="border: 1px solid #212121;">{{ $value->usulan_judul }}</td>
        <td style="border: 1px solid #212121;">{{ $value->dosen_usulan[0]->dosen->full_name }}</td>
        <td style="border: 1px solid #212121;">{{ $value->dosen_usulan[1]->dosen->full_name }}</td>
    </tr>
    @endforeach
</html>
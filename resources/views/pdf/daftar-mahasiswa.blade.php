<title>Daftar Mahasiswa {{ $tipeSeminar }}</title>
<style>
.right {
	text-align: right;
}

.left {
	text-align: left;
}

.center {
	text-align: center;
}

h2 {
    font-size: 2rem;
}

.container{
	margin-right: -30px;
	margin-left: 30px;
	margin: 40px 50px;
}
@page {
  margin-top: 0.1in;
  margin-left: 0.5in;
  margin-right: 0.5in;
  margin-bottom: 0.02in;
}

.row {
	display: inline-block;
}

table {
	display: table;
    
	width: 100%;
    font-size: .8rem;
}

.table-container {
    padding: 0 3rem;
}

tbody tr td {
    padding: 5px;
      white-space: pre-wrap;        /* css */
  white-space: -moz-pre-wrap;   /* Mozilla */
  white-space: -pre-wrap;       /* Chrome*/
  white-space: -o-pre-wrap;     /* Opera 7 */
  word-wrap: break-word; /* Internet Explorer 5.5+ */
}

table.bordered {
    margin: 5px;
}

table.bordered tr td {
    border: .5px solid black;
}

td, th {
	padding:3px 5px 3px 5px; 
	padding-right:10px;
}

.form-box {
	padding:3px; 
	border:1px solid black;
}

td p {
    margin: 0;
}
</style>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<br>

<h2 style="font-size: 1.3rem; text-align: center; margin-bottom: 0; text-transform: uppercase">
    <strong>
        Daftar Mahasiswa {{$tipeSeminar}} Semester {{ $semester }} {{$tahunAjaran}}
    </strong>
</h2>

<div class="table-container" style="padding: 0rem">

    <?php $i=1 ?>    
    @foreach($labTA as $labkey => $lab)
    <table class="bordered" style="margin-top: 2rem">
        <tbody>
            <tr>
                <td style="text-align: left; border: none; font-size: 1.2rem" colspan="5"><strong>{{$lab->description}}</strong></td>
            </tr>
            <tr class="center">
                <td width="1%"><strong>No</strong></td>
                <td width="10%"><strong>NRP</strong></td>
                <td width="19%"><strong>Nama</strong></td>
                <td width="40%"><strong>Judul TA</strong></td>
                <td width="30%"><strong>Dosen Pembimbing</strong></td>
            </tr>
            <?php $j=0 ?>
            @foreach($users as $userkey => $user)
                @if($user->tugas_akhir->lab_ta_id == $lab->id)
                    <tr>
                        <td class="center" width="1%"><strong>{{ $i }}</strong></td>
                        <td width="10%">{{ $user->username }}</td>
                        <td width="19%">{{ $user->full_name }}</td>
                        <td width="40%">{!! $user->tugas_akhir->judul !!}</td>
                        <td width="30%">{{ $user->tugas_akhir->dosen_pembimbing->full_name }}</td>
                    </tr>
                <?php $j++ ?>
                <?php $i++ ?>
                @endif
            @endforeach
            @if($j == 0)
                <tr>
                    <td class="center" width="1%">-</td>
                    <td class="center" width="10%">-</td>
                    <td class="center" width="19%">-</td>
                    <td class="center" width="40%">-</td>
                    <td class="center" width="30%">-</td>
                </tr>
            @endif
        </tbody>
    </table>
    @endforeach
</div>
<br>

<script>
    window.onload = function() { window.print();  }
</script>


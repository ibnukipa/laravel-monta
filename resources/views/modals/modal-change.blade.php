<div id="confirmChange" class="modal" style="border-radius: 0">
	<div class="modal-content no-padding">
		<h4 class="red" style="padding: 1rem 2rem; font-size: 1.5rem">Konfirmasi</h4>
		<p class="content center" style="padding: 1rem 2rem; font-size: 1.2rem"></p>
	</div>
	<div class="modal-footer no-padding center">
		<ul>
			<li class="btn-hov">
				{!! Form::button("<i class='material-icons left' style='line-height: inherit; margin-right: 5px; font-size: 1.3em'>check</i><span class='font_button'>Iya</span>", array('class' => 'btn bayangan_2dp red yes')) !!}
			</li>
			<li class="btn-hov">
				{!! Form::button("<i class='material-icons left' style='line-height: inherit; margin-right: 5px; font-size: 1.3em'>close</i><span class='font_button'>Tidak</span>", array('class' => 'modal-close btn bayangan_2dp grey-dark')) !!}
			</li>
		</ul>
	</div>
</div>
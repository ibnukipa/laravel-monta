<div id="modalDetail" class="modal bottom-sheet" style="min-height: 100vh">
    <div class="modal-content" style="padding: 1rem 10rem">
        {!! Form::button("<i class='material-icons left' style='line-height: inherit; margin-right: 5px; font-size: 1.3em'>close</i><span class='font_button'>Tutup</span>", array('style' => 'position: absolute; right: 10.5rem; top: .5rem', 'class' => 'modal-close btn bayangan_2dp grey-dark')) !!}
        <h4 class="head-form" style="margin-bottom: 1rem; text-align: left; margin-left: .5rem">
            
        </h4>
        <div class="body">
            
        </div>
    </div>
</div>
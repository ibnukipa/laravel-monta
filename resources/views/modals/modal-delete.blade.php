<div id="confirmDelete" class="modal" style="border-radius: 0">
	<div class="modal-content no-padding">
		<h4 class="red" style="padding: 1rem 2rem; font-size: 1.5rem">Konfirmasi</h4>
		<p class="content center" style="padding: 1rem 2rem; font-size: 1.2rem">Apakah Anda yakin akan menghapus ini?</p>
	</div>
	<div class="modal-footer no-padding center">
		<ul>
			<li class="btn-hov">
				{!! Form::button("<i class='material-icons left'' style='line-height: inherit; margin-right: 5px; font-size: 1.3em'>warning</i><span class='font_button'>Hapus</span>", array('id' => 'yes', 'class' => 'btn bayangan_2dp red')) !!}
			</li>
			<li class="btn-hov">
				{!! Form::button("<i class='material-icons left'' style='line-height: inherit; margin-right: 5px; font-size: 1.3em'>close</i><span class='font_button'>Tutup</span>", array('class' => 'modal-close btn bayangan_2dp grey-dark')) !!}
			</li>
		</ul>
	</div>
</div>
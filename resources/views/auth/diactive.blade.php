@extends('app')

@section('template_title')
	Akun Kadaluarsa
@endsection

@section('template_fastload_css')
    body {
            margin: 0;
            padding: 0;
            width: 100%;
            height: 100%;
            color: #B0BEC5;
        }
    .container {
        text-align: center;
        vertical-align: middle;
    }
    .content {
        text-align: center;
        display: inline-block;
    }
    .title {
        font-size: 96px;
        margin-bottom: 40px;
    }
    .quote {
        font-size: 24px;
    }
@endsection

@section('content')
<div class="container" style="padding-top: 30vh">
    <div class="content">
        <div class="title"> MAAF! </div>
        <div class="quote">Akun Anda dinonaktifkan!</div>
    </div>
</div>
@endsection

@section('template_scripts')

@endsection
@extends('app')

@section('template_title')
	Reset Password
@endsection

@section('template_fastload_css')
    body {
            margin: 0;
            padding: 0;
            width: 100%;
            height: 100%;
        }
    .container {
        text-align: center;
        vertical-align: middle;
    }
    .content {
        text-align: center;
        display: inline-block;
    }
    .title {
        font-size: 96px;
        margin-bottom: 40px;
    }
    .quote {
        font-size: 24px;
    }
@endsection

@section('content')
<div class="container" style="padding-top: 13vh">
    
    <div class="content">
        <h4 class="head-form" style="margin-bottom: 1rem; text-align: left; margin-left: .5rem">
            RESET PASSWORD
        </h4>
        <div style="padding: 0 .4rem">
            @include('partials.form-status')
        </div>
        {!! Form::open(array('url' => url('reset'), 'method' => 'POST', 'class' => ' form-blue lockscreen-credentials form-horizontal', 'role' => 'form', 'files'=>true)) !!}
            <div class="row no-margin">
                
                <div class="input-field col s12 ">
                    <div style="line-height: 0; display: inline-block; vertical-align: middle; margin-top: -4px; width: 100%">
                        <span class="judul_kedua disable_select center">Username (NRP/NIP)</span>
                        <span class="judul_utama disable_select center" style="font-size: 2rem;color: #0288D1; padding: 5px; text-transform: capitalize">
                            {{Auth::user()->username}}
                        </span>
                    </div>
                </div>

                <div class="input-field col s6 offset-s3 center mr-top2">
                    {{-- <i class="material-icons prefix active">chrome_reader_mode</i> --}}
                    {!! Form::password('password_baru', array('id' => 'password_baru', 'class' => 'validate valid', 'required' => 'required')) !!}
                    {!! Form::label('password_baru', 'Password Baru: ' , array('class' => '')); !!}
                    <div style="margin-top: -.5rem; text-align: right; color: rgba(0, 0, 0, .3); font-size: .8rem; font-weight: 600">
                        <span>minimal 6 karakter dan maksimal 20 karakter</span>
                    </div>
                </div>

                <div class="input-field col s6 offset-s3 center mr-top2">
                    {{-- <i class="material-icons prefix active">chrome_reader_mode</i> --}}
                    {!! Form::password('password_baru_k', array('id' => 'password_baru_k', 'class' => 'validate valid', 'required' => 'required')) !!}
                    {!! Form::label('password_baru_k', 'Password Baru Konfirmasi: ' , array('class' => '')); !!}
                </div>

                <div class="col s12 mr-top2" style="text-align: center; border-top: 1px solid rgba(0, 0, 0, .1); padding-top: 1rem" >
                    <ul>
                        <li class="btn-hov">
                            {!! Form::button("<i class='material-icons left'' style='line-height: inherit; margin-right: 5px; font-size: 1.3em'>update</i><span class='font_button'>Update Password Saya</span>", array('class' => 'btn bayangan_2dp blue','type' => 'submit')) !!}
                        </li>
                    </ul>
                </div>

            </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection

@section('template_scripts')
    <script type="text/javascript">
		document.addEventListener("DOMContentLoaded", function(event) {
		  	matching_password_check();
		});

        function matching_password_check() {
            var password = document.getElementById("password_baru");
            var confirm_password = document.getElementById("password_baru_k");
            function validatePassword(){
                if(password.value != confirm_password.value) {
                    confirm_password.setCustomValidity("Password tidak cocok!");
                } else {
                    confirm_password.setCustomValidity('');
                }
            }
            
            password.onchange 			= validatePassword;
            confirm_password.onkeyup 	= validatePassword;
        }
        
    </script>
@endsection
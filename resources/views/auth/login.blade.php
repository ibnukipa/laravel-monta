@extends('app')

@section('template_title')
	Login
@endsection

@section('content')
<section id="login" style="background: #3e464c">

	<div class="container">
		<div class="row" style="padding: 6rem 3rem">
			<div class="col s6 offset-s3">
				@include('partials.form-status')
			</div>
			<div class="col s6 offset-s3">	
				
				<div class="card-panel bayangan_2dp">
					<div class="row" style="padding: 0; margin-bottom: 7%">
						<div class="col s12" style="margin: 0; display: table; text-align: center; padding-top: 20px">
							<div style="line-height: 0; display: inline-block; vertical-align: middle;">
				                <span class="judul_utama disable_select" style="font-size: 30px; font-family: 'antique'; color: #689F38;">
				                	monTA <span style="color: #3e464c; opacity: .5">login</span>
				                </span>
				                <span class="judul_kedua disable_select" style="font-weight: 500">Teknik Lingkungan ITS</span>
				            </div>
						</div>
					</div>
					<div class="row" style="padding: 0 30px; margin-bottom: 30px">
				    	@include('form.login')
				    </div>
				    <div class="progress" style=" margin: 0; background-color: transparent">
			            <div class="indeterminate" style="background-color: #689F38"></div>
			        </div>
				</div>
			</div>
		</div>
	</div>
	<div style="position: absolute; bottom: 0; min-height: 2rem; background-color: ; width: 100% ">
		<div class="row no-margin">
			<div class="col s6">
				<p style="padding: 1rem; color: rgba(255, 255, 255, .3)">
					Copyright © 2016 Jurusan Teknik Lingkungan - All Rights Reserved
				</p>
			</div>

			<!--<div class="col s6">
				<p style="padding: 1rem; color: rgba(255, 255, 255, .3); float: right">
					Powered by Kipa
				</p>
			</div>-->
		</div>
	</div>
</section>

@endsection

@section('template_scripts')

@endsection
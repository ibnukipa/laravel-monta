<script type="text/javascript">

	function doTolak(caller) {
		var modalTarget = $(caller).attr('data-target');
		var pesan 		= $(caller).attr('data-pesan');
		var url 		= $(caller).attr('data-url');

		$('#'+modalTarget).openModal();
		modalTarget = $('#'+modalTarget);
		
		modalTarget.find('.modal-footer #yes').data('url', url);
		modalTarget.find('p.content').html(pesan);
	}

	$('#confirmTolak').find('.modal-footer #yes').on('click', function(){
	  	location.href = $(this).data('url');
	});

</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#input_toefl').filer({
            changeInput: '<a data-warna="biru" class="jFiler-input-button bayangan_2dp" style="margin-top: 1rem">Cari berkas...</a>',
            limit: 1,
            extensions: ["jpg", "png", "pdf"],
            showThumbs: true,
            captions : {
                errors: {
                    filesType: "Maaf! file yang Anda pilih tidak didukung.",
                }
            },
            files: [
                {
                    name: "nilai_toefl_{{ $file_toefl['name'] }}",
                    size: "{{ $file_toefl['size'] }}",
                    type: "{{ $file_toefl['type'] }}",
                    file: "{{ URL::route('proposal.file', ['nilai_toefl']) }}"
                },
            ],
            onEmpty: function() {
                var filerKit = $('#input_toefl').prop('jFiler');
                filerKit.append({
                    name: "nilai_toefl_{{ $file_toefl['name'] }}",
                    size: "{{ $file_toefl['size'] }}",
                    type: "{{ $file_toefl['type'] }}",
                    file: "{{ URL::route('proposal.file', ['nilai_toefl']) }}"
                });
            },
        });
        
        $('#kategori_proposal').material_select();
        $('#' + 'usulan_dosen1').select2({
            placeholder: 'Pilih dosen..'
        });
        $('#' + 'usulan_dosen2').select2({
            placeholder: 'Pilih dosen..'
        });

        $('#' + 'usulan_lab1').select2({
			placeholder: 'Pilih Lab..'
		});
		$('#' + 'usulan_lab2').select2({
			placeholder: 'Pilih Lab..'
		});

        $("select").change(function() {
            var selector = this.id;
            var tujuan;
            if(selector === 'usulan_dosen1') {
                var tujuan = 'usulan_dosen2'; 
            } else if(selector === 'usulan_dosen2') {
                var tujuan = 'usulan_dosen1';
            } else {
                return false;
            }
            var vals = this.value;
            $('#' + tujuan).find('option').each(function() {
                $(this).prop('disabled', false);
                if(this.value == vals)
                    $(this).prop('disabled', true);
            });
            $('#' + tujuan).select2();
        });
    });
</script>
<script type="text/javascript">

    function doUpload(caller) {
        var modalId = $(caller).attr('data-target');
        var url     = $(caller).attr('data-url');
        var judul   = $(caller).attr('data-judul');
        var post = $.ajax({
            url         : url,
            type        : 'GET',
            datatype    : 'JSON',
            success     : function(data) {
                $('#'+modalId).find('.body').html(data);
            }
        });
        $('#'+modalId).find('.head-form').html(judul);
        $('#'+modalId).openModal();
    } 
</script>
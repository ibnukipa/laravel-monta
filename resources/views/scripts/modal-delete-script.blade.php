<script type="text/javascript">

	function doDelete(caller) {
		var modalTarget = $(caller).attr('data-target');
		var pesan 		= $(caller).attr('data-pesan');
		var form 		= $(caller).closest('form');
		$('#'+modalTarget).openModal();
		modalTarget = $('#'+modalTarget);
		
		modalTarget.find('.modal-footer #yes').data('form', form);
		modalTarget.find('p.content').html(pesan);
	}

	$('#confirmDelete').find('.modal-footer #yes').on('click', function(){
	  	$(this).data('form').submit();
	});

</script>
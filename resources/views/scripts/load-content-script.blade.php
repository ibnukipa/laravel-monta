<script type="text/javascript">
		function load_content(caller) {
			if($(caller).prop('tagName') != 'A')
				select_this(caller);
			var breadcrumbs	= caller.getAttribute('data-bread');
			var urlTarget 	= caller.getAttribute('data-url');
			var content 	= $('#content');
			$('.status-content').html(breadcrumbs);
			$.ajax({
				url			: urlTarget,
				type 		: 'GET',
				datatype 	: 'JSON',
				success		: function(data) {
					
					if(data.status == undefined) {
						content.html(data);
						$(".progress").css({"visibility": "hidden", "display": "none"});
						$(content).slideDown( 200 );
					} else {
						var status = "<div class='chip alert grey-dark mr-top1' style='font-size: 1rem'>"+ data.status + "<i class='close material-icons'>close</i></div>"
						content.find('#status').html(status);
						
						content.css({"display": "block"});
						$(".progress").css({"visibility": "hidden", "display": "none"});
						return false;
					}
				},
				beforeSend 	: function(jqXHR, options) {
					$(".progress").css({"visibility": "visible", "display": "block"});
					content.css({"display": "none"});
					setTimeout(function() {
						$.ajax($.extend(options, {beforeSend: $.noop}));
					}, 1000);
					return false;
				}
			});
		}
</script>
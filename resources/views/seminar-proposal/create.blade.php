<h4 class="head-form">
	{{ $judul }}
</h4>

<h6 class="center" style="margin-bottom: 20px; color: {{Auth::user()->warna['utama']}}; opacity: .5">Teknik Lingkungan ITS</h6>
<div class="divider"></div>

{!! Form::open(array('url' => 'seminar-proposal', 'method' => 'POST', 'class' => ' form-blue lockscreen-credentials form-horizontal', 'role' => 'form', 'files'=>true)) !!}
	<div class="row no-margin">
        <div class="col s12" style="margin-bottom: 1rem">
			@include('partials.form-status')
        </div>
		<div class="input-field col s8">
			<i class="material-icons prefix active">account_box</i>
            {!! Form::text('nama_lengkap', Auth::user()->full_name, array('id' => 'nama_lengkap', 'class' => '', 'readonly' => 'readonly')) !!}
   			{!! Form::label('nama_lengkap', 'Nama Lengkap' , array('class' => 'active')); !!}
		</div>
		<div class="input-field col s4">
			<i class="material-icons prefix active">chrome_reader_mode</i>
			{!! Form::text('username', Auth::user()->username, array('id' => 'username', 'class' => '', 'readonly' => 'readonly')) !!}
   			{!! Form::label('username', 'NRP' , array('class' => 'active')); !!}
		</div>

		<div class="input-field col s12 mr-top2">
			<i class="material-icons prefix">import_contacts</i>
			{!! Form::textarea('judul_ta', Auth::user()->tugas_akhir->judul, array('id' => 'judul_ta', 'class' => 'form-control ckeditor', 'required' => 'required')) !!}
   			{!! Form::label('judul_ta', 'Judul Tugas Akhir' , array('class' => 'active')); !!}
		</div>

        <div class="input-field col s6 mr-top3">
			<i class="material-icons prefix active">person</i>
			{!! Form::text('dosen_pembimbing', Auth::user()->tugas_akhir->dosen_pembimbing->full_name, array('id' => 'dosen_pembimbing', 'class' => '', 'readonly' => 'readonly')) !!}
   			{!! Form::label('dosen_pembimbing', 'Dosen Pembimbing' , array('class' => 'active')); !!}
		</div>

		<div class="input-field col s6 mr-top3">
			<i class="material-icons prefix" style="margin-top: -0.5rem">donut_large</i>
			{!! Form::text('lab', Auth::user()->tugas_akhir->lab_ta->description, array('id' => 'lab', 'class' => '','style' => 'font-size: 1rem', 'readonly' => 'readonly')) !!}
            {!! Form::label('lab', 'Lab :' , array('class' => 'active', 'style' => '')); !!}
		</div>

		<div class="input-field col s12 mr-top3">
			<i class="material-icons prefix" style="margin-top: -0.5rem">chevron_right</i>
			{!! Form::select('kategori_ta', $kategoriTA , old('kategori_ta'), array('id' => 'kategori_ta', 'style' => 'width: 90%;', 'required' => 'required')) !!}
            {!! Form::label('kategori_ta', 'Kategori Tugas Akhir' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
		</div>

        <div class="input-field col s8 mr-top2">
			<i class="material-icons prefix active">insert_drive_file</i>
            {!! Form::text('nama_mk1', null, array('id' => 'nama_mk1', 'class' => 'validate', 'required' => 'required')) !!}
   			{!! Form::label('nama_mk1', 'Nama Mata Kuliah 1:' , array()); !!}
		</div>

        <div class="input-field col s3 mr-top2">
			<i class="material-icons prefix active">chevron_right</i>
            {!! Form::text('nilai_mk1', null, array('id' => 'nilai_mk1', 'class' => 'validate', 'required' => 'required')) !!}
   			{!! Form::label('nilai_mk1', 'Nilai Mata Kuliah 1:' , array()); !!}
		</div>

        <div class="input-field col s8 mr-top2">
			<i class="material-icons prefix active">insert_drive_file</i>
            {!! Form::text('nama_mk2', null, array('id' => 'nama_mk2', 'class' => 'validate', 'required' => 'required')) !!}
   			{!! Form::label('nama_mk2', 'Nama Mata Kuliah 2:' , array()); !!}
		</div>

		<div class="input-field col s3 mr-top2">
			<i class="material-icons prefix active">chevron_right</i>
            {!! Form::text('nilai_mk2', null, array('id' => 'nilai_mk2', 'class' => 'validate', 'required' => 'required')) !!}
   			{!! Form::label('nilai_mk2', 'Nilai Mata Kuliah 2:' , array()); !!}
		</div>

        <div class="input-field col s8 mr-top2">
			<i class="material-icons prefix active">insert_drive_file</i>
            {!! Form::text('nama_mk3', null, array('id' => 'nama_mk3', 'class' => 'validate', '' => '')) !!}
   			{!! Form::label('nama_mk3', 'Nama Mata Kuliah 3 :' , array()); !!}
			<div style="margin-top: -.5rem; text-align: right; color: rgba(0, 0, 0, .3); font-size: .8rem; font-weight: 600">
				<span>bisa dikosongkan</span>
			</div>
		</div>

		<div class="input-field col s3 mr-top2">
			<i class="material-icons prefix active">chevron_right</i>
            {!! Form::text('nilai_mk3', null, array('id' => 'nilai_mk3', 'class' => 'validate', '' => '')) !!}
   			{!! Form::label('nilai_mk3', 'Nilai Mata Kuliah 3:' , array()); !!}
			<div style="margin-top: -.5rem; text-align: right; color: rgba(0, 0, 0, .3); font-size: .8rem; font-weight: 600">
				<span>bisa dikosongkan</span>
			</div>
		</div>

		<div class="input-field col s8 mr-top2" style="margin-bottom: 10px; margin-top: 2.5rem;">
			<i class="material-icons prefix">attachment</i>
			{!! Form::file('proposal_ta', array('id' => 'proposal_ta', 'required' => 'required')) !!}
            {!! Form::label('proposal_ta', 'Proposal TA :' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
			{!! Form::label('proposal_ta', "(.pdf) (max: 30MB)" , array('class' => 'active', 'style' => 'font-size: .8rem;top: 1rem; color: rgba("0, 0, 0, .5")', 'required' => 'required')); !!}			            			
		</div>

		<div class="input-field col s8 mr-top2" style="margin-bottom: 10px; margin-top: 2.5rem;">
			<i class="material-icons prefix">attachment</i>
			{!! Form::file('verifikasi_dosbing', array('id' => 'verifikasi_dosbing', 'required' => 'required')) !!}
            {!! Form::label('verifikasi_dosbing', 'Verifikasi Dosen Pembimbing (PTA-02) :' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
			{!! Form::label('verifikasi_dosbing', "(.pdf | .jpg | .jpeg | .png) (max: 3MB)" , array('class' => 'active', 'style' => 'font-size: .8rem;top: 1rem; color: rgba("0, 0, 0, .5")', 'required' => 'required')); !!}			            			
		</div>
		
         <div class="col s12 mr-top2" style="text-align: right; border-top: 1px solid rgba(0, 0, 0, .1); padding-top: 1rem" >
            <ul>
                <li class="btn-hov">
                    {!! Form::button("<i class='material-icons left' style='line-height: inherit; margin-right: 5px; font-size: 1.3em'>save</i><span class='font_button'>Simpan</span>", array('class' => 'btn bayangan_2dp green','type' => 'submit')) !!}
                </li>
            </ul>
        </div>

	</div>
{!! Form::close() !!}
<script src="{{URL::asset('js/dash.js')}}"></script>
<script type="text/javascript">
	CKEDITOR.replace( 'judul_ta', {
        contentsCss : '{{ URL::asset('css/style.css') }}',
        removeButtons : 'Cut,Undo,Copy,Redo,Scayt,Link,Image,Maximize,Source,Table,Unlink,Anchor,HorizontalRule,SpecialChar,RemoveFormat,Strike,NumberedList,BulletedList,Outdent,Indent,Styles,Format,About'
    });
    $(document).ready(function() {
		$('#proposal_ta').filer({
            changeInput: '<a data-warna="biru" class="jFiler-input-button bayangan_2dp" style="margin-top: 1rem">Cari berkas...</a>',
            limit: 1,
			maxSize: 30,
            extensions: ["pdf"],
            showThumbs: true,
            captions : {
                errors: {
                    filesType: "Maaf! file yang Anda pilih tidak didukung.",
                }
            }
        });

		$('#verifikasi_dosbing').filer({
            changeInput: '<a data-warna="biru" class="jFiler-input-button bayangan_2dp" style="margin-top: 1rem">Cari berkas...</a>',
            limit: 1,
			maxSize: 3,
            extensions: ["pdf", "jpg", "jpeg", "png"],
            showThumbs: true,
            captions : {
                errors: {
                    filesType: "Maaf! file yang Anda pilih tidak didukung.",
                }
            }
        });

        $('#kategori_ta').material_select();

		$('#judul_ta').validate({
			ignore 		: [],
			debug 		: false,
			rules 		: {
					cktext 	: {
						required 	: function() {
							CKEDITOR.instances.cktext.updateElement();
						},
						minlength 	: 10
					} 	
			},
			messages 	: {
					cktext 	: {
						required:"Please enter Text",
                        minlength:"Please enter 10 characters"
					}
			}
		});
    });
	

</script>

@extends('app')

@section('template_title')
	Dashboard {{ Auth::user()->name }}
@endsection

@section('content')
<div class="wadah" style="">
	<div class="row">
		<div class="col s3" style="width: 19%; padding: 0; position: fixed; left: 0; z-index: 9">
			<div class="card" style="box-shadow: none; margin: 0">
				<section id="menu-user">
					@if(Auth::user()->hasRole('mahasiswa'))
						@include('partials.menu-mahasiswa')
					@elseif(Auth::user()->hasRole('koordinator') || Auth::user()->hasRole('kaprodi'))
						@include('partials.menu-koordinator')
					@elseif(Auth::user()->hasRole('operator'))
						@include('partials.menu-operator')
					@elseif(Auth::user()->hasRole('administrator'))
						@include('partials.menu-admin')
					@endif
				</section>
			</div>
		</div>
		<section id="content-container">
			<div class="col s10 right" style="width: 81%; padding: 0;">
				<div class="card bayangan_2dp" style="margin: 0">
			        <div class="status-content" style="background: #364150 ; color: white; font-weight: 500">
					{!! Breadcrumbs::render('welcome') !!}
			        </div>
			        <div class="progress" style="margin: 0; background-color: #B3E5FC;">
					    <div class="indeterminate" style="background-color: #0288D1"></div>
					</div>
					<section id="content" style="border-bottom: 3px solid {{Auth::user()->warna['utama']}} ; padding-top: 20px; color: red ?>">
						{{-- <div class="row">
							<div class="col s12" style="text-align: left;">
								<ul>
									<li class="btn-hov">
										<a target="_blank" href="{{ route('berkas.jadwal.mahasiswa', ['proposal']) }}" style="padding: 0 15px;" class="btn bayangan_2dp grey-dark">
											<i class="material-icons left" style="line-height: inherit; margin-right: 5px; font-size: 1.3em">file_download</i>
											<span class="font_button" style="text-transform: none;">Info Grafis MonTA</span>
										</a>
									</li>
									<li class="btn-hov">
										<a target="_blank" href="{{ route('berkas.jadwal.mahasiswa', ['proposal']) }}" style="padding: 0 15px;" class="btn bayangan_2dp grey-dark">
											<i class="material-icons left" style="line-height: inherit; margin-right: 5px; font-size: 1.3em">file_download</i>
											<span class="font_button" style="text-transform: none;">User Manual Mahasiswa</span>
										</a>
									</li>
								</ul>
							</div>
						</div> --}}
						<img src="{{ URL::asset('img/info-grafis.png') }}" alt="" style="width: 100%;">
					</section>
				</div>
			</div>
		</section>
	</div>
</div>
@endsection

@section('template_scripts')
	<script src="{{URL::asset('js/dash.js')}}"></script>
	@include('scripts.load-content-script')
@endsection

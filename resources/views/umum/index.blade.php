
@extends('app')

@section('template_title')
	Tugas Akhir
@endsection

@section('content')
<div class="wadah" style="">
	<div class="row">
		<section id="content-container">
			<div class="col s10 right" style="width: 100%; padding: 0;">
				<div class="card bayangan_2dp" style="margin: 0">
			        <div class="progress" style="margin: 0; background-color: #B3E5FC;">
					    <div class="indeterminate" style="background-color: #0288D1"></div>
					</div>
					<section id="content" style="border-bottom: 3px solid #3e464c ; padding-top: 20px; color: red ?>">
						<h4 class="head-form">
							Tugas Akhir
						</h4>

						<h6 class="center" style="margin-bottom: 20px; color: #3e464c; opacity: .5">Teknik Lingkungan ITS</h6>
						<div class="divider"></div>

						<div class="row">
							<div class="col s12" style="padding: 1rem">
                                <table id="users_table" class="highlight">
                                    <thead>
                                        <tr class="grey-light">
											<th>NRP</th>
                                            <th>Nama</th>
                                            <th>Dosen Pembimbing</th>
                                            <th>Status</th>
                                            <th class="center">Tindakan</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
											<th>NRP</th>
                                            <th>Nama</th>
                                            <th>Dosen Pembimbing</th>
                                            <th>Status</th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
										
                                        @foreach($users as $value)
                                            <tr>
												<td>{{ $value->username }}</td>
                                                <td>{{ $value->full_name }}</td>
                                                <td>
                                                    @if($value->tugas_akhir)
                                                        {{$value->tugas_akhir['dosen_pembimbing']->full_name}}
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($value->proposal['status_id'] <= 15)
                                                        {{ $value->proposal['status']->name }}
                                                    @else
                                                        {{ $value->tugas_akhir->status->name }}
                                                    @endif
                                                </td>
                                                <td class="center">
                                                    <ul class="no-margin">
                                                        <li style="display: inline-block">
															<a onclick="openDetail(this)" data-url="{{ route('umum.tugas-akhir.detail', [ $value->id]) }}" data-target="modalDetail" data-judul="Detail Tugas Akhir" style="margin-left: .5rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto" class="green">
																<span class="font_button" style="text-transform: none">Detail</span>
															</a>
                                                        </li>
                                                    </ul>
												</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div> 
							</div>
						</div>
					</section>
				</div>
			</div>
		</section>
	</div>
</div>

@include('modals.modal-detail')
@endsection

@section('template_scripts')
	<script src="{{URL::asset('js/dash.js')}}"></script>
    @include('scripts.load-content-script')
	@include('scripts.modal-detail-script')
	
	{{-- Data Table --}}
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
		$(function () {
			$(document).ready(function() {
			    //Setup untuk elemen search HTML
			    $('#users_table tfoot th').each( function () {
			        var title = $(this).text();
			        if(title != '')
                    
			        	$(this).html( '<div id="users_table_filter" class="dataTables_filter"><label><input type="search" class="form-control input-sm" placeholder="Cari '+title+'..." ></label></div>' );
			    } );
			 
			    // DataTable
			    var table = $('#users_table').DataTable({
			    	language: {
			    		searchPlaceholder: "Cari Data...",
                        "zeroRecords": "Tidak ada data Tugas Akhir.",
			    	},
			    	"columnDefs": [
				    	{
				    		"targets"	: [4],
				    		"searchable": false,
				    		'bSortable': false,
				    	}
			    	],
			    	"paging": true,
			    	"autoWidth": false,
					"searching": true
			    });
			 
			    // Menerapkan serach masing-masing kolom
			    table.columns().every( function () {
			        var that = this;
			 
			        $( 'input', this.footer() ).on( 'keyup change', function () {
			            if ( that.search() !== this.value ) {
			                that
			                    .search( this.value )
			                    .draw();
			            }
			        } );
			    } );
			} );
		});
    </script>
@endsection

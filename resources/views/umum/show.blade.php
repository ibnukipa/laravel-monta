<div class="form-blue" style="margin: 1rem 10px">
    <div class="row no-margin">
        <div class="input-field col s6 ">
            <i class="material-icons prefix active">account_box</i>
            {!! Form::text('full_name', $user->full_name, array('id' => 'full_name', 'class' => '', 'readonly' => 'readonly')) !!}
            {!! Form::label('full_name', 'Nama Lengkap' , array('class' => 'active')); !!}
        </div>

        <div class="input-field col s6 ">
            <i class="material-icons prefix active">chrome_reader_mode</i>
            {!! Form::text('username', $user->username, array('id' => 'username', 'class' => '', 'readonly' => 'readonly')) !!}
            {!! Form::label('username', 'NRP' , array('class' => 'active')); !!}
        </div>
        @if($user->tugas_akhir)
        <div class="input-field col s6 ">
            <i class="material-icons prefix active">person</i>
            {!! Form::text('dosen_pembimbing', $user->tugas_akhir->dosen_pembimbing->full_name, array('id' => 'dosen_pembimbing', 'class' => '', 'readonly' => 'readonly')) !!}
            {!! Form::label('dosen_pembimbing', 'Dosen Pembimbing' , array('class' => 'active')); !!}
        </div>
        
        <div class="input-field col s6 ">
            <i class="material-icons prefix active">donut_large</i>
            {!! Form::text('lab_ta', $user->tugas_akhir->lab_ta->description, array('id' => 'lab_ta', 'style' => 'font-size: 1rem', 'readonly' => 'readonly')) !!}
            {!! Form::label('lab_ta', 'Lab Tugas Akhir' , array('class' => 'active')); !!}
        </div>
        @endif

        @if($user->tugas_akhir)
        <div class="input-field col s12 ">
            <div style="line-height: 0; display: inline-block; vertical-align: middle; margin-top: -4px; width: 100%">
                <span class="judul_kedua disable_select center">Judul Tugas Akhir</span>
                <span class="judul_utama disable_select center" style="padding: 5px; text-transform: capitalize">
                    {!!$user->tugas_akhir->judul!!}
                </span>
            </div>
        </div>
        @endif
        <div class="input-field col s12 ">
            <div style="line-height: 0; display: inline-block; vertical-align: middle; margin-top: -4px; width: 100%">
                <span class="judul_kedua disable_select center">Status Tugas Akhir</span>
                <span class="judul_utama disable_select center" style="padding: 5px; text-transform: capitalize">
                    @if($user->proposal->status_id == 16)
                     {{$user->tugas_akhir->status->name}}
                    @else
                        {{$user->proposal->status->name}}
                    @endif
                </span>
            </div>
        </div>
        
        <div class="input-field col s12">
                @if($user->tugas_akhir && !$user->tugas_akhir->jadwal->isEmpty())
                        @foreach($user->tugas_akhir->jadwal as $jadwal)
                            <div class="row no-margin grey-light" style="margin-bottom: 1rem; padding: 1rem">
                                <div class="input-field col s6" style="display: block">
                                    <i class="material-icons prefix active">date_range</i>
                                    {!! Form::text('waktu'.$jadwal->id, $jadwal->tanggal .', '. date(('H:i'),strtotime($jadwal->jadwal_jam->mulai)) .' - '. date(('H:i'),strtotime($jadwal->jadwal_jam->selesai)), array('id' => 'waktu'.$jadwal->id, 'style' => '', 'readonly' => 'readonly')) !!}
                                    {!! Form::label('waktu'.$jadwal->id, 'Waktu Seminar ' , array('class' => 'active')); !!}
                                </div>
                                <div class="input-field col s6">
                                    <i class="material-icons prefix active">room</i>
                                    {!! Form::text('ruang'.$jadwal->id, $jadwal->jadwal_ruang->description, array('id' => 'ruang'.$jadwal->id, 'style' => '', 'readonly' => 'readonly')) !!}
                                    {!! Form::label('ruang'.$jadwal->id, 'Ruang Seminar ' , array('class' => 'active')); !!}
                                </div>

                                @foreach($jadwal->dosen_penguji as $key => $dosen_penguji)
                                    <div class="input-field col s6">
                                        <i class="material-icons prefix active" style="font-size: 2rem">person</i>
                                        {!! Form::text('nama'.$dosen_penguji->id, $dosen_penguji->dosen->full_name, array('id' => 'nama'.$dosen_penguji->id, 'style' => 'font-size: 1rem', 'readonly' => 'readonly')) !!}
                                        {!! Form::label('nama'.$dosen_penguji->id, 'Dosen Penguji '.($key+1) , array('class' => 'active', 'style' => 'font-size: .8rem')); !!}
                                    </div>
                                @endforeach

                                <div class="input-field col s6">
                                    <i class="material-icons prefix active">donut_small</i>
                                    {!! Form::text('tipe'.$jadwal->id, $jadwal->jadwal_kategori->description, array('id' => 'tipe'.$jadwal->id, 'style' => '', 'readonly' => 'readonly')) !!}
                                    {!! Form::label('tipe'.$jadwal->id, 'Tipe Seminar ' , array('class' => 'active')); !!}
                                </div>
                            </div>
                        @endforeach
                @else
                    <div class="chip alert red mr-top1">
                        Jadwal Online tidak tersedia
                        <i class="close material-icons">close</i>
                    </div>
                @endif
        </div>
    </div>
</div>
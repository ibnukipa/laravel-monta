{!! Form::open(array('url' => url('sidang/'. Auth::user()->id), 'method' => 'PATCH', 'class' => ' form-blue lockscreen-credentials form-horizontal', 'role' => 'form', 'files'=>true)) !!}
	<div class="row no-margin">
		<div class="input-field col s6">
            <i class="material-icons prefix active">account_box</i>
            {!! Form::text('nama_lengkap', Auth::user()->full_name, array('id' => 'nama_lengkap', 'class' => '', 'readonly' => 'readonly')) !!}
            {!! Form::label('nama_lengkap', 'Nama Lengkap' , array('class' => 'active')); !!}
        </div>

        <div class="input-field col s3">
            <i class="material-icons prefix active">chrome_reader_mode</i>
            {!! Form::text('username', Auth::user()->username, array('id' => 'username', 'class' => '', 'readonly' => 'readonly')) !!}
            {!! Form::label('username', 'NRP' , array('class' => 'active')); !!}
        </div>

        <div class="input-field col s3">
            <i class="material-icons prefix">local_library</i>
            {!! Form::number('nilai_toefl', Auth::user()->proposal->nilai_toefl, array('id' => 'nilai_toefl', 'class' => '', 'readonly' => 'readonly')) !!}
            {!! Form::label('nilai_toefl', 'Nilai TOEFL' , array()); !!}
        </div>

		<div class="input-field col s12 mr-top2">
            <i class="material-icons prefix">import_contacts</i>
            <div style="line-height: 2rem; margin-left: 3rem; margin-top: 1rem; min-height: 100px; font-size: 1.5rem; border-bottom: 1px dotted rgba(25, 118, 210, 1)">
                {!! Auth::user()->tugas_akhir->judul !!}
            </div>
            {!! Form::label('judul_ta', 'Judul Tugas Akhir' , array('class' => 'active')); !!}
        </div>

        <div class="input-field col s6 mr-top2">
            <i class="material-icons prefix active">local_library</i>
            {!! Form::text('dosen_pembimbing', Auth::user()->tugas_akhir->dosen_pembimbing->full_name, array('id' => 'dosen_pembimbing', 'class' => '', 'readonly' => 'readonly')) !!}
            {!! Form::label('dosen_pembimbing', 'Dosen Pembimbing : ' , array('class' => 'active')); !!}
        </div>

        <div class="input-field col s6 mr-top2">
            <i class="material-icons prefix active">label</i>
            {!! Form::text('kategori_ta', Auth::user()->tugas_akhir->kategori_ta->description, array('style' => 'font-size: 1rem', 'id' => 'yang_merupakan', 'class' => '', 'readonly' => 'readonly')) !!}
            {!! Form::label('kategori_ta', 'Kategori Tugas Akhir: ' , array('class' => 'active')); !!}
        </div>

		<div class="input-field col s4 mr-top2" style="margin-bottom: 10px; margin-top: 2.5rem;">
			<i class="material-icons prefix" style="margin-top: -1rem">attachment</i>
            {!! Form::label('laporan_ta', 'File Laporan Tugas Akhir' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}

            <a class="grey-dark" target="_blank" href="{{ URL::route('proposal.file', ['laporan_ta']) }}" style=" margin-left: 3rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                <span class="font_button" style="text-transform: none">Lihat</span>
            </a>

            <a class="green" target="_blank" href="{{ URL::route('proposal.file.download', ['laporan_ta']) }}" style=" margin-left: .5rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                <span class="font_button" style="text-transform: none">Download</span>
            </a>

		</div>

        <div class="input-field col s4 mr-top2" style="margin-bottom: 10px; margin-top: 2.5rem;">
			<i class="material-icons prefix" style="margin-top: -1rem">attachment</i>
            {!! Form::label('berita_acara', 'File Berita Acara Seminar Kemajuan TA' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}

            <a class="grey-dark" target="_blank" href="{{ URL::route('proposal.file', ['berita_acara']) }}" style=" margin-left: 3rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                <span class="font_button" style="text-transform: none">Lihat</span>
            </a>

            <a class="green" target="_blank" href="{{ URL::route('proposal.file.download', ['berita_acara']) }}" style=" margin-left: .5rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                <span class="font_button" style="text-transform: none">Download</span>
            </a>
			
		</div>

        <div class="input-field col s4 mr-top2" style="margin-bottom: 10px; margin-top: 2.5rem;">
			<i class="material-icons prefix" style="margin-top: -1rem">attachment</i>
            {!! Form::label('lembar_asistensi', 'File Kegiatan Asistensi (FTA-03)' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}

            <a class="grey-dark" target="_blank" href="{{ URL::route('proposal.file', ['lembar_asistensi']) }}" style=" margin-left: 3rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                <span class="font_button" style="text-transform: none">Lihat</span>
            </a>

            <a class="green" target="_blank" href="{{ URL::route('proposal.file.download', ['lembar_asistensi']) }}" style=" margin-left: .5rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                <span class="font_button" style="text-transform: none">Download</span>
            </a>				
		</div>

        <div class="input-field col s4 mr-top2" style="margin-bottom: 10px; margin-top: 2.5rem;">
			<i class="material-icons prefix" style="margin-top: -1rem">attachment</i>
            {!! Form::label('form_perbaikan', 'File Perbaikan (FTA-04)' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}

            <a class="grey-dark" target="_blank" href="{{ URL::route('proposal.file', ['form_perbaikan']) }}" style=" margin-left: 3rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                <span class="font_button" style="text-transform: none">Lihat</span>
            </a>

            <a class="green" target="_blank" href="{{ URL::route('proposal.file.download', ['form_perbaikan']) }}" style=" margin-left: .5rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                <span class="font_button" style="text-transform: none">Download</span>
            </a>				
		</div>

        <div class="input-field col s4 mr-top2" style="margin-bottom: 10px; margin-top: 2.5rem;">
			<i class="material-icons prefix" style="margin-top: -1rem">attachment</i>
            {!! Form::label('borang_cek_format', 'Borang Cek Format Laporan TA : ' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}

            <a class="grey-dark" target="_blank" href="{{ URL::route('proposal.file', ['borang_cek_format']) }}" style=" margin-left: 3rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                <span class="font_button" style="text-transform: none">Lihat</span>
            </a>

            <a class="green" target="_blank" href="{{ URL::route('proposal.file.download', ['borang_cek_format']) }}" style=" margin-left: .5rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                <span class="font_button" style="text-transform: none">Download</span>
            </a>				
		</div>
		
         <div class="col s12 mr-top2" style="text-align: right; border-top: 1px solid rgba(0, 0, 0, .1); padding-top: 1rem" >
            <ul>
                <li class="btn-hov">
                    {!! Form::button("<i class='material-icons left'' style='line-height: inherit; margin-right: 5px; font-size: 1.3em'>save</i><span class='font_button'>Simpan Perubahan</span>", array('class' => 'btn bayangan_2dp blue','type' => 'submit')) !!}
                </li>
            </ul>
        </div>

	</div>
{!! Form::close() !!}

<script type="text/javascript">
	
   $(document).ready(function() {
        $('#laporan_ta').filer({
            changeInput: '<a data-warna="biru" class="jFiler-input-button bayangan_2dp">Cari berkas...</a>',
            limit: 1,
            extensions: ["pdf"],
            showThumbs: true,
            captions : {
                errors: {
                    filesType: "Maaf! file yang Anda pilih tidak didukung.",
                }
            },
            files: [
                {
                    name: "{{ $file_laporan_ta['name'] }}",
                    size: "{{ $file_laporan_ta['size'] }}",
                    type: "{{ $file_laporan_ta['type'] }}",
                    file: "{{ URL::route('proposal.file', ['laporan_ta']) }}"
                },
            ],
            onEmpty: function() {
                var filerKit = $('#laporan_ta').prop('jFiler');
                filerKit.append({
                    name: "{{ $file_laporan_ta['name'] }}",
                    size: "{{ $file_laporan_ta['size'] }}",
                    type: "{{ $file_laporan_ta['type'] }}",
                    file: "{{ URL::route('proposal.file', ['laporan_ta']) }}"
                });
            },
        }); 

        $('#berita_acara').filer({
            changeInput: '<a data-warna="biru" class="jFiler-input-button bayangan_2dp">Cari berkas...</a>',
            limit: 1,
            extensions: ["pdf", "jpg", "png"],
            showThumbs: true,
            captions : {
                errors: {
                    filesType: "Maaf! file yang Anda pilih tidak didukung.",
                }
            },
            files: [
                {
                    name: "{{ $file_berita_acara['name'] }}",
                    size: "{{ $file_berita_acara['size'] }}",
                    type: "{{ $file_berita_acara['type'] }}",
                    file: "{{ URL::route('proposal.file', ['berita_acara']) }}"
                },
            ],
            onEmpty: function() {
                var filerKit = $('#berita_acara').prop('jFiler');
                filerKit.append({
                    name: "{{ $file_berita_acara['name'] }}",
                    size: "{{ $file_berita_acara['size'] }}",
                    type: "{{ $file_berita_acara['type'] }}",
                    file: "{{ URL::route('proposal.file', ['berita_acara']) }}"
                });
            },
        }); 

        $('#lembar_asistensi').filer({
            changeInput: '<a data-warna="biru" class="jFiler-input-button bayangan_2dp">Cari berkas...</a>',
            limit: 1,
            extensions: ["pdf", "jpg", "png"],
            showThumbs: true,
            captions : {
                errors: {
                    filesType: "Maaf! file yang Anda pilih tidak didukung.",
                }
            },
            files: [
                {
                    name: "{{ $file_lembar_asistensi['name'] }}",
                    size: "{{ $file_lembar_asistensi['size'] }}",
                    type: "{{ $file_lembar_asistensi['type'] }}",
                    file: "{{ URL::route('proposal.file', ['lembar_asistensi']) }}"
                },
            ],
            onEmpty: function() {
                var filerKit = $('#lembar_asistensi').prop('jFiler');
                filerKit.append({
                    name: "{{ $file_lembar_asistensi['name'] }}",
                    size: "{{ $file_lembar_asistensi['size'] }}",
                    type: "{{ $file_lembar_asistensi['type'] }}",
                    file: "{{ URL::route('proposal.file', ['lembar_asistensi']) }}"
                });
            },
        }); 

        $('#form_perbaikan').filer({
            changeInput: '<a data-warna="biru" class="jFiler-input-button bayangan_2dp">Cari berkas...</a>',
            limit: 1,
            extensions: ["pdf", "jpg", "png"],
            showThumbs: true,
            captions : {
                errors: {
                    filesType: "Maaf! file yang Anda pilih tidak didukung.",
                }
            },
            files: [
                {
                    name: "{{ $file_form_perbaikan['name'] }}",
                    size: "{{ $file_form_perbaikan['size'] }}",
                    type: "{{ $file_form_perbaikan['type'] }}",
                    file: "{{ URL::route('proposal.file', ['form_perbaikan']) }}"
                },
            ],
            onEmpty: function() {
                var filerKit = $('#form_perbaikan').prop('jFiler');
                filerKit.append({
                    name: "{{ $file_form_perbaikan['name'] }}",
                    size: "{{ $file_form_perbaikan['size'] }}",
                    type: "{{ $file_form_perbaikan['type'] }}",
                    file: "{{ URL::route('proposal.file', ['form_perbaikan']) }}"
                });
            },
        });       
    });
</script>
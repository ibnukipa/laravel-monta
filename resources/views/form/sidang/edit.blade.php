<div class="panel lime-transparent">
{!! Form::open(array('url' => url('sidang/'. Auth::user()->id), 'method' => 'PATCH', 'class' => ' form-blue lockscreen-credentials form-horizontal', 'role' => 'form', 'files'=>true)) !!}
	<div class="row no-margin">
		<div class="input-field col s6">
            <i class="material-icons prefix active">account_box</i>
            {!! Form::text('nama_lengkap', Auth::user()->full_name, array('id' => 'nama_lengkap', 'class' => '', 'readonly' => 'readonly')) !!}
            {!! Form::label('nama_lengkap', 'Nama Lengkap' , array('class' => 'active')); !!}
        </div>

        <div class="input-field col s3">
            <i class="material-icons prefix active">chrome_reader_mode</i>
            {!! Form::text('username', Auth::user()->username, array('id' => 'username', 'class' => '', 'readonly' => 'readonly')) !!}
            {!! Form::label('username', 'NRP' , array('class' => 'active')); !!}
        </div>

        <div class="input-field col s3">
            <i class="material-icons prefix">local_library</i>
            {!! Form::number('nilai_toefl', Auth::user()->proposal->nilai_toefl, array('id' => 'nilai_toefl', 'class' => '', 'readonly' => 'readonly')) !!}
            {!! Form::label('nilai_toefl', 'Nilai TOEFL' , array()); !!}
        </div>

		<div class="input-field col s12 mr-top2">
			<i class="material-icons prefix">import_contacts</i>
			{!! Form::textarea('judul_ta', Auth::user()->tugas_akhir->judul, array('id' => 'judul_ta', 'class' => 'form-control ckeditor')) !!}
   			{!! Form::label('judul_ta', 'Judul Tugas Akhir' , array('class' => 'active')); !!}
		</div>

        <div class="input-field col s6 mr-top2">
            <i class="material-icons prefix active">local_library</i>
            {!! Form::text('dosen_pembimbing', Auth::user()->tugas_akhir->dosen_pembimbing->full_name, array('id' => 'dosen_pembimbing', 'class' => '', 'readonly' => 'readonly')) !!}
            {!! Form::label('dosen_pembimbing', 'Dosen Pembimbing : ' , array('class' => 'active')); !!}
        </div>

        <div class="input-field col s6 mr-top2">
            <i class="material-icons prefix active">label</i>
            {!! Form::text('kategori_ta', Auth::user()->tugas_akhir->kategori_ta->description, array('style' => 'font-size: 1rem', 'id' => 'yang_merupakan', 'class' => '', 'readonly' => 'readonly')) !!}
            {!! Form::label('kategori_ta', 'Kategori Tugas Akhir: ' , array('class' => 'active')); !!}
        </div>

		<div class="input-field col s6 mr-top2" style="margin-bottom: 10px; margin-top: 2.5rem;">
			<i class="material-icons prefix">attachment</i>
			{!! Form::file('laporan_ta', array('id' => 'laporan_ta')) !!}
            {!! Form::label('laporan_ta', 'Laporan Tugas Akhir :' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
            {!! Form::label('laporan_ta', "(.pdf) (max: 30MB)" , array('class' => 'active', 'style' => 'font-size: .8rem;top: 1rem; color: rgba("0, 0, 0, .5")', 'required' => 'required')); !!}			            

            <div style="margin-top: .5rem">
                <a class="grey-dark" target="_blank" href="{{ URL::route('proposal.file', ['laporan_ta']) }}" style=" margin-left: 3rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                    <span class="font_button" style="text-transform: none">Lihat</span>
                </a>

                <a class="green" target="_blank" href="{{ URL::route('proposal.file.download', ['laporan_ta']) }}" style=" margin-left: .5rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                    <span class="font_button" style="text-transform: none">Download</span>
                </a>
            </div>

		</div>

        <div class="input-field col s6 mr-top2" style="margin-bottom: 10px; margin-top: 2.5rem;">
			<i class="material-icons prefix">attachment</i>
			{!! Form::file('berita_acara', array('id' => 'berita_acara')) !!}
            {!! Form::label('berita_acara', 'Berita Acara Seminar Kemajuan TA : ' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
            {!! Form::label('berita_acara', "(.pdf, .jpg, .png) (max: 1MB)" , array('class' => 'active', 'style' => 'font-size: .8rem;top: 1rem; color: rgba("0, 0, 0, .5")', 'required' => 'required')); !!}			

            <div style="margin-top: .5rem">
                <a class="grey-dark" target="_blank" href="{{ URL::route('proposal.file', ['berita_acara']) }}" style=" margin-left: 3rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                    <span class="font_button" style="text-transform: none">Lihat</span>
                </a>

                <a class="green" target="_blank" href="{{ URL::route('proposal.file.download', ['berita_acara']) }}" style=" margin-left: .5rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                    <span class="font_button" style="text-transform: none">Download</span>
                </a>
            </div>
				
		</div>

        <div class="input-field col s6 mr-top2" style="margin-bottom: 10px; margin-top: 2.5rem;">
			<i class="material-icons prefix">attachment</i>
			{!! Form::file('lembar_asistensi', array('id' => 'lembar_asistensi')) !!}
            {!! Form::label('lembar_asistensi', 'Lembar Kegiatan Asistensi : ' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
            {!! Form::label('lembar_asistensi', "(.pdf, .jpg, .png) (max: 1MB)" , array('class' => 'active', 'style' => 'font-size: .8rem;top: 1rem; color: rgba("0, 0, 0, .5")', 'required' => 'required')); !!}			

            <div style="margin-top: .5rem">
                <a class="grey-dark" target="_blank" href="{{ URL::route('proposal.file', ['lembar_asistensi']) }}" style=" margin-left: 3rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                    <span class="font_button" style="text-transform: none">Lihat</span>
                </a>

                <a class="green" target="_blank" href="{{ URL::route('proposal.file.download', ['lembar_asistensi']) }}" style=" margin-left: .5rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                    <span class="font_button" style="text-transform: none">Download</span>
                </a>
            </div>
				
		</div>

        <div class="input-field col s6 mr-top2" style="margin-bottom: 10px; margin-top: 2.5rem;">
			<i class="material-icons prefix">attachment</i>
			{!! Form::file('form_perbaikan', array('id' => 'form_perbaikan')) !!}
            {!! Form::label('form_perbaikan', 'Form Perbaikan Laporan : ' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
            {!! Form::label('form_perbaikan', "(.pdf, .jpg, .png) (max: 1MB)" , array('class' => 'active', 'style' => 'font-size: .8rem;top: 1rem; color: rgba("0, 0, 0, .5")', 'required' => 'required')); !!}				

            <div style="margin-top: .5rem">
                <a class="grey-dark" target="_blank" href="{{ URL::route('proposal.file', ['form_perbaikan']) }}" style=" margin-left: 3rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                    <span class="font_button" style="text-transform: none">Lihat</span>
                </a>

                <a class="green" target="_blank" href="{{ URL::route('proposal.file.download', ['form_perbaikan']) }}" style=" margin-left: .5rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                    <span class="font_button" style="text-transform: none">Download</span>
                </a>
            </div>
			
		</div>

        <div class="input-field col s6 mr-top2" style="margin-bottom: 10px; margin-top: 2.5rem;">
			<i class="material-icons prefix">attachment</i>
			{!! Form::file('borang_cek_format', array('id' => 'borang_cek_format')) !!}
            {!! Form::label('borang_cek_format', 'Borang Cek Format Laporan TA : ' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
            {!! Form::label('borang_cek_format', "(.pdf, .jpg, .png) (max: 1MB)" , array('class' => 'active', 'style' => 'font-size: .8rem;top: 1rem; color: rgba("0, 0, 0, .5")', 'required' => 'required')); !!}				

            <div style="margin-top: .5rem">
                <a class="grey-dark" target="_blank" href="{{ URL::route('proposal.file', ['borang_cek_format']) }}" style=" margin-left: 3rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                    <span class="font_button" style="text-transform: none">Lihat</span>
                </a>

                <a class="green" target="_blank" href="{{ URL::route('proposal.file.download', ['borang_cek_format']) }}" style=" margin-left: .5rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                    <span class="font_button" style="text-transform: none">Download</span>
                </a>
            </div>				
		</div>
		
         <div class="col s12 mr-top2" style="text-align: right; border-top: 1px solid rgba(0, 0, 0, .1); padding-top: 1rem" >
            <ul>
                <li class="btn-hov">
                    {!! Form::button("<i class='material-icons left'' style='line-height: inherit; margin-right: 5px; font-size: 1.3em'>save</i><span class='font_button'>Simpan Perubahan</span>", array('class' => 'btn bayangan_2dp blue','type' => 'submit')) !!}
                </li>
            </ul>
        </div>

	</div>
{!! Form::close() !!}
<script type="text/javascript">
	CKEDITOR.replace( 'judul_ta', {
        contentsCss : '{{ URL::asset('css/style.css') }}',
        removeButtons : 'Cut,Undo,Copy,Redo,Scayt,Link,Image,Maximize,Source,Table,Unlink,Anchor,HorizontalRule,SpecialChar,RemoveFormat,Strike,NumberedList,BulletedList,Outdent,Indent,Styles,Format,About'
    });
	
    $(document).ready(function() {
        $('#laporan_ta').filer({
            changeInput: '<a data-warna="biru" class="jFiler-input-button bayangan_2dp" style="margin-top: 1rem">Cari berkas...</a>',
            limit: 1,
            extensions: ["pdf"],
            showThumbs: true,
            captions : {
                errors: {
                    filesType: "Maaf! file yang Anda pilih tidak didukung.",
                }
            },
            files: [
                {
                    name: "laporan_ta_{{ $file_laporan_ta['name'] }}",
                    size: "{{ $file_laporan_ta['size'] }}",
                    type: "{{ $file_laporan_ta['type'] }}",
                    file: "{{ URL::route('proposal.file', ['laporan_ta']) }}"
                },
            ],
            onEmpty: function() {
                var filerKit = $('#laporan_ta').prop('jFiler');
                filerKit.append({
                    name: "laporan_ta_{{ $file_laporan_ta['name'] }}",
                    size: "{{ $file_laporan_ta['size'] }}",
                    type: "{{ $file_laporan_ta['type'] }}",
                    file: "{{ URL::route('proposal.file', ['laporan_ta']) }}"
                });
            },
        }); 

        $('#berita_acara').filer({
            changeInput: '<a data-warna="biru" class="jFiler-input-button bayangan_2dp" style="margin-top: 1rem">Cari berkas...</a>',
            limit: 1,
            extensions: ["pdf", "jpg", "png"],
            showThumbs: true,
            captions : {
                errors: {
                    filesType: "Maaf! file yang Anda pilih tidak didukung.",
                }
            },
            files: [
                {
                    name: "berita_acara_kemajuan_{{ $file_berita_acara['name'] }}",
                    size: "{{ $file_berita_acara['size'] }}",
                    type: "{{ $file_berita_acara['type'] }}",
                    file: "{{ URL::route('proposal.file', ['berita_acara']) }}"
                },
            ],
            onEmpty: function() {
                var filerKit = $('#berita_acara').prop('jFiler');
                filerKit.append({
                    name: "berita_acara_kemajuan_{{ $file_berita_acara['name'] }}",
                    size: "{{ $file_berita_acara['size'] }}",
                    type: "{{ $file_berita_acara['type'] }}",
                    file: "{{ URL::route('proposal.file', ['berita_acara']) }}"
                });
            },
        }); 

        $('#lembar_asistensi').filer({
            changeInput: '<a data-warna="biru" class="jFiler-input-button bayangan_2dp" style="margin-top: 1rem">Cari berkas...</a>',
            limit: 1,
            extensions: ["pdf", "jpg", "png"],
            showThumbs: true,
            captions : {
                errors: {
                    filesType: "Maaf! file yang Anda pilih tidak didukung.",
                }
            },
            files: [
                {
                    name: "kegiatan_asistensi_{{ $file_lembar_asistensi['name'] }}",
                    size: "{{ $file_lembar_asistensi['size'] }}",
                    type: "{{ $file_lembar_asistensi['type'] }}",
                    file: "{{ URL::route('proposal.file', ['lembar_asistensi']) }}"
                },
            ],
            onEmpty: function() {
                var filerKit = $('#lembar_asistensi').prop('jFiler');
                filerKit.append({
                    name: "kegiatan_asistensi_{{ $file_lembar_asistensi['name'] }}",
                    size: "{{ $file_lembar_asistensi['size'] }}",
                    type: "{{ $file_lembar_asistensi['type'] }}",
                    file: "{{ URL::route('proposal.file', ['lembar_asistensi']) }}"
                });
            },
        }); 

        $('#form_perbaikan').filer({
            changeInput: '<a data-warna="biru" class="jFiler-input-button bayangan_2dp" style="margin-top: 1rem">Cari berkas...</a>',
            limit: 1,
            extensions: ["pdf", "jpg", "png"],
            showThumbs: true,
            captions : {
                errors: {
                    filesType: "Maaf! file yang Anda pilih tidak didukung.",
                }
            },
            files: [
                {
                    name: "perbaikan_laporan_{{ $file_form_perbaikan['name'] }}",
                    size: "{{ $file_form_perbaikan['size'] }}",
                    type: "{{ $file_form_perbaikan['type'] }}",
                    file: "{{ URL::route('proposal.file', ['form_perbaikan']) }}"
                },
            ],
            onEmpty: function() {
                var filerKit = $('#form_perbaikan').prop('jFiler');
                filerKit.append({
                    name: "perbaikan_laporan_{{ $file_form_perbaikan['name'] }}",
                    size: "{{ $file_form_perbaikan['size'] }}",
                    type: "{{ $file_form_perbaikan['type'] }}",
                    file: "{{ URL::route('proposal.file', ['form_perbaikan']) }}"
                });
            },
        });

        $('#borang_cek_format').filer({
            changeInput: '<a data-warna="biru" class="jFiler-input-button bayangan_2dp" style="margin-top: 1rem">Cari berkas...</a>',
            limit: 1,
            extensions: ["pdf", "jpg", "png"],
            showThumbs: true,
            captions : {
                errors: {
                    filesType: "Maaf! file yang Anda pilih tidak didukung.",
                }
            },
            files: [
                {
                    name: "cek_format_{{ $file_borang_cek_format['name'] }}",
                    size: "{{ $file_borang_cek_format['size'] }}",
                    type: "{{ $file_borang_cek_format['type'] }}",
                    file: "{{ URL::route('proposal.file', ['borang_cek_format']) }}"
                },
            ],
            onEmpty: function() {
                var filerKit = $('#borang_cek_format').prop('jFiler');
                filerKit.append({
                    name: "cek_format_{{ $file_borang_cek_format['name'] }}",
                    size: "{{ $file_borang_cek_format['size'] }}",
                    type: "{{ $file_borang_cek_format['type'] }}",
                    file: "{{ URL::route('proposal.file', ['borang_cek_format']) }}"
                });
            },
        });    
    });
</script>
</div>
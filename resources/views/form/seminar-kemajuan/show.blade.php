{!! Form::open(array('url' => url('seminar-kemajuan/'. Auth::user()->id), 'method' => 'PATCH', 'class' => ' form-blue lockscreen-credentials form-horizontal', 'role' => 'form', 'files'=>true)) !!}
	<div class="row no-margin">
		<div class="input-field col s6">
			<i class="material-icons prefix active">account_box</i>
            {!! Form::text('nama_lengkap', Auth::user()->full_name, array('id' => 'nama_lengkap', 'class' => '', 'readonly' => 'readonly')) !!}
   			{!! Form::label('nama_lengkap', 'Nama Lengkap' , array('class' => 'active')); !!}
		</div>
		<div class="input-field col s3">
			<i class="material-icons prefix active">chrome_reader_mode</i>
			{!! Form::text('username', Auth::user()->username, array('id' => 'username', 'class' => '', 'readonly' => 'readonly')) !!}
   			{!! Form::label('username', 'NRP' , array('class' => 'active')); !!}
		</div>

		<div class="input-field col s3">
			<i class="material-icons prefix active">chevron_right</i>
            {!! Form::number('progres_kemajuan',  Auth::user()->tugas_akhir->progres_penulisan, array('id' => 'progres_kemajuan', 'class' => '', 'readonly' => 'readonly')) !!}
   			{!! Form::label('progres_kemajuan', 'Penulisan Tugas Akhir (%):' , array('class' => 'active')); !!}
		</div>

		<div class="input-field col s12 mr-top2">
            <i class="material-icons prefix">import_contacts</i>
            <div style="line-height: 2rem; margin-left: 3rem; margin-top: 1rem; min-height: 100px; font-size: 1.5rem; border-bottom: 1px dotted rgba(25, 118, 210, 1)">
                {!! Auth::user()->tugas_akhir->judul !!}
            </div>
            {!! Form::label('judul_ta', 'Judul Tugas Akhir' , array('class' => 'active')); !!}
        </div>

        <div class="input-field col s6 mr-top3">
			<i class="material-icons prefix active">person</i>
			{!! Form::text('dosen_pembimbing', Auth::user()->tugas_akhir->dosen_pembimbing->full_name, array('id' => 'dosen_pembimbing', 'class' => '', 'readonly' => 'readonly')) !!}
   			{!! Form::label('dosen_pembimbing', 'Dosen Pembimbing' , array('class' => 'active')); !!}
		</div>

		<div class="input-field col s6 mr-top3">
			<i class="material-icons prefix active">donut_large</i>
			{!! Form::text('lab_ta', Auth::user()->tugas_akhir->lab_ta->description, array('id' => 'lab_ta', 'class' => '','style' => 'font-size: 1rem', 'readonly' => 'readonly')) !!}
   			{!! Form::label('lab_ta', 'Lab TA' , array('class' => 'active')); !!}
		</div>

		<div class="input-field col s12 mr-top2">
            <i class="material-icons prefix active">label</i>
            {!! Form::text('kategori_ta', Auth::user()->tugas_akhir->kategori_ta->description, array('style' => 'font-size: 1.2rem', 'id' => 'yang_merupakan', 'class' => '', 'readonly' => 'readonly')) !!}
            {!! Form::label('kategori_ta', 'Kategori Tugas Akhir: ' , array('class' => 'active')); !!}
        </div>

		<div class="input-field col s4 mr-top2" style="margin-bottom: 10px; margin-top: 2.5rem;">
			<i class="material-icons prefix" style="margin-top: -1rem">attachment</i>
            {!! Form::label('', 'File Draft Laporan TA' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}

            <a class="grey-dark" target="_blank" href="{{ URL::route('proposal.file', ['laporan_ta']) }}" style=" margin-left: 3rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                <span class="font_button" style="text-transform: none">Lihat</span>
            </a>

            <a class="green" target="_blank" href="{{ URL::route('proposal.file.download', ['laporan_ta']) }}" style=" margin-left: .5rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                <span class="font_button" style="text-transform: none">Download</span>
            </a>
		</div>

        <div class="input-field col s4 mr-top2" style="margin-bottom: 10px; margin-top: 2.5rem;">
            <i class="material-icons prefix" style="margin-top: -1rem">attachment</i>
            {!! Form::label('', 'File Proposal TA' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
            
            <a class="grey-dark" target="_blank" href="{{ URL::route('proposal.file', ['proposal_ta']) }}" style=" margin-left: 3rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                <span class="font_button" style="text-transform: none">Lihat</span>
            </a>

            <a class="green" target="_blank" href="{{ URL::route('proposal.file.download', ['proposal_ta']) }}" style=" margin-left: .5rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                <span class="font_button" style="text-transform: none">Download</span>
            </a>
        </div>


        <div class="input-field col s4 mr-top2" style="margin-bottom: 10px; margin-top: 2.5rem;">
            <i class="material-icons prefix" style="margin-top: -1rem">attachment</i>
            {!! Form::label('verifi_kemajuan', 'Verifikasi Dosen Pembimbing (KTA-01)' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
            
            <a class="grey-dark" target="_blank" href="{{ URL::route('proposal.file', ['verifi_kemajuan']) }}" style=" margin-left: 3rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                <span class="font_button" style="text-transform: none">Lihat</span>
            </a>

            <a class="green" target="_blank" href="{{ URL::route('proposal.file.download', ['verifi_kemajuan']) }}" style=" margin-left: .5rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                <span class="font_button" style="text-transform: none">Download</span>
            </a>
        </div>

        

	</div>
{!! Form::close() !!}

<script type="text/javascript">
	
    $(document).ready(function() {
         $('#laporan_ta').filer({
            changeInput: '<a data-warna="biru" class="jFiler-input-button bayangan_2dp">Cari berkas...</a>',
            limit: 1,
            extensions: ["pdf"],
            showThumbs: true,
            captions : {
                errors: {
                    filesType: "Maaf! file yang Anda pilih tidak didukung.",
                }
            },
            files: [
                {
                    name: "{{ $file_laporan_ta['name'] }}",
                    size: "{{ $file_laporan_ta['size'] }}",
                    type: "{{ $file_laporan_ta['type'] }}",
                    file: "{{ URL::route('proposal.file', ['laporan_ta']) }}"
                },
            ],
            onEmpty: function() {
                var filerKit = $('#laporan_ta').prop('jFiler');
                filerKit.append({
                    name: "{{ $file_laporan_ta['name'] }}",
                    size: "{{ $file_laporan_ta['size'] }}",
                    type: "{{ $file_laporan_ta['type'] }}",
                    file: "{{ URL::route('proposal.file', ['laporan_ta']) }}"
                });
            },
        });

        $('#verifi_kemajuan').filer({
            changeInput: '<a data-warna="biru" class="jFiler-input-button bayangan_2dp" style="margin-top: 1rem">Cari berkas...</a>',
            limit: 1,
			maxSize: 3,
            extensions: ["pdf", "jpg", "jpeg", "png"],
            showThumbs: true,
            captions : {
                errors: {
                    filesType: "Maaf! file yang Anda pilih tidak didukung.",
                }
            },
            files: [
                {
                    name: "verifi_kemajuan_{{ $file_verifi_kemajuan['name'] }}",
                    size: "{{ $file_verifi_kemajuan['size'] }}",
                    type: "{{ $file_verifi_kemajuan['type'] }}",
                    file: "{{ URL::route('proposal.file', ['verifi_kemajuan']) }}"
                },
            ],
            onEmpty: function() {
                var filerKit = $('#verifi_kemajuan').prop('jFiler');
                filerKit.append({
                    name: "{{ $file_verifi_kemajuan['name'] }}",
                    size: "{{ $file_verifi_kemajuan['size'] }}",
                    type: "{{ $file_verifi_kemajuan['type'] }}",
                    file: "{{ URL::route('proposal.file', ['verifi_kemajuan']) }}"
                });
            },
        });
        
    });
</script>
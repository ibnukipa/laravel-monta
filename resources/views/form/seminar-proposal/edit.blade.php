<div class="panel lime-transparent">
{!! Form::open(array('url' => url('seminar-proposal/'. Auth::user()->id), 'method' => 'PATCH', 'class' => ' form-blue lockscreen-credentials form-horizontal', 'role' => 'form', 'files'=>true)) !!}
	<div class="row no-margin">
		<div class="input-field col s8">
			<i class="material-icons prefix active">account_box</i>
            {!! Form::text('nama_lengkap', Auth::user()->full_name, array('id' => 'nama_lengkap', 'class' => '', 'readonly' => 'readonly')) !!}
   			{!! Form::label('nama_lengkap', 'Nama Lengkap' , array('class' => 'active')); !!}
		</div>
		<div class="input-field col s4">
			<i class="material-icons prefix active">chrome_reader_mode</i>
			{!! Form::text('username', Auth::user()->username, array('id' => 'username', 'class' => '', 'readonly' => 'readonly')) !!}
   			{!! Form::label('username', 'NRP' , array('class' => 'active')); !!}
		</div>

		<div class="input-field col s12 mr-top2">
			<i class="material-icons prefix">import_contacts</i>
			{!! Form::textarea('judul_ta', Auth::user()->tugas_akhir->judul, array('id' => 'judul_ta', 'class' => 'form-control ckeditor')) !!}
   			{!! Form::label('judul_ta', 'Judul Tugas Akhir' , array('class' => 'active')); !!}
		</div>

        <div class="input-field col s6 mr-top3">
			<i class="material-icons prefix active">person</i>
			{!! Form::text('dosen_pembimbing', Auth::user()->tugas_akhir->dosen_pembimbing->full_name, array('id' => 'dosen_pembimbing', 'class' => '', 'readonly' => 'readonly')) !!}
   			{!! Form::label('dosen_pembimbing', 'Dosen Pembimbing' , array('class' => 'active')); !!}
		</div>

        <div class="input-field col s6 mr-top3">
			<i class="material-icons prefix active" style="margin-top: -0.5rem">donut_large</i>
			{!! Form::text('lab', Auth::user()->tugas_akhir->lab_ta->description, array('id' => 'lab', 'class' => '','style' => 'font-size: 1rem', 'readonly' => 'readonly')) !!}
            {!! Form::label('lab', 'Lab :' , array('class' => 'active', 'style' => '')); !!}
		</div>

		<div class="input-field col s12 mr-top3">
            
			<i class="material-icons prefix" style="margin-top: -0.5rem">chevron_right</i>
			{!! Form::select('kategori_ta', $kategoriTA , Auth::user()->tugas_akhir->kategori_ta_id, array('id' => 'kategori_ta', 'style' => 'width: 90%', 'required' => 'required')) !!}
            {!! Form::label('kategori_ta', 'Kategori Tugas Akhir : ' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
		</div>

        <div class="input-field col s8 mr-top2">
			<i class="material-icons prefix active">insert_drive_file</i>
            {!! Form::text('nama_mk1', $tugas_akhir->mata_kuliah_keahlian[0]->name, array('id' => 'nama_mk1', 'class' => 'validate', 'required' => 'required')) !!}
   			{!! Form::label('nama_mk1', 'Nama Mata Kuliah 1:' , array()); !!}
		</div>

        <div class="input-field col s3 mr-top2">
			<i class="material-icons prefix active">chevron_right</i>
            {!! Form::text('nilai_mk1', $tugas_akhir->mata_kuliah_keahlian[0]->nilai, array('id' => 'nilai_mk1', 'class' => 'validate', 'required' => 'required')) !!}
   			{!! Form::label('nilai_mk1', 'Nilai Mata Kuliah 1:' , array()); !!}
		</div>

        <div class="input-field col s8 mr-top2">
			<i class="material-icons prefix active">insert_drive_file</i>
            {!! Form::text('nama_mk2', $tugas_akhir->mata_kuliah_keahlian[1]->name, array('id' => 'nama_mk2', 'class' => 'validate', 'required' => 'required')) !!}
   			{!! Form::label('nama_mk2', 'Nama Mata Kuliah 2:' , array()); !!}
		</div>

		<div class="input-field col s3 mr-top2">
			<i class="material-icons prefix active">chevron_right</i>
            {!! Form::text('nilai_mk2', $tugas_akhir->mata_kuliah_keahlian[1]->nilai, array('id' => 'nilai_mk2', 'class' => 'validate', 'required' => 'required')) !!}
   			{!! Form::label('nilai_mk2', 'Nilai Mata Kuliah 2:' , array()); !!}
		</div>

        @if(isset($tugas_akhir->mata_kuliah_keahlian[2]))
        <div class="input-field col s8 mr-top2">
			<i class="material-icons prefix active">insert_drive_file</i>
            {!! Form::text('nama_mk3', $tugas_akhir->mata_kuliah_keahlian[2]->name, array('id' => 'nama_mk3', 'class' => 'validate', '' => '')) !!}
   			{!! Form::label('nama_mk3', 'Nama Mata Kuliah 3 :' , array()); !!}
			<div style="margin-top: -.5rem; text-align: right; color: rgba(0, 0, 0, .3); font-size: .8rem; font-weight: 600">
				<span>bisa dikosongkan</span>
			</div>
		</div>

		<div class="input-field col s3 mr-top2">
			<i class="material-icons prefix active">chevron_right</i>
            {!! Form::text('nilai_mk3', $tugas_akhir->mata_kuliah_keahlian[2]->nilai, array('id' => 'nilai_mk3', 'class' => 'validate', '' => '')) !!}
   			{!! Form::label('nilai_mk3', 'Nilai Mata Kuliah 3:' , array()); !!}
			<div style="margin-top: -.5rem; text-align: right; color: rgba(0, 0, 0, .3); font-size: .8rem; font-weight: 600">
				<span>bisa dikosongkan</span>
			</div>
		</div>
        @else
        <div class="input-field col s8 mr-top2">
			<i class="material-icons prefix active">insert_drive_file</i>
            {!! Form::text('nama_mk3', null, array('id' => 'nama_mk3', 'class' => 'validate', '' => '')) !!}
   			{!! Form::label('nama_mk3', 'Nama Mata Kuliah 3 :' , array()); !!}
			<div style="margin-top: -.5rem; text-align: right; color: rgba(0, 0, 0, .3); font-size: .8rem; font-weight: 600">
				<span>bisa dikosongkan</span>
			</div>
		</div>

		<div class="input-field col s3 mr-top2">
			<i class="material-icons prefix active">chevron_right</i>
            {!! Form::text('nilai_mk3', null, array('id' => 'nilai_mk3', 'class' => 'validate', '' => '')) !!}
   			{!! Form::label('nilai_mk3', 'Nilai Mata Kuliah 3:' , array()); !!}
			<div style="margin-top: -.5rem; text-align: right; color: rgba(0, 0, 0, .3); font-size: .8rem; font-weight: 600">
				<span>bisa dikosongkan</span>
			</div>
		</div>
        @endif
		<div class="input-field col s6 mr-top2" style="margin-bottom: 10px; margin-top: 5rem;">
			<i class="material-icons prefix">attachment</i>
			{!! Form::file('proposal_ta', array('id' => 'proposal_ta')) !!}
            {!! Form::label('proposal_ta', 'Proposal TA :' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
			{!! Form::label('proposal_ta', "(.pdf) (max: 30MB)" , array('class' => 'active', 'style' => 'font-size: .8rem;top: 1rem; color: rgba("0, 0, 0, .5")', 'required' => 'required')); !!}

            <div style="margin-top: .5rem">
                <a class="grey-dark" target="_blank" href="{{ URL::route('proposal.file', ['proposal_ta']) }}" style=" margin-left: 3rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                    <span class="font_button" style="text-transform: none">Lihat</span>
                </a>

                <a class="green" target="_blank" href="{{ URL::route('proposal.file.download', ['proposal_ta']) }}" style=" margin-left: .5rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                    <span class="font_button" style="text-transform: none">Download</span>
                </a>
            </div>
		</div>

        <div class="input-field col s6 mr-top2" style="margin-bottom: 10px; margin-top: 5rem;">
			<i class="material-icons prefix">attachment</i>
			{!! Form::file('verifi_proposal', array('id' => 'verifi_proposal')) !!}
            {!! Form::label('verifi_proposal', 'Verifikasi Dosen Pembimbing (PTA-02) :' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
			{!! Form::label('verifi_proposal', "(.pdf | .jpg | .jpeg | .png) (max: 3MB)" , array('class' => 'active', 'style' => 'font-size: .8rem;top: 1rem; color: rgba("0, 0, 0, .5")', 'required' => 'required')); !!}

            <div style="margin-top: .5rem">
                <a class="grey-dark" target="_blank" href="{{ URL::route('proposal.file', ['verifi_proposal']) }}" style=" margin-left: 3rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                    <span class="font_button" style="text-transform: none">Lihat</span>
                </a>

                <a class="green" target="_blank" href="{{ URL::route('proposal.file.download', ['verifi_proposal']) }}" style=" margin-left: .5rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                    <span class="font_button" style="text-transform: none">Download</span>
                </a>
            </div>
		</div>
		
         <div class="col s12 mr-top2" style="text-align: right; border-top: 1px solid rgba(0, 0, 0, .1); padding-top: 1rem" >
            <ul>
                <li class="btn-hov">
                    {!! Form::button("<i class='material-icons left'' style='line-height: inherit; margin-right: 5px; font-size: 1.3em'>save</i><span class='font_button'>Simpan Perubahan</span>", array('class' => 'btn bayangan_2dp blue','type' => 'submit')) !!}
                </li>
            </ul>
        </div>

	</div>
{!! Form::close() !!}
<script type="text/javascript">
	CKEDITOR.replace( 'judul_ta', {
        contentsCss : '{{ URL::asset('css/style.css') }}',
        removeButtons : 'Cut,Undo,Copy,Redo,Scayt,Link,Image,Maximize,Source,Table,Unlink,Anchor,HorizontalRule,SpecialChar,RemoveFormat,Strike,NumberedList,BulletedList,Outdent,Indent,Styles,Format,About'
    });
	
    $(document).ready(function() {
        $('#proposal_ta').filer({
            changeInput: '<a data-warna="biru" class="jFiler-input-button bayangan_2dp" style="margin-top: 1rem">Cari berkas...</a>',
            limit: 1,
            maxSize: 30,
            extensions: ["pdf"],
            showThumbs: true,
            captions : {
                errors: {
                    filesType: "Maaf! file yang Anda pilih tidak didukung.",
                }
            },
            files: [
                {
                    name: "proposal_ta_{{ $file_proposal['name'] }}",
                    size: "{{ $file_proposal['size'] }}",
                    type: "{{ $file_proposal['type'] }}",
                    file: "{{ URL::route('proposal.file', ['proposal_ta']) }}"
                },
            ],
            onEmpty: function() {
                var filerKit = $('#proposal_ta').prop('jFiler');
                filerKit.append({
                    name: "{{ $file_proposal['name'] }}",
                    size: "{{ $file_proposal['size'] }}",
                    type: "{{ $file_proposal['type'] }}",
                    file: "{{ URL::route('proposal.file', ['proposal_ta']) }}"
                });
            },
        });

        $('#verifi_proposal').filer({
            changeInput: '<a data-warna="biru" class="jFiler-input-button bayangan_2dp" style="margin-top: 1rem">Cari berkas...</a>',
            limit: 1,
			maxSize: 3,
            extensions: ["pdf", "jpg", "jpeg", ".png"],
            showThumbs: true,
            captions : {
                errors: {
                    filesType: "Maaf! file yang Anda pilih tidak didukung.",
                }
            },
            files: [
                {
                    name: "verifi_dosbing_{{ $file_verifi_proposal['name'] }}",
                    size: "{{ $file_verifi_proposal['size'] }}",
                    type: "{{ $file_verifi_proposal['type'] }}",
                    file: "{{ URL::route('proposal.file', ['verifi_proposal']) }}"
                },
            ],
            onEmpty: function() {
                var filerKit = $('#verifi_proposal').prop('jFiler');
                filerKit.append({
                    name: "{{ $file_verifi_proposal['name'] }}",
                    size: "{{ $file_verifi_proposal['size'] }}",
                    type: "{{ $file_verifi_proposal['type'] }}",
                    file: "{{ URL::route('proposal.file', ['verifi_proposal']) }}"
                });
            },
        });
        
        $('#kategori_ta').material_select();
        
    });
</script>
</div>
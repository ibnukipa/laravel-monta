
{!! Form::open(array('url' => url('proposal/'. Auth::user()->id), 'method' => 'PATCH', 'class' => ' form-blue lockscreen-credentials form-horizontal', 'role' => 'form', 'files'=>true)) !!}
    <div class="row no-margin">
        <div class="input-field col s6">
            <i class="material-icons prefix active">account_box</i>
            {!! Form::text('nama_lengkap', Auth::user()->full_name, array('id' => 'nama_lengkap', 'class' => '', 'readonly' => 'readonly')) !!}
            {!! Form::label('nama_lengkap', 'Nama Lengkap' , array('class' => 'active')); !!}
        </div>

        <div class="input-field col s3">
            <i class="material-icons prefix active">chrome_reader_mode</i>
            {!! Form::text('username', Auth::user()->username, array('id' => 'username', 'class' => '', 'readonly' => 'readonly')) !!}
            {!! Form::label('username', 'NRP' , array('class' => 'active')); !!}
        </div>

        <div class="input-field col s3">
            <i class="material-icons prefix">local_library</i>
            {!! Form::number('nilai_toefl', Auth::user()->proposal->nilai_toefl, array('id' => 'nilai_toefl', 'class' => '', 'readonly' => 'readonly')) !!}
            {!! Form::label('nilai_toefl', 'Nilai TOEFL' , array()); !!}
        </div>

        <div class="input-field col s12 mr-top2">
            <i class="material-icons prefix">import_contacts</i>
            <div style="line-height: 2rem; margin-left: 3rem; margin-top: 1rem; min-height: 100px; font-size: 1.5rem; border-bottom: 1px dotted rgba(25, 118, 210, 1)">
                {!! Auth::user()->tugas_akhir->judul !!}
            </div>
            {!! Form::label('judul_ta', 'Judul Tugas Akhir' , array('class' => 'active')); !!}
        </div>
        
        <div class="input-field col s6 mr-top2">
            <i class="material-icons prefix active">local_library</i>
            {!! Form::text('dosen_pembimbing', Auth::user()->tugas_akhir->dosen_pembimbing->full_name, array('id' => 'dosen_pembimbing', 'class' => '', 'readonly' => 'readonly')) !!}
            {!! Form::label('dosen_pembimbing', 'Dosen Pembimbing : ' , array('class' => 'active')); !!}
        </div>

        <div class="input-field col s6 mr-top2">
            <i class="material-icons prefix active">label</i>
            {!! Form::text('kategori_ta', Auth::user()->tugas_akhir->kategori_ta->description, array('style' => 'font-size: 1rem', 'id' => 'yang_merupakan', 'class' => '', 'readonly' => 'readonly')) !!}
            {!! Form::label('kategori_ta', 'Kategori Tugas Akhir: ' , array('class' => 'active')); !!}
        </div>

        <div class="input-field col s4 mr-top2" style="margin-bottom: 10px; margin-top: 2.5rem;">
            <i class="material-icons prefix" style="margin-top: -1rem">attachment</i>
            {!! Form::label('input_toefl', 'File Nilai TOEFL' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}

            <a class="grey-dark" target="_blank" href="{{ URL::route('proposal.file', ['nilai_toefl']) }}" style=" margin-left: 3rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                <span class="font_button" style="text-transform: none">Lihat</span>
            </a>

            <a class="green" target="_blank" href="{{ URL::route('proposal.file.download', ['nilai_toefl']) }}" style=" margin-left: .5rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                <span class="font_button" style="text-transform: none">Download</span>
            </a>

        </div>

        <div class="input-field col s4 mr-top2" style="margin-bottom: 10px; margin-top: 2.5rem;">
            <i class="material-icons prefix" style="margin-top: -1rem">attachment</i>
            {!! Form::label('proposal_ta', 'File Proposal TA' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}

            <a class="grey-dark" target="_blank" href="{{ URL::route('proposal.file', ['proposal_ta']) }}" style=" margin-left: 3rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                <span class="font_button" style="text-transform: none">Lihat</span>
            </a>

            <a class="green" target="_blank" href="{{ URL::route('proposal.file.download', ['proposal_ta']) }}" style=" margin-left: .5rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                <span class="font_button" style="text-transform: none">Download</span>
            </a>
        </div>

        <div class="input-field col s4 mr-top2" style="margin-bottom: 10px; margin-top: 2.5rem;">
            <i class="material-icons prefix" style="margin-top: -1rem">attachment</i>
            {!! Form::label('verifi_proposal', 'File Verifikasi Dosen Pembimbing' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}

            <a class="grey-dark" target="_blank" href="{{ URL::route('proposal.file', ['verifi_proposal']) }}" style=" margin-left: 3rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                <span class="font_button" style="text-transform: none">Lihat</span>
            </a>

            <a class="green" target="_blank" href="{{ URL::route('proposal.file.download', ['verifi_proposal']) }}" style=" margin-left: .5rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                <span class="font_button" style="text-transform: none">Download</span>
            </a>
        </div>        
    </div>
{!! Form::close() !!}
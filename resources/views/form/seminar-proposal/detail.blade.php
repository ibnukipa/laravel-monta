{!! Form::open(array('id' => 'formSeminarProposal', 'url' => '#', 'method' => 'POST', 'class' => ' form-blue lockscreen-credentials form-horizontal', 'role' => 'form', 'files'=>true)) !!}
    <div class="row no-margin">
        <div class="input-field col s6">
            <i class="material-icons prefix active">account_box</i>
            {!! Form::text('nama_lengkap', $user['nama_lengkap'], array('id' => 'nama_lengkap', 'class' => '', 'readonly' => 'readonly')) !!}
            {!! Form::label('nama_lengkap', 'Nama Lengkap' , array('class' => 'active')); !!}
        </div>
        <div class="input-field col s3">
            <i class="material-icons prefix active">chrome_reader_mode</i>
            {!! Form::text('username', $user['username'], array('id' => 'username', 'class' => '', 'readonly' => 'readonly')) !!}
            {!! Form::label('username', 'NRP' , array('class' => 'active')); !!}
        </div>

        <div class="input-field col s3">
            <i class="material-icons prefix active">local_library</i>
            {!! Form::text('nilai_toefl', $user['nilai_toefl'], array('id' => 'nilai_toefl', 'class' => '', 'readonly' => 'readonly')) !!}
            {!! Form::label('nilai_toefl', 'Nilai TOEFL' , array('class' => 'active')); !!}
        </div>

        <div class="input-field col s12 mr-top3">
            <i class="material-icons prefix active">import_contacts</i>
            <div style="line-height: 2rem; margin-left: 3rem; margin-top: 1rem; min-height: 100px; font-size: 1.5rem; border-bottom: 1px dotted rgba(25, 118, 210, 1)">
                {!! $user['judul_ta'] !!}
            </div>
            {!! Form::label('judul_ta', 'Judul Tugas Akhir' , array('class' => 'active')); !!}
        </div>

        <div class="input-field col s6 mr-top3">
            <i class="material-icons prefix active">person</i>
            {!! Form::text('dosen_pembimbing', $user['dosen_pembimbing'], array('style' => 'font-size: 1.3rem; font-weight: 400','id' => 'kategori_proposal', 'class' => '', 'required' => 'required', 'readonly' => 'readonly')) !!}
            {!! Form::label('dosen_pembimbing', 'Dosen Pembimbing: ' , array('class' => 'active')); !!}
        </div>

        <div class="input-field col s6 mr-top3">
            <i class="material-icons prefix active">chevron_right</i>
            {!! Form::text('kategori_ta', $user['kategori_ta'], array('style' => 'font-size: 1.2rem; font-weight: 400','id' => 'kategori_proposal', 'class' => '', 'readonly' => 'readonly')) !!}
            {!! Form::label('kategori_ta', 'Kategori Tugas Akhir: ' , array('class' => 'active')); !!}
        </div>
        @foreach ($user['mata_kuliah'] as $key => $mata_kuliah)
            <div class="input-field col s8 mr-top2">
                <i class="material-icons prefix active">insert_drive_file</i>
                {!! Form::text('nama_mk'.$key, $mata_kuliah->name, array('style' => 'font-size: 1.2rem; font-weight: 400', 'id' => 'nama_mk'.$key, 'class' => '', 'readonly' => 'readonly')) !!}
                {!! Form::label('nama_mk'.$key, 'Nama Mata Kuliah '.($key + 1).':' , array('class' => 'active')); !!}
            </div>

            <div class="input-field col s4 mr-top2">
                <i class="material-icons prefix active">chevron_right</i>
                {!! Form::text('nilai_mk'.$key, $mata_kuliah->nilai, array('style' => 'font-size: 1.2rem; font-weight: 400', 'id' => 'nilai_mk'.$key, 'class' => '', 'readonly' => 'readonly')) !!}
                {!! Form::label('nilai_mk'.$key, 'Nilai Mata Kuliah '.($key + 1).':' , array('class' => 'active')); !!}
            </div>
        @endforeach
        <div class="input-field col s4 mr-top2" style="margin-bottom: 10px; margin-top: 2.5rem;">
            <i class="material-icons prefix" style="margin-top: -1rem">attachment</i>
            {!! Form::label('input_toefl', 'File Nilai TOEFL' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}

            <a class="grey-dark" target="_blank" href="file/pendaftar/nilai_toefl/{{ $proposal->id }}" style=" margin-left: 3rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                <span class="font_button" style="text-transform: none">Lihat</span>
            </a>

            <a class="green" target="_blank" href="file/pendaftar/nilai_toefl/{{ $proposal->id }}/download" style=" margin-left: .5rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                <span class="font_button" style="text-transform: none">Download</span>
            </a>

        </div>

        <div class="input-field col s4 mr-top2" style="margin-bottom: 10px; margin-top: 2.5rem;">
            <i class="material-icons prefix" style="margin-top: -1rem">attachment</i>
            {!! Form::label('proposal_ta', 'File Proposal TA' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}

            <a class="grey-dark" target="_blank" href="file/pendaftar/proposal_ta/{{ $proposal->user->tugas_akhir->id }}" style=" margin-left: 3rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                <span class="font_button" style="text-transform: none">Lihat</span>
            </a>

            <a class="green" target="_blank" href="file/pendaftar/proposal_ta/{{ $proposal->user->tugas_akhir->id }}/download" style=" margin-left: .5rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                <span class="font_button" style="text-transform: none">Download</span>
            </a>
        </div>

        <div class="input-field col s4 mr-top2" style="margin-bottom: 10px; margin-top: 2.5rem;">
            <i class="material-icons prefix" style="margin-top: -1rem">attachment</i>
            {!! Form::label('verifi_proposal', 'File Verifikasi Dosen Pembimbing' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}

            <a class="grey-dark" target="_blank" href="file/pendaftar/verifi_proposal/{{ $proposal->user->tugas_akhir->id }}" style=" margin-left: 3rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                <span class="font_button" style="text-transform: none">Lihat</span>
            </a>

            <a class="green" target="_blank" href="file/pendaftar/verifi_proposal/{{ $proposal->user->tugas_akhir->id }}/download" style=" margin-left: .5rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                <span class="font_button" style="text-transform: none">Download</span>
            </a>
        </div>

        <div class="input-field col s12 mr-top3">
            <i class="material-icons prefix">speaker_notes</i>
            {!! Form::textarea('catatan', null, array('placeholder' => 'Tulis catatan/alasan mengenai terima/tolak yang dilakukan', 'style' => 'font-size: 1.3rem', 'id' => 'catatan', 'class' => 'materialize-textarea', 'required' => 'required')) !!}
            {!! Form::label('catatan', 'Catatan: ' , array('class' => 'active')); !!}
        </div>

        <div class="col s12 mr-top2" style="text-align: right; border-top: 1px solid rgba(0, 0, 0, .1); padding-top: 1rem" >
            <ul>
                <li class="btn-hov">
                    <a onclick="submitForm('pendaftar/{{ $proposal->id }}/terima')" class="btn bayangan_2dp green">
                        <i class="material-icons left" style="line-height: inherit; margin-right: 5px; font-size: 1.3em">done_all</i>
                        <span class="font_button">Terima</span>
                    </a>
                </li>
                <li class="btn-hov">
                    <a onclick="submitForm('pendaftar/{{ $proposal->id }}/tolak')" class="btn bayangan_2dp red">
                        <i class="material-icons left" style="line-height: inherit; margin-right: 5px; font-size: 1.3em">do_not_disturb</i>
                        <span class="font_button">Tolak</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
{!! Form::close() !!}
<script>
    function submitForm(action)
    {
        document.getElementById('formSeminarProposal').action = action;
        document.getElementById('formSeminarProposal').submit();
    }
</script>
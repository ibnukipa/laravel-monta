
{!! Form::open(array('id' => 'login_form','url' => 'auth/login', 'method' => 'POST', 'class' => 'lockscreen-credentials form-horizontal', 'role' => 'form')) !!}
	<div class="input-field col s12 ">
			<i class="material-icons prefix">chrome_reader_mode</i>
			{!! Form::label('username', 'NRP/NIP' , array('class' => 'validate')); !!}
			{!! Form::text('username', null, array('id' => 'username', 'class' => 'form-control', 'placeholder' => null, 'required' => 'required')) !!}
	</div>

	<div class="input-field col s12  @if($errors->has('password')) invalid @endif">
			<i class="material-icons prefix">https</i>
			{!! Form::label('password', 'Password' , array('class' => 'validate')); !!}
			{!! Form::password('password', array('id' => 'password', 'class' => 'form-control', 'placeholder' => null, 'required' => 'required')) !!}
	</div>

	<div class="input-field col s12">
		{!! Form::button('Masuk', array('style' => 'background-image: -webkit-linear-gradient(top,#8BC34A,#7EB83D); width: 100%; padding: 0 15px;','class' => 'btn bayangan_2dp','type' => 'submit')) !!}
	</div>
            
{!! Form::close() !!}
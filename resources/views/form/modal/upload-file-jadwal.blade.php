
{!! Form::open(array('url' => route('jadwal.file.upload.store'), 'method' => 'POST', 'class' => ' form-blue lockscreen-credentials form-horizontal', 'role' => 'form', 'files'=>true)) !!}
	<div class="row no-margin">
        <div class="input-field col s6">
            <i class="material-icons prefix active">spellcheck</i>
            {!! Form::text('name', null, array('id' => 'name', 'placeholder' => 'Contoh: jadwal revisi' ,'class' => 'validation', 'required' => 'required')) !!}
            {!! Form::label('name', 'Nama ' , array('class' => 'active')); !!}
        </div>

        <div class="input-field col s3">
            <i class="material-icons prefix active">account_box</i>
            {!! Form::text('tahun_ajaran', $tahunAjaran, array('id' => 'tahun_ajaran', 'class' => '', 'readonly' => 'readonly')) !!}
            {!! Form::label('tahun_ajaran', 'Tahun Ajaran' , array('class' => 'active')); !!}
        </div>
        <div class="input-field col s3">
            <i class="material-icons prefix active">account_box</i>
            {!! Form::text('semester', $semester, array('id' => 'semester', 'class' => '', 'readonly' => 'readonly')) !!}
            {!! Form::label('semester', 'Semester' , array('class' => 'active')); !!}
        </div>
        {{-- <div class="input-field col s12 mr-top3">
            <i class="material-icons prefix" style="margin-top: -0.5rem">class</i>
            {!! Form::select('tahun_ajaran', array() , null, array('id' => 'tahun_ajaran', 'style' => 'width: 90%', 'required' => 'required')) !!}
            {!! Form::label('tahun_ajaran', 'Tahun Ajaran: ' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
        </div>

        <div class="input-field col s12 mr-top3">
            <i class="material-icons prefix" style="margin-top: -0.5rem">class</i>
            {!! Form::select('semester', array() , null, array('id' => 'tahun_ajaran', 'style' => 'width: 90%', 'required' => 'required')) !!}
            {!! Form::label('semester', 'Tahun Ajaran: ' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
        </div> --}}

		<div class="input-field col s6" style="margin-bottom: 10px; margin-top: 2.5rem;">
			<i class="material-icons prefix">attachment</i>
            {{ Form::hidden('kategori', $realkategori) }}
			{!! Form::file('jadwal_file', array('id' => 'jadwal_file', 'required' => 'required')) !!}
            {!! Form::label('jadwal_file', 'Upload File Jadwal '.  $kategori , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
			{!! Form::label('jadwal_file', "(.xls, .xlsx) (max: 2MB)" , array('class' => 'active', 'style' => 'font-size: .8rem;top: 1rem; color: rgba("0, 0, 0, .5")', 'required' => 'required')); !!}			
		</div>
		
         <div class="col s12 mr-top2" style="text-align: right; border-top: 1px solid rgba(0, 0, 0, .1); padding-top: 1rem" >
            <ul>
                <li class="btn-hov">
                    {!! Form::button("<i class='material-icons left'' style='line-height: inherit; margin-right: 5px; font-size: 1.3em'>input</i><span class='font_button'>Submit</span>", array('class' => 'btn bayangan_2dp green','type' => 'submit')) !!}
                </li>
            </ul>
        </div>

	</div>
{!! Form::close() !!}

<script type="text/javascript">
	
    $(document).ready(function() {
		$('#jadwal_file').filer({
            changeInput: '<a data-warna="biru" class="jFiler-input-button bayangan_2dp" style="margin-top: 1rem">Cari berkas ...</a>',
            limit: 1,
			maxSize: 100,
            extensions: ["xls", "xlsx"],
            showThumbs: true,
            
            captions : {
                errors: {
                    filesType: "Maaf! file yang Anda pilih tidak didukung.",
                }
            }
        });
    });
	

</script>

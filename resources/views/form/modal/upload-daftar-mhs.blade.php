<h4 class="head-form">
	Upload Daftar Mahasiswa
</h4>

<h6 class="center" style="margin-bottom: 20px; color: {{Auth::user()->warna['utama']}}; opacity: .5">Teknik Lingkungan ITS</h6>
<div class="divider"></div>

{!! Form::open(array('url' => route('mahasiswa.upload.daftar.store'), 'method' => 'POST', 'class' => ' form-blue lockscreen-credentials form-horizontal', 'role' => 'form', 'files'=>true)) !!}
	<div class="row no-margin">
		<div class="input-field col s6" style="margin-bottom: 10px; margin-top: 2.5rem;">
			<i class="material-icons prefix">attachment</i>
			{!! Form::file('daftar_mhs', array('id' => 'daftar_mhs', 'required' => 'required')) !!}
            {!! Form::label('daftar_mhs', 'Daftar Mahasiswa (.xls, .xlsx) (max: 10MB)' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}			
		</div>
		
         <div class="col s12 mr-top2" style="text-align: right; border-top: 1px solid rgba(0, 0, 0, .1); padding-top: 1rem" >
            <ul>
                <li class="btn-hov">
                    {!! Form::button("<i class='material-icons left'' style='line-height: inherit; margin-right: 5px; font-size: 1.3em'>input</i><span class='font_button'>Submit</span>", array('class' => 'btn bayangan_2dp green','type' => 'submit')) !!}
                </li>
            </ul>
        </div>

	</div>
{!! Form::close() !!}

<script type="text/javascript">
	
    $(document).ready(function() {
		$('#daftar_mhs').filer({
            changeInput: '<a data-warna="biru" class="jFiler-input-button bayangan_2dp">Cari berkas ...</a>',
            limit: 1,
			maxSize: 10,
            extensions: ["xls", "xlsx"],
            showThumbs: true,
            
            captions : {
                errors: {
                    filesType: "Maaf! file yang Anda pilih tidak didukung.",
                }
            }
        });
    });
	

</script>

<div class="panel lime-transparent">
{!! Form::open(array('url' => url('proposal/'. Auth::user()->id), 'method' => 'PATCH', 'class' => ' form-blue lockscreen-credentials form-horizontal', 'role' => 'form', 'files'=>true)) !!}
    <div class="row no-margin">
        <div class="input-field col s6">
            <i class="material-icons prefix active">account_box</i>
            {!! Form::text('nama_lengkap', Auth::user()->full_name, array('id' => 'nama_lengkap', 'class' => '', 'disabled' => 'disabled')) !!}
            {!! Form::label('nama_lengkap', 'Nama Lengkap' , array('class' => 'active')); !!}
        </div>
        <div class="input-field col s3">
            <i class="material-icons prefix active">chrome_reader_mode</i>
            {!! Form::text('username', Auth::user()->username, array('id' => 'username', 'class' => '', 'disabled' => 'disabled')) !!}
            {!! Form::label('username', 'NRP' , array('class' => 'active')); !!}
        </div>
        <div class="input-field col s3">
            <i class="material-icons prefix">local_library</i>
            {!! Form::number('nilai_toefl', Auth::user()->proposal->nilai_toefl, array('id' => 'nilai_toefl', 'class' => 'validate', 'required' => 'required')) !!}
            {!! Form::label('nilai_toefl', 'Nilai TOEFL' , array()); !!}
        </div>
        
        <div class="input-field col s6 mr-top3">	
            <i class="material-icons prefix" style="margin-top: -0.5rem">person</i>
            {!! Form::select('usulan_dosen1', $dosen , Auth::user()->proposal->dosen_usulan->first()->dosen_id, array('placeholder' =>'', 'class' =>'js-example-disabled-results', 'id' => 'usulan_dosen1', 'style' => 'width: 90%', 'required' => 'required')) !!}
            {!! Form::label('usulan_dosen1', 'Usulan Calon Dosen Pembimbing 1' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
        </div>
    
        <div class="input-field col s6 mr-top3">
            <i class="material-icons prefix" style="margin-top: -0.5rem">person</i>
            {!! Form::select('usulan_dosen2', $dosen , Auth::user()->proposal->dosen_usulan->last()->dosen_id, array('placeholder' =>'', 'class' =>'js-example-disabled-results', 'id' => 'usulan_dosen2', 'style' => 'width: 90%', 'required' => 'required')) !!}
            {!! Form::label('usulan_dosen2', 'Usulan Calon Dosen Pembimbing 2' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
        </div>

        <div class="input-field col s6 mr-top3">
			<i class="material-icons prefix" style="margin-top: -0.5rem">donut_large</i>
			{!! Form::select('usulan_lab1', $lab , Auth::user()->proposal->lab_usulan->first()->lab_id, array('placeholder' =>'', 'class' =>'js-example-disabled-results', 'id' => 'usulan_lab1', 'style' => 'width: 90%', 'required' => 'required')) !!}
            {!! Form::label('usulan_lab1', 'Lab : ' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
		</div>

        <div class="input-field col s6 mr-top3">
			<i class="material-icons prefix" style="margin-top: -0.5rem">donut_large</i>
			{!! Form::select('usulan_lab2', $lab , Auth::user()->proposal->lab_usulan->last()->lab_id, array('placeholder' =>'', 'class' =>'js-example-disabled-results', 'id' => 'usulan_lab2', 'style' => 'width: 90%', 'required' => 'required')) !!}
            {!! Form::label('usulan_lab2', 'Lab :' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
		</div>

        <div class="input-field col s12 mr-top3">
            <i class="material-icons prefix">import_contacts</i>
            {!! Form::textarea('usulan_judul', Auth::user()->proposal->usulan_judul, array('id' => 'usulan_judul', 'class' => 'materialize-textarea validate', 'required' => 'required', 'style' => 'font-size: 1.3rem; line-height: 1.3')) !!}
            {!! Form::label('usulan_judul', 'Peminatan/Topik/Judul TA' , array('class' => 'active')); !!}
        </div>

        <div class="input-field col s12 mr-top3">
            <i class="material-icons prefix" style="margin-top: -0.5rem">class</i>
            {!! Form::select('kategori_proposal', $kategoriProposal , Auth::user()->proposal->kategori_proposal->id, array('id' => 'kategori_proposal', 'style' => 'width: 90%', 'required' => 'required')) !!}
            {!! Form::label('kategori_proposal', 'Yang merupakan: ' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
        </div>
        
        <div class="input-field col s6" style="margin-bottom: 10px; margin-top: 2.5rem;">
            <i class="material-icons prefix">attachment</i>
            {!! Form::file('input_toefl', array('id' => 'input_toefl')) !!}
            {!! Form::label('input_toefl', 'Nilai TOEFL :' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
			{!! Form::label('input_toefl', "(.jpg / .png / .pdf) (max: 1MB)" , array('class' => 'active', 'style' => 'font-size: .8rem;top: 1rem; color: rgba("0, 0, 0, .5")', 'required' => 'required')); !!}

            <div style="margin-top: .5rem">
                <a class="grey-dark" target="_blank" href="{{ URL::route('proposal.file', ['nilai_toefl']) }}" style=" margin-left: 3rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                    <span class="font_button" style="text-transform: none">Lihat</span>
                </a>

                <a class="green" target="_blank" href="{{ URL::route('proposal.file.download', ['nilai_toefl']) }}" style=" margin-left: .5rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                    <span class="font_button" style="text-transform: none">Download</span>
                </a>
            </div>

        </div>
        <div class="col s12 mr-top2" style="text-align: right; border-top: 1px solid rgba(0, 0, 0, .1); padding-top: 1rem" >
            <ul>
                <li class="btn-hov">
                    {!! Form::button("<i class='material-icons left'' style='line-height: inherit; margin-right: 5px; font-size: 1.3em'>save</i><span class='font_button'>Simpan Perubahan</span>", array('class' => 'btn bayangan_2dp blue','type' => 'submit')) !!}
                </li>
            </ul>
        </div>
    </div>
{!! Form::close() !!}
@include('scripts.pendaftaran-proposal-edit-script')
</div>
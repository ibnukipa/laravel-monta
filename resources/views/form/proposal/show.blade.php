<div class="panel lime-transparent">
{!! Form::open(array('url' => url('proposal/'. Auth::user()->id), 'method' => 'PATCH', 'class' => ' form-blue lockscreen-credentials form-horizontal', 'role' => 'form', 'files'=>true)) !!}
    <div class="row no-margin">
        <div class="input-field col s6">
            <i class="material-icons prefix active">account_box</i>
            {!! Form::text('nama_lengkap', Auth::user()->full_name, array('id' => 'nama_lengkap', 'class' => '', 'readonly' => 'readonly')) !!}
            {!! Form::label('nama_lengkap', 'Nama Lengkap' , array('class' => 'active')); !!}
        </div>

        <div class="input-field col s3">
            <i class="material-icons prefix active">chrome_reader_mode</i>
            {!! Form::text('username', Auth::user()->username, array('id' => 'username', 'class' => '', 'readonly' => 'readonly')) !!}
            {!! Form::label('username', 'NRP' , array('class' => 'active')); !!}
        </div>

        <div class="input-field col s3">
            <i class="material-icons prefix">local_library</i>
            {!! Form::number('nilai_toefl', Auth::user()->proposal->nilai_toefl, array('id' => 'nilai_toefl', 'class' => 'validate', 'required' => 'required')) !!}
            {!! Form::label('nilai_toefl', 'Nilai TOEFL' , array()); !!}
        </div>

        <div class="input-field col s12 mr-top2">
            <i class="material-icons prefix">import_contacts</i>
            {!! Form::textarea('usulan_judul', Auth::user()->proposal->usulan_judul, array('id' => 'usulan_judul', 'class' => 'materialize-textarea', 'readonly' => 'readonly')) !!}
            {!! Form::label('usulan_judul', 'Peminatan/Topik/Judul TA' , array('class' => 'active')); !!}
        </div>
        
        <div class="input-field col s6 mr-top2">
            <i class="material-icons prefix active">person</i>
            {!! Form::text('dosen_pembimbing', Auth::user()->tugas_akhir->dosen_pembimbing->full_name, array('id' => 'dosen_pembimbing', 'class' => '', 'readonly' => 'readonly')) !!}
            {!! Form::label('dosen_pembimbing', 'Ususulan Dosen Pembimbing terpilih: ' , array('class' => 'active')); !!}
        </div>

        <div class="input-field col s6 mr-top2">
			<i class="material-icons prefix" style="margin-top: -0.5rem">donut_large</i>
			{!! Form::text('lab', Auth::user()->tugas_akhir->lab_ta->description, array('id' => 'lab', 'class' => '','style' => 'font-size: 1rem', 'readonly' => 'readonly')) !!}
            {!! Form::label('lab', 'Lab :' , array('class' => 'active', 'style' => '')); !!}
		</div>

        <div class="input-field col s12 mr-top2">
            <i class="material-icons prefix active">class</i>
            {!! Form::text('yang_merupakan', Auth::user()->proposal->kategori_proposal->description, array('style' => 'font-size: 1.3rem', 'id' => 'yang_merupakan', 'class' => '', 'readonly' => 'readonly')) !!}
            {!! Form::label('yang_merupakan', 'Yang Merupakan: ' , array('class' => 'active')); !!}
        </div>

        <div class="input-field col s6" style="margin-bottom: 10px; margin-top: 2.5rem;">
            <i class="material-icons prefix">attachment</i>
            {!! Form::file('input_toefl', array('id' => 'input_toefl')) !!}
            {!! Form::label('input_toefl', 'Nilai TOEFL :' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
			{!! Form::label('input_toefl', "(.jpg / .png / .pdf) (max: 1MB)" , array('class' => 'active', 'style' => 'font-size: .8rem;top: 1rem; color: rgba("0, 0, 0, .5")', 'required' => 'required')); !!}

            {{-- <a target="_blank" href="{{ URL::route('proposal.file', ['nilai_toefl']) }}" style="height: 20px;padding: 0 6px;line-height: 20px; margin-left: 3rem" class="btn bayangan_2dp lime">
                <i class="material-icons left" style="line-height: inherit; margin-right: 5px; font-size: 1rem">remove_red_eye</i>
                <span style="font-size: .9rem; text-transform: capitalize">Lihat</span>
            </a>

            <a target="_blank" href="{{ URL::route('proposal.file.download', ['nilai_toefl']) }}" style="height: 20px; padding: 0 6px;line-height: 20px;" class="btn bayangan_2dp lime">
                <i class="material-icons left" style="line-height: inherit; margin-right: 5px; font-size: 1rem">file_download</i>
                <span style="font-size: .9rem; text-transform: capitalize">Unduh</span>
            </a> --}}
            <div style="margin-top: .5rem">
                <a class="grey-dark" target="_blank" href="{{ URL::route('proposal.file', ['nilai_toefl']) }}" style=" margin-left: 3rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                    <span class="font_button" style="text-transform: none">Lihat</span>
                </a>

                <a class="green" target="_blank" href="{{ URL::route('proposal.file.download', ['nilai_toefl']) }}" style=" margin-left: .5rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                    <span class="font_button" style="text-transform: none">Download</span>
                </a>
            </div>

        </div>
        
        <div class="col s12 mr-top2" style="text-align: right; border-top: 1px solid rgba(0, 0, 0, .1); padding-top: 1rem" >
            <ul>
                <li class="btn-hov">
                    {!! Form::button("<i class='material-icons left'' style='line-height: inherit; margin-right: 5px; font-size: 1.3em'>save</i><span class='font_button'>Simpan Perubahan</span>", array('class' => 'btn bayangan_2dp blue','type' => 'submit')) !!}
                </li>
            </ul>
        </div>
    </div>
{!! Form::close() !!}
@include('scripts.pendaftaran-proposal-edit-script')
</div>
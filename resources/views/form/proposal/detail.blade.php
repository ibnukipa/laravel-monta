{!! Form::open(array('id' => 'formDetailPendaftarProposal', 'url' => '#', 'method' => 'POST', 'class' => ' form-blue lockscreen-credentials form-horizontal', 'role' => 'form', 'files'=>true)) !!}
    <div class="row no-margin">
        <div class="input-field col s6">
            <i class="material-icons prefix active">account_box</i>
            {!! Form::text('nama_lengkap', $user['nama_lengkap'], array('id' => 'nama_lengkap', 'class' => '', 'readonly' => 'readonly')) !!}
            {!! Form::label('nama_lengkap', 'Nama Lengkap' , array('class' => 'active')); !!}
        </div>
        <div class="input-field col s3">
            <i class="material-icons prefix active">chrome_reader_mode</i>
            {!! Form::text('username', $user['username'], array('id' => 'username', 'class' => '', 'readonly' => 'readonly')) !!}
            {!! Form::label('username', 'NRP' , array('class' => 'active')); !!}
        </div>
        <div class="input-field col s3">
            <i class="material-icons prefix active">local_library</i>
            {!! Form::text('nilai_toefl', $user['nilai_toefl'], array('id' => 'nilai_toefl', 'class' => '', 'required' => 'required', 'readonly' => 'readonly')) !!}
            {!! Form::label('nilai_toefl', 'Nilai TOEFL' , array('class' => 'active')); !!}
        </div>

        <div class="input-field col s12 mr-top3">
            <i class="material-icons prefix active">import_contacts</i>
            {!! Form::textarea('usulan_judul', $user['usulan_judul'], array('style' => 'font-size: 1.3rem', 'id' => 'usulan_judul', 'class' => 'materialize-textarea', 'required' => 'required', 'readonly' => 'readonly')) !!}
            {!! Form::label('usulan_judul', 'Peminatan/Topik/Judul TA' , array('class' => 'active')); !!}
        </div>
        
        <div class="input-field col s6 mr-top3 active">
            <i class="material-icons prefix active" style="margin-top: -0.5rem">person</i>
            {!! Form::select('usulan_dosen', array(), null, array('id' => 'usulan_dosen', 'style' => 'width: 90%; font-size: 1.3rem', 'required' => 'required')) !!}
            {!! Form::label('usulan_dosen', 'Mengusulkan dosen:  ' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
        </div>
        
        <div class="input-field col s6 mr-top3 active">
            <i class="material-icons prefix active" style="margin-top: -0.5rem">donut_large</i>
            {!! Form::select('usulan_lab', array(), null, array('id' => 'usulan_lab', 'style' => 'width: 90%; font-size: 1.3rem', 'required' => 'required')) !!}
            {!! Form::label('usulan_lab', 'Mengusulkan Lab:  ' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
        </div>

        <div class="input-field col s12 mr-top3">
            <i class="material-icons prefix active">class</i>
            {!! Form::text('kategori_proposal', $user['kategori_proposal'], array('style' => 'font-weight: 400','id' => 'kategori_proposal', 'class' => '', 'required' => 'required', 'readonly' => 'readonly')) !!}
            {!! Form::label('kategori_proposal', 'Yang merupakan: ' , array('class' => 'active')); !!}
        </div>
        
        <div class="input-field col s4" style="margin-bottom: 10px; margin-top: 2.5rem;">
            <i class="material-icons prefix" style="margin-top: -1rem">attachment</i>
            {!! Form::label('', 'File Nilai TOEFL' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
            
            <a class="grey-dark" target="_blank" href="file/pendaftar/nilai_toefl/{{ $proposal->id }}" style=" margin-left: 3rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                <span class="font_button" style="text-transform: none">Lihat</span>
            </a>

            <a class="green" target="_blank" href="file/pendaftar/nilai_toefl/{{ $proposal->id }}/download" style=" margin-left: .5rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
                <span class="font_button" style="text-transform: none">Download</span>
            </a>
        </div>

        <div class="input-field col s12 mr-top3">
            <i class="material-icons prefix">speaker_notes</i>
            {!! Form::textarea('catatan', null, array('placeholder' => 'Tulis catatan/alasan mengenai terima/tolak yang dilakukan', 'style' => 'font-size: 1.3rem', 'id' => 'catatan', 'class' => 'materialize-textarea', 'required' => 'required')) !!}
            {!! Form::label('catatan', 'Catatan: ' , array('class' => 'active')); !!}
        </div>

        <div class="col s12 mr-top2" style="text-align: right; border-top: 1px solid rgba(0, 0, 0, .1); padding-top: 1rem" >
            <ul>
                <li class="btn-hov">
                    {{-- {!! Form::button("<i class='material-icons left'' style='line-height: inherit; margin-right: 5px; font-size: 1.3em'>done_all</i><span class='font_button'>Terima</span>", array('class' => 'btn bayangan_2dp green', 'type' => 'submit')) !!} --}}
                    <a onclick="submitForm('pendaftar/{{ $proposal->id }}/terima')" class="btn bayangan_2dp green">
                        <i class="material-icons left" style="line-height: inherit; margin-right: 5px; font-size: 1.3em">done_all</i>
                        <span class="font_button">Terima</span>
                    </a>
                </li>
                <li class="btn-hov">
                    <a onclick="submitForm('pendaftar/{{ $proposal->id }}/tolak')" class="btn bayangan_2dp red">
                        <i class="material-icons left" style="line-height: inherit; margin-right: 5px; font-size: 1.3em">do_not_disturb</i>
                        <span class="font_button">Tolak</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
{!! Form::close() !!}
<script>
    function submitForm(action)
    {
        document.getElementById('formDetailPendaftarProposal').action = action;
        document.getElementById('formDetailPendaftarProposal').submit();
    }
    var dataDosen = [
        {
            id: '1',
            text: 'Dosen Usulan',
            children: [
                @foreach ($usulandosen as $value)
                    { id: '{{ $value->id }}', text: '{{ $value->full_name }}' },
                @endforeach
            ]
        },
        {
            id: '1',
            text: 'Dosen Lain',
            children: [
                @foreach ($dosenelse as $value)
                    { id: '{{ $value->id }}', text: '{{ $value->full_name }}' },
                @endforeach
            ]
        },
    ];

    var dataLab = [
        {
            id: '1',
            text: 'Lab Usulan',
            children: [
                @foreach ($usulanlab as $value)
                    { id: '{{ $value->id }}', text: '{{ $value->description }}' },
                @endforeach
            ]
        },
        {
            id: '1',
            text: 'Lab Lain',
            children: [
                @foreach ($labelse as $value)
                    { id: '{{ $value->id }}', text: '{{ $value->description }}' },
                @endforeach
            ]
        },
    ]

    $('#' + 'usulan_dosen').select2({
        placeholder: 'Pilih dosen..',
        data: dataDosen
    });

    $('#' + 'usulan_lab').select2({
        placeholder: 'Pilih lab..',
        data: dataLab
    });
</script>
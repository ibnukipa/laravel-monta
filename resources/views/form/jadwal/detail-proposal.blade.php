{{-- {{dd($user[0]->tugas_akhir['jadwal'][0]->dosen_penguji[0]->dosen->full_name)}} --}}
<div class="form-blue">
    <div class="row no-margin">
        <div class="input-field col s6">
            <i class="material-icons prefix active">account_box</i>
            {!! Form::text('nama_lengkap', $user[0]->full_name, array('id' => 'nama_lengkap', 'class' => '', 'readonly' => 'readonly')) !!}
            {!! Form::label('nama_lengkap', 'Nama Lengkap' , array('class' => 'active')); !!}
        </div>

        <div class="input-field col s4">
            <i class="material-icons prefix active">account_box</i>
            {!! Form::text('nrp', $user[0]->username, array('id' => 'nrp', 'class' => '', 'readonly' => 'readonly')) !!}
            {!! Form::label('nrp', 'NRP' , array('class' => 'active')); !!}
        </div>

        <div class="input-field col s12 mr-top2">
            <i class="material-icons prefix active">import_contacts</i>
            <div style="line-height: 2rem; margin-left: 3rem; margin-top: 1rem; min-height: 100px; font-size: 1.5rem; border-bottom: 1px dotted rgba(25, 118, 210, 1)">
                {!! $user[0]->tugas_akhir['judul'] !!}
            </div>
            {!! Form::label('judul_ta', 'Judul Tugas Akhir' , array('class' => 'active')); !!}
        </div>

        <div class="input-field col s6 mr-top2">
            <i class="material-icons prefix active">person</i>
            {!! Form::text('dosen_pembimbing', $user[0]->tugas_akhir['dosen_pembimbing']->full_name, array('style' => 'font-size: 1.3rem; font-weight: 400','id' => 'kategori_proposal', 'class' => '', 'required' => 'required', 'readonly' => 'readonly')) !!}
            {!! Form::label('dosen_pembimbing', 'Dosen Pembimbing: ' , array('class' => 'active')); !!}
        </div>

        <?php $waktu = new Waktu ?>
        <div class="input-field col s6 mr-top2">
            <i class="material-icons prefix active">date_range</i>
            {!! Form::text('waktu_seminar',
                $waktu->hari[date('N', strtotime($jadwal->tanggal))] .
                date(', d M Y', strtotime($jadwal->tanggal)) ." (". date('H:i', strtotime($jadwal->jadwal_jam['mulai'])) ." - ".date('H:i', strtotime($jadwal->jadwal_jam['selesai'])) .")"
            , array('style' => 'font-size: 1.3rem; font-weight: 400','id' => 'waktu_seminar', 'class' => '', 'required' => 'required', 'readonly' => 'readonly')) !!}
            {!! Form::label('waktu_seminar', 'Waktu Seminar : ' , array('class' => 'active')); !!}
        </div>

        <div class="input-field col s6 mr-top2">
            <i class="material-icons prefix active">person</i>
            {!! Form::text('dosen_penguji1', $jadwal->dosen_penguji[0]->dosen->full_name, array('style' => 'font-size: 1.3rem; font-weight: 400','id' => 'dosen_penguji1', 'class' => '', 'required' => 'required', 'readonly' => 'readonly')) !!}
            {!! Form::label('dosen_penguji1', 'Dosen Penguji 1: ' , array('class' => 'active')); !!}
        </div>

        <div class="input-field col s6 mr-top2">
            <i class="material-icons prefix active">person</i>
            {!! Form::text('dosen_penguji2', $jadwal->dosen_penguji[1]->dosen->full_name, array('style' => 'font-size: 1.3rem; font-weight: 400','id' => 'dosen_penguji2', 'class' => '', 'required' => 'required', 'readonly' => 'readonly')) !!}
            {!! Form::label('dosen_penguji2', 'Dosen Penguji 2: ' , array('class' => 'active')); !!}
        </div>

        <div class="input-field col s6 mr-top2">
            <i class="material-icons prefix active">person</i>
            {!! Form::text('dosen_penguji3', $jadwal->dosen_penguji[2]->dosen->full_name, array('style' => 'font-size: 1.3rem; font-weight: 400','id' => 'dosen_penguji3', 'class' => '', 'required' => 'required', 'readonly' => 'readonly')) !!}
            {!! Form::label('dosen_penguji3', 'Dosen Penguji 3: ' , array('class' => 'active')); !!}
        </div>

        <div class="input-field col s6 mr-top2">
            <i class="material-icons prefix active">donut_small</i>
            {!! Form::text('dosen_penguji3', $jadwal->jadwal_kategori->description, array('style' => 'font-size: 1.3rem; font-weight: 400','id' => 'dosen_penguji3', 'class' => '', 'required' => 'required', 'readonly' => 'readonly')) !!}
            {!! Form::label('dosen_penguji3', 'Tipe Seminar: ' , array('class' => 'active')); !!}
        </div>
    </div>
</div>
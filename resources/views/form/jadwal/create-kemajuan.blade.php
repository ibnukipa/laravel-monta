<h4 class="head-form">
	Tambah Jadwal Seminar Kemajuan TA
</h4>

<h6 class="center" style="margin-bottom: 20px; color: {{Auth::user()->warna['utama']}}; opacity: .5">Teknik Lingkungan ITS</h6>
<div class="divider"></div>

{!! Form::open(array('url' => route('jadwal.store', ['seminar-kemajuan']), 'method' => 'POST', 'class' => ' form-blue lockscreen-credentials form-horizontal', 'role' => 'form', 'files'=>true)) !!}
	<div class="row no-margin">
        <div class="col s12" style="margin-bottom: 1rem">
			@include('partials.form-status')
        </div>
		
        <div class="input-field col s6" style="margin-top: 1.4rem">	
			<i class="material-icons prefix" style="margin-top: -0.5rem">person</i>
			{!! Form::select('mahasiswa', $mahasiswaList , old('mahasiswa'), array('placeholder' =>'', 'class' =>'js-example-disabled-results', 'id' => 'mahasiswa', 'style' => 'width: 90%', 'required' => 'required')) !!}
            {!! Form::label('mahasiswa', 'NRP Mahasiswa' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
		</div>

        <div class="input-field col s6 mr-top1">
			<i class="material-icons prefix active">account_box</i>
            {!! Form::text('dosen_pembimbing', null, array('id' => 'dosen_pembimbing', 'class' => '', 'disabled' => 'disabled')) !!}
   			{!! Form::label('dosen_pembimbing', 'Dosen Pembimbing: ' , array('class' => 'active')); !!}
		</div>

        <div class="input-field col s12 mr-top2">
            <i class="material-icons prefix">import_contacts</i>
            <div id="judul_ta" style="line-height: 2rem; margin-left: 3rem; margin-top: 1rem; min-height: 100px; font-size: 1.5rem; border-bottom: 1px dotted rgba(25, 118, 210, 1)">
                
            </div>
            {!! Form::label('judul_ta', 'Judul Tugas Akhir' , array('class' => 'active')); !!}
        </div>

        <div class="input-field col s4 mr-top3">	
            <i class="material-icons prefix" style="margin-top: -0.5rem">date_range</i>
            {!! Form::input('date', 'tanggal_seminar', null, ['id' =>'tanggal_seminar', 'class' => 'datepicker', 'placeholder' => 'Pilih tanggal...', 'style' => 'font-size: 1.3rem']); !!}
            {!! Form::label('tanggal_seminar', 'Tanggal Seminar Kemajuan TA' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
        </div>

        <div class="input-field col s4 mr-top3">
            <i class="material-icons prefix" style="margin-top: -0.5rem">access_time</i>
            {!! Form::select('jam_seminar', $jadwalJam , null, array('placeholder' => 'Pilih jam...', 'id' => 'jam_seminar', 'style' => 'width: 90%; font-size: 1.3rem', 'required' => 'required')) !!}
            {!! Form::label('jam_seminar', 'Jam Seminar Kemajuan TA: ' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
        </div>

        <div class="input-field col s4 mr-top3">
            <i class="material-icons prefix" style="margin-top: -0.5rem">room</i>
            {!! Form::select('ruang_seminar', $jadwalRuang , null, array('placeholder' => 'Pilih ruangan...', 'id' => 'ruang_seminar', 'style' => '', 'required' => 'required')) !!}
            {!! Form::label('ruang_seminar', 'Ruangan Seminar Kemajuan TA: ' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
        </div>

        <div class="input-field col s6 mr-top3">	
			<i class="material-icons prefix" style="margin-top: -0.5rem">person</i>
			{!! Form::select('dosen_penguji1', array() , old('dosen_penguji1'), array('placeholder' => '', 'class' =>'js-example-disabled-results', 'id' => 'dosen_penguji1', 'style' => 'width: 90%', 'required' => 'required')) !!}
            {!! Form::label('dosen_penguji1', 'Dosen Penguji 1: ' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
		</div>

        <div class="input-field col s6 mr-top3">	
			<i class="material-icons prefix" style="margin-top: -0.5rem">person</i>
			{!! Form::select('dosen_penguji2', array() , old('dosen_penguji2'), array('placeholder' => '', 'class' =>'js-example-disabled-results', 'id' => 'dosen_penguji2', 'style' => 'width: 90%', 'required' => 'required')) !!}
            {!! Form::label('dosen_penguji2', 'Dosen Penguji 2: ' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
		</div>

        <div class="input-field col s6 mr-top3">	
			<i class="material-icons prefix" style="margin-top: -0.5rem">person</i>
			{!! Form::select('dosen_penguji3', array() , old('dosen_penguji3'), array('placeholder' => '', 'class' =>'js-example-disabled-results', 'id' => 'dosen_penguji3', 'style' => 'width: 90%', 'required' => 'required')) !!}
            {!! Form::label('dosen_penguji3', 'Dosen Penguji 3: ' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
		</div>
		
         <div class="col s12 mr-top2" style="text-align: right; border-top: 1px solid rgba(0, 0, 0, .1); padding-top: 1rem" >
            <ul>
                <li class="btn-hov">
                    {!! Form::button("<i class='material-icons left'' style='line-height: inherit; margin-right: 5px; font-size: 1.3em'>save</i><span class='font_button'>Simpan</span>", array('class' => 'btn bayangan_2dp green','type' => 'submit')) !!}
                </li>
            </ul>
        </div>

	</div>
{!! Form::close() !!}

<script type="text/javascript">
	
    $(document).ready(function() {
		$('#mahasiswa').select2({
			placeholder: 'Pilih mahasiswa..'
		});

        $('#dosen_penguji1').select2({
            placeholder: 'Pilih Dosen Penguji 1..'
        });

        $('#dosen_penguji2').select2({
            placeholder: 'Pilih Dosen Penguji 2..'
        });

        $('#dosen_penguji3').select2({
            placeholder: 'Pilih Dosen Penguji 3..'
        });

        $('#ruang_seminar').material_select();
        $('#jam_seminar').material_select();

        $('.datepicker').pickadate({
            selectMonths: true,
            selectYears: 15,
            format: 'dd-mm-yyyy'
        });

        var mahasiswa       = $('#mahasiswa');     
        var dos_bing        = $('#dosen_pembimbing');
        var judul_ta        = $('#judul_ta')
        var tanggalSeminar  = $('#tanggal_seminar');
        var jamSeminar      = $('#jam_seminar');
        var ruangSeminar    = $('#ruang_seminar');
        var dp1             = $('#dosen_penguji1');
        var dp2             = $('#dosen_penguji2');
        var dp3             = $('#dosen_penguji3');

        tanggalSeminar.parent().hide();
        tanggalSeminar.val(null);

        jamSeminar.parent().parent().hide();
        jamSeminar.val(null);

        ruangSeminar.parent().parent().hide();
        ruangSeminar.val(null);

        dp1.parent().hide();
        dp1.val(null);

        dp2.parent().hide();
        dp2.val(null);

        dp3.parent().hide();
        dp3.val(null);

        mahasiswa.on('change', function() {
            var url 	= '{{ route('ajax.getDosenPembimbing') }}';
            var data    = {
                                user_id     : $(this).val()
                          };
            $.ajax({
                url         : url,
                type        : 'POST',
                data        : data,
                datatype    : 'JSON',
                success     : function(data) {
                    dos_bing.val(data.dosenPembimbing['full_name']);
                    judul_ta.html(data.tugas_akhir['judul']);
                    tanggalSeminar.parent().show();
                    ruangSeminar.parent().parent().show();                    
                    jamSeminar.parent().parent().show();
                    dp2.parent().show();
                    dp1.parent().show();
                    dp3.parent().show();
                }
            });
        });

        tanggalSeminar.on('change', function() {
            if(tanggalSeminar.val() != '' && ruangSeminar.val() > 0 && jamSeminar.val() > 0) {
                getDosenPenguji();
            }
        });

        ruangSeminar.on('change', function() {
            if(tanggalSeminar.val() != '' && ruangSeminar.val() > 0 && jamSeminar.val() > 0) {
                getDosenPenguji();
            }
        });

        jamSeminar.on('change', function() {
            if(tanggalSeminar.val() != '' && ruangSeminar.val() > 0 && jamSeminar.val() > 0) {
                getDosenPenguji();
            }
        });

        function getRuangan() {
            var url     = '{{ route('ajax.getRuangan') }}';
            var data    = {
                    tanggal     : tanggalSeminar.val(),
            };
            
            $.ajax({
                url         : url,
                type        : 'POST',
                data        : data,
                datatype    : 'JSON',
                success     : function(data) {
                    $('#judul_ta').html(data);
                    {{-- ruangSeminar.empty();
                    ruangSeminar.parent().parent().show();

                    $.each(data, function(key, data){
                        console.log(data.name);
                        ruangSeminar.append('<option value="'+ data.id +'">'+ data.name +'</option>');
                    });
                    
                    ruangSeminar.material_select(); --}}
                }
            });

        }

        function getDosenPenguji() {
            var url    = '{{ route('ajax.getDosenPenguji') }}';
            var data   = {
                    tanggal     : tanggalSeminar.val(),
                    ruang       : ruangSeminar.val(),
                    jam         : jamSeminar.val(),
                    kategori    : 1
            };
            $.ajax({
                url         : url,
                type        : 'POST',
                data        : data,
                datatype    : 'JSON',
                success     : function(data) {

                    $('#dosen_penguji1').empty();
                    $('#dosen_penguji2').empty();
                    $('#dosen_penguji3').empty();

                    $.each(data, function(){
                        
                        $('#dosen_penguji1').append('<option value="'+ this.id +'">'+ this.full_name +'</option>');
                        $('#dosen_penguji2').append('<option value="'+ this.id +'">'+ this.full_name +'</option>');
                        $('#dosen_penguji3').append('<option value="'+ this.id +'">'+ this.full_name +'</option>');
                    });

                    $('#dosen_penguji1').change();
                    $('#dosen_penguji2').change();
                    $('#dosen_penguji3').change();

                    
                }
            });

            $('#dosen_penguji1').select2({
                placeholder: 'Pilih Dosen Penguji 1..'
            });

            $('#dosen_penguji2').select2({
                placeholder: 'Pilih Dosen Penguji 2..'
            });

            $('#dosen_penguji3').select2({
                placeholder: 'Pilih Dosen Penguji 2..'
            });
            
            dp2.parent().show();
            dp1.parent().show();
            dp3.parent().show();
        }

        $("select").change(function() {
			var selector = this.id;
			var tujuan;
            var tujuan2;
			if(selector === 'dosen_penguji1') {
				var tujuan  = 'dosen_penguji2'; 
                var tujuan2 = 'dosen_penguji3';
			} else if(selector === 'dosen_penguji2') {
				var tujuan = 'dosen_penguji1';
                var tujuan2 = 'dosen_penguji3';
			} else if(selector === 'dosen_penguji3') {
                var tujuan = 'dosen_penguji1';
                var tujuan2 = 'dosen_penguji2';
            } else {
				return false;
			}
            var val1 = this.value
            var val2 = $('#'+tujuan).val();
            var val3 = $('#'+tujuan2).val();
            
			$('#' + tujuan).find('option').each(function() {
				$(this).prop('disabled', false);
				if(this.value == val1)
					$(this).prop('disabled', true);
                if(this.value == val3)
					$(this).prop('disabled', true);
			});

			$('#' + tujuan2).find('option').each(function() {
				$(this).prop('disabled', false);
				if(this.value == val1)
					$(this).prop('disabled', true);
                if(this.value == val2)
					$(this).prop('disabled', true);
			});

			$('#' + tujuan).select2();
            $('#' + tujuan2).select2();
		});

        
    });
	

</script>

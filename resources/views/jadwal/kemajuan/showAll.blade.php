
@extends('app')

@section('template_title')
	Jadwal Seminar Kemajuan TA
@endsection

@section('content')
<div class="wadah" style="">
	<div class="row">
		<div class="col s3" style="width: 19%; padding: 0; position: fixed; left: 0; z-index: 9">
			<div class="card" style="box-shadow: none; margin: 0">
				<section id="menu-user">
					@if(Auth::user()->hasRole('mahasiswa'))
						@include('partials.menu-mahasiswa')
					@elseif(Auth::user()->hasRole('koordinator') || Auth::user()->hasRole('kaprodi'))
						@include('partials.menu-koordinator')
					@elseif(Auth::user()->hasRole('operator'))
						@include('partials.menu-operator')
					@endif
				</section>
			</div>
		</div>
		<section id="content-container">
			<div class="col s10 right" style="width: 81%; padding: 0;">
				<div class="card bayangan_2dp" style="margin: 0">
			        <div class="status-content" style="background: #364150 ; color: white; font-weight: 500">
					{!! Breadcrumbs::render('pendaftaran-seminar-kemajuan-jadwal') !!}
			        </div>
			        <div class="progress" style="margin: 0; background-color: #B3E5FC;">
					    <div class="indeterminate" style="background-color: #0288D1"></div>
					</div>
					<section id="content" style="border-bottom: 3px solid {{Auth::user()->warna['utama']}} ; padding-top: 20px; color: red ?>">
						<h4 class="head-form">
							Jadwal Seminar Kemajuan TA
						</h4>

						<h6 class="center" style="margin-bottom: 20px; color: {{Auth::user()->warna['utama']}}; opacity: .5">Teknik Lingkungan ITS</h6>
						<div class="divider"></div>

						<div class="row">
							<div class="col s12">
								@include('partials.form-status')
							</div>
                            <div class="col s12 mr-top1" style="text-align: left;">
								<ul>
									<li class="btn-hov">
										<a data-url="{{ route('jadwal.create', ['seminar-kemajuan']) }}" data-bread="{{Breadcrumbs::render('pendaftaran-seminar-kemajuan-jadwal-create')}}" onclick="load_content(this)" style="padding: 0 15px;" class="btn bayangan_2dp green">
											<i class="material-icons left" style="line-height: inherit; margin-right: 5px; font-size: 1.3em">add</i>
											<span class="font_button">Jadwal Online</span>
										</a>
									</li>
									<li class="btn-hov">
										<a data-url="{{ route('jadwal.file', ['seminar-kemajuan']) }}" data-bread="{{Breadcrumbs::render('pendaftaran-seminar-kemajuan-file-jadwal')}}" onclick="load_content(this)" style="padding: 0 15px;" class="btn bayangan_2dp blue">
											<i class="material-icons left" style="line-height: inherit; margin-right: 5px; font-size: 1.3em">collections</i>
											<span class="font_button">File Jadwal</span>
										</a>
									</li>
									<li class="btn-hov pull-right">
										<a target="_blank" href="{{ route('berkas.jadwal.mahasiswa', ['seminar-kemajuan']) }}" style="padding: 0 15px;" class="btn bayangan_2dp grey-dark">
											<i class="material-icons left" style="line-height: inherit; margin-right: 5px; font-size: 1.3em">print</i>
											<span class="font_button">Daftar Mahasiswa</span>
										</a>
									</li>
									<li class="btn-hov pull-right mr-right1">
										<a target="_blank" href="{{ route('berkas.jadwal.mahasiswa', ['seminar-kemajuan-down']) }}" style="padding: 0 15px;" class="btn bayangan_2dp grey-dark">
											<i class="material-icons left" style="line-height: inherit; margin-right: 5px; font-size: 1.3em">file_download</i>
											<span class="font_button">Daftar Mahasiswa</span>
										</a>
									</li>
								</ul>
							</div>
							<div class="col s12" style="padding: 1rem">
                                <table id="users_table" class="highlight">
                                    <thead>
                                        <tr class="grey-light">
											<th>Waktu Seminar</th>
                                            <th>Ruangan</th>
                                            <th>NRP</th>
											<th class="center">File</th>
                                            <th class="center">Tindakan</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
											<th>Waktu Seminar</th>
                                            <th>Ruangan</th>
                                            <th>NRP</th>
											<th></th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
										
                                        @foreach($users as $value)
                                            <tr>
												<td>{{ date("d/m/Y",strtotime($value->tugas_akhir['jadwal'][0]['tanggal'])) . ", " . date("H:i",strtotime($value->tugas_akhir['jadwal'][0]->jadwal_jam['mulai'])) ." - ".date("H:i",strtotime($value->tugas_akhir['jadwal'][0]->jadwal_jam['selesai'])) }}</td>
                                                <td>{{ $value->tugas_akhir['jadwal'][0]->jadwal_ruang['description'] }}</td>
                                                <td>{{ $value->username }}</td>
												<td class="center">
                                                    <ul class="no-margin">
                                                        <!--<li style="display: inline-block">
															<a target="_blank" href="{{ route('berkas', [$value->id, 'kta02']) }}" style="margin-left: .5rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto" class="grey-dark">
																<span class="font_button" style="text-transform: none">KTA-02</span>
															</a>
                                                        </li>
														<li style="display: inline-block">
															<a target="_blank" href="{{ route('berkas', [$value->id, 'kta03']) }}" style="margin-left: .5rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto" class="grey-dark">
																<span class="font_button" style="text-transform: none">KTA-03</span>
															</a>
                                                        </li>-->
														<li style="display: inline-block">
															<a target="_blank" href="{{ route('filezip.download', ['kemajuan-proposal', $value->username]) }}" style="margin-left: .5rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto" class="grey-dark">
																<span class="font_button" style="text-transform: none">Download (KTA-02 & KTA-03)</span>
															</a>
                                                        </li>													
                                                    </ul>
												</td>
                                                <td class="center">
                                                    <ul class="no-margin">
                                                        <li style="display: inline-block">
															<a onclick="openDetail(this)" data-url="{{ route('jadwal.detail', [ $value->tugas_akhir['jadwal'][0]->id]) }}" data-target="modalDetail" data-judul="Detail Jadwal Seminar Kemajuan TA" style="margin-left: .5rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto" class="green">
																<span class="font_button" style="text-transform: none">Detail</span>
															</a>
                                                        </li>
                                                        <li style="display: inline-block">
															{!! Form::open(array('url' => url('jadwal/' . $value->tugas_akhir['jadwal'][0]->id .'/destroy'))) !!}
																{!! Form::hidden('_method', 'DELETE') !!}
																<a onclick="doDelete(this)" data-target="confirmDelete" data-judul="Apakah Anda yakin akan menghapus Jadawal Seminar Proposal TA" style="margin-left: .5rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto" class="red">
																	<span class="font_button" style="text-transform: none">Hapus</span>
																</a>
															{!! Form::close() !!}
                                                        </li>
														
                                                    </ul>
												</td>
                                            </tr>
											{{-- @endif --}}
                                        @endforeach
                                    </tbody>
                                </table>
                            </div> 
							</div>
						</div>
					</section>
				</div>
			</div>
		</section>
	</div>
</div>
@include('modals.modal-delete')
{{-- @include('modals.modal-tolak') --}}
@include('modals.modal-detail')
@endsection

@section('template_scripts')
	<script src="{{URL::asset('js/dash.js')}}"></script>
    @include('scripts.load-content-script')
	@include('scripts.modal-delete-script')
	{{-- @include('scripts.modal-tolak-script') --}}
	@include('scripts.modal-detail-script')
	
	{{-- Data Table --}}
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
		$(function () {
			$(document).ready(function() {
			    //Setup untuk elemen search HTML
			    $('#users_table tfoot th').each( function () {
			        var title = $(this).text();
			        if(title != '')
                    
			        	$(this).html( '<div id="users_table_filter" class="dataTables_filter"><label><input type="search" class="form-control input-sm" placeholder="Cari '+title+'..." ></label></div>' );
			    } );
			 
			    // DataTable
			    var table = $('#users_table').DataTable({
			    	language: {
			    		searchPlaceholder: "Cari Data...",
                        "zeroRecords": "Tidak ada data Jadwal Seminar Kemajuan TA.",
			    	},
			    	"columnDefs": [
				    	{
				    		"targets"	: [3, 4],
				    		"searchable": false,
				    		'bSortable': false,
				    	}
			    	],
			    	"paging": true,
			    	"autoWidth": false,
					"searching": true
			    });
			 
			    // Menerapkan serach masing-masing kolom
			    table.columns().every( function () {
			        var that = this;
			 
			        $( 'input', this.footer() ).on( 'keyup change', function () {
			            if ( that.search() !== this.value ) {
			                that
			                    .search( this.value )
			                    .draw();
			            }
			        } );
			    } );
			} );
		});
    </script>
@endsection

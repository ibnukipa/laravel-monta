
    <h4 class="head-form">
        File Jadwal Seminar Proposal TA
    </h4>

    <h6 class="center" style="margin-bottom: 20px; color: {{Auth::user()->warna['utama']}}; opacity: .5">Teknik Lingkungan ITS</h6>
    <div class="divider"></div>

    <div class="row">
        <div class="col s12">
            @include('partials.form-status')
        </div>
        @if(Auth::user()->hasRole('koordinator'))
        <div class="col s12 mr-top1" style="text-align: left;">
            <ul>
                <li class="btn-hov">
                    <a onclick="doUpload(this)" data-url="{{route('jadwal.file.upload', ['seminar-proposal'])}}" data-target="modalUpload" data-judul="Upload Jadwal Seminar Proposal TA" style="padding: 0 15px;" class="btn bayangan_2dp green">
                        <i class="material-icons left" style="line-height: inherit; margin-right: 5px; font-size: 1.3em">file_upload</i>
                        <span class="font_button">Upload File</span>
                    </a>
                </li>
            </ul>
        </div>
        @endif
        <div class="col s12" style="padding: 1rem">
            <table id="users_table" class="highlight">
                <thead>
                    <tr class="grey-light">
                        <th>Tanggal Terbuat</th>
                        <th>Nama</th>
                        <th>Tipe Seminar</th>
                        <th>Tahun Ajaran</th>
                        {{-- <th>Semester</th> --}}
                        <th>Tindakan</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Tanggal Terbuat</th>
                        <th>Nama</th>
                        <th>Tipe Seminar</th>
                        <th>Tahun Ajaran</th>
                        {{-- <th>Semester</th> --}}
                        <th></th>
                    </tr>
                </tfoot>
                <tbody>
                    
                    @foreach($fileJadwal as $value)
                        <tr>
                            <td>{{ date('d/m/Y',strtotime($value->created_at)) }}</td>
                            <td>{{ $value->name }}</td>
                            <td>{{ $value->tipe }}</td>
                            <td>{{ $value->tahun_ajaran }} ({{ $value->semester }})</td>
                            <td>
                                <ul class="no-margin">
                                    <li style="display: inline-block">
                                        <a target="_blank" href="{{route('file.download', ['jadwal', $value->id])}}" style="margin-left: .5rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto" class="green">
                                            <span class="font_button" style="text-transform: none">Download</span>
                                        </a>
                                    </li>
                                    @if(Auth::user()->hasRole('koordinator'))
                                    <li style="display: inline-block">
                                        {{-- <a target="_blank" href="{{route('file.download', ['jadwal', $value->id])}}" style="margin-left: .5rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto" class="red">
                                            <span class="font_button" style="text-transform: none">Hapus</span>
                                        </a> --}}
                                        {!! Form::open(array('url' =>route('jadwal.file.upload.destroy', [$value->id]))) !!}
                                            {!! Form::hidden('_method', 'DELETE') !!}
                                            <a onclick="doDelete(this)" data-target="confirmDelete" data-judul="Apakah Anda yakin akan menghapus Jadawal Seminar Proposal TA" style="margin-left: .5rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto" class="red">
                                                <span class="font_button" style="text-transform: none">Hapus</span>
                                            </a>
                                        {!! Form::close() !!}
                                    </li>
                                    @endif
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div> 
        </div>
    </div>
@include('modals.modal-upload')
@include('modals.modal-delete')

	<script src="{{URL::asset('js/dash.js')}}"></script>
    @include('scripts.modal-upload-script')
    @include('scripts.modal-delete-script')
	
	{{-- Data Table --}}
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
		$(function () {
			$(document).ready(function() {
			    //jadwal
			    $('#users_table tfoot th').each( function () {
			        var title = $(this).text();
			        if(title != '')
                    
			        	$(this).html( '<div id="users_table_filter" class="dataTables_filter"><label><input type="search" class="form-control input-sm" placeholder="Cari '+title+'..." ></label></div>' );
			    } );
			 
			    // DataTable
			    var table = $('#users_table').DataTable({
			    	language: {
			    		searchPlaceholder: "Cari Data...",
                        "zeroRecords": "Tidak ada data Jadwal Seminar Proposal TA.",
			    	},
			    	"columnDefs": [
				    	{
				    		"targets"	: [4],
				    		"searchable": false,
				    		'bSortable': false,
				    	}
			    	],
			    	"paging": true,
			    	"autoWidth": false,
                    "searching": true
			    });
			 
			    // Menerapkan serach masing-masing kolom
			    table.columns().every( function () {
			        var that = this;
			 
			        $( 'input', this.footer() ).on( 'keyup change', function () {
			            if ( that.search() !== this.value ) {
			                that
			                    .search( this.value )
			                    .draw();
			            }
			        } );
			    } );
			} );
		});
    </script>
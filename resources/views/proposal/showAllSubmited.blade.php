
@extends('app')

@section('template_title')
	Dashboard {{ Auth::user()->name }}
@endsection

@section('template_linked_css')
    
@endsection

@section('content')
<div class="wadah" style="">
	<div class="row">
		<div class="col s3" style="width: 19%; padding: 0; position: fixed; left: 0; z-index: 9">
			<div class="card" style="box-shadow: none; margin: 0">
				<section id="menu-user">
					@if(Auth::user()->hasRole('mahasiswa'))
						@include('partials.menu-mahasiswa')
					@elseif(Auth::user()->hasRole('koordinator') || Auth::user()->hasRole('kaprodi'))
						@include('partials.menu-koordinator')
					@elseif(Auth::user()->hasRole('operator'))
						@include('partials.menu-operator')
					@elseif(Auth::user()->hasRole('administrator'))
						@include('partials.menu-admin')
					@endif
				</section>
			</div>
		</div>
		<section id="content-container">
			<div class="col s10 right" style="width: 81%; padding: 0;">
				<div class="card bayangan_2dp" style="margin: 0">
			        <div class="status-content" style="background: #364150 ; color: white; font-weight: 500">
					{!! Breadcrumbs::render('pendaftaran-proposal-list') !!}
			        </div>
			        <div class="progress" style="margin: 0; background-color: #B3E5FC;">
					    <div class="indeterminate" style="background-color: #0288D1"></div>
					</div>
					<section id="content" style="border-bottom: 3px solid {{Auth::user()->warna['utama']}} ; padding-top: 20px; color: red ?>">
						<h4 class="head-form">
							List Pendaftar Proposal TA
						</h4>

						<h6 class="center" style="margin-bottom: 20px; color: {{Auth::user()->warna['utama']}}; opacity: .5">Teknik Lingkungan ITS</h6>
						<div class="divider"></div>

						<div class="row">
							<div class="col s12">
								@include('partials.form-status')
							</div>
							<div class="col s12 mr-top1" style="text-align: left;">
								<ul>
									<li class="btn-hov pull-right">
										<a target="_blank" href="{{ route('berkas.jadwal.mahasiswa', ['pendaftar-proposal']) }}" style="padding: 0 15px;" class="btn bayangan_2dp grey-dark">
											<i class="material-icons left" style="line-height: inherit; margin-right: 5px; font-size: 1.3em">file_download</i>
											<span class="font_button">Pendaftar Proposal TA (Excel)</span>
										</a>

										<a target="_blank" href="{{ route('berkas.jadwal.mahasiswa', ['proposal-diterima']) }}" style="padding: 0 15px;" class="btn bayangan_2dp grey-dark">
											<i class="material-icons left" style="line-height: inherit; margin-right: 5px; font-size: 1.3em">file_download</i>
											<span class="font_button">Pendaftar Proposal TA DITERIMA (Excel)</span>
										</a>
									</li>
								</ul>
							</div>
							<div class="col s12" style="padding: 1rem">
                                <table id="users_table" class="highlight">
                                    <thead>
                                        <tr class="grey-light">
											<th>Tanggal Masuk</th>
                                            <th>NRP</th>
                                            <th>Nama Lengkap</th>
                                            <th>Nilai TOEFL</th>
                                            <th>Tindakan</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
											<th>Tanggal Masuk</th>
                                            <th>NRP</th>
                                            <th>Nama Lengkap</th>
                                            <th>Nilai TOEFL</th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        @foreach($allProposal as $key => $value)
                                            <tr>
												<td>{{ date('M d, Y',strtotime($value->updated_at)) }}</td>
                                                <td>{{ $value->username }}</td>
                                                <td>{{ $value->full_name }}</td>
                                                <td>{{ $value->nilai_toefl }}</td>
                                                <td>
                                                    <ul class="no-margin">
                                                        <li style="display: inline-block">
															<a onclick="openDetail(this)" data-url="{{ route('proposal.pendaftar.detail', [$value->id]) }}" data-target="modalDetail" data-judul="Detail Pendaftar Proposal TA" style="margin-left: .5rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto" class="green">
																<span class="font_button" style="text-transform: none">Detail</span>
															</a>
                                                        </li>
                                                        <li style="display: inline-block">
															{{-- <a onclick="doTolak(this)" data-target="confirmTolak" data-pesan="Apakah Anda yakin untuk menolak Pendaftaran ini?" data-url="{{ route('proposal.pendaftar.tolak', [$value->id]) }}" style="margin-left: .5rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto" class="red">
																<span class="font_button" style="text-transform: none">Tolak</span>
															</a> --}}
                                                            {{-- <a onclick="doTolak(this)" data-target="confirmTolak" data-pesan="Apakah Anda yakin untuk menolak Pendaftaran ini?" data-url="{{ route('proposal.pendaftar.tolak', [$value->id]) }}" style="padding: 2px 10px; line-height: 100%; height: auto" class="btn bayangan_2dp red">
                                                                <span class="font_button" style="font-size: .8rem">Tolak</span>
                                                            </a> --}}
                                                        </li>
                                                    </ul>
												</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div> 
							</div>
						</div>
					</section>
				</div>
			</div>
		</section>
	</div>
</div>
@include('modals.modal-tolak')
@include('modals.modal-detail')
@endsection

@section('template_scripts')
	<script src="{{URL::asset('js/dash.js')}}"></script>

	@include('scripts.modal-tolak-script')
	@include('scripts.modal-detail-script')
	
	{{-- Data Table --}}
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
		$(function () {
			$(document).ready(function() {
			    //Setup untuk elemen search HTML
			    $('#users_table tfoot th').each( function () {
			        var title = $(this).text();
			        if(title != '')
                    
			        	$(this).html( '<div id="users_table_filter" class="dataTables_filter"><label><input type="search" class="form-control input-sm" placeholder="Cari '+title+'..." ></label></div>' );
			    } );
			 
			    // DataTable
			    var table = $('#users_table').DataTable({
			    	language: {
			    		searchPlaceholder: "Cari Data...",
                        "zeroRecords": "Tidak ada data Pendaftaran Proposal.",
			    	},
			    	"columnDefs": [
				    	{
				    		"targets"	: [4],
				    		"searchable": false,
				    		'bSortable': false,
				    	}
			    	],
			    	"paging": true,
			    	"autoWidth": false,
					"searching": true
			    });
			 
			    // Menerapkan serach masing-masing kolom
			    table.columns().every( function () {
			        var that = this;
			 
			        $( 'input', this.footer() ).on( 'keyup change', function () {
			            if ( that.search() !== this.value ) {
			                that
			                    .search( this.value )
			                    .draw();
			            }
			        } );
			    } );
			} );
		});
    </script>
@endsection

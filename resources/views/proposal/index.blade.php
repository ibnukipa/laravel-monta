
@extends('app')

@section('template_title')
	Proposal TA {{ Auth::user()->name }}
@endsection

@section('content')
<div class="wadah" style="">
	<div class="row">
		<div class="col s3" style="width: 19%; padding: 0; position: fixed; left: 0; z-index: 9">
			<div class="card" style="box-shadow: none; margin: 0">
				<section id="menu-user">
					@if(Auth::user()->hasRole('mahasiswa'))
						@include('partials.menu-mahasiswa')
					@elseif(Auth::user()->hasRole('koordinator') || Auth::user()->hasRole('kaprodi'))
						@include('partials.menu-koordinator')
					@elseif(Auth::user()->hasRole('operator'))
						@include('partials.menu-operator')
					@elseif(Auth::user()->hasRole('administrator'))
						@include('partials.menu-admin')
					@endif
				</section>
			</div>
		</div>
		<section id="content-container">
			<div class="col s10 right" style="width: 81%; padding: 0;">
				<div class="card bayangan_2dp" style="margin: 0">
			        <div class="status-content" style="background: #364150 ; color: white; font-weight: 500">
					{!! Breadcrumbs::render('pendaftaran-proposal') !!}
			        </div>
			        <div class="progress" style="margin: 0; background-color: #B3E5FC;">
					    <div class="indeterminate" style="background-color: #0288D1"></div>
					</div>
					<section id="content" style="border-bottom: 3px solid {{Auth::user()->warna['utama']}} ; padding-top: 20px; color: red ?>">
						<h4 class="head-form">
							Pendaftaran Proposal TA
						</h4>

						<h6 class="center" style="margin-bottom: 20px; color: {{Auth::user()->warna['utama']}}; opacity: .5">Teknik Lingkungan ITS</h6>
						<div class="divider"></div>

						<div class="row">
							<div class="col s12">
								@include('partials.form-status')
							</div>

							@if(Auth::user()->proposal == null)
							<div class="col s12 mr-top1" style="text-align: left;">
								<ul>									
									<li class="btn-hov">
										<a data-url="{{ route('proposal.create') }}" data-bread="{{Breadcrumbs::render('pendaftaran-proposal-create')}}" onclick="load_content(this)" style="padding: 0 15px;" class="btn bayangan_2dp green">
											<i class="material-icons left" style="line-height: inherit; margin-right: 5px; font-size: 1.3em">note_add</i>
											<span class="font_button">Daftarkan Proposal</span>
										</a>
									</li>
									
								</ul>
							</div>
							
							@elseif((Auth::user()->proposal->status_id == 1 || Auth::user()->proposal->status_id == 3 || Auth::user()->proposal->status_id == 15))
							<div class="col s12 mr-top1" style="text-align: left;">
								<ul>
									<li class="btn-hov">
										<a href="{{ route('proposal.submit') }}" style="padding: 0 15px;" class="btn bayangan_2dp green">
											<i class="material-icons left" style="line-height: inherit; margin-right: 5px; font-size: 1.3em">playlist_add_check</i>
											<span class="font_button">Submit</span>
										</a>
									</li>
									<li class="btn-hov">
										{!! Form::open(array('url' => 'proposal/' . Auth::user()->proposal->id)) !!}
											{!! Form::hidden('_method', 'DELETE') !!}
											{!! Form::button("<i class='material-icons left'>delete</i><span class='font_button'>Hapus</span>", array('data-pesan' => 'Apakah Anda yakin akan menghapus Pendaftaran Proposal Anda?', 'data-target' => 'confirmDelete', 'onclick' => 'doDelete(this)', 'class' => 'btn bayangan_2dp red')) !!}
										{!! Form::close() !!}
									</li>
									<li class="btn-hov">
										<a data-url="{{ route('catatan.mahasiswa', [Auth::user()->id ,'pendaftaran-proposal']) }}" data-bread="{{Breadcrumbs::render('catatan-pendaftaran-proposal')}}" onclick="load_content(this)" style="padding: 0 15px;" class="btn bayangan_2dp grey-dark">
											<i class="material-icons left" style="line-height: inherit; margin-right: 5px; font-size: 1.3em">speaker_notes</i>
											<span class="font_button">Catatan</span>
										</a>
									</li>
								</ul>
							</div>
							<div class="col s12 mr-top1">
								@include('form.proposal.edit')
								@include('modals.modal-delete')
								@include('scripts.modal-delete-script')
							</div>
							
							@elseif(Auth::user()->proposal->status_id >= 5)
							<div class="col s12 mr-top1" style="text-align: left;">
								<ul>
									<li class="btn-hov">
										<a data-url="{{ route('catatan.mahasiswa', [Auth::user()->id ,'pendaftaran-proposal']) }}" data-bread="{{Breadcrumbs::render('catatan-pendaftaran-proposal')}}" onclick="load_content(this)" style="padding: 0 15px;" class="btn bayangan_2dp grey-dark">
											<i class="material-icons left" style="line-height: inherit; margin-right: 5px; font-size: 1.3em">speaker_notes</i>
											<span class="font_button">Catatan </span>
										</a>
									</li>
								</ul>
							</div>
							<div class="col s12 mr-top1">
								@include('form.proposal.show')
							</div>
							@endif
						</div>
					</section>
				</div>
			</div>
		</section>
	</div>
</div>

@endsection

@section('template_scripts')
	<script src="{{URL::asset('js/dash.js')}}"></script>
	@include('scripts.load-content-script')
@endsection

<h4 class="head-form">
	{{ $judul }}
</h4>

<h6 class="center" style="margin-bottom: 20px; color: {{Auth::user()->warna['utama']}}; opacity: .5">Teknik Lingkungan ITS</h6>
<div class="divider"></div>

{!! Form::open(array('url' => 'proposal', 'method' => 'POST', 'class' => ' form-blue lockscreen-credentials form-horizontal', 'role' => 'form', 'files'=>true)) !!}
	<div class="row no-margin">
        <div class="col s12" style="margin-bottom: 1rem">
			@include('partials.form-status')
        </div>
		<div class="input-field col s6">
			<i class="material-icons prefix active">account_box</i>
            {!! Form::text('nama_lengkap', Auth::user()->full_name, array('id' => 'nama_lengkap', 'class' => '', 'disabled' => 'disabled')) !!}
   			{!! Form::label('nama_lengkap', 'Nama Lengkap' , array('class' => 'active')); !!}
		</div>
		<div class="input-field col s3">
			<i class="material-icons prefix active">chrome_reader_mode</i>
			{!! Form::text('username', Auth::user()->username, array('id' => 'username', 'class' => '', 'disabled' => 'disabled')) !!}
   			{!! Form::label('username', 'NRP' , array('class' => 'active')); !!}
		</div>
		<div class="input-field col s3">
			<i class="material-icons prefix">local_library</i>
			{!! Form::number('nilai_toefl', null, array('id' => 'nilai_toefl', 'class' => 'validate', 'required' => 'required')) !!}
   			{!! Form::label('nilai_toefl', 'Nilai TOEFL' , array()); !!}
		</div>

        <div class="input-field col s6 mr-top3">
			<i class="material-icons prefix" style="margin-top: -0.5rem">person</i>
			{!! Form::select('usulan_dosen1', $dosen , old('usulan_dosen1'), array('placeholder' =>'', 'class' =>'js-example-disabled-results', 'id' => 'usulan_dosen1', 'style' => 'width: 90%', 'required' => 'required')) !!}
            {!! Form::label('usulan_dosen1', 'Usulan Calon Dosen Pembimbing 1' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
		</div>

        <div class="input-field col s6 mr-top3">
			<i class="material-icons prefix" style="margin-top: -0.5rem">person</i>
			{!! Form::select('usulan_dosen2', $dosen , old('usulan_dosen2'), array('placeholder' =>'', 'class' =>'js-example-disabled-results', 'id' => 'usulan_dosen2', 'style' => 'width: 90%', 'required' => 'required')) !!}
            {!! Form::label('usulan_dosen2', 'Usulan Calon Dosen Pembimbing 2' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
		</div>

		<div class="input-field col s6 mr-top3">
			<i class="material-icons prefix" style="margin-top: -0.5rem">donut_large</i>
			{!! Form::select('usulan_lab1', $lab , old('usulan_lab1'), array('placeholder' =>'', 'class' =>'js-example-disabled-results', 'id' => 'usulan_lab1', 'style' => 'width: 90%', 'required' => 'required')) !!}
            {!! Form::label('usulan_lab1', 'Lab : ' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
		</div>

        <div class="input-field col s6 mr-top3">
			<i class="material-icons prefix" style="margin-top: -0.5rem">donut_large</i>
			{!! Form::select('usulan_lab2', $lab , old('usulan_lab2'), array('placeholder' =>'', 'class' =>'js-example-disabled-results', 'id' => 'usulan_lab2', 'style' => 'width: 90%', 'required' => 'required')) !!}
            {!! Form::label('usulan_lab2', 'Lab :' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
		</div>

		<div class="input-field col s12 mr-top3">
			<i class="material-icons prefix">import_contacts</i>
			{!! Form::textarea('usulan_judul', null, array('id' => 'usulan_judul', 'class' => 'materialize-textarea validate', 'required' => 'required', 'style' => 'font-size: 1.3rem; line-height: 1.3')) !!}
   			{!! Form::label('usulan_judul', 'Peminatan/Topik/Judul TA' , array()); !!}
		</div>

		<div class="input-field col s12 mr-top3">
			<i class="material-icons prefix" style="margin-top: -0.5rem">class</i>
			{!! Form::select('kategori_proposal', $kategoriProposal , old('kategori_proposal'), array('id' => 'kategori_proposal', 'style' => '', 'required' => 'required')) !!}
            {!! Form::label('kategori_proposal', 'Yang merupakan: ' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
		</div>

		<div class="input-field col s6" style="margin-bottom: 10px; margin-top: 2.5rem;">
			<i class="material-icons prefix">attachment</i>
			{!! Form::file('input_toefl', array('id' => 'input_toefl', 'required' => 'required')) !!}
            {!! Form::label('input_toefl', 'Nilai TOEFL :' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
			{!! Form::label('input_toefl', "(.jpg / .png / .pdf) (max: 1MB)" , array('class' => 'active', 'style' => 'font-size: .8rem;top: 1rem; color: rgba("0, 0, 0, .5")', 'required' => 'required')); !!}			            
			
		</div>
		
         <div class="col s12 mr-top2" style="text-align: right; border-top: 1px solid rgba(0, 0, 0, .1); padding-top: 1rem" >
            <ul>
                <li class="btn-hov">
                    {!! Form::button("<i class='material-icons left'' style='line-height: inherit; margin-right: 5px; font-size: 1.3em'>save</i><span class='font_button'>Simpan</span>", array('class' => 'btn bayangan_2dp green','type' => 'submit')) !!}
                </li>
            </ul>
        </div>

	</div>
{!! Form::close() !!}

<script type="text/javascript">
	
    $(document).ready(function() {
		
		$('#input_toefl').filer({
            changeInput: '<a data-warna="biru" class="jFiler-input-button bayangan_2dp" style="margin-top: 1rem">Cari berkas...</a>',
            limit: 1,
			maxSize: 1,
            extensions: ["jpg", "png", "pdf"],
            showThumbs: true,
            captions : {
                errors: {
                    filesType: "Maaf! file yang Anda pilih tidak didukung.",
                }
            }
        });

        $('#kategori_proposal').material_select();

		$('#' + 'usulan_dosen1').select2({
			placeholder: 'Pilih dosen..'
		});
		
		$('#' + 'usulan_dosen2').select2({
			placeholder: 'Pilih dosen..'
		});

		$('#' + 'usulan_lab1').select2({
			placeholder: 'Pilih Lab..'
		});
		$('#' + 'usulan_lab2').select2({
			placeholder: 'Pilih Lab..'
		});

		$("select").change(function() {
			var selector = this.id;
			var tujuan;
			if(selector === 'usulan_dosen1') {
				var tujuan = 'usulan_dosen2'; 
			} else if(selector === 'usulan_dosen2') {
				var tujuan = 'usulan_dosen1';
			} else {
				return false;
			}
			var vals = this.value;
			$('#' + tujuan).find('option').each(function() {
				$(this).prop('disabled', false);
				if(this.value == vals)
					$(this).prop('disabled', true);
			});
			$('#' + tujuan).select2();
		});
    });
	

</script>

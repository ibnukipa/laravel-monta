<h4 class="head-form" style="text-align: left; padding-left: 2rem">
	Catatan {{ $kategori }}
</h4>

<h6 style="margin-bottom: 20px; padding-left: 2rem; color: {{Auth::user()->warna['utama']}}; opacity: .5">
    {{ Auth::user()->full_name }} - {{Auth::user()->username}}
</h6>
<div class="divider"></div>

<div style="padding: 1rem 2rem">
    <ul class="collection">
        @if($listCatatan->isEmpty())
            <li class="collection-item dismissable">
                <div>
                    <span style="font-size: 1rem; font-weight: 600">Belum ada catatan.</span>
                    <a style="top: 0;margin-left: .5rem; border-radius: 3px; padding: 2px 10px; line-height: 100%; height: auto" class="secondary-content lime">
                        <span class="font_button" style="text-transform: none">nothing</span>
                    </a>
                </div>
            </li>
        @endif
        @foreach ($listCatatan as $key => $value)
        <li class="collection-item dismissable">
            <div>
                [ {{ $value->created_at }} ]
                <br>
                <span style="font-size: 1rem; font-weight: 600">
                    @if($value->description)
                        {{ $value->description }}
                    @else
                        tidak ada catatan
                    @endif
                </span>
                <a style="top: 0;margin-left: .5rem; border-radius: 3px; padding: 2px 10px; line-height: 100%; height: auto" class="secondary-content lime">
                    <span class="font_button" style="text-transform: none">{{ $value->name }}</span>
                </a>
            </div>
        </li>
        @endforeach
    </ul>
</div>
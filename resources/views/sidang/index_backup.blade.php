
@extends('app')

@section('template_title')
	Seminar Proposal TA {{ Auth::user()->name }}
@endsection

@section('content')
<div class="wadah" style="">
	<div class="row">
		<div class="col s3" style="width: 19%; padding: 0; position: fixed; left: 0; z-index: 9">
			<div class="card" style="box-shadow: none; margin: 0">
				<section id="menu-user">
					@include('partials.menu-mahasiswa')
				</section>
			</div>
		</div>
		<section id="content-container">
			<div class="col s10 right" style="width: 81%; padding: 0;">
				<div class="card bayangan_2dp" style="margin: 0">
			        <div class="status-content" style="background: #364150 ; color: white; font-weight: 500">
					{!! Breadcrumbs::render('pengajuan-seminar-proposal') !!}
			        </div>
			        <div class="progress" style="margin: 0; background-color: #B3E5FC;">
					    <div class="indeterminate" style="background-color: #0288D1"></div>
					</div>
					<section id="content" style="border-bottom: 3px solid {{Auth::user()->warna['utama']}} ; padding-top: 20px; color: red ?>">
						<h4 class="head-form">
							Pengajuan Sidang Ujian Lisan Tugas Akhir
						</h4>

						<h6 class="center" style="margin-bottom: 20px; color: {{Auth::user()->warna['utama']}}; opacity: .5">Teknik Lingkungan ITS</h6>
						<div class="divider"></div>

						<div class="row">
							<div class="col s12">
								@include('partials.form-status')
							</div>
							@if( Auth::user()->tugas_akhir->status_id < 6  && (Auth::user()->proposal->status_id == 12 || Auth::user()->proposal->status_id == 14))
							<div class="col s12 mr-top1" style="text-align: left;">
								<ul>
									<li class="btn-hov">
										<a href="{{ route('sidang.submit') }}" style="padding: 0 15px;" class="btn bayangan_2dp green">
											<i class="material-icons left" style="line-height: inherit; margin-right: 5px; font-size: 1.3em">playlist_add_check</i>
											<span class="font_button">submit</span>
										</a>
									</li>
								</ul>
							</div>
							<div class="col s12 mr-top1">
								@include('form.sidang.edit')
							</div>
							@endif
						</div>
					</section>
				</div>
			</div>
		</section>
	</div>
</div>

@endsection

@section('template_scripts')
	<script src="{{URL::asset('js/dash.js')}}"></script>
	@include('scripts.load-content-script')
@endsection

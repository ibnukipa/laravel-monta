
@extends('app')

@section('template_title')
	Sidang Ujian Lisan TA {{ Auth::user()->name }}
@endsection

@section('content')
<div class="wadah" style="">
	<div class="row">
		<div class="col s3" style="width: 19%; padding: 0; position: fixed; left: 0; z-index: 9">
			<div class="card" style="box-shadow: none; margin: 0">
				<section id="menu-user">
					@if(Auth::user()->hasRole('mahasiswa'))
						@include('partials.menu-mahasiswa')
					@elseif(Auth::user()->hasRole('koordinator') || Auth::user()->hasRole('kaprodi'))
						@include('partials.menu-koordinator')
					@elseif(Auth::user()->hasRole('operator'))
						@include('partials.menu-operator')
					@elseif(Auth::user()->hasRole('administrator'))
						@include('partials.menu-admin')
					@endif
				</section>
			</div>
		</div>
		<section id="content-container">
			<div class="col s10 right" style="width: 81%; padding: 0;">
				<div class="card bayangan_2dp" style="margin: 0">
			        <div class="status-content" style="background: #364150 ; color: white; font-weight: 500">
					{!! Breadcrumbs::render('pendaftaran-sidang') !!}
			        </div>
			        <div class="progress" style="margin: 0; background-color: #B3E5FC;">
					    <div class="indeterminate" style="background-color: #0288D1"></div>
					</div>
					<section id="content" style="border-bottom: 3px solid {{Auth::user()->warna['utama']}} ; padding-top: 20px; color: red ?>">
						<h4 class="head-form">
							Pengajuan Sidang Ujian Lisan TA
						</h4>

						<h6 class="center" style="margin-bottom: 20px; color: {{Auth::user()->warna['utama']}}; opacity: .5">Teknik Lingkungan ITS</h6>
						<div class="divider"></div>

						<div class="row">
							<div class="col s12">
								@include('partials.form-status')
							</div>
							@if((isset($lewat) && $lewat) || (isset($belum)) && $belum)
								
							@elseif(\Auth::user()->tugas_akhir->status_id == 8)
							<div class="col s12 mr-top1" style="text-align: left;">
								<ul>
									<li class="btn-hov">
										<a data-url="{{ route('sidang.create') }}" data-bread="{{Breadcrumbs::render('pendaftaran-sidang-create')}}" onclick="load_content(this)" style="padding: 0 15px;" class="btn bayangan_2dp green">
											<i class="material-icons left" style="line-height: inherit; margin-right: 5px; font-size: 1.3em">note_add</i>
											<span class="font_button">Ajukan Sidang Ujian Lisan TA</span>
										</a>
									</li>
								</ul>
							</div>
							<div class="col s12 mr-top1" style="text-align: left;">
								<div class="chip alert grey-light mr-top1" style="font-size: 1rem">
									File-file yang dibutuhkan :
								</div>
								<div class="row no-margin">
									<div class="input-field col s4 mr-top2" style="margin-bottom: 10px; margin-top: 2.5rem;">
										<i class="material-icons prefix" style="margin-top: -1rem">attachment</i>
										{!! Form::label('', 'Laporan Tugas Akhir' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
										
										<a class="grey-light" href="#" style="margin-top: 1rem; margin-left: 3rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
											<span class="font_button" style="text-transform: none">tidak tersedia</span>
										</a>
										
									</div>

									<div class="input-field col s4 mr-top2" style="margin-bottom: 10px; margin-top: 2.5rem;">
										<i class="material-icons prefix" style="margin-top: -1rem">attachment</i>
										{!! Form::label('', 'Berita Acara Seminar Kemajuan TA' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
										
										<a class="grey-light" href="#" style=" margin-left: 3rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
											<span class="font_button" style="text-transform: none">tidak tersedia</span>
										</a>
									</div>

									<div class="input-field col s4 mr-top2" style="margin-bottom: 10px; margin-top: 2.5rem;">
										<i class="material-icons prefix" style="margin-top: -1rem">attachment</i>
										{!! Form::label('', 'File Kegiatan Asistesi (FTA-03)' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
										
										<!--<a class="grey-dark" target="_blank" href="{{ route('berkas', [Auth::user()->id, 'fta03']) }}" style=" margin-left: 3rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
											<span class="font_button" style="text-transform: none">Print</span>
										</a>-->
										<a class="green" target="_blank" href="{{ route('file.download', ['kegiatan-asistensi', Auth::user()->id]) }}" style=" margin-left: 3rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
											<span class="font_button" style="text-transform: none">Download</span>
										</a>
									</div>

									<div class="input-field col s4 mr-top2" style="margin-bottom: 10px; margin-top: 2.5rem;">
										<i class="material-icons prefix" style="margin-top: -1rem">attachment</i>
										{!! Form::label('', 'File Perbaikan Laporan (FTA-04)' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
										
										<!--<a class="grey-dark" target="_blank" href="{{ route('berkas', [Auth::user()->id, 'fta04']) }}" style=" margin-left: 3rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
											<span class="font_button" style="text-transform: none">Print</span>
										</a>-->
										<a class="green" target="_blank" href="{{ route('file.download', ['perbaikan-laporan', Auth::user()->id]) }}" style=" margin-left: 3rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
											<span class="font_button" style="text-transform: none">Download</span>
										</a>
									</div>

									<div class="input-field col s4 mr-top2" style="margin-bottom: 10px; margin-top: 2.5rem;">
										<i class="material-icons prefix" style="margin-top: -1rem">attachment</i>
										{!! Form::label('', 'File Borang Cek Format Laporan' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
										
										<a class="green" target="_blank" href="{{ route('file.download', ['single-cek-format', Auth::user()->id]) }}" style=" margin-left: 3rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
											<span class="font_button" style="text-transform: none">Download</span>
										</a>
									</div>

									{{-- <div class="input-field col s4 mr-top2" style="margin-bottom: 10px; margin-top: 2.5rem;">
										<i class="material-icons prefix" style="margin-top: -1rem">attachment</i>
										{!! Form::label('proposal_ta', 'Proposal TA (Versi Terakhir Upload)' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}

										<a class="grey-dark" target="_blank" href="{{ URL::route('proposal.file', ['proposal_ta']) }}" style=" margin-left: 3rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
											<span class="font_button" style="text-transform: none">Lihat</span>
										</a>

										<a class="green" target="_blank" href="{{ URL::route('proposal.file.download', ['proposal_ta']) }}" style=" margin-left: .5rem; border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto">
											<span class="font_button" style="text-transform: none">Download</span>
										</a>
									</div> --}}
								</div>
							</div>
							@elseif(\Auth::user()->tugas_akhir->status_id == 10 || \Auth::user()->tugas_akhir->status_id == 12 || \Auth::user()->tugas_akhir->status_id == 18)
							<div class="col s12 mr-top1" style="text-align: left;">
								<ul>
									<li class="btn-hov">
										<a href="{{ route('sidang.submit') }}" style="padding: 0 15px;" class="btn bayangan_2dp green">
											<i class="material-icons left" style="line-height: inherit; margin-right: 5px; font-size: 1.3em">playlist_add_check</i>
											<span class="font_button">Submit</span>
										</a>
									</li>
									<li class="btn-hov">
										{!! Form::open(array('url' => 'sidang/' . Auth::user()->tugas_akhir->id)) !!}
											{!! Form::hidden('_method', 'DELETE') !!}
											{!! Form::button("<i class='material-icons left'>delete</i><span class='font_button'>Hapus</span>", array('data-pesan' => 'Apakah Anda yakin akan menghapus Pengajuan Seminar Kemajuan TA Anda?', 'data-target' => 'confirmDelete', 'onclick' => 'doDelete(this)', 'class' => 'btn bayangan_2dp red')) !!}
										{!! Form::close() !!}
									</li>
									<li class="btn-hov">
										<a data-url="{{ route('catatan.mahasiswa', [Auth::user()->id ,'sidang']) }}" data-bread="{{Breadcrumbs::render('catatan-sidang')}}" onclick="load_content(this)" style="padding: 0 15px;" class="btn bayangan_2dp grey-dark">
											<i class="material-icons left" style="line-height: inherit; margin-right: 5px; font-size: 1.3em">speaker_notes</i>
											<span class="font_button">Catatan </span>
										</a>
									</li>
								</ul>
							</div>
							<div class="col s12 mr-top1">
								@include('form.sidang.edit')
								@include('modals.modal-delete')
								@include('scripts.modal-delete-script')
							</div>
							@elseif(Auth::user()->tugas_akhir->status_id >= 14)
							<div class="col s12 mr-top1" style="text-align: left;">
								<ul>
									<li class="btn-hov">
										<a data-url="{{ route('catatan.mahasiswa', [Auth::user()->id ,'sidang']) }}" data-bread="{{Breadcrumbs::render('catatan-sidang')}}" onclick="load_content(this)" style="padding: 0 15px;" class="btn bayangan_2dp grey-dark">
											<i class="material-icons left" style="line-height: inherit; margin-right: 5px; font-size: 1.3em">speaker_notes</i>
											<span class="font_button">Catatan </span>
										</a>
									</li>
								</ul>
							</div>
							<div class="col s12 mr-top1">
								@include('form.sidang.show')
							</div>
							@endif
						</div>
					</section>
				</div>
			</div>
		</section>
	</div>
</div>

@endsection

@section('template_scripts')
	<script src="{{URL::asset('js/dash.js')}}"></script>
	@include('scripts.load-content-script')
@endsection

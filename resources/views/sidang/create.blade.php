<h4 class="head-form">
	{{ $judul }}
</h4>

<h6 class="center" style="margin-bottom: 20px; color: {{Auth::user()->warna['utama']}}; opacity: .5">Teknik Lingkungan ITS</h6>
<div class="divider"></div>

{!! Form::open(array('url' => 'sidang', 'method' => 'POST', 'class' => ' form-blue lockscreen-credentials form-horizontal', 'role' => 'form', 'files'=>true)) !!}
	<div class="row no-margin">
        <div class="col s12" style="margin-bottom: 1rem">
			@include('partials.form-status')
        </div>
		<div class="input-field col s6">
			<i class="material-icons prefix active">account_box</i>
            {!! Form::text('nama_lengkap', Auth::user()->full_name, array('id' => 'nama_lengkap', 'class' => '', 'readonly' => 'readonly')) !!}
   			{!! Form::label('nama_lengkap', 'Nama Lengkap' , array('class' => 'active')); !!}
		</div>

		<div class="input-field col s6">
			<i class="material-icons prefix active">chrome_reader_mode</i>
			{!! Form::text('username', Auth::user()->username, array('id' => 'username', 'class' => '', 'readonly' => 'readonly')) !!}
   			{!! Form::label('username', 'NRP' , array('class' => 'active')); !!}
		</div>

        <div class="input-field col s12 mr-top2">
			<i class="material-icons prefix">import_contacts</i>
			{!! Form::textarea('judul_ta', Auth::user()->tugas_akhir->judul, array('id' => 'judul_ta', 'class' => 'form-control ckeditor', 'required' => 'required')) !!}
   			{!! Form::label('judul_ta', 'Judul Tugas Akhir' , array('class' => 'active')); !!}
		</div>

		{{-- <div class="input-field col s12 mr-top2">
            <i class="material-icons prefix">import_contacts</i>
            <div style="line-height: 2rem; margin-left: 3rem; margin-top: 1rem; min-height: 100px; font-size: 1.5rem; border-bottom: 1px dotted rgba(25, 118, 210, 1)">
                {!! Auth::user()->tugas_akhir->judul !!}
            </div>
            {!! Form::label('judul_ta', 'Judul Tugas Akhir' , array('class' => 'active')); !!}
        </div> --}}

        <div class="input-field col s6 mr-top3">
			<i class="material-icons prefix active">person</i>
			{!! Form::text('dosen_pembimbing', Auth::user()->tugas_akhir->dosen_pembimbing->full_name, array('id' => 'dosen_pembimbing', 'class' => '', 'readonly' => 'readonly')) !!}
   			{!! Form::label('dosen_pembimbing', 'Dosen Pembimbing' , array('class' => 'active')); !!}
		</div>

		<div class="input-field col s6 mr-top3">
			<i class="material-icons prefix active">donut_large</i>
			{!! Form::text('lab_ta', Auth::user()->tugas_akhir->lab_ta->description, array('id' => 'lab_ta', 'class' => '', 'style' => 'font-size: 1rem', 'readonly' => 'readonly')) !!}
   			{!! Form::label('lab_ta', 'Lab TA' , array('class' => 'active')); !!}
		</div>

		<div class="input-field col s12 mr-top2">
            <i class="material-icons prefix active">label</i>
            {!! Form::text('kategori_ta', Auth::user()->tugas_akhir->kategori_ta->description, array('style' => 'font-size: 1.2rem', 'id' => 'yang_merupakan', 'class' => '', 'readonly' => 'readonly')) !!}
            {!! Form::label('kategori_ta', 'Kategori Tugas Akhir: ' , array('class' => 'active')); !!}
        </div>

		<div class="input-field col s8 mr-top2" style="margin-bottom: 10px; margin-top: 2.5rem;">
			<i class="material-icons prefix">attachment</i>
			{!! Form::file('laporan_ta', array('id' => 'laporan_ta')) !!}
            {!! Form::label('laporan_ta', 'Laporan Tugas Akhir :' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1', 'required' => 'required')); !!}
            {!! Form::label('laporan_ta', "(.pdf) (max: 30MB)" , array('class' => 'active', 'style' => 'font-size: .8rem;top: 1rem; color: rgba("0, 0, 0, .5")', 'required' => 'required')); !!}			            
		</div>

        <div class="input-field col s8 mr-top2" style="margin-bottom: 10px; margin-top: 2.5rem;">
			<i class="material-icons prefix">attachment</i>
			{!! Form::file('berita_acara', array('id' => 'berita_acara')) !!}
            {!! Form::label('berita_acara', 'Berita Acara Seminar Kemajuan TA (Form KTA-02) : ' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1', 'required' => 'required')); !!}
            {!! Form::label('berita_acara', "(.pdf, .jpg, .png) (max: 1MB)" , array('class' => 'active', 'style' => 'font-size: .8rem;top: 1rem; color: rgba("0, 0, 0, .5")', 'required' => 'required')); !!}			
		</div>

        <div class="input-field col s8 mr-top2" style="margin-bottom: 10px; margin-top: 2.5rem;">
			<i class="material-icons prefix">attachment</i>
			{!! Form::file('lembar_asistensi', array('id' => 'lembar_asistensi')) !!}
            {!! Form::label('lembar_asistensi', 'Lembar Kegiatan Asistensi (Form FTA-03) : ' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1', 'required' => 'required')); !!}
            {!! Form::label('lembar_asistensi', "(.pdf, .jpg, .png) (max: 1MB)" , array('class' => 'active', 'style' => 'font-size: .8rem;top: 1rem; color: rgba("0, 0, 0, .5")', 'required' => 'required')); !!}			
		</div>

        <div class="input-field col s8 mr-top2" style="margin-bottom: 10px; margin-top: 2.5rem;">
			<i class="material-icons prefix">attachment</i>
			{!! Form::file('form_perbaikan', array('id' => 'form_perbaikan')) !!}
            {!! Form::label('form_perbaikan', "Form Perbaikan (Form FTA-04) : " , array('class' => 'active', 'style' => 'top: 0; color: #0288D1', 'required' => 'required')); !!}
            {!! Form::label('form_perbaikan', "(.pdf, .jpg, .png) (max: 1MB)" , array('class' => 'active', 'style' => 'font-size: .8rem;top: 1rem; color: rgba("0, 0, 0, .5")', 'required' => 'required')); !!}				
		</div>

        <div class="input-field col s8 mr-top2" style="margin-bottom: 10px; margin-top: 2.5rem;">
			<i class="material-icons prefix">attachment</i>
			{!! Form::file('borang_cek_format', array('id' => 'borang_cek_format')) !!}
            {!! Form::label('borang_cek_format', "Borang Cek Format Laporan TA : " , array('class' => 'active', 'style' => 'top: 0; color: #0288D1', 'required' => 'required')); !!}
            {!! Form::label('borang_cek_format', "(.pdf, .jpg, .png) (max: 1MB)" , array('class' => 'active', 'style' => 'font-size: .8rem;top: 1rem; color: rgba("0, 0, 0, .5")', 'required' => 'required')); !!}				
		</div>

         <div class="col s12 mr-top2" style="text-align: right; border-top: 1px solid rgba(0, 0, 0, .1); padding-top: 1rem" >
            <ul>
                <li class="btn-hov">
                    {!! Form::button("<i class='material-icons left'' style='line-height: inherit; margin-right: 5px; font-size: 1.3em'>save</i><span class='font_button'>Simpan</span>", array('class' => 'btn bayangan_2dp green','type' => 'submit')) !!}
                </li>
            </ul>
        </div>
	</div>
{!! Form::close() !!}
<script src="{{URL::asset('js/dash.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        CKEDITOR.replace( 'judul_ta', {
            contentsCss : '{{ URL::asset('css/style.css') }}',
            removeButtons : 'Cut,Undo,Copy,Redo,Scayt,Link,Image,Maximize,Source,Table,Unlink,Anchor,HorizontalRule,SpecialChar,RemoveFormat,Strike,NumberedList,BulletedList,Outdent,Indent,Styles,Format,About'
        });
		$('#laporan_ta').filer({
            changeInput: '<a data-warna="biru" class="jFiler-input-button bayangan_2dp" style="margin-top: 1rem">Cari berkas...</a>',
            limit: 1,
            maxSize: 30,
            extensions: ["pdf"],
            showThumbs: true,
            captions : {
                errors: {
                    filesType: "Maaf! file yang Anda pilih tidak didukung.",
                }
            },
        }); 

        $('#berita_acara').filer({
            changeInput: '<a data-warna="biru" class="jFiler-input-button bayangan_2dp" style="margin-top: 1rem">Cari berkas...</a>',
            limit: 1,
            maxSize: 1,
            extensions: ["pdf", "jpg", "png"],
            showThumbs: true,
            captions : {
                errors: {
                    filesType: "Maaf! file yang Anda pilih tidak didukung.",
                }
            },
        }); 

        $('#lembar_asistensi').filer({
            changeInput: '<a data-warna="biru" class="jFiler-input-button bayangan_2dp" style="margin-top: 1rem">Cari berkas...</a>',
            limit: 1,
            maxSize: 1,
            extensions: ["pdf", "jpg", "png"],
            showThumbs: true,
            captions : {
                errors: {
                    filesType: "Maaf! file yang Anda pilih tidak didukung.",
                }
            },
        }); 

        $('#form_perbaikan').filer({
            changeInput: '<a data-warna="biru" class="jFiler-input-button bayangan_2dp" style="margin-top: 1rem">Cari berkas...</a>',
            limit: 1,
            maxSize: 1,
            extensions: ["pdf", "jpg", "png"],
            showThumbs: true,
            captions : {
                errors: {
                    filesType: "Maaf! file yang Anda pilih tidak didukung.",
                }
            },
        });     

        $('#borang_cek_format').filer({
            changeInput: '<a data-warna="biru" class="jFiler-input-button bayangan_2dp" style="margin-top: 1rem">Cari berkas...</a>',
            limit: 1,
            maxSize: 1,
            extensions: ["pdf", "jpg", "png"],
            showThumbs: true,
            captions : {
                errors: {
                    filesType: "Maaf! file yang Anda pilih tidak didukung.",
                }
            },
        });     

    });
</script>

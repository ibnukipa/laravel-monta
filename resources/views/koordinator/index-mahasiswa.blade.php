
@extends('app')

@section('template_title')
	Daftar Mahasiswa {{ Auth::user()->name }}
@endsection

@section('template_linked_css')
    <link rel="stylesheet" href="{{ URL::asset('css/themes/jquery.filer-dragdropbox-theme.css') }}" />
@endsection

@section('content')
<div class="wadah" style="">
	<div class="row">
		<div class="col s3" style="width: 19%; padding: 0; position: fixed; left: 0; z-index: 9">
			<div class="card" style="box-shadow: none; margin: 0">
				<section id="menu-user">
					@if(Auth::user()->hasRole('mahasiswa'))
						@include('partials.menu-mahasiswa')
					@elseif(Auth::user()->hasRole('koordinator') || Auth::user()->hasRole('kaprodi') )
						@include('partials.menu-koordinator')
					@elseif(Auth::user()->hasRole('operator'))
						@include('partials.menu-operator')
					@elseif(Auth::user()->hasRole('administrator'))
						@include('partials.menu-admin')
					@endif
				</section>
			</div>
		</div>
		<section id="content-container">
			<div class="col s10 right" style="width: 81%; padding: 0;">
				<div class="card bayangan_2dp" style="margin: 0">
			        <div class="status-content" style="background: #364150 ; color: white; font-weight: 500">
					{!! Breadcrumbs::render('daftar-mahasiswa') !!}
			        </div>
			        <div class="progress" style="margin: 0; background-color: #B3E5FC;">
					    <div class="indeterminate" style="background-color: #0288D1"></div>
					</div>
					<section id="content" style="border-bottom: 3px solid {{Auth::user()->warna['utama']}} ; padding-top: 20px; color: red ?>">
						<h4 class="head-form">
							DAFTAR MAHASISWA
						</h4>

						<h6 class="center" style="margin-bottom: 20px; color: {{Auth::user()->warna['utama']}}; opacity: .5">Teknik Lingkungan ITS</h6>
						<div class="divider"></div>

    					<div class="row">
							<div class="col s12">
								@include('partials.form-status')
							</div>
                            <div class="col s12 mr-top1" style="text-align: left;">
								<ul>
									<li class="btn-hov">
										<a data-url="{{ route('mahasiswa.upload.daftar.create') }}" data-bread="{{Breadcrumbs::render('upload-daftar-mahasiswa')}}" onclick="load_content(this)" style="padding: 0 15px;" class="btn bayangan_2dp green">
											<i class="material-icons left" style="line-height: inherit; margin-right: 5px; font-size: 1.3em">file_upload</i>
											<span class="font_button">Upload Data Mahasiswa</span>
										</a>
									</li>
									<li class="btn-hov">
										<a data-url="{{ route('mahasiswa.akun.create') }}" data-bread="" onclick="load_content(this)" style="padding: 0 15px;" class="btn bayangan_2dp blue">
											<i class="material-icons left" style="line-height: inherit; margin-right: 5px; font-size: 1.3em">add</i>
											<span class="font_button">Tambah Data Mahasiswa</span>
										</a>
									</li>
								</ul>
							</div>
                            
							<div class="col s12" style="padding: 1rem">
                                <table id="mahasiswa" class="highlight">
                                    <thead>
                                        <tr class="grey-light">
                                            <th>NRP/Username</th>
                                            <th>Nama Lengkap</th>
                                            <th>Tindakan</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>NRP/Username</th>
                                            <th>Nama Lengkap</th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        @foreach($mahasiswa as $key => $value)
                                            <tr>
                                                <td>{{ $value->username }}</td>
                                                <td>{{ $value->full_name }}</td>
                                                <td>
                                                    <ul class="no-margin">
                                                        <li style="display: inline-block">
                                                            {!! Form::open(array('url' => url('mahasiswa/' . $value->id))) !!}
																{!! Form::hidden('_method', 'DELETE') !!}
																<a onclick="doDelete(this)" data-target="confirmDelete" data-judul="Apakah Anda yakin akan menghapus Data Mahasiswa ini?" style="border-radius: 3px; cursor: pointer;padding: 2px 10px; line-height: 100%; height: auto" class=" red">
											                        <span class="font_button" style="text-transform: none">Hapus</span>
																</a>
															{!! Form::close() !!}
                                                        </li>
                                                    </ul>
												</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div> 
							</div>
						</div>
					</section>
				</div>
			</div>
		</section>
	</div>
</div>
@include('modals.modal-delete')
@endsection

@section('template_scripts')
	<script src="{{URL::asset('js/dash.js')}}"></script>
	@include('scripts.load-content-script')
    @include('scripts.modal-delete-script')
	{{-- Data Table --}}
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
		$(function () {
			$(document).ready(function() {
			    //Setup untuk elemen search HTML
			    $('#mahasiswa tfoot th').each( function () {
			        var title = $(this).text();
			        if(title != '')
                    
			        	$(this).html( '<div id="mahasiswa_filter" class="dataTables_filter"><label><input type="search" class="form-control input-sm" placeholder="Cari '+title+'..." ></label></div>' );
			    } );
			 
			    // DataTable
			    var table = $('#mahasiswa').DataTable({
			    	language: {
			    		"searchPlaceholder": "Cari Data...",
                        "zeroRecords": "Tidak ada data Pendaftaran Proposal.",
			    	},
			    	"columnDefs": [
				    	{
				    		"targets"	: [2],
				    		"searchable": false,
				    		'bSortable': false,
				    	}
			    	],
			    	"paging": true,
			    	"autoWidth": false,
			    });
			 
			    // Menerapkan serach masing-masing kolom
			    table.columns().every( function () {
			        var that = this;
			        $( 'input', this.footer() ).on( 'keyup change', function () {
			            if ( that.search() !== this.value ) {
			                that
			                    .search( this.value )
			                    .draw();
			            }
			        } );
			    } );
			} );
		});
    </script>
@endsection

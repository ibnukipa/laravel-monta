{!! Form::open(array('url' => route('pengaturan-seminar.update', [$id]), 'method' => 'PATCH', 'class' => ' form-blue lockscreen-credentials form-horizontal', 'role' => 'form', 'files'=>true)) !!}
	<div class="row no-margin">
        <div class="col s12" style="margin-bottom: 1rem">
			@include('partials.form-status')
        </div>

        <div class="input-field col s4 mr-top1">
			<i class="material-icons prefix active">class</i>
            {!! Form::text('tahun_ajaran', $tahunAjaran, array('id' => 'tahun_ajaran', 'class' => '', 'readonly' => 'readonly')) !!}
   			{!! Form::label('tahun_ajaran', 'Tahun Ajaran' , array('class' => 'active')); !!}
		</div>
        <div class="input-field col s4 mr-top1">
			<i class="material-icons prefix active">class</i>
            {!! Form::text('semester', $semester, array('id' => 'semester', 'class' => '', 'readonly' => 'readonly')) !!}
   			{!! Form::label('semester', 'Semester' , array('class' => 'active')); !!}
		</div>

        <div class="input-field col s4 mr-top1">
			<i class="material-icons prefix active">class</i>
            {!! Form::text('tipe_seminar', $tipeSeminar, array('id' => 'tipe_seminar', 'class' => '', 'readonly' => 'readonly')) !!}
   			{!! Form::label('tipe_seminar', 'Tipe Seminar : ' , array('class' => 'active')); !!}
		</div>

        <div class="input-field col s12 mr-top3">	
            <i class="material-icons prefix" style="margin-top: -0.5rem">date_range</i>
            {!! Form::input('date', 'due_to', $due_to, ['id' =>'due_to', 'class' => 'datepicker', 'placeholder' => 'Pilih tanggal...', 'style' => 'font-size: 1.3rem']); !!}
            {!! Form::label('due_to', 'Tanggal Terakhir Pengumpulan Berkas Seminar terpilih: ' , array('class' => 'active', 'style' => 'top: 0; color: #0288D1')); !!}
        </div>
		
         <div class="col s12 mr-top2" style="text-align: right; border-top: 1px solid rgba(0, 0, 0, .1); padding-top: 1rem" >
            <ul>
                <li class="btn-hov">
                    {!! Form::button("<i class='material-icons left'' style='line-height: inherit; margin-right: 5px; font-size: 1.3em'>save</i><span class='font_button'>Simpan Perubahan</span>", array('class' => 'btn bayangan_2dp blue','type' => 'submit')) !!}
                </li>
            </ul>
        </div>

	</div>
{!! Form::close() !!}

<script type="text/javascript">
	
    $(document).ready(function() {
        $('.datepicker').pickadate({
            selectMonths: true,
            selectYears: 15,
            format: 'dd-mm-yyyy'
        });
    });

</script>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes" name="viewport">
		<meta property="og:title" content="">
	    <meta property="og:url" content="">
	    <meta property="og:description" content="">
	    <meta property="og:image" content="{{ URL::asset('img/favicon.png') }}">
	    <meta property="og:type" content="website">

		<meta name="robots" content="noindex">
		<meta name="googlebot" content="noindex">

    	<link rel="icon" type="image/png" href="{{ URL::asset('favicon.png') }}">

		<title>@if (trim($__env->yieldContent('template_title')))@yield('template_title') | @endif MonTA</title>

		<!-- Fonts -->
		<link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>
	    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=latin" rel="stylesheet">
		@yield('template_linked_fonts')

		<!-- CSS -->
		{!! HTML::style(asset('/css/reset.css'), array('type' => 'text/css', 'rel' => 'stylesheet')) !!}
		@yield('template_linked_css')
		<link type="text/css" rel="stylesheet" href="{{ URL::asset('css/materialize.css')}}"  media="screen,projection"/>
    	<link href="{{ URL::asset('css/select2.min.css') }}" rel="stylesheet" />
    	<link rel="stylesheet" href="{{ URL::asset('css/animate.css') }}" />
		<link rel="stylesheet" href="{{ URL::asset('css/jquery.filer.css') }}" />
    	<link rel="stylesheet" href="{{ URL::asset('css/style.css') }}" />

		<!-- Icon Fonts -->
		<link rel="stylesheet" href="{{ URL::asset('fonts/iconfont/material-icons.css') }}" />
		{!! HTML::style(asset('https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css'), array('type' => 'text/css', 'rel' => 'stylesheet')) !!}

		<style type="text/css">
			@yield('template_fastload_css')
		</style>
		<script src="{{ URL::asset('js/jquery-1.12.1.js')}}"></script>
		<script src="http://cdn.ckeditor.com/4.5.10/standard/ckeditor.js"></script>
		<script src="{{ URL::asset('js/jquery.filer.min.js')}}"></script>
		<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>
		<script src="{{ URL::asset('js/jquery.leanModal.min.js')}}"></script>		
		<script src="{{ URL::asset('js/materialize.js')}}"></script>
		<script src="{{ URL::asset('js/select2.min.js')}}"></script>

	    {{-- HTML5 Shim and Respond.js for IE8 support --}}
	    <!--[if lt IE 9]>
	      {!! HTML::script('https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js', array('type' => 'text/javascript')) !!}
	      {!! HTML::script('//s3.amazonaws.com/nwapi/nwmatcher/nwmatcher-1.2.5-min.js', array('type' => 'text/javascript')) !!}
	      {!! HTML::script('//html5base.googlecode.com/svn-history/r38/trunk/js/selectivizr-1.0.3b.js', array('type' => 'text/javascript')) !!}
	      {!! HTML::script('https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js', array('type' => 'text/javascript')) !!}
	    <![endif]-->
	    <!--[if gte IE 9]>
	      <style type="text/css">.gradient {filter: none;}</style>
	    <![endif]-->

	</head>
	<body style="width: 100%; margin: 0 auto; background: whitesmoke; min-width: 1300px">
		<span id="date_time"></span>
		{{-- @if(!Auth::guest()) --}}
		
		@include('partials.nav')
		{{-- @endif --}}
		
		@yield('content')
		<script type="text/javascript" src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
		@yield('template_scripts')
	    <script>
			function resizeIframe(obj) {
				obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
			}
	    	$(".head_form_20").height($(".head_form_20").width());

	       
	        $(document).ready(function(){
	            $(".button-collapse").sideNav();
	        });
	    </script>
	    <script src="{{URL::asset('js/smoothscroll.js')}}"></script>
	    <script>
	    	$('.dropdown-button').dropdown({
				inDuration: 300,
				outDuration: 225,
				hover: true
				}
			);
	    </script>
		<script type="text/javascript">
			tday=new Array("Minggu","Senin","Selasa","Rabu","Kamis","Jum'at","Sabtu");
			tmonth=new Array("01","02","03","04","05","06","07","08","09","10","11","12");
			{{-- tmonth=new Array("Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"); --}}

			function GetClock(){
			var d=new Date();
			var nday=d.getDay(),nmonth=d.getMonth(),ndate=d.getDate(),nyear=d.getYear();
			if(nyear<1000) nyear+=1900;
			var nhour=d.getHours(),nmin=d.getMinutes(),nsec=d.getSeconds(),ap;

			if(nhour==0){ap=" AM";nhour=12;}
			else if(nhour<12){ap=" AM";}
			else if(nhour==12){ap=" PM";}
			else if(nhour>12){ap=" PM";nhour-=12;}

			if(nmin<=9) nmin="0"+nmin;
			if(nsec<=9) nsec="0"+nsec;

			document.getElementById('date_real').innerHTML=""+tday[nday]+", "+ndate+"-"+tmonth[nmonth]+"-"+nyear+"";
			document.getElementById('time_real').innerHTML=""+nhour+":"+nmin+":"+nsec+ap+"";
			}

			window.onload=function(){
				GetClock();
				setInterval(GetClock,1000);
			}
		</script>
	</body>
</html>
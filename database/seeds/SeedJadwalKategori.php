<?php

use Illuminate\Database\Seeder;
use App\Models\JadwalKategori;

class SeedJadwalKategori extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jadwal_kategori')->delete();

        JadwalKategori::create([
            'name'			=> 'seminar-proposal',
            'description'	=> 'Seminar Proposal TA'
        ]);

        JadwalKategori::create([
            'name'			=> 'seminar-kemajuan',
            'description'	=> 'Seminar Kemajuan TA'
        ]);

        JadwalKategori::create([
            'name'			=> 'sidang-lisan',
            'description'	=> 'Sidang TA'
        ]);
    }
}

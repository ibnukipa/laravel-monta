<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

         $this->call('SeedRoles');
         $this->call('SeedUsers');
         $this->call('SeedLab');
         $this->call('SeedKategoriTA');
         $this->call('SeedKategoriProposal');
         $this->call('SeedJadwalJam');
         $this->call('SeedJadwalKategori');
         $this->call('SeedJadwalRuang');
         $this->call('SeedDosen');
         $this->call('SeedStatusProposal');
         $this->call('SeedStatusTugasAkhir');

        Model::reguard();
    }
}
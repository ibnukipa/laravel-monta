<?php

use Illuminate\Database\Seeder;
use App\Models\User;
class SeedUsers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        //Administrator
        $user = User::create([
            'username'      => 'dev_admin',
            'password'      => bcrypt('dev_admin'),
            'full_name'     => 'Administrator',
            'email'         => 'admin@gmail.com',
            'nohp'          => '08675956593',
            'active'        => 1,
        ]);        
        $user->roles()->attach(1);

        // Koordinator TA
        $user = User::create([ 
            'username'      => 'dev_koordinator',
            'password'      => bcrypt('dev_koordinator'),
            'full_name'     => 'Koordinator',
            'email'         => 'koor@gmail.com',
            'nohp'          => '08675956593',
            'active'        => 1,
        ]);
        $user->roles()->attach(3);

        // Operator
        $user = User::create([ 
            'username'      => 'dev_operator',
            'password'      => bcrypt('dev_operator'),
            'full_name'     => 'Operator',
            'email'         => 'operator@gmail.com',
            'nohp'          => '08675956593',
            'active'        => 1,
        ]);
        $user->roles()->attach(4);

        // kaprodi
        $user = User::create([ 
            'username'      => 'dev_kaprodi',
            'password'      => bcrypt('dev_kaprodi'),
            'full_name'     => 'Kaprodi',
            'email'         => 'kaprodi@gmail.com',
            'nohp'          => '08675956593',
            'active'        => 1,
        ]);
        $user->roles()->attach(5);

    }
}

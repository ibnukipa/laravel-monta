<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class SeedRoles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('roles')->delete();

        Role::create([
            'name'   => 'administrator'
        ]);

        Role::create([
            'name'   => 'mahasiswa'
        ]);

        Role::create([
            'name'   => 'koordinator'
        ]);

        Role::create([
            'name'   => 'operator'
        ]);

        Role::create([
            'name'   => 'kaprodi'
        ]);

    }
}
<?php

use Illuminate\Database\Seeder;
use App\Models\StatusTugasAkhir;

class SeedStatusTugasAkhir extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('status_tugas_akhir')->delete();

        StatusTugasAkhir::create([
            'name'      => 'pengajuan Seminar Kemajuan TA belum disubmit'
        ]);

        StatusTugasAkhir::create([
            'name'      => 'mengajukan Seminar Kemajuan TA'
        ]);

        StatusTugasAkhir::create([
            'name'      => 'pengajuan Seminar Kemajuan TA perlu revisi'
        ]);

        StatusTugasAkhir::create([
            'name'      => 'pengajuan Seminar Kemajuan TA ditolak'
        ]);

        StatusTugasAkhir::create([
            'name'      => 'pengajuan Seminar Kemajuan TA diterima'
        ]);

        StatusTugasAkhir::create([
            'name'      => 'Seminar Kemajuan TA'
        ]);

        StatusTugasAkhir::create([
            'name'      => 'Seminar Ulang Kemajuan TA'
        ]);

        StatusTugasAkhir::create([
            'name'      => 'dapat mengikuti ujian Tugas Akhir'
        ]);

        StatusTugasAkhir::create([
            'name'      => 'tidak dapat mengikuti ujian Tugas Akhir'
        ]);

        StatusTugasAkhir::create([
            'name'      => 'pengajuan Sidang Ujian Lisan TA belum disubmit'
        ]);

        StatusTugasAkhir::create([
            'name'      => 'mengajukan Sidang Ujian Lisan TA'
        ]);

        StatusTugasAkhir::create([
            'name'      => 'pengajuan Sidang Ujian Lisan TA perlu revisi'
        ]);

        StatusTugasAkhir::create([
            'name'      => 'pengajuan Sidang Ujian Lisan TA ditolak'
        ]);

        StatusTugasAkhir::create([
            'name'      => 'pengajuan Sidang Ujian Lisan TA diterima'
        ]);

        StatusTugasAkhir::create([
            'name'      => 'Sidang Ujian Lisan TA'
        ]);

        StatusTugasAkhir::create([
            'name'      => 'Sidang Ulang Ujian Lisan TA'
        ]);

        StatusTugasAkhir::create([
            'name'      => 'LULUS'
        ]);

        StatusTugasAkhir::create([
            'name'      => 'TIDAK LULUS'
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Models\JadwalJam;

class SeedJadwalJam extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jadwal_jam')->delete();

        //Seminar Proposal
        JadwalJam::create([
            'kategori_id'   => 1,
            'mulai'			=> '08:00:00',
            'selesai'	    => '09:30:00'
        ]);

        JadwalJam::create([
            'kategori_id'   => 1,
            'mulai'			=> '09:30:00',
            'selesai'	    => '11:00:00'
        ]);

        JadwalJam::create([
            'kategori_id'   => 1,
            'mulai'			=> '11:00:00',
            'selesai'	    => '12:30:00'
        ]);

        JadwalJam::create([
            'kategori_id'   => 1,
            'mulai'			=> '13:30:00',
            'selesai'	    => '15:00:00'
        ]);

        JadwalJam::create([
            'kategori_id'   => 1,
            'mulai'			=> '15:00:00',
            'selesai'	    => '16:30:00'
        ]);

        //Seminar Kemajuan
        JadwalJam::create([
            'kategori_id'   => 2,
            'mulai'			=> '08:00:00',
            'selesai'	    => '09:30:00'
        ]);

        JadwalJam::create([
            'kategori_id'   => 2,
            'mulai'			=> '09:30:00',
            'selesai'	    => '11:00:00'
        ]);

        JadwalJam::create([
            'kategori_id'   => 2,
            'mulai'			=> '11:00:00',
            'selesai'	    => '12:30:00'
        ]);

        JadwalJam::create([
            'kategori_id'   => 2,
            'mulai'			=> '13:30:00',
            'selesai'	    => '15:00:00'
        ]);

        JadwalJam::create([
            'kategori_id'   => 2,
            'mulai'			=> '15:00:00',
            'selesai'	    => '16:30:00'
        ]);

        //Sidang
        JadwalJam::create([
            'kategori_id'   => 3,
            'mulai'			=> '07:30:00',
            'selesai'	    => '09:30:00'
        ]);

        JadwalJam::create([
            'kategori_id'   => 3,
            'mulai'			=> '08:00:00',
            'selesai'	    => '10:00:00'
        ]);
        JadwalJam::create([
            'kategori_id'   => 3,
            'mulai'			=> '09:30:00',
            'selesai'	    => '11:30:00'
        ]);

        JadwalJam::create([
            'kategori_id'   => 3,
            'mulai'			=> '10:00:00',
            'selesai'	    => '12:00:00'
        ]);

        JadwalJam::create([
            'kategori_id'   => 3,
            'mulai'			=> '13:00:00',
            'selesai'	    => '15:00:00'
        ]);

        JadwalJam::create([
            'kategori_id'   => 3,
            'mulai'			=> '13:30:00',
            'selesai'	    => '15:30:00'
        ]);

        JadwalJam::create([
            'kategori_id'   => 3,
            'mulai'			=> '15:00:00',
            'selesai'	    => '17:00:00'
        ]);

        JadwalJam::create([
            'kategori_id'   => 3,
            'mulai'			=> '15:30:00',
            'selesai'	    => '17:30:00'
        ]);

    }
}

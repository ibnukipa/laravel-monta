<?php

use Illuminate\Database\Seeder;
use App\Models\LabTA;

class SeedLab extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lab_ta')->delete();

        LabTA::create([
            'name'			=> 'air',
            'description'	=> 'Teknologi Pengolahan Air'
        ]);

        LabTA::create([
            'name'			=> 'kualitas',
            'description'	=> 'Manajemen Kualitas Lingkungan'
        ]);

        LabTA::create([
            'name'			=> 'udara',
            'description'	=> 'Pengendalian Pencemaran Udara dan Perubahan Iklim'
        ]);

        LabTA::create([
            'name'			=> 'padat',
            'description'	=> 'Limbah Padat dan B3'
        ]);

        LabTA::create([
            'name'			=> 'remidiasi',
            'description'	=> 'Remidiasi Lingkungan'
        ]);

    }
}

<?php

use Illuminate\Database\Seeder;
use App\Models\JadwalRuang;

class SeedJadwalRuang extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jadwal_ruang')->delete();

        JadwalRuang::create([
            'name'			=> '103',
            'description'	=> 'TL-103'
        ]);

        JadwalRuang::create([
            'name'			=> '104',
            'description'	=> 'TL-104'
        ]);

        JadwalRuang::create([
            'name'			=> '105',
            'description'	=> 'TL-105'
        ]);
    }
}

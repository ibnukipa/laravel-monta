<?php

use Illuminate\Database\Seeder;
use App\Models\KategoriTA;

class SeedKategoriTA extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kategori_ta')->delete();

        KategoriTA::create([
            'name'			=> 'perencanaan',
            'description'	=> 'Perencanaan baru atau perencanaan ulang'
        ]);

        KategoriTA::create([
            'name'			=> 'penelitian',
            'description'	=> 'Penelitian laboratorium atau penelitian lapangan'
        ]);

        KategoriTA::create([
            'name'			=> 'kajian',
            'description'	=> 'Kajian Pustaka dengan Studi Kasus'
        ]);
    }
}

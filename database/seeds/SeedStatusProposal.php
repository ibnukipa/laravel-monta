<?php

use Illuminate\Database\Seeder;
use App\Models\StatusProposal;

class SeedStatusProposal extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('status_proposal')->delete();

        //Pendaftaran Peroposal
        StatusProposal::create([
            'name'      => 'pendaftaran Proposal TA belum disubmit'
        ]);

        StatusProposal::create([
            'name'      => 'mendaftarkan Proposal TA'
        ]);

        StatusProposal::create([
            'name'      => 'pendaftaran Proposal TA perlu revisi'
        ]);

        StatusProposal::create([
            'name'      => 'pendaftaran Proposal TA ditolak'
        ]);

        StatusProposal::create([
            'name'      => 'pendaftaran Proposal TA diterima'
        ]);

        //Pengajuan Seminar Proposal TA
        StatusProposal::create([
            'name'      => 'pengajuan Seminar Proposal TA belum disubmit'
        ]);

        StatusProposal::create([
            'name'      => 'mengajukan Seminar Proposal TA'
        ]);

        StatusProposal::create([
            'name'      => 'pengajuan Seminar Proposal TA perlu revisi'
        ]);

        StatusProposal::create([
            'name'      => 'pengajuan Seminar Proposal TA ditolak'
        ]);

        StatusProposal::create([
            'name'      => 'pengajuan Seminar Proposal TA diterima'
        ]);

        StatusProposal::create([
            'name'      => 'Seminar Proposal TA'
        ]);

        StatusProposal::create([
            'name'      => 'Proposal TA diterima'
        ]);

        StatusProposal::create([
            'name'      => 'Seminar Ulang Proposal TA'
        ]);

        StatusProposal::create([
            'name'      => 'Proposal TA diterima dan revisi'
        ]);

        StatusProposal::create([
            'name'      => 'Proposal TA dibatalkan'
        ]);

        StatusProposal::create([
            'name'      => 'nothing'
        ]);

    }
}

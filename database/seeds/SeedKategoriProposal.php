<?php

use Illuminate\Database\Seeder;
use App\Models\KategoriProposal;

class SeedKategoriProposal extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kategori_proposal')->delete();

        KategoriProposal::create([
            'name'			=> 'mandiri',
            'description'	=> 'Topik mandiri'
        ]);

        KategoriProposal::create([
            'name'			=> 'disetujui',
            'description'	=> 'Topik mandiri dan sudah disetujui oleh dosen ybs sbg pembimbing'
        ]);

        KategoriProposal::create([
            'name'			=> 'penelitian',
            'description'	=> 'Penelitian dosen'
        ]);
    }
}

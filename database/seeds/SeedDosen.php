<?php

use Illuminate\Database\Seeder;
use App\Models\Dosen;

class SeedDosen extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dosen')->delete();

        Dosen::create([
            'nip'			=> '19500114 197903 1 001',
            'full_name'	    => 'Prof.Ir. Wahyono Hadi, M.Sc Ph.D',
            'phone'	        => '',
            'email'	        => '',
            'lab_ta_id'	    => 1,
        ]);

        Dosen::create([
            'nip'			=> '19530502 198103 1 004',
            'full_name'	    => 'Dr. Ir. Mohammad Razif, MM.',
            'phone'	        => '',
            'email'	        => '',
            'lab_ta_id'	    => 2,
        ]);

        Dosen::create([
            'nip'			=> '19520707 198103 1 005',
            'full_name'	    => 'Ir. Hariwiko Indarjanto, M.Eng',
            'phone'	        => '',
            'email'	        => '',
            'lab_ta_id'	    => 2,
        ]);

        Dosen::create([
            'nip'			=> '19530706 198403 2 004',
            'full_name'	    => 'Prof. Dr. Yulinah Trihadiningrum, M.App.Sc',
            'phone'	        => '',
            'email'	        => '',
            'lab_ta_id'	    => 2,
        ]);

        Dosen::create([
            'nip'			=> '19540824 198403 1 001',
            'full_name'	    => 'Prof. Dr. Ir. Sarwoko Mangkoedihardjo, M.ScEs',
            'phone'	        => '',
            'email'	        => '',
            'lab_ta_id'	    => 2,
        ]);

        Dosen::create([
            'nip'			=> '19550128 198503 2 001',
            'full_name'	    => 'Prof.Dr.Ir. Nieke Karnaningroem, M.Sc',
            'phone'	        => '',
            'email'	        => '',
            'lab_ta_id'	    => 2,
        ]);

        Dosen::create([
            'nip'			=> '19590811 198701 1 001',
            'full_name'	    => 'Ir. Agus Slamet, M.Sc',
            'phone'	        => '',
            'email'	        => '',
            'lab_ta_id'	    => 2,
        ]);

        Dosen::create([
            'nip'			=> '19600618 198803 1 002',
            'full_name'	    => 'Prof.Ir. Joni Hermana, M.Sc.Es Ph.D',
            'phone'	        => '',
            'email'	        => '',
            'lab_ta_id'	    => 2,
        ]);

        Dosen::create([
            'nip'			=> '19570602 198903 2 002',
            'full_name'	    => 'Ir. Atiek Moesriati, M.Kes',
            'phone'	        => '',
            'email'	        => '',
            'lab_ta_id'	    => 2,
        ]);

        Dosen::create([
            'nip'			=> '19600308 198903 1 001',
            'full_name'	    => 'Ir. Eddy Setiadi Soedjono, Dipl.SE,M.Sc.Ph.D.',
            'phone'	        => '',
            'email'	        => '',
            'lab_ta_id'	    => 2,
        ]);

        Dosen::create([
            'nip'			=> '19620816 199003 1 004',
            'full_name'	    => 'Ir. Mas Agus Mardyanto, M.E., Ph.D',
            'phone'	        => '',
            'email'	        => '',
            'lab_ta_id'	    => 2,
        ]);

        Dosen::create([
            'nip'			=> '19650317 199102 1 001',
            'full_name'	    => 'Ir. Bowo Djoko Marsono, M.Eng.',
            'phone'	        => '',
            'email'	        => '',
            'lab_ta_id'	    => 2,
        ]);

        Dosen::create([
            'nip'			=> '19560204 199203 2 001',
            'full_name'	    => 'Dr. Ir. Ellina S Pandebesie, MT.',
            'phone'	        => '',
            'email'	        => '',
            'lab_ta_id'	    => 2,
        ]);

        Dosen::create([
            'nip'			=> '19650508 199303 1 001',
            'full_name'	    => 'Dr. Ir. R. Irwan Bagyo Santoso, MT.',
            'phone'	        => '',
            'email'	        => '',
            'lab_ta_id'	    => 2,
        ]);

        Dosen::create([
            'nip'			=> '19680128 199403 1 003',
            'full_name'	    => 'Dr. Ali Masduqi, ST. MT.',
            'phone'	        => '',
            'email'	        => '',
            'lab_ta_id'	    => 2,
        ]);

        Dosen::create([
            'nip'			=> '19710818 199412 2 001',
            'full_name'	    => 'Susi Agustina Wilujeng, ST., MT',
            'phone'	        => '',
            'email'	        => '',
            'lab_ta_id'	    => 2,
        ]);

        Dosen::create([
            'nip'			=> '19660116 199703 1 001',
            'full_name'	    => 'Dr. Ir. Rachmat Boedisantoso, MT.',
            'phone'	        => '',
            'email'	        => '',
            'lab_ta_id'	    => 2,
        ]);

        Dosen::create([
            'nip'			=> '19710818 199703 2 001',
            'full_name'	    => 'Bieby Voijant Tangahu, ST, MT, Ph.D.',
            'phone'	        => '',
            'email'	        => '',
            'lab_ta_id'	    => 2,
        ]);

        Dosen::create([
            'nip'			=> '19750212 199903 2 001',
            'full_name'	    => 'I D A A Warmadewanthi, ST, MT, Ph.D',
            'phone'	        => '',
            'email'	        => '',
            'lab_ta_id'	    => 2,
        ]);

        Dosen::create([
            'nip'			=> '19730601 200003 1 001',
            'full_name'	    => 'Adhi Yuniarto, ST.,MT., Ph.D.',
            'phone'	        => '',
            'email'	        => '',
            'lab_ta_id'	    => 2,
        ]);

        Dosen::create([
            'nip'			=> '19750523 200212 2 001',
            'full_name'	    => 'Harmin Sulistiyaning Titah, ST, MT, Ph.D.',
            'phone'	        => '',
            'email'	        => '',
            'lab_ta_id'	    => 2,
        ]);

        Dosen::create([
            'nip'			=> '19770209 200312 2 001',
            'full_name'	    => 'Alia Damayanti, ST.,MT.,Ph.D',
            'phone'	        => '',
            'email'	        => '',
            'lab_ta_id'	    => 2,
        ]);

        Dosen::create([
            'nip'			=> '19711114 200312 2 001',
            'full_name'	    => 'Ipung Fitri Purwanti, ST.,MT, Ph.D.',
            'phone'	        => '',
            'email'	        => '',
            'lab_ta_id'	    => 2,
        ]);

        Dosen::create([
            'nip'			=> '19751018 200501 1 003',
            'full_name'	    => 'Abdu Fadli Assomadi, S.Si, MT',
            'phone'	        => '',
            'email'	        => '',
            'lab_ta_id'	    => 2,
        ]);

        Dosen::create([
            'nip'			=> '19820119 200501 1 001',
            'full_name'	    => 'Dr.Eng. Arie Dipareza Syafei, ST, MEPM',
            'phone'	        => '',
            'email'	        => '',
            'lab_ta_id'	    => 2,
        ]);

        Dosen::create([
            'nip'			=> '19820804 200501 1 001',
            'full_name'	    => 'Arseto Yekti Bagastyo, ST, MT, M.Phil, Ph.D.',
            'phone'	        => '',
            'email'	        => '',
            'lab_ta_id'	    => 2,
        ]);

        Dosen::create([
            'nip'			=> '19811223 200604 1 001',
            'full_name'	    => 'Welly Herumurti, ST., M.Sc.',
            'phone'	        => '',
            'email'	        => '',
            'lab_ta_id'	    => 2,
        ]);

        Dosen::create([
            'nip'			=> '19830304 200604 1 002',
            'full_name'	    => 'Alfan Purnomo, ST., MT.',
            'phone'	        => '',
            'email'	        => '',
            'lab_ta_id'	    => 2,
        ]);
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDosenPengujiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dosen_penguji', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('jadwal_id')
                    ->unsigned()
                    ->index();
            $table->foreign('jadwal_id')
                    ->references('id')
                    ->on('jadwal')
                    ->onDelete('cascade');
            $table->integer('dosen_id')
                    ->unsigned()
                    ->index();
            $table->foreign('dosen_id')
                    ->references('id')
                    ->on('dosen')
                    ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dosen_penguji');
    }
}

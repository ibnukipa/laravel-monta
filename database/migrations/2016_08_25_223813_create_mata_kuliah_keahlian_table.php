<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMataKuliahKeahlianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mata_kuliah_keahlian', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('nilai');

            $table->integer('tugas_akhir_id')
                    ->unsigned()
                    ->index();
            $table->foreign('tugas_akhir_id')
                    ->references('id')
                    ->on('tugas_akhir')
                    ->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mata_kuliah_keahlian');
    }
}

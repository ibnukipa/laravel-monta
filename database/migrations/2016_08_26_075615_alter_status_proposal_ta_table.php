<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterStatusProposalTaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('proposal_ta', function (Blueprint $table) {
            $table->integer('status_id')
                    ->after('path_nilai_toefl2')
                    ->unsigned()
                    ->index();
            $table->foreign('status_id')
                    ->references('id')
                    ->on('status_proposal')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('proposal_ta', function (Blueprint $table) {
            $table->dropForeign('proposal_ta_status_id_foreign');
            $table->dropColumn('status_id');
        });
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDosenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dosen', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nip')->unique();
            $table->text('full_name');
            $table->text('phone');
            $table->text('email');

            $table->integer('lab_ta_id')
                    ->unsigned()
                    ->index()
                    ->nullable();
            $table->foreign('lab_ta_id')
                    ->references('id')
                    ->on('lab_ta')
                    ->onDelete('cascade');
                    
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dosen');
    }
}

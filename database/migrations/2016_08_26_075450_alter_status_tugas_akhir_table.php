<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterStatusTugasAkhirTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tugas_akhir', function (Blueprint $table) {
            $table->integer('status_id')
                    ->after('path_proposal')
                    ->unsigned()
                    ->index();
            $table->foreign('status_id')
                    ->references('id')
                    ->on('status_tugas_akhir')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tugas_akhir', function (Blueprint $table) {
            $table->dropForeign('tugas_akhir_status_id_foreign');
            $table->dropColumn('status_id');
        });
    }
}

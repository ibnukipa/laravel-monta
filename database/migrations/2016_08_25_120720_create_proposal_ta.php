<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProposalTa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proposal_ta', function (Blueprint $table) {
            $table->increments('id');
            $table->text('usulan_judul');
            $table->string('nilai_toefl');
            $table->string('path_nilai_toefl1');
            $table->string('path_nilai_toefl2')->nullable();
            $table->string('tahun_ajaran');

            $table->integer('user_id')
                    ->unsigned()
                    ->index();
            $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');

            $table->integer('kategori_proposal_id')
                    ->unsigned()
                    ->index();
            $table->foreign('kategori_proposal_id')
                    ->references('id')
                    ->on('kategori_proposal')
                    ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('proposal_ta');
    }
}

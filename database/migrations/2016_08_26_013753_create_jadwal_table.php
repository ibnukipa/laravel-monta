<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJadwalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jadwal', function (Blueprint $table) {
            $table->increments('id');

            $table->date('tanggal');
            
            $table->integer('tugas_akhir_id')
                    ->unsigned()
                    ->index();
            $table->foreign('tugas_akhir_id')
                    ->references('id')
                    ->on('tugas_akhir')
                    ->onDelete('cascade');
            
            $table->integer('jadwal_kategori_id')
                    ->unsigned()
                    ->index();
            $table->foreign('jadwal_kategori_id')
                    ->references('id')
                    ->on('jadwal_kategori')
                    ->onDelete('cascade');
                    
            $table->integer('jadwal_ruang_id')
                    ->unsigned()
                    ->index();
            $table->foreign('jadwal_ruang_id')
                    ->references('id')
                    ->on('jadwal_ruang')
                    ->onDelete('cascade');

            $table->integer('jadwal_jam_id')
                    ->unsigned()
                    ->index();
            $table->foreign('jadwal_jam_id')
                    ->references('id')
                    ->on('jadwal_jam')
                    ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('jadwal');
    }
}

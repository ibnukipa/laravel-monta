<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLabUsulanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lab_usulan', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('proposal_ta_id')
                    ->unsigned()
                    ->index();
            $table->foreign('proposal_ta_id')
                    ->references('id')
                    ->on('proposal_ta')
                    ->onDelete('cascade');
                    
            $table->integer('lab_id')
                    ->unsigned()
                    ->index();
            $table->foreign('lab_id')
                    ->references('id')
                    ->on('lab_ta')
                    ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lab_usulan');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTugasAkhirTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tugas_akhir', function (Blueprint $table) {
            $table->increments('id');
            $table->text('judul');
            $table->string('path_proposal')->nullable();
            $table->string('path_laporan_ta')->nullable();
            $table->string('path_berita_acara_kta')->nullable();
            $table->string('path_lembar_kegiatan_asistensi')->nullable();
            $table->string('path_form_perbaikan')->nullable();
            $table->string('path_borang_cek_format')->nullable();
            $table->string('tahun_ajaran');
            $table->string('semester');
            $table->string('progres_penulisan')->nullable();

            $table->integer('user_id')
                    ->unsigned()
                    ->index();
            $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');

            $table->integer('dosen_pembimbing_id')
                    ->unsigned()
                    ->index();
            $table->foreign('dosen_pembimbing_id')
                    ->references('id')
                    ->on('dosen')
                    ->onDelete('cascade');

            $table->integer('kategori_ta_id')
                    ->unsigned()
                    ->index()
                    ->nullable();
            $table->foreign('kategori_ta_id')
                    ->references('id')
                    ->on('kategori_ta')
                    ->onDelete('cascade');

            $table->integer('lab_ta_id')
                    ->unsigned()
                    ->index()
                    ->nullable();
            $table->foreign('lab_ta_id')
                    ->references('id')
                    ->on('lab_ta')
                    ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tugas_akhir');
    }
}

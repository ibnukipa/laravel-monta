<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPathVarifiToTugasAkhir extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tugas_akhir', function (Blueprint $table) {
            $table->string('path_verifi_proposal')->nullable()->after('path_borang_cek_format');
            $table->string('path_verifi_kemajuan')->nullable()->after('path_verifi_proposal');
            $table->string('path_verifi_sidang')->nullable()->after('path_verifi_kemajuan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tugas_akhir', function (Blueprint $table) {
            $table->dropColumn('path_verifi_proposal');
            $table->dropColumn('path_verifi_kemajuan');
            $table->dropColumn('path_verifi_sidang');
        });
    }
}

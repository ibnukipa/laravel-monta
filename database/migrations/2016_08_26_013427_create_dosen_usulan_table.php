<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDosenUsulanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dosen_usulan', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('proposal_ta_id')
                    ->unsigned()
                    ->index();
            $table->foreign('proposal_ta_id')
                    ->references('id')
                    ->on('proposal_ta')
                    ->onDelete('cascade');
                    
            $table->integer('dosen_id')
                    ->unsigned()
                    ->index();
            $table->foreign('dosen_id')
                    ->references('id')
                    ->on('dosen')
                    ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dosen_usulan');
    }
}

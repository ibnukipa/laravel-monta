<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKegiatanAsistensiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kegiatan_asistensi', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('tugas_akhir_id')
                    ->unsigned()
                    ->index();
            $table->foreign('tugas_akhir_id')
                    ->references('id')
                    ->on('tugas_akhir')
                    ->onDelete('cascade');

            $table->date('tanggal');
            $table->text('description');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('kegiatan_asistensi');
    }
}

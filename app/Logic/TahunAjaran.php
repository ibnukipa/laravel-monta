<?php namespace App\Logic;

class TahunAjaran {

    protected $tahun;
    protected $bulan;

    public function __construct() {
        $this->tahun = date("Y");
        $this->bulan = date("m");

        // dd($this->bulan);
    }

    public function getDirTahunAjaran() {
        if($this->bulan >= 7 )
            $pembilang = $this->tahun;
        else
            $pembilang = $this->tahun - 1;

        $penyebut = $pembilang + 1;

        return $pembilang .'-'. $penyebut;
    }

    public function getTahunAjaran() {
        if($this->bulan >= 7 ) {
            $pembilang = $this->tahun;
        }
        else {
            $pembilang = $this->tahun - 1;
        }

        $penyebut = $pembilang + 1;

        return $pembilang .'/'. $penyebut;
    }

    public function getSemester() {
        if($this->bulan >=7) {
            return "ganjil";
        } else {
            return "genap";
        }
    }
}
<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use App\Logic\User\CaptureIp;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['username', 'full_name', 'email', 'password', 'nohp', 'active'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public $warna = [
        'utama'         => '#3e464c',
        'keuda'         => '#8BC34A',
        'ketiga'        => '#7EB83D',
        'keempat'       => '#689F38',
    ];

    public function accountIsActive($code) {

        $user = User::where('activation_code', '=', $code)->first();

        $userIpAddress                          = new CaptureIp;
        $user->signup_confirmation_ip_address   = $userIpAddress->getClientIp();

        $user->active                           = 1;

        $user->activation_code                  = '';

        if($user->save()) {
            \Auth::login($user);
        }
        return true;
    }

    public function roles()
    {
        return $this->belongsToMany('App\Models\Role')->withTimestamps();
    }

    public function hasRole($name)
    {
        foreach($this->roles as $role)
        {
            if($role->name == $name) return true;
        }

        return false;
    }

    public function assignRole($role)
    {
        return $this->roles()->attach($role);
    }

    public function removeRole($role)
    {
        return $this->roles()->detach($role);
    }

    public function tugas_akhir() {
        return $this->hasOne('App\Models\TugasAkhir');
    }

    public function proposal() {
        return $this->hasOne('App\Models\Proposal');
    }

    public function catatan() {
        return $this->hasMany('App\Models\Catatan', 'source_id');
    }
    
}

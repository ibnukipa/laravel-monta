<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DosenUsulan extends Model {

	protected $table = 'dosen_usulan';

    protected $fillable = [
        'proposal_ta_id', 
        'dosen_id'
        ];

    public function proposal() {
        return $this->belongsTo('App\Models\Proposal', 'proposal_ta_id');
    }

    public function dosen() {
        return $this->belongsTo('App\Models\Dosen', 'dosen_id');
    }
}
<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KegiatanAsistensi extends Model {

	protected $table = 'kegiatan_asistensi';

    protected $fillable = ['tugas_akhir_id', 'tanggal', 'description'];

    public function tugas_akhir() {
        return $this->belongsTo('App\Models\TugasAkhir', 'tugas_akhir_id');
    }
    
}
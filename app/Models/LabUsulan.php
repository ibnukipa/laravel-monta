<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LabUsulan extends Model {

	protected $table = 'lab_usulan';

    protected $fillable = [
        'proposal_ta_id', 
        'lab_id'
        ];

    public function proposal() {
        return $this->belongsTo('App\Models\Proposal', 'proposal_ta_id');
    }

    public function lab() {
        return $this->belongsTo('App\Models\LabTA', 'lab_id');
    }
}
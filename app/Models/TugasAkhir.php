<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TugasAkhir extends Model {

	protected $table = 'tugas_akhir';

    protected $fillable = [
        'judul', 
        'status_id',
        'path_proposal',
        'user_id', 
        'dosen_pembimbing_id', 
        'kategori_ta_id', 
        'lab_ta_id',
        'tahun_ajaran'
    ];
    
    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function dosen_pembimbing(){
        return $this->belongsTo('App\Models\Dosen', 'dosen_pembimbing_id');
    }

    public function kategori_ta() {
        return $this->belongsTo('App\Models\KategoriTA', 'kategori_ta_id');
    }

    public function lab_ta() {
        return $this->belongsTo('App\Models\LabTA', 'lab_ta_id');
    }

    public function mata_kuliah_keahlian() {
        return $this->hasMany('App\Models\MataKuliahKeahlian', 'tugas_akhir_id');
    }

    public function kegiatan_asistensi() {
        return $this->hasMany('App\Models\KegiatanAsistensi', 'tugas_akhir_id');
    }

    public function status() {
        return $this->belongsTo('App\Models\StatusTugasAkhir', 'status_id');
    }

    public function jadwal() {
        return $this->hasMany('App\Models\Jadwal', 'tugas_akhir_id');
    }
    
}
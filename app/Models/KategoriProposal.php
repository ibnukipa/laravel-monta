<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KategoriProposal extends Model {

	protected $table = 'kategori_proposal';

    protected $fillable = ['name', 'description'];
    
}
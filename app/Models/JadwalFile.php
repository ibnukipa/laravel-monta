<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JadwalFile extends Model {

	protected $table = 'jadwal_file';

    protected $fillable = [
        'tahun_ajaran',
        'semester',
        'path_file'
    ];
}
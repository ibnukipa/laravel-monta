<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JadwalKategori extends Model {

	protected $table = 'jadwal_kategori';

    protected $fillable = ['name', 'description'];
    
}
<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MataKuliahKeahlian extends Model {

	protected $table = 'mata_kuliah_keahlian';

    protected $fillable = ['name', 'nilai', 'tugas_akhir_id'];

    public function tugas_akhir() {
        return $this->belongsTo('App\Models\TugasAkhir', 'tugas_akhir_id');
    }
    
}
<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Proposal extends Model {

	protected $table = 'proposal_ta';

    protected $fillable = [
        'usulan_judul', 
        'nilai_toefl', 
        'path_nilai_toefl1',
        'path_nilai_toefl2',
        'status_id',
        'user_id',
        'kategori_proposal_id',
        'tahun_ajaran'
        ];

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function kategori_proposal() {
        return $this->belongsTo('App\Models\KategoriProposal', 'kategori_proposal_id');
    }

    public function dosen_usulan() {
        return $this->hasMany('App\Models\DosenUsulan', 'proposal_ta_id');
    }

    public function lab_usulan() {
        return $this->hasMany('App\Models\LabUsulan', 'proposal_ta_id');
    }

    public function status() {
        return $this->belongsTo('App\Models\StatusProposal', 'status_id');
    }
}
<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Jadwal extends Model {

	protected $table = 'jadwal';

    protected $fillable = [
        'tanggal',
        'tugas_akhir_id',
        'jadwal_ketegori_id',
        'jadwal_ruang_id',
        'jadwal_jam_id'
    ];

    public function tugas_akhir() {
        return $this->belongsTo('App\Models\TugasAkhir', 'tugas_akhir_id');
    }

    public function jadwal_kategori() {
        return $this->belongsTo('App\Models\JadwalKategori', 'jadwal_kategori_id');
    }

    public function jadwal_ruang() {
        return $this->belongsTo('App\Models\JadwalRuang', 'jadwal_ruang_id');
    }

    public function jadwal_jam() {
        return $this->belongsTo('App\Models\JadwalJam', 'jadwal_jam_id');
    }

    public function dosen_penguji() {
        return $this->hasMany('App\Models\DosenPenguji', 'jadwal_id');
    }
    
}
<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LabTA extends Model {

	protected $table = 'lab_ta';

    protected $fillable = ['name', 'description'];
    
}
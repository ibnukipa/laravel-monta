<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StatusTugasAkhir extends Model {

	protected $table = 'status_tugas_akhir';

    protected $fillable = ['name'];
    
}
<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KategoriTA extends Model {

	protected $table = 'kategori_ta';

    protected $fillable = ['name', 'description'];
    
}
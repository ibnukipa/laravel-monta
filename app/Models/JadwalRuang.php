<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JadwalRuang extends Model {

	protected $table = 'jadwal_ruang';

    protected $fillable = ['name', 'description'];
    
}
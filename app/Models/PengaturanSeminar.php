<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PengaturanSeminar extends Model {

	protected $table = 'pengaturan_seminar';

    protected $fillable = [
        'tahun_ajaran',
        'tipe_seminar',
        'semester',
        'due_to',
        ];
}
<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DosenPenguji extends Model {

	protected $table = 'dosen_penguji';

    protected $fillable = [
        'jadwal_id',
        'dosen_id'
    ];

    public function jadwal() {
        return $this->belongsTo('App\Models\Jadwal', 'jadwal_id');
    }

    public function dosen() {
        return $this->belongsTo('App\Models\Dosen', 'dosen_id');
    }
    
}
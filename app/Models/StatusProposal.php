<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StatusProposal extends Model {

	protected $table = 'status_proposal';

    protected $fillable = ['name'];
    
}
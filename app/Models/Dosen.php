<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dosen extends Model {

	protected $table = 'dosen';

    protected $fillable = ['nip', 'full_name', 'phone', 'email', 'lab_ta_id'];

    public function lab_ta() {
        return $this->belongsTo('App\Models\LabTA', 'lab_ta_id');
    }
    
}
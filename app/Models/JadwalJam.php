<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JadwalJam extends Model {

	protected $table = 'jadwal_jam';

    protected $fillable = ['mulai', 'selesai'];
    
}
<?php
    // Dashboard
    Breadcrumbs::register('welcome', function($breadcrumbs){
        $breadcrumbs->push('Selamat datang, ' . Auth::user()->full_name, url('/'));
    });

    //Catatan
    Breadcrumbs::register('catatan-pendaftaran-proposal', function($breadcrumbs){
        $breadcrumbs->push('Catatan Pendaftaran Proposal TA');
    });

    Breadcrumbs::register('catatan-seminar-proposal', function($breadcrumbs){
        $breadcrumbs->push('Catatan Pengajuan Seminar Proposal TA');
    });

    Breadcrumbs::register('catatan-seminar-kemajuan', function($breadcrumbs){
        $breadcrumbs->push('Catatan Pengajuan Seminar Kemajuan TA');
    });

    Breadcrumbs::register('catatan-sidang', function($breadcrumbs){
        $breadcrumbs->push('Catatan Pengajuan Sidang Ujian Lisan TA');
    });

    //Pengaturan Waktu Seminar
    Breadcrumbs::register('pengaturan-waktu', function($breadcrumbs){
        $breadcrumbs->push('Pengaturan Waktu Seminar');
    });

        Breadcrumbs::register('pengaturan-waktu-create', function($breadcrumbs){
            $breadcrumbs->parent('pengaturan-waktu');
            $breadcrumbs->push('Tambah');
        });

    //Daftar Mahasiswa
    Breadcrumbs::register('daftar-mahasiswa', function($breadcrumbs){
        $breadcrumbs->push('Daftar Mahasiswa');
    });

        Breadcrumbs::register('upload-daftar-mahasiswa', function($breadcrumbs){
            $breadcrumbs->parent('daftar-mahasiswa');
            $breadcrumbs->push('Upload Daftar Mahasiswa');
        });

    //Proposal TA
    Breadcrumbs::register('proposal', function($breadcrumbs){
        $breadcrumbs->push('Proposal TA');
    });

        //Pendaftaran Proposal TA
        Breadcrumbs::register('pendaftaran-proposal', function($breadcrumbs){
            $breadcrumbs->parent('proposal');
            $breadcrumbs->push('Pendaftaran Proposal TA', url('/'));
        });

            Breadcrumbs::register('pendaftaran-proposal-create', function($breadcrumbs){
                $breadcrumbs->parent('pendaftaran-proposal');
                $breadcrumbs->push('Formulir Pendafataran Proposal TA', url('/'));
            });

        Breadcrumbs::register('pendaftaran-proposal-list', function($breadcrumbs){
            $breadcrumbs->parent('proposal');
            $breadcrumbs->push('List Pendafatar Proposal TA', url('/'));
        });

        //Pengajuan Seminar Proposal TA
        Breadcrumbs::register('pendaftaran-seminar-proposal', function($breadcrumbs){
            $breadcrumbs->parent('proposal');
            $breadcrumbs->push('Pengajuan Seminar Proposal TA', url('/'));
        });

        Breadcrumbs::register('pendaftaran-seminar-proposal-create', function($breadcrumbs){
            $breadcrumbs->parent('proposal');
            $breadcrumbs->push('Formulir Seminar Proposal TA', url('/'));
        });

        Breadcrumbs::register('pendaftaran-seminar-proposal-list', function($breadcrumbs){
            $breadcrumbs->parent('proposal');
            $breadcrumbs->push('List Pendafatar Seminar Proposal TA', url('/'));
        });

        Breadcrumbs::register('pendaftaran-seminar-proposal-jadwal', function($breadcrumbs){
            $breadcrumbs->parent('proposal');
            $breadcrumbs->push('Jadwal Seminar Proposal TA', url('/'));
        });

            Breadcrumbs::register('pendaftaran-seminar-proposal-jadwal-create', function($breadcrumbs){
                $breadcrumbs->parent('pendaftaran-seminar-proposal-jadwal');
                $breadcrumbs->push('Tambah', url('/'));
            });
        
        Breadcrumbs::register('pendaftaran-seminar-proposal-file-jadwal', function($breadcrumbs){
            $breadcrumbs->parent('proposal');
            $breadcrumbs->push('File Jadwal Seminar Proposal TA', url('/'));
        });

        Breadcrumbs::register('pendaftaran-seminar-proposal-on', function($breadcrumbs){
            $breadcrumbs->parent('proposal');
            $breadcrumbs->push('Seminar Proposal TA', url('/'));
        });

    //Asisteni TA
    Breadcrumbs::register('asistensi', function($breadcrumbs){
        $breadcrumbs->push('Asistensi TA');
    });

        Breadcrumbs::register('asistensi-kegiatan', function($breadcrumbs){
            $breadcrumbs->parent('asistensi');
            $breadcrumbs->push('Kegiatan Asistensi TA');
        });

        Breadcrumbs::register('asistensi-kegiatan-create', function($breadcrumbs){
            $breadcrumbs->parent('asistensi');
            $breadcrumbs->push('Membuat Kegiatan Asistensi TA');
        });

    //Sidang TA
    Breadcrumbs::register('sidang', function($breadcrumbs){
        $breadcrumbs->push('Sidang TA');
    });
        Breadcrumbs::register('pendaftaran-sidang', function($breadcrumbs){
            $breadcrumbs->parent('sidang');
            $breadcrumbs->push('Pengajuan Sidang Ujian Lisan TA', url('/'));
        });

        Breadcrumbs::register('pendaftaran-sidang-create', function($breadcrumbs){
            $breadcrumbs->parent('sidang');
            $breadcrumbs->push('Formulir Sidang Ujian Lisan TA', url('/'));
        });

        Breadcrumbs::register('pendaftaran-sidang-list', function($breadcrumbs){
            $breadcrumbs->parent('sidang');
            $breadcrumbs->push('List Pendafatar Sidang Ujian Lisan TA', url('/'));
        });

        Breadcrumbs::register('pendaftaran-sidang-jadwal', function($breadcrumbs){
            $breadcrumbs->parent('sidang');
            $breadcrumbs->push('Jadwal Sidang Ujian Lisan TA', url('/'));
        });

            Breadcrumbs::register('pendaftaran-sidang-jadwal-create', function($breadcrumbs){
                $breadcrumbs->parent('pendaftaran-sidang-jadwal');
                $breadcrumbs->push('Tambah', url('/'));
            });
        
        Breadcrumbs::register('pendaftaran-sidang-file-jadwal', function($breadcrumbs){
            $breadcrumbs->parent('sidang');
            $breadcrumbs->push('File Jadwal Sidang Ujian Lisan TA', url('/'));
        });

        Breadcrumbs::register('pendaftaran-sidang-on', function($breadcrumbs){
            $breadcrumbs->parent('sidang');
            $breadcrumbs->push('Sidang Ujian Lisan TA', url('/'));
        });

    //Kemajuan TA
    Breadcrumbs::register('kemajuan', function($breadcrumbs){
        $breadcrumbs->push('Kemajuan TA');
    });
        
        Breadcrumbs::register('pendaftaran-seminar-kemajuan', function($breadcrumbs){
            $breadcrumbs->parent('kemajuan');
            $breadcrumbs->push('Pengajuan Seminar Kemajuan TA', url('/'));
        });

        Breadcrumbs::register('pendaftaran-seminar-kemajuan-create', function($breadcrumbs){
            $breadcrumbs->parent('kemajuan');
            $breadcrumbs->push('Formulir Seminar Kemajuan TA', url('/'));
        });

        Breadcrumbs::register('pendaftaran-seminar-kemajuan-list', function($breadcrumbs){
            $breadcrumbs->parent('kemajuan');
            $breadcrumbs->push('List Pendafatar Seminar Kemajuan TA', url('/'));
        });

        Breadcrumbs::register('pendaftaran-seminar-kemajuan-jadwal', function($breadcrumbs){
            $breadcrumbs->parent('kemajuan');
            $breadcrumbs->push('Jadwal Seminar Kemajuan TA', url('/'));
        });

            Breadcrumbs::register('pendaftaran-seminar-kemajuan-jadwal-create', function($breadcrumbs){
                $breadcrumbs->parent('pendaftaran-seminar-kemajuan-jadwal');
                $breadcrumbs->push('Tambah', url('/'));
            });
        
        Breadcrumbs::register('pendaftaran-seminar-kemajuan-file-jadwal', function($breadcrumbs){
            $breadcrumbs->parent('kemajuan');
            $breadcrumbs->push('File Jadwal Seminar Kemajuan TA', url('/'));
        });

        Breadcrumbs::register('pendaftaran-seminar-kemajuan-on', function($breadcrumbs){
            $breadcrumbs->parent('kemajuan');
            $breadcrumbs->push('Seminar Kemajuan TA', url('/'));
        });

?>
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Jadwal;
use App\Models\JadwalFile;
use App\Models\JadwalRuang;
use App\Models\JadwalKategori;
use App\Models\JadwalJam;
use App\Models\DosenPenguji;
use App\Models\User;
use App\Models\TugasAkhir;
use App\Models\Proposal;
use App\Logic\TahunAjaran;

use File;

class JadwalController extends Controller
{
    protected $notif_color = 'grey-dark';
    protected $notes_color = 'blue-dark';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index($kategori)
    {
        
        if($kategori == 'seminar-proposal') {
            $users = User::whereHas('tugas_akhir', function($q) {
                            $q->whereHas('jadwal', function($q) {
                                $q->where('jadwal.jadwal_kategori_id', '=', 1);
                            });
                        })
                        ->whereHas('proposal', function($q) {
                            $q      ->where('proposal_ta.status_id', '>=', 10)
                                    ->where('proposal_ta.status_id', '<=', 13)
                                    ->where('proposal_ta.status_id', '!=', 12);
                        })
                        ->get();
            
            return view('jadwal.proposal.showAll')->with([
                'm_proposal_ta' => true,
                'users'             => $users,
            ]);

        } else if($kategori == 'seminar-kemajuan') {
            // dd($kategori);
            $users = User::whereHas('tugas_akhir', function($q) {
                            $q->whereHas('jadwal', function($q) {
                                $q->where('jadwal.jadwal_kategori_id', '=', 2);
                            })
                            ->where('tugas_akhir.status_id', '>=', 5)
                            ->where('tugas_akhir.status_id', '<=', 7)
                            ;
                        })
                        ->with(['tugas_akhir' => function($q) {
                                $q->with(['jadwal' => function($q) {
                                    $q->where('jadwal.jadwal_kategori_id', '=', 2);
                                }]);
                            }])
                        // ->whereHas('proposal', function($q) {
                        //     $q      ->where('proposal_ta.status_id', '>=', 10)
                        //             ->where('proposal_ta.status_id', '<=', 13)
                        //             ->where('proposal_ta.status_id', '!=', 12);
                        // })
                        ->get();
            
            return view('jadwal.kemajuan.showAll')->with([
                'm_kemajuan_ta'     => true,
                'users'             => $users,
            ]);
        } else if($kategori == 'sidang') {
            $users = User::whereHas('tugas_akhir', function($q) {
                            $q->whereHas('jadwal', function($q) {
                                $q->where('jadwal.jadwal_kategori_id', '=', 3);
                            })
                            ->where('tugas_akhir.status_id', '>=', 14)
                            ->where('tugas_akhir.status_id', '<=', 16)
                            ;
                        })
                        ->with(['tugas_akhir' => function($q) {
                                $q->with(['jadwal' => function($q) {
                                    $q->where('jadwal.jadwal_kategori_id', '=', 3);
                                }]);
                            }])
                        ->get();
            
            return view('jadwal.sidang.showAll')->with([
                'm_sidang_ta' => true,
                'users'             => $users,
            ]);
        }

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($kategori)
    {
        
        if($kategori == 'seminar-proposal') {
            $jadwalRuang        = JadwalRuang::select('id', \DB::raw('CONCAT(name, " - ", description) AS name'))
                                    ->lists('name', 'id');
            $jadwalJam          = JadwalJam::where('kategori_id', '=', 1)
                                    ->select('id', \DB::raw('CONCAT(mulai, " - ", selesai) AS jam'))
                                    ->lists('jam', 'id');

            $mahasiswaList      = User::join('proposal_ta', 'users.id', '=', 'proposal_ta.user_id')
                                    ->join('tugas_akhir', 'users.id', '=', 'tugas_akhir.user_id')
                                    ->whereNotIn('tugas_akhir.id', function($q){
                                            $q->select('jadwal.tugas_akhir_id')
                                            ->from('jadwal')
                                            ->where('jadwal.jadwal_kategori_id', '=', 1)
                                            ;
                                        })
                                    ->where('proposal_ta.status_id', '>=', 10)
                                    ->where('proposal_ta.status_id', '<=', 13)
                                    ->where('proposal_ta.status_id', '!=', 12)
                                    ->select(
                                        'users.id',
                                        \DB::raw('CONCAT(users.username, " - ", full_name) AS name')
                                    )
                                    ->lists('name', 'users.id');
            return view('form.jadwal.create-proposal', [
                'mahasiswaList'         => $mahasiswaList,
                'jadwalRuang'           => $jadwalRuang,
                'jadwalJam'             => $jadwalJam
            ]);
        } else if($kategori == 'seminar-kemajuan') {
            $jadwalRuang        = JadwalRuang::select('id', \DB::raw('CONCAT(name, " - ", description) AS name'))
                                    ->lists('name', 'id');
            $jadwalJam          = JadwalJam::where('kategori_id', '=', 2)
                                    ->select('id', \DB::raw('CONCAT(mulai, " - ", selesai) AS jam'))
                                    ->lists('jam', 'id');

            $mahasiswaList      = User::
                                    // join('proposal_ta', 'users.id', '=', 'proposal_ta.user_id')
                                    // ->
                                    join('tugas_akhir', 'users.id', '=', 'tugas_akhir.user_id')
                                    ->whereNotIn('tugas_akhir.id', function($q){
                                            $q->select('jadwal.tugas_akhir_id')
                                            ->from('jadwal')
                                            ->where('jadwal.jadwal_kategori_id', '=', 2)
                                            ;
                                        })
                                    ->where('tugas_akhir.status_id', '>=', 5)
                                    ->where('tugas_akhir.status_id', '<=', 7)
                                    ->select(
                                        'users.id',
                                        \DB::raw('CONCAT(users.username, " - ", full_name) AS name')
                                    )
                                    ->lists('name', 'users.id');
            return view('form.jadwal.create-kemajuan', [
                'mahasiswaList'         => $mahasiswaList,
                'jadwalRuang'           => $jadwalRuang,
                'jadwalJam'             => $jadwalJam
            ]);
        } else if($kategori == 'sidang') {
            $jadwalRuang        = JadwalRuang::select('id', \DB::raw('CONCAT(name, " - ", description) AS name'))
                                    ->lists('name', 'id');
            $jadwalJam          = JadwalJam::where('kategori_id', '=', 3)
                                    ->select('id', \DB::raw('CONCAT(mulai, " - ", selesai) AS jam'))
                                    ->lists('jam', 'id');

            $mahasiswaList      = User::
                                    join('tugas_akhir', 'users.id', '=', 'tugas_akhir.user_id')
                                    ->whereNotIn('tugas_akhir.id', function($q){
                                            $q->select('jadwal.tugas_akhir_id')
                                            ->from('jadwal')
                                            ->where('jadwal.jadwal_kategori_id', '=', 3)
                                            ;
                                        })
                                    ->where('tugas_akhir.status_id', '>=', 14)
                                    ->where('tugas_akhir.status_id', '<=', 16)
                                    ->select(
                                        'users.id',
                                        \DB::raw('CONCAT(users.username, " - ", full_name) AS name')
                                    )
                                    ->lists('name', 'users.id');
            return view('form.jadwal.create-sidang', [
                'mahasiswaList'         => $mahasiswaList,
                'jadwalRuang'           => $jadwalRuang,
                'jadwalJam'             => $jadwalJam
            ]);
        }
    }

    public function file($kategori) {
        if($kategori == 'seminar-proposal') {
            $fileJadwal = JadwalFile::where('tipe', '=', 'seminar-proposal')->orderBy('created_at', 'asc')->get();
            return view('jadwal.proposal.showAllFile')->with([
                'fileJadwal'     => $fileJadwal,
            ]);
        } else if($kategori == 'seminar-kemajuan') {
            $fileJadwal = JadwalFile::where('tipe', '=', 'seminar-kemajuan')->orderBy('created_at', 'asc')->get();
            return view('jadwal.kemajuan.showAllFile')->with([
                'fileJadwal'    => $fileJadwal,
            ]);
        } else if($kategori == 'sidang') {
            $fileJadwal = JadwalFile::where('tipe', '=', 'sidang')->orderBy('created_at', 'asc')->get();
            return view('jadwal.sidang.showAllFile')->with([
                'fileJadwal'    => $fileJadwal,
            ]);
        }
    }

    public function form_upload($kategori) {
        $tahunAjaran = new TahunAjaran;
        if($kategori == 'seminar-proposal') {
            return view('form.modal.upload-file-jadwal')->with([
                'kategori'          => 'Seminar Proposal',
                'realkategori'      => 'seminar-proposal',
                'tahunAjaran'       => $tahunAjaran->getTahunAjaran(),
                'semester'          => $tahunAjaran->getSemester()
            ]);
        } else if($kategori == 'seminar-kemajuan') {
            return view('form.modal.upload-file-jadwal')->with([
                'kategori'          => 'Seminar Kemajuan',
                'realkategori'      => 'seminar-kemajuan',
                'tahunAjaran'       => $tahunAjaran->getTahunAjaran(),
                'semester'          => $tahunAjaran->getSemester()
            ]);
        } else if($kategori == 'sidang') {
            return view('form.modal.upload-file-jadwal')->with([
                'kategori'          => 'Sidang Ujian Lisan',
                'realkategori'      => 'sidang',
                'tahunAjaran'       => $tahunAjaran->getTahunAjaran(),
                'semester'          => $tahunAjaran->getSemester()
            ]);
        }
    }

    public function form_upload_store(Request $request) {

        $tahunAjaran    = new TahunAjaran;
        $file = $request->file('jadwal_file');

        // $isJadwalExist              = JadwalFile::where('tahun_ajaran','=',$request->input('tahun_ajaran'))
        //                                             ->where('semester', '=', $request->input('semester'))
        //                                             ->first();
        // if($isJadwalExist) {
        //     $jadwalFile = $isJadwalExist;
        // } else {
        $jadwalFile                 = new JadwalFile;
        $jadwalFile->name           = $request->input('name');
        $jadwalFile->tipe           = $request->input('kategori');
        $jadwalFile->tahun_ajaran   = $request->input('tahun_ajaran');
        $jadwalFile->semester       = $request->input('semester');
        // }

        if($file)
        {
            chmod('uploads/jadwal-seminar/', 0775);
            $destinationPath    = 'uploads/jadwal-seminar/'.$tahunAjaran->getDirTahunAjaran();
            $extension          = $file->clientExtension();
            $filename           = $request->input('kategori').'_'.$tahunAjaran->getDirTahunAjaran().'_'.$request->input('semester').'.'.$extension;
            if (!file_exists($destinationPath)) {
                mkdir($destinationPath, 0775);
            }
            chmod($destinationPath, 0775);
            $file->move($destinationPath, $filename);

            $jadwalFile->path_file = base_path().'/'.$destinationPath.'/'.$filename;
        }

        if($jadwalFile->save()) {
            return redirect()->route('jadwal.index', [$request->input('kategori')])->with('status', [
                    'color'     => $this->notif_color,
                    'content'   => 'Berhasil Upload file Jadwal!', 
                    ]
            );
        } else {
            return redirect()->route('jadwal.index', [$request->input('kategori')])->with('status', [
                    'color'     => $this->notif_color,
                    'content'   => 'Gagal Upload file Jadwal!', 
                    ]
            );
        }
    }

    public function file_destroy($id) {
        $jadwalFile = JadwalFile::find($id); 
        File::delete($jadwalFile->path_file);
        $jadwalFile->delete();
        
        return redirect()->back()->with('status', [
                'color'     => $this->notif_color,
                'content'   => 'Berhasil menghapus File Jadwal!', 
                ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($kategori, Request $request)
    {

        $user                       = User::find($request->input('mahasiswa'));
        $tugasAkhir                 = $user->tugas_akhir;
        $newJadwal                  = new Jadwal;
        $newJadwal->tanggal         = date('Y-m-d',strtotime($request->input('tanggal_seminar')));
        $newJadwal->jadwal_ruang_id = $request->input('ruang_seminar');
        $newJadwal->jadwal_jam_id   = $request->input('jam_seminar');
        
        if($kategori == 'seminar-proposal') {
            if($user->proposal->status_id != 13)
                $user->proposal->status_id = 11;
            $user->proposal->save();
            $newJadwal->jadwal_kategori_id   = 1;
            
            $tugasAkhir->jadwal()->save($newJadwal);

            $dosenPenguji1              = new DosenPenguji;
            $dosenPenguji1->dosen_id    = $request->input('dosen_penguji1');

            $dosenPenguji2              = new DosenPenguji;
            $dosenPenguji2->dosen_id    = $request->input('dosen_penguji2');

            $dosenPenguji3              = new DosenPenguji;
            $dosenPenguji3->dosen_id    = $request->input('dosen_penguji3');

            $newJadwal->dosen_penguji()->save($dosenPenguji1);
            $newJadwal->dosen_penguji()->save($dosenPenguji2);
            $newJadwal->dosen_penguji()->save($dosenPenguji3);

            return redirect()->route('jadwal.index', ['seminar-proposal'])->with('status', [
                    'color'     => $this->notif_color,
                    'content'   => 'Berhasil membuat Jadwal Seminar Proposal TA!', 
                    ]
            );
        } else if($kategori == 'seminar-kemajuan') {
            $user->tugas_akhir->status_id = 6;
            $user->tugas_akhir->save();

            $newJadwal->jadwal_kategori_id   = 2;
            
            $tugasAkhir->jadwal()->save($newJadwal);

            $dosenPenguji1              = new DosenPenguji;
            $dosenPenguji1->dosen_id    = $request->input('dosen_penguji1');

            $dosenPenguji2              = new DosenPenguji;
            $dosenPenguji2->dosen_id    = $request->input('dosen_penguji2');

            $dosenPenguji3              = new DosenPenguji;
            $dosenPenguji3->dosen_id    = $request->input('dosen_penguji3');

            $newJadwal->dosen_penguji()->save($dosenPenguji1);
            $newJadwal->dosen_penguji()->save($dosenPenguji2);
            $newJadwal->dosen_penguji()->save($dosenPenguji3);

            return redirect()->route('jadwal.index', ['seminar-kemajuan'])->with('status', [
                    'color'     => $this->notif_color,
                    'content'   => 'Berhasil membuat Jadwal Seminar Kemajuan TA!', 
                    ]
            );
        } else if($kategori == 'sidang') {

            $user->tugas_akhir->status_id = 15;
            $user->tugas_akhir->save();

            $newJadwal->jadwal_kategori_id   = 3;
            
            $tugasAkhir->jadwal()->save($newJadwal);

            $dosenPenguji1              = new DosenPenguji;
            $dosenPenguji1->dosen_id    = $request->input('dosen_penguji1');

            $dosenPenguji2              = new DosenPenguji;
            $dosenPenguji2->dosen_id    = $request->input('dosen_penguji2');

            $dosenPenguji3              = new DosenPenguji;
            $dosenPenguji3->dosen_id    = $request->input('dosen_penguji3');

            $newJadwal->dosen_penguji()->save($dosenPenguji1);
            $newJadwal->dosen_penguji()->save($dosenPenguji2);
            $newJadwal->dosen_penguji()->save($dosenPenguji3);

            return redirect()->route('jadwal.index', ['sidang'])->with('status', [
                    'color'     => $this->notif_color,
                    'content'   => 'Berhasil membuat Jadwal Sidang Ujian Lisan TA!', 
                    ]
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $jadwal             = Jadwal::find($id);
        $tugasAkhir         = TugasAkhir::find($jadwal->tugas_akhir_id);
        $user               = User::
                                where('users.id', '=', $tugasAkhir->user_id)
                                ->get();
        
        return view('form.jadwal.detail-proposal')->with([
            'user'      => $user,
            'jadwal'    => $jadwal,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jadwal = Jadwal::find($id);
        $tugasAkhir = TugasAkhir::find($jadwal->tugas_akhir_id);

        if($jadwal->jadwal_kategori_id == 1) {
            $proposal   = Proposal::where('user_id', '=', $tugasAkhir->user_id)->first();
            $proposal->status_id = 10;
            $proposal->save();
            $jadwal->delete();

            return redirect()->route('jadwal.index', ['seminar-proposal'])->with('status', [
                    'color'     => $this->notif_color,
                    'content'   => 'Berhasil menghapus Jadwal Seminar Proposal TA!', 
                    ]
            );

        } else if($jadwal->jadwal_kategori_id == 2) {
            $tugasAkhir->status_id = 5;
            $tugasAkhir->save();

            $jadwal->delete();

            return redirect()->route('jadwal.index', ['seminar-kemajuan'])->with('status', [
                    'color'     => $this->notif_color,
                    'content'   => 'Berhasil menghapus Jadwal Seminar Kemajuan TA!', 
                    ]
            );
        }

        
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\User;
use App\Models\Dosen;
use App\Models\Jadwal;
use App\Models\JadwalRuang;

class AjaxController extends Controller
{
    public function getDosenPembimbing(Request $request) {
        $user               = User::find($request->input('user_id'));
        $tugas_akhir        = $user->tugas_akhir;
        $dosenPembimbing    = $tugas_akhir->dosen_pembimbing;
        return response()->json([
            'dosenPembimbing'   => $dosenPembimbing,
            'tugas_akhir'       => $tugas_akhir
        ]);
    }

    public function getRuangan(Request $request) {
        // $tanggal        = date('Y-m-d',strtotime($request->input('tanggal')));
        // $jadwalRuang        = JadwalRuang::
        //                             whereNotIn('id', function($q) use ($tanggal) {
        //                                 $q
        //                                 ->select('jadwal_ruang.id')
        //                                 ->from('jadwal_ruang')
        //                                 ->where(function($q) use ($tanggal) { $q->distinct()->select(\DB::raw('COUNT(jadwal.jadwal_jam_id)'))->from('jadwal')->where('jadwal.tanggal', '=', $tanggal)->get(); } 
        //                                     , '=', 
        //                                     function($q) {
        //                                         $q->select(\DB::raw('COUNT(jadwal_jam.id)')->from('jadwal_jam'));
        //                                     }
        //                                 )->get()
        //                                 ;
        //                             })
        //                             ->select('id', \DB::raw('CONCAT(name, " - ", description) AS name'))
        //                             ->get();
        // $hasil = \DB::table('jadwal_ruang')
        //             ->select('jadwal_ruang.id')
        //             ->having(function($q) use ($tanggal) { $q->distinct()->select(\DB::raw('COUNT(jadwal.jadwal_jam_id)'))->from('jadwal')->where('jadwal.tanggal', '=', $tanggal)->get(); } 
        //                                     , '=', 
        //                                     function($q) {
        //                                         $q->select(\DB::raw('COUNT(jadwal_jam.id)'))->from('jadwal_jam')->get();
        //                                     }
        //                                 )
        //             ->get();

        // $hasil = Jadwal::distinct()->select(\DB::raw('COUNT(jadwal.jadwal_jam_id) as jumlahid'))->where('jadwal.tanggal', '=', $tanggal)->get();
        // dd($hasil);
        // return response()->json($jadwalRuang);
    }

    public function getDosenPenguji(Request $request) {
        $tanggal        = date('Y-m-d',strtotime($request->input('tanggal')));
        $ruang          = $request->input('ruang');
        $jam            = $request->input('jam');
        $kategori       = $request->input('kategori');
        
        $dosenList      = Dosen::
                            whereNotIn('id', function($q) use ($tanggal, $ruang, $jam, $kategori){
                                $q->select('dosen_penguji.dosen_id')
                                ->from('jadwal')
                                ->leftJoin('dosen_penguji', 'jadwal.id', '=', 'dosen_penguji.jadwal_id')
                                ->where('jadwal.jadwal_kategori_id', '=', $kategori)
                                ->where('jadwal.tanggal', '=', $tanggal)
                                ->Where('jadwal.jadwal_jam_id', '=', $jam)
                                ;
                            })
                            ->select('dosen.id', 'dosen.full_name')
                            ->get()
                            ;
        
        return response()->json($dosenList);
    }
}

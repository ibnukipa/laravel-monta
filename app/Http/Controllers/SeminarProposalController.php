<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Dosen;
use App\Models\KategoriProposal;
use App\Models\KategoriTA;
use App\Models\MataKuliahKeahlian;
use App\Models\TugasAkhir;
use App\Models\Proposal;
use App\Models\User;
use App\Models\Catatan;
use App\Models\PengaturanSeminar;
use App\Models\JadwalFile;
use App\Models\Jadwal;
use App\Logic\TahunAjaran;

use File;

class SeminarProposalController extends Controller
{
    protected $notif_color = 'grey-dark';
    protected $notes_color = 'blue-dark';

    protected $tahunAjaran;

    public function __construct() {
        $this->tahunAjaran = new TahunAjaran;

        if(\Auth::user()->hasRole('mahasiswa')) {
            if(!\Auth::user()->proposal || \Auth::user()->proposal->status_id < 5 || \Auth::user()->proposal->status_id == 15) {
                return redirect('/')->send();
            }
        }
    }

    public function change($id, $status) {
        $user = User::find($id);
        $tugasAkhir = $user->tugas_akhir;
        
        $proposal   = $user->proposal;
        $jadwal     = Jadwal::where('tugas_akhir_id', '=', $tugasAkhir->id)->get();
            
        if(!$jadwal->isEmpty()) {
            $jadwal = $jadwal[0];
        } else {
            $jadwal = null;
        }
            
        if($status == 'ulang'){
            $proposal->status_id = 13;
            $proposal->save();
            
            if($jadwal) {    
                $jadwal->delete();
            }
            
        } else if($status == 'lolos') {
            $proposal->status_id = 14;
            $proposal->save();
        } else if($status == 'batal') {
            $proposal->status_id = 15;
            $proposal->save();
            $tugasAkhir->delete();
        }

        return redirect()->route('seminar-proposal.on')->with('status', [
                'color'     => $this->notif_color,
                'content'   => 'Berhasil merubah data Seminar Proposal TA!', 
                ]
        );
    }

    public function showAllOn() {
        // if($kategori == 'seminar-proposal') {
            $users = User::
                        // whereHas('tugas_akhir', function($q) {
                        //     $q->has('jadwal');
                        // })
                        // ->
                        with([
                                        'tugas_akhir' => function($q) {
                                            $q->with([
                                                    'jadwal' => function($q) {
                                                        $q->
                                                        with(['jadwal_kategori', 'jadwal_ruang', 'jadwal_jam'])
                                                        ->where('jadwal.jadwal_kategori_id', '=', 1);
                                                    },
                                                ]);
                                        },
                                    ])
                        ->join('proposal_ta', 'users.id', '=', 'proposal_ta.user_id')
                        ->where('proposal_ta.status_id', '=', 10)
                        ->orWhere('proposal_ta.status_id', '=', 11)
                        ->orWhere('proposal_ta.status_id', '=', 13)
                        ->select('users.*')
                        ->get();
            // dd($users);
            return view('seminar-proposal.showAllOn')->with([
                'm_proposal_ta' => true,
                'users'             => $users,
            ]);

        // }
    }

    public function showAllSubmited() {
        $users              = User::join('tugas_akhir', 'users.id','=','tugas_akhir.user_id')
                                ->join('proposal_ta', 'users.id', '=', 'proposal_ta.user_id')
                                ->join('dosen', 'tugas_akhir.dosen_pembimbing_id', '=', 'dosen.id')
                                ->where('proposal_ta.status_id', '=', '7')
								->select(
                                    'users.*', 
                                    'tugas_akhir.*', 
                                    'proposal_ta.*', 
                                    'users.id as userId', 
                                    'proposal_ta.id as proposalId', 
                                    'proposal_ta.status_id as statusProposal',
                                    'tugas_akhir.id as tugasAkhirId',
                                    'tugas_akhir.status_id as statusTugasAkhir',
                                    'dosen.full_name as dosenName'
                                )
                                ->get();
        
        return view('seminar-proposal.showAllSubmited')->with([
            'm_proposal_ta' => true,
            'users'             => $users,
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        
        $pengaturanSeminar      = PengaturanSeminar::
                                        where('tahun_ajaran', '=', $this->tahunAjaran->getTahunAjaran())
                                        ->where('semester', '=', $this->tahunAjaran->getSemester())
                                        ->where('tipe_seminar', '=', 'proposal')
                                        ->first();
        if($pengaturanSeminar && (\Auth::user()->proposal->status_id <= 6)) {
            if($pengaturanSeminar->due_to <= date("d-m-Y")) {
                return view('seminar-proposal.index')->with([
                    'm_proposal_ta' => true,
                    'lewat'     => true,
                    'tipeSeminar'  => 'Seminar Proposal TA'
                ]);
            }
        } else if(\Auth::user()->proposal->status_id <= 6 ) {
            return view('seminar-proposal.index')->with([
                'm_proposal_ta' => true,
                'belum'         => true,
                'tipeSeminar'  => 'Seminar Proposal TA'
            ]);
        }

        $arrayWith      = [];
        if(\Auth::user()->proposal->status_id == 5 ) {
            $arrayWith = [
                'm_proposal_ta' => true,
                'status'    => [
                    'color'     => $this->notes_color,
                    'content'   => 'Silahkan membuat Pengajuan Seminar Proposal TA.'
                ],
            ];
        } else if(\Auth::user()->proposal->status_id == 6 || \Auth::user()->proposal->status_id == 8) {
            $kategoriTA                 = KategoriTA::lists('description', 'id');
            
            $path_proposal              = FileController::getPath('proposal_ta');
            $path_verifi_proposal       = FileController::getPath('verifi_proposal');
            $tugas_akhir                = TugasAkhir::find(\Auth::user()->tugas_akhir->id);
            
            if(file_exists($path_proposal))
                $file_proposal          = new \Symfony\Component\HttpFoundation\File\File($path_proposal);
            
            if(file_exists($path_verifi_proposal))
                $file_verifi_proposal   = new \Symfony\Component\HttpFoundation\File\File($path_verifi_proposal);

            $arrayWith = [
                'm_proposal_ta' => true,
                'status'    => [
                    'color'     => $this->notes_color,
                    'content'   => 'Submit segera Formulir Seminar Proposal TA Anda agar bisa doproses oleh Koordinator TA.'
                ],
                'kategoriTA'    => $kategoriTA,
                'tugas_akhir'   => $tugas_akhir,
                'file_proposal'    => [
                        'name'      => basename($path_proposal),
                        'type'      => $file_proposal->getMimeType(),
                        'size'      => $file_proposal->getSize()
                ],
                'file_verifi_proposal'    => [
                        'name'      => basename($path_verifi_proposal),
                        'type'      => $file_verifi_proposal->getMimeType(),
                        'size'      => $file_verifi_proposal->getSize()
                ],
            ];
        } else if(\Auth::user()->proposal->status_id == 7) {
            $arrayWith = [
                'm_proposal_ta' => true,
                'status'    => [
                    'color'     => $this->notes_color,
                    'content'   => 'Pengajuan Seminar Proposal TA Anda sedang diproses.'
                ]
            ];
        } else if(\Auth::user()->proposal->status_id >= 10) {
            $arrayWith = [
                'status'    => [
                    'color'     => $this->notes_color,
                    'content'   => 'Selamat! Pengajuan Seminar Proposal TA Anda telah disetujui.'
                ],
                'user'          => User::where('users.id', '=', \Auth::user()->id)->get(),
                'm_proposal_ta' => true
            ];
        } else {
            return redirect()->back();
        }
        
        return view('seminar-proposal.index')->with($arrayWith);
    }

    public function jadwal() {
        $user = \Auth::user();
        $jadwalProposal = null;
        if(!$user->tugas_akhir->jadwal->isEmpty()) {
            foreach ($user->tugas_akhir->jadwal as $key => $jadwal) {
                if($jadwal->jadwal_kategori->name === 'seminar-proposal')
                    $jadwalProposal = $jadwal;
            }
        }

        $arrayWith = [
                // 'status'    => [
                //     'color'     => $this->notes_color,
                //     'content'   => 'Selamat! Pengajuan Seminar Proposal TA Anda telah disetujui.'
                // ],
                'm_proposal_ta' => true,
                'user'          => User::where('users.id', '=', \Auth::user()->id)->get(),
                'jadwal'        => $jadwalProposal
            ];
        return view('seminar-proposal.jadwal')->with($arrayWith);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if(!$request->ajax()) {
            return redirect('/');
        }
        $kategoriTA                 = KategoriTA::lists('description', 'id');
        
        return view('seminar-proposal.create')->with([
            'judul'             => 'Formulir Seminar Proposal TA',
            'kategoriTA'        => $kategoriTA,
        ]);
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $user                       = \Auth::user();
        $tugasAkhir                 = $user->tugas_akhir;
        $proposal                   = $user->proposal;
        
        $fileProposalTA             = $request->file('proposal_ta');
        $fileVerifiProposal         = $request->file('verifikasi_dosbing');
        
        if($fileProposalTA)
        {
            $destinationPath    = 'uploads/tugas-akhir/'. $this->tahunAjaran->getDirTahunAjaran();
            $extension          = $fileProposalTA->clientExtension();
            $filename           = $user->username.'.'.$extension;
            if (!file_exists($destinationPath)) {
                mkdir($destinationPath, 0775);
            }
            chmod($destinationPath, 0775);
            $destinationPath    = 'uploads/tugas-akhir/'. $this->tahunAjaran->getDirTahunAjaran() .'/proposal';
            if (!file_exists($destinationPath)) {
                mkdir($destinationPath, 0775);
            }
            chmod($destinationPath, 0775);
            $fileProposalTA->move($destinationPath, $filename);

            $tugasAkhir->path_proposal = base_path().'/'.$destinationPath.'/'.$filename;
        }

        if($fileVerifiProposal)
        {
            $destinationPath    = 'uploads/tugas-akhir/'. $this->tahunAjaran->getDirTahunAjaran();
            $extension          = $fileVerifiProposal->clientExtension();
            $filename           = $user->username.'.'.$extension;
            if (!file_exists($destinationPath)) {
                mkdir($destinationPath, 0775);
            }
            chmod($destinationPath, 0775);
            $destinationPath    = 'uploads/tugas-akhir/'. $this->tahunAjaran->getDirTahunAjaran() .'/verifi_proposal';
            if (!file_exists($destinationPath)) {
                mkdir($destinationPath, 0775);
            }
            chmod($destinationPath, 0775);
            $fileVerifiProposal->move($destinationPath, $filename);

            $tugasAkhir->path_verifi_proposal = base_path().'/'.$destinationPath.'/'.$filename;
        }
        
        $tugasAkhir->kategori_ta_id     = $request->input('kategori_ta');
        $tugasAkhir->judul              = $request->input('judul_ta');

        $mataKuliah1                = new MataKuliahKeahlian;
        $mataKuliah2                = new MataKuliahKeahlian;
        $mataKuliah3                = new MataKuliahKeahlian;

        $mataKuliah1->name          = $request->input('nama_mk1');
        $mataKuliah2->name          = $request->input('nama_mk2');

        $mataKuliah1->nilai         = $request->input('nilai_mk1');
        $mataKuliah2->nilai         = $request->input('nilai_mk2');
    
        if($request->input('nama_mk3') != '') {
            $mataKuliah3->name          = $request->input('nama_mk3');
            $mataKuliah3->nilai         = $request->input('nilai_mk3');
            $tugasAkhir->mata_kuliah_keahlian()->save($mataKuliah3); 
        }

        $tugasAkhir->mata_kuliah_keahlian()->save($mataKuliah1);
        $tugasAkhir->mata_kuliah_keahlian()->save($mataKuliah2);

        $proposal->status_id      = 6;
        $tugasAkhir->save();
        $proposal->save();

        return redirect('seminar-proposal')->with('status', [
                'color'     => $this->notif_color,
                'content'   => 'Berhasil membuat Pengajuan Seminar Proposal TA!', 
                ]
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user       = User::find($id);

        $proposal   = $user->proposal;
        $tugasAkhir = $user->tugas_akhir;

        $userProposal   = [
                'nama_lengkap'      => $user->full_name,
                'username'          => $user->username,
                'nilai_toefl'       => $proposal->nilai_toefl,
                'judul_ta'          => $tugasAkhir->judul,
                'dosen_pembimbing'  => $tugasAkhir->dosen_pembimbing->full_name,
                'kategori_ta'       => $tugasAkhir->kategori_ta->description,
                'mata_kuliah'       => $tugasAkhir->mata_kuliah_keahlian,
                'userId'            => $id
        ];

        // $path_proposal              = FileController::getPath('proposal_ta', $tugasAkhir->id);
        // $path_verifi_proposal       = FileController::getPath('verifi_proposal', $tugasAkhir->id);

        // if(file_exists($path_proposal))
        //         $file               = new \Symfony\Component\HttpFoundation\File\File($path_proposal);
        // if(file_exists($path_verifi_proposal))
        //         $file               = new \Symfony\Component\HttpFoundation\File\File($path_verifi_proposal);
                            
        return view('form.seminar-proposal.detail', [
            'user'          => $userProposal,
            'proposal'      => $proposal,
            // 'file_toefl'        => [
            //         'name'      => basename($path),
            //         'type'      => $file->getMimeType(),
            //         'size'      => $file->getSize()
            // ]
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->input());
        $user                       = \Auth::user();
        $tugasAkhir                 = $user->tugas_akhir;
        $proposal                   = $user->proposal;
        
        $fileProposalTA             = $request->file('proposal_ta');
        $fileVerifiProposal         = $request->file('verifi_proposal');
        
        if($fileProposalTA)
        {
            $destinationPath    = 'uploads/'. $this->tahunAjaran->getDirTahunAjaran() .'/proposal';
            $extension          = $fileProposalTA->clientExtension();
            $filename           = $user->username.'.'.$extension;
            $fileProposalTA->move($destinationPath, $filename);

            $tugasAkhir->path_proposal = base_path().'/'.$destinationPath.'/'.$filename;
        }

        if($fileVerifiProposal)
        {
            $destinationPath    = 'uploads/tugas-akhir/'. $this->tahunAjaran->getDirTahunAjaran() .'/verifi_proposal';
            $extension          = $fileVerifiProposal->clientExtension();
            $filename           = $user->username.'.'.$extension;
            if (!file_exists($destinationPath)) {
                mkdir($destinationPath, 0775);
            } 
            chmod($destinationPath, 0775);
            $fileVerifiProposal->move($destinationPath, $filename);

            $tugasAkhir->path_verifi_proposal = base_path().'/'.$destinationPath.'/'.$filename;
        }

        $tugasAkhir->kategori_ta_id     = $request->input('kategori_ta');
        $tugasAkhir->judul              = $request->input('judul_ta');
        $tugasAkhir->mata_kuliah_keahlian()->delete();

        $mataKuliah1                = new MataKuliahKeahlian;
        $mataKuliah2                = new MataKuliahKeahlian;
        $mataKuliah3                = new MataKuliahKeahlian;

        $mataKuliah1->name          = $request->input('nama_mk1');
        $mataKuliah2->name          = $request->input('nama_mk2');
        
        $mataKuliah1->nilai         = $request->input('nilai_mk1');
        $mataKuliah2->nilai         = $request->input('nilai_mk2');

        $tugasAkhir->mata_kuliah_keahlian()->save($mataKuliah1);
        $tugasAkhir->mata_kuliah_keahlian()->save($mataKuliah2);

        if($request->input('nama_mk3') != '') {
            $mataKuliah3->name          = $request->input('nama_mk3');
            $mataKuliah3->nilai         = $request->input('nilai_mk3');
            $tugasAkhir->mata_kuliah_keahlian()->save($mataKuliah3); 
        }

        $tugasAkhir->save();
        $proposal->save();

        return redirect('seminar-proposal')->with('status', [
                'color'     => $this->notif_color,
                'content'   => 'Berhasil mengupdate data Pengajuan Seminar Proposal TA!', 
                ]
        );
    }

    public function submit() {
        $user           = \Auth::user();
        $user->proposal->status_id = 7;
        $user->proposal->save();

        return redirect('seminar-proposal')->with('status', [
                'color'     => $this->notif_color,
                'content'   => 'Submit Pengajuan Semianar Proposal TA berhasil!', 
                ]
        );
    }

    public function tolak($id, Request $request) {
        Proposal::where('id', '=', $id)->update(['status_id' => 8]);
        $proposal = Proposal::find($id);
        $user     = User::find($proposal->user_id);
        $catatan = new Catatan;
        $catatan->tipe          = 'seminar-proposal';
        $catatan->name          = 'ditolak';
        $catatan->description   = $request->input('catatan');
        $user->catatan()->save($catatan);

        return redirect('seminar-proposal/pendaftar')->with('status', [
                'color'     => $this->notif_color,
                'content'   => 'Berhasil menolak/membatalkan Pengajuan Seminar Proposal TA '.$proposal->user->full_name.' ('.$proposal->user->username.')' 
        ]);
    }

    public function terima($id, Request $request) {
        $proposal   = Proposal::find($id);
        $user       = User::find($proposal->user_id);

        $tugasAkhir = $user->tugas_akhir;
        
        $proposal->status_id = 10;
        $proposal->save();

        $catatan = new Catatan;
        $catatan->tipe          = 'seminar-proposal';
        $catatan->name          = 'diterima';
        $catatan->description   = $request->input('catatan');
        $user->catatan()->save($catatan);

        return redirect('seminar-proposal/pendaftar')->with('status', [
                'color'     => $this->notif_color,
                'content'   => 'Berhasil! Pengajuan Seminar Proposal TA ('.$user->username.') telah diterima.', 
                ]
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user                       = \Auth::user();
        $tugasAkhir                 = $user->tugas_akhir;
        $proposal                   = $user->proposal;

        $tugasAkhir->judul          = $proposal->usulan_judul;
        $tugasAkhir->kategori_ta_id = null;
        $tugasAkhir->mata_kuliah_keahlian()->delete();

        File::delete($tugasAkhir->path_proposal);
        $tugasAkhir->path_proposal  = null;

        $proposal->status_id        = 5;

        $tugasAkhir->save();
        $proposal->save();

        return redirect('seminar-proposal')->with('status', [
                'color'     => $this->notif_color,
                'content'   => 'Berhasil menghapus Pengajuan Seminar Proposal TA!', 
                ]
        );
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

use App\Http\Requests;

use App\Models\Dosen;
use App\Models\KategoriProposal;
use App\Models\Proposal;
use App\Models\LabTA;
use App\Models\User;
use App\Models\StatusProposal;
use App\Models\DosenUsulan;
use App\Models\LabUsulan;
use App\Models\Catatan;

use App\Logic\TahunAjaran;

use Input;
use File;
use Storage;

class PendaftaranProposalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $notif_color = 'grey-dark';
    protected $notes_color = 'blue-dark';
    protected $tahunAjaran;

    public function __construct() {
        $this->tahunAjaran = new TahunAjaran;
    }

    public function showAllSubmited() {
        
        $user               = \Auth::user();

        $allProposal        = \DB::table('proposal_ta')
                                ->join('users', 'users.id','=','proposal_ta.user_id')
                                ->where('status_id', '=', '2')
								->select('proposal_ta.*', 'users.full_name', 'users.username')
                                ->get();
        
        $total_proposal     = \DB::table('proposal_ta')
                                ->where('status_id', '=', '2')
                                ->count();

        return view('proposal.showAllSubmited')->with([
            'm_proposal_ta' => true,
            'allProposal'           => $allProposal,
            'total_proposal'        => $total_proposal,
            'pendaftar-proposal'    => true
        ]);
        // return view('proposal.showAll');
    }

    public function index()
    {
        $arrayWith      = [];
        $status_id_proposal = 0;

        // cek sudah punya proposal atau tidak
        if(\Auth::user()->proposal != null)
            $status_id_proposal = \Auth::user()->proposal->status_id;
        else if(\Auth::user()->proposal == null) {
            $arrayWith = [
                'status'    => [
                    'color'     => $this->notes_color,
                    'content'   => 'Tidak ada pendaftaran Proposal TA. Silahkan daftrakan Proposal Anda.'
                ],
                'm_proposal_ta' => true
            ];
        }

        // pendaftaran proposal sudah dibuat dan belum disubmit atau revisi
        if($status_id_proposal == 1 || $status_id_proposal == 3 || $status_id_proposal == 15) {
            $dosen                  = Dosen::lists('full_name', 'id');
            $kategoriProposal       = KategoriProposal::lists('description', 'id');
            $lab                    = LabTA::lists('description', 'id');
            $path                   = FileController::getPath('nilai_toefl');
            
            if(file_exists($path))
                $file               = new \Symfony\Component\HttpFoundation\File\File($path);
            
            
            $arrayWith = [
                'status'    => [
                    'color'     => $this->notes_color,
                    'content'   => 'Submit segera formulir pendaftaran proposal Anda agar bisa doproses oleh Koordinator TA.'
                ],
                'dosen'             => $dosen,
                'kategoriProposal'  => $kategoriProposal,
                'lab'               => $lab,
                'file_toefl'        => [
                    'name'      => basename($path),
                    'type'      => $file->getMimeType(),
                    'size'      => $file->getSize()
                ],
                'm_proposal_ta' => true
            ];
        }
        // pendaftaran proposal sedan diproses
        else if($status_id_proposal == 2) {
            $arrayWith = [
                'status'    => [
                    'color'     => $this->notes_color,
                    'content'   => 'Pendaftaran Proposal Anda sedang diproses.'
                ],
                'm_proposal_ta' => true
            ];
        }
        // pendaftaran proposal sudah disetujui 
        else if($status_id_proposal >= 5){

            $path                   = FileController::getPath('nilai_toefl');
            $file                   = new \Symfony\Component\HttpFoundation\File\File($path);

            $arrayWith = [
                'status'    => [
                    'color'     => $this->notes_color,
                    'content'   => 'Selamat! Pendaftaran Proposal Anda telah disetujui.'
                ],
                'file_toefl'        => [
                    'name'      => basename($path),
                    'type'      => $file->getMimeType(),
                    'size'      => $file->getSize()
                ],
                'm_proposal_ta' => true
            ];
        }

        return view('proposal.index')->with($arrayWith);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dosen                      = Dosen::lists('full_name', 'id');
        $lab                        = LabTA::lists('description', 'id');
        $kategoriProposal           = KategoriProposal::lists('description', 'id');
        
        if(\Auth::user()->proposal == null) {
            return view('proposal.create')->with([
                'judul'             => 'Formulir Pendaftaran Proposal TA',
                'dosen'             => $dosen,
                'lab'               => $lab,
                'kategoriProposal'  => $kategoriProposal,
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // dd($request->input());
        $user                       = \Auth::user();

        $proposal                   = new Proposal;
        $proposal->usulan_judul     = $request->input('usulan_judul');
        $proposal->nilai_toefl      = $request->input('nilai_toefl');
        
        $file = $request->file('input_toefl');
        
        if($file)
        {
            $destinationPath    = 'uploads/tugas-akhir/'. $this->tahunAjaran->getDirTahunAjaran();
            $extension          = $file->clientExtension();
            $filename           = $user->username.'.'.$extension;
            if (!file_exists($destinationPath)) {
                mkdir($destinationPath, 0775);
            }
            chmod($destinationPath, 0775);
            $destinationPath    = 'uploads/tugas-akhir/'. $this->tahunAjaran->getDirTahunAjaran() .'/nilai_toefl';
            if (!file_exists($destinationPath)) {
                mkdir($destinationPath, 0775);
            }
            chmod($destinationPath, 0775);
            $file->move($destinationPath, $filename);

            $proposal->path_nilai_toefl1 = base_path().'/'.$destinationPath.'/'.$filename;
        }

        $proposal->status_id            = 1;
        $proposal->tahun_ajaran         = $this->tahunAjaran->getTahunAjaran();
        $proposal->kategori_proposal_id = $request->input('kategori_proposal');
        
        $user->proposal()->save($proposal);

        $dosenUsulan               = new DosenUsulan;
        $dosenUsulan->dosen_id     = $request->input('usulan_dosen1');
        $proposal->dosen_usulan()->save($dosenUsulan);

        $dosenUsulan               = new DosenUsulan;
        $dosenUsulan->dosen_id     = $request->input('usulan_dosen2');
        $proposal->dosen_usulan()->save($dosenUsulan);

        $labUsulan                  = new LabUsulan;
        $labUsulan->lab_id        = $request->input('usulan_lab1');
        $proposal->lab_usulan()->save($labUsulan);

        $labUsulan                  = new LabUsulan;
        $labUsulan->lab_id        = $request->input('usulan_lab2');
        $proposal->lab_usulan()->save($labUsulan);

        return redirect('proposal')->with('status', [
                'color'     => $this->notif_color,
                'content'   => 'Berhasil membuat proposal!', 
                ]
        );
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        // if(!$request->ajax()) {
        //     return redirect()->back();
        // }

        $proposal       = Proposal::find($id);

        $userProposal   = [
                'nama_lengkap'      => $proposal->user->full_name,
                'username'          => $proposal->user->username,
                'nilai_toefl'       => $proposal->nilai_toefl,
                'kategori_proposal' => $proposal->kategori_proposal->description,
                'usulan_judul'      => $proposal->usulan_judul,
                'id'                => $id,
        ];

        $path                   = FileController::getPath('nilai_toefl', $id);

        $file                   = new \Symfony\Component\HttpFoundation\File\File($path);
        
        $usulanDosen = \DB::table('dosen_usulan')
                            ->leftJoin('dosen', 'dosen.id', '=', 'dosen_usulan.dosen_id')
                            ->where('dosen_usulan.proposal_ta_id', '=', $id)
                            ->select('dosen.id', 'dosen.full_name')
                            ->get();
        
        $usulanLab  = \DB::table('lab_usulan')
                            ->leftJoin('lab_ta', 'lab_usulan.lab_id', '=', 'lab_ta.id')
                            ->where('lab_usulan.proposal_ta_id', '=', $id)
                            ->select('lab_ta.id', 'lab_ta.description')
                            ->get();
        
        $dosenElse = \DB::table('dosen')
                            ->whereNotIn('id', function($q) use ($id) {
                                $q
                                    ->from('dosen_usulan')
                                    ->select('dosen_id')
                                  
                                  ->leftJoin('dosen', 'dosen_usulan.dosen_id', '=', 'dosen.id')
                                  ->where('dosen_usulan.proposal_ta_id', '=', $id)
                                  ->get();
                            })
                            ->select('id', 'full_name')
                            ->get();
        
        $labElse = \DB::table('lab_ta')
                            ->whereNotIn('id', function($q) use ($id) {
                                $q
                                    ->from('lab_usulan')
                                    ->select('lab_id')
                                  
                                  ->leftJoin('lab_ta', 'lab_usulan.lab_id', '=', 'lab_ta.id')
                                  ->where('lab_usulan.proposal_ta_id', '=', $id)
                                  ->get();
                            })
                            ->select('id', 'description')
                            ->get();
        
        return view('form.proposal.detail', [
            'usulandosen'   => $usulanDosen,
            'dosenelse'     => $dosenElse,
            'usulanlab'     => $usulanLab,
            'labelse'       => $labElse,
            'user'          => $userProposal,
            'proposal'      => $proposal,
            'file_toefl'        => [
                    'name'      => basename($path),
                    'type'      => $file->getMimeType(),
                    'size'      => $file->getSize()
            ]
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user                       = \Auth::user();

        $input                      = Input::only('usulan_judul', 'nilai_toefl');

        $file = $request->file('input_toefl');

        if($file)
        {
            $destinationPath    = 'uploads/'. $this->tahunAjaran->getDirTahunAjaran() .'/nilai_toefl';
            $extension          = $file->clientExtension();
            $filename           = $user->username.'.'.$extension;

            File::delete($user->proposal->path_nilai_toefl1);
            $file->move($destinationPath, $filename);
            $user->proposal->path_nilai_toefl1 = base_path().'/'.$destinationPath.'/'.$filename;
        }

        $user->proposal->fill($input)->save();

        if($request->input('kategori_proposal'))
            $user->proposal->kategori_proposal_id = $request->input('kategori_proposal');

        $user->proposal->save();

        if($request->input('usulan_dosen1') && $request->input('usulan_dosen2'))
        {
            $user->proposal->dosen_usulan()->delete();

            $dosenUsulan               = new DosenUsulan;
            $dosenUsulan->dosen_id     = $request->input('usulan_dosen1');
            $user->proposal->dosen_usulan()->save($dosenUsulan);

            $dosenUsulan               = new DosenUsulan;
            $dosenUsulan->dosen_id     = $request->input('usulan_dosen2');
            $user->proposal->dosen_usulan()->save($dosenUsulan);
        }

        if($request->input('usulan_lab1') && $request->input('usulan_lab2')) {
            $user->proposal->lab_usulan()->delete();
            $labUsulan                  = new LabUsulan;
            $labUsulan->lab_id        = $request->input('usulan_lab1');
            $user->proposal->lab_usulan()->save($labUsulan);

            $labUsulan                  = new LabUsulan;
            $labUsulan->lab_id        = $request->input('usulan_lab2');
            $user->proposal->lab_usulan()->save($labUsulan);
        }

        return redirect('proposal')->with('status', [
                'color'     => $this->notif_color,
                'content'   => 'Berhasil update Pendaftaran Proposal TA!', 
                ]
        );
    }

    public function submit() {
        $user           = \Auth::user();
        $user->proposal->status_id = 2;
        $user->proposal->save();

        return redirect('proposal')->with('status', [
                'color'     => $this->notif_color,
                'content'   => 'Submit Pendaftaran Proposal TA berhasil!', 
                ]
        );
    }

    public function tolak($id, Request $request) {
        
        Proposal::where('id', '=', $id)->update(['status_id' => 3]);

        $proposal = Proposal::find($id);
        $user = User::where('id', '=', $proposal->user_id)->first();

        $catatan = new Catatan;
        $catatan->tipe          = 'pendaftaran-proposal';
        $catatan->name          = 'ditolak';
        $catatan->description   = $request->input('catatan');
        $user->catatan()->save($catatan);

        return redirect('proposal/pendaftar')->with('status', [
                'color'     => $this->notif_color,
                'content'   => 'Berhasil menolak/membatalkan Pendaftaran Proposal TA '.$proposal->user->full_name.' ('.$proposal->user->username.')' 
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $proposal = Proposal::find($id);
        
        File::delete($proposal->path_nilai_toefl1);
        $proposal->delete();
        
        return redirect('proposal')->with('status', [
                'color'     => $this->notif_color,
                'content'   => 'Berhasil menghapus Pendaftaran Proposal TA!', 
                ]
        );
    }
}

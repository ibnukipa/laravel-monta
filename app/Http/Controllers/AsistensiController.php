<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\User;
use App\Models\KegiatanAsistensi;

class AsistensiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $notif_color = 'grey-dark';
    protected $notes_color = 'blue-dark';

    public function __construct() {
        if(\Auth::user()->hasRole('mahasiswa')) {
            if(!\Auth::user()->proposal || (\Auth::user()->proposal->status_id != 12 || \Auth::user()->proposal->status_id != 14 || \Auth::user()->proposal->status_id < 16) && \Auth::user()->tugas_akhir->status_id < 1) {
                return redirect('/')->send();
            }
        }

    }

    public function index()
    {
        $id = \Auth::user()->tugas_akhir->id;
        $data   = KegiatanAsistensi::where('tugas_akhir_id', '=', $id)->get();
        
        return view('asistensi.index')->with([
            'm_asistensi_ta'    => true,
            'kegiatans'     => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('asistensi.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = \Auth::user();
        $kegiatanAsistensi = new KegiatanAsistensi;

        $kegiatanAsistensi->tanggal         = date('Y-m-d',strtotime($request->input('tanggal_asistensi')));
        $kegiatanAsistensi->description     = $request->input('keterangan');

        $user->tugas_akhir->kegiatan_asistensi()->save($kegiatanAsistensi);

        return redirect()->route('asistensi.index')->with('status', [
                'color'     => $this->notif_color,
                'content'   => 'Menambah Kegiatan Asistensi TA', 
                ]
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

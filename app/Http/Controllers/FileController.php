<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

use App\Http\Requests;
use App\Models\User;
use App\Models\Proposal;
use App\Models\TugasAkhir;
use App\Models\JadwalFile;
use App\Models\Jadwal;
use App\Models\KegiatanAsistensi;
use App\Logic\HTMLtoOpenXML;

use TemplateProcessor;
use Storage;
use ZipArchive;
use TahunAjaran;
use Waktu;

class FileController extends Controller
{
    public static function getPath($tipe_file, $id = null ) {
        if($tipe_file == 'jadwal') {
            $fileJadwal = JadwalFile::find($id);
            if(!file_exists($fileJadwal->path_file)) {
                return 'uploads/not_found.png';
            }

            return $fileJadwal->path_file;
        }
        else if($tipe_file == 'nilai_toefl') {
            
            $id = ($id == null) ? \Auth::user()->proposal->id : $id;

            $proposal       = Proposal::find($id);

            if(!$proposal) {
                return redirect()->back();
            }

            if(!file_exists($proposal->path_nilai_toefl1))
                return 'uploads/not_found.png';

            return $proposal->path_nilai_toefl1;

        } else if($tipe_file == 'proposal_ta') {
            
            $id = ($id == null) ? \Auth::user()->tugas_akhir->id : $id;
            $tugas_akhir = TugasAkhir::find($id);
            
            if(!$tugas_akhir)
                return redirect()->back();

            if(!file_exists($tugas_akhir->path_proposal))
                return 'uploads/not_found.png';
            
            return $tugas_akhir->path_proposal;

        } else if($tipe_file == 'laporan_ta') {
            
            $id = ($id == null) ? \Auth::user()->tugas_akhir->id : $id;
            $tugas_akhir = TugasAkhir::find($id);
            if(!$tugas_akhir)
                return redirect()->back();

            if(!file_exists($tugas_akhir->path_laporan_ta) || $tugas_akhir->path_laporan_ta == null)
                return 'uploads/not_found.png';
            
            return $tugas_akhir->path_laporan_ta;

        } else if($tipe_file == 'berita_acara') {

            $id = ($id == null) ? \Auth::user()->tugas_akhir->id : $id;
            $tugas_akhir = TugasAkhir::find($id);
            if(!$tugas_akhir)
                return redirect()->back();

            if(!file_exists($tugas_akhir->path_berita_acara_kta))
                return 'uploads/not_found.png';
            
            return $tugas_akhir->path_berita_acara_kta;

        } else if($tipe_file == 'lembar_asistensi') {

            $id = ($id == null) ? \Auth::user()->tugas_akhir->id : $id;
            $tugas_akhir = TugasAkhir::find($id);
            if(!$tugas_akhir)
                return redirect()->back();

            if(!file_exists($tugas_akhir->path_lembar_kegiatan_asistensi))
                return 'uploads/not_found.png';
            
            return $tugas_akhir->path_lembar_kegiatan_asistensi;

        } else if($tipe_file == 'form_perbaikan') {

            $id = ($id == null) ? \Auth::user()->tugas_akhir->id : $id;
            $tugas_akhir = TugasAkhir::find($id);
            if(!$tugas_akhir)
                return redirect()->back();

            if(!file_exists($tugas_akhir->path_form_perbaikan))
                return 'uploads/not_found.png';
            
            return $tugas_akhir->path_form_perbaikan;

        } else if($tipe_file == 'borang_cek_format') {

            $id = ($id == null) ? \Auth::user()->tugas_akhir->id : $id;
            $tugas_akhir = TugasAkhir::find($id);
            if(!$tugas_akhir)
                return redirect()->back();

            if(!file_exists($tugas_akhir->path_borang_cek_format))
                return 'uploads/not_found.png';
            
            return $tugas_akhir->path_borang_cek_format;

        } else if($tipe_file == 'verifi_proposal') {

            $id = ($id == null) ? \Auth::user()->tugas_akhir->id : $id;
            $tugas_akhir = TugasAkhir::find($id);
            if(!$tugas_akhir)
                return redirect()->back();

            if(!file_exists($tugas_akhir->path_verifi_proposal))
                return 'uploads/not_found.png';
            
            return $tugas_akhir->path_verifi_proposal;

        } else if($tipe_file == 'verifi_kemajuan') {

            $id = ($id == null) ? \Auth::user()->tugas_akhir->id : $id;
            $tugas_akhir = TugasAkhir::find($id);
            if(!$tugas_akhir)
                return redirect()->back();

            if(!file_exists($tugas_akhir->path_verifi_kemajuan))
                return 'uploads/not_found.png';
            
            return $tugas_akhir->path_verifi_kemajuan;

        } else {
            return redirect()->back();
        }
    }

    public function file_preview($tipe_file, $proposal_id = null) {
        
        $path = $this->getPath($tipe_file, $proposal_id);
        
        $handler        = new \Symfony\Component\HttpFoundation\File\File($path);

        $lifetime       = 31556926;

        $file_time      = $handler->getMTime();

        $header_content_type    = $handler->getMimeType();
        $header_content_length  = $handler->getSize();

        $headers = array(
            'Content-Type' => $header_content_type,
            'Content-Length' => $header_content_length
        );

        return Response::make(file_get_contents($path), 200, $headers);
    }

    public function file_download_zip($tipe_file, $username = null) {

        $dir = 'uploads/berkas_sistem';
        $zipFileName = 'temp'.'.zip';
        $zip = new ZipArchive;

        if(file_exists($dir . '/' . $zipFileName)) {
            unlink($dir . '/' . $zipFileName);
        }
        
        if ($zip->open($dir . '/' . $zipFileName, ZipArchive::CREATE) === TRUE) {    
            // dd($tipe_file == 'kemajuan-proposal');  
            if($tipe_file == 'seminar-proposal') {
                $zip->addFile($this->berita_acara($username, "PTA03", 1), $username."_PTA-03.docx");        
                $zip->addFile($this->hasil_evaluasi($username, "PTA04", 1), $username."_PTA-04.docx");   
            } else if($tipe_file == 'kemajuan-proposal') {
                $zip->addFile($this->berita_acara($username, "KTA02", 2), $username."_KTA-02.docx");        
                $zip->addFile($this->hasil_evaluasi($username, "KTA03", 2), $username."_KTA-03.docx");   
            } else if($tipe_file == 'sidang-ta') {
                $zip->addFile($this->berita_acara($username, "UTA02", 3), $username."_UTA-02.docx");        
                $zip->addFile($this->hasil_evaluasi($username, "UTA03", 3), $username."_UTA-03.docx");   
            }
              
        }

        $zip->close();

        $headers = array(
                'Content-Type' => 'application/octet-stream',
            );

        $filetopath=$dir.'/'.$zipFileName;

        if(file_exists($filetopath)){
            return response()->download($filetopath,$tipe_file.'_'.$username.'.zip',$headers);
        }
    }

    private function berita_acara($username, $kode_berkas, $id_jadwal_kategori) {
        $tahunAjaran    = new TahunAjaran;
        $waktu          = new Waktu;
        $user           = User::where('username', $username)->first();
        $koordinator    = User::whereHas('roles', function($q) {
                                    $q->where('name', '=', 'koordinator');
                            })->first();

        $jadwal     = Jadwal::where('tugas_akhir_id', '=', $user->tugas_akhir->id)
                                ->where('jadwal.jadwal_kategori_id', '=', $id_jadwal_kategori)
                                ->first();

        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor('uploads/berkas_sistem/template/berita_acara/'.$kode_berkas.'.docx');
        
        //SETTING VALUE
        $hari_tanggal   = $waktu->hari[date('N', strtotime($jadwal->tanggal))] . date(', d ', strtotime($jadwal->tanggal)) . $waktu->bulan[date('n', strtotime($jadwal->tanggal))] . date(' Y', strtotime($jadwal->tanggal));
        $jam            = date('H:i', strtotime($jadwal->jadwal_jam->mulai)) ."-". date('H:i', strtotime($jadwal->jadwal_jam->selesai));
        $tempat         = $jadwal->jadwal_ruang->description;
        if($user->proposal->accepted_at)
            $accepted_date  = date('d', strtotime($user->proposal->accepted_at)) .'/'. date('m', strtotime($user->proposal->accepted_at)) .'/'. date('Y', strtotime($user->proposal->accepted_at));
        else 
            $accepted_date = '-';
        $judul          = $user->tugas_akhir->judul;
        $judul          = str_replace(array('<strong>', '</strong>'), array('<b>', '</b>'), $judul);
        $judulDoc       = HTMLtoOpenXML::getInstance()->fromHTML($judul);

        $templateProcessor->setValue('semester', 'Semester '.$tahunAjaran->getSemester().' '.$tahunAjaran->getTahunAjaran());
        $templateProcessor->setValue('toefl', $user->proposal->nilai_toefl);
        $templateProcessor->setValue('accepted', $accepted_date);
        $templateProcessor->setValue('hari_tanggal', $hari_tanggal);
        $templateProcessor->setValue('jam', $jam);
        $templateProcessor->setValue('tempat', $tempat);
        $templateProcessor->setValue('judul_ta', $judulDoc);
        $templateProcessor->setValue('user_fullname', $user->full_name);
        $templateProcessor->setValue('user_nrp', $user->username);
        $templateProcessor->setValue('user_bidang_ta', $user->tugas_akhir->kategori_ta->description);
        $templateProcessor->setValue('user_pembimbing', $user->tugas_akhir->dosen_pembimbing->full_name);

        $temp_file = tempnam(sys_get_temp_dir(), $kode_berkas.$user->username);
        $templateProcessor->saveAs($temp_file);
        return $temp_file;
    }

    private function hasil_evaluasi($username, $kode_berkas, $id_jadwal_kategori) {
        $tahunAjaran    = new TahunAjaran;
        $waktu          = new Waktu;
        $user           = User::where('username', $username)->first();
        $koordinator    = User::whereHas('roles', function($q) {
                                    $q->where('name', '=', 'koordinator');
                            })->first();

        $jadwal     = Jadwal::where('tugas_akhir_id', '=', $user->tugas_akhir->id)
                                ->where('jadwal.jadwal_kategori_id', '=', $id_jadwal_kategori)
                                ->first();
        
        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor('uploads/berkas_sistem/template/berita_acara/'.$kode_berkas.'.docx');

        if($user->proposal->accepted_at)
            $accepted_date  = date('d', strtotime($user->proposal->accepted_at)) .'/'. date('m', strtotime($user->proposal->accepted_at)) .'/'. date('Y', strtotime($user->proposal->accepted_at));
        else 
            $accepted_date = '-';

        $tgl_ttd = date(' d ', strtotime($jadwal->tanggal)) . $waktu->bulan[date('n', strtotime($jadwal->tanggal))] . date(' Y', strtotime($jadwal->tanggal));

        $judul          = $user->tugas_akhir->judul;
        $judul          = str_replace(array('<strong>', '</strong>'), array('<b>', '</b>'), $judul);
        $judulDoc       = HTMLtoOpenXML::getInstance()->fromHTML($judul);

        //SETTING VALUE

        $templateProcessor->setValue('toefl', $user->proposal->nilai_toefl);
        $templateProcessor->setValue('accepted', $accepted_date);
        $templateProcessor->setValue('judul_ta', $judulDoc);
        $templateProcessor->setValue('user_fullname', $user->full_name);
        $templateProcessor->setValue('user_nrp', $user->username);
        $templateProcessor->setValue('user_bidang_ta', $user->tugas_akhir->kategori_ta->description);
        $templateProcessor->setValue('user_pembimbing', $user->tugas_akhir->dosen_pembimbing->full_name);
        $templateProcessor->setValue('tgl_ttd', $tgl_ttd);
        $templateProcessor->setValue('name_koordinator', $koordinator->full_name);

        $temp_file = tempnam(sys_get_temp_dir(), $kode_berkas.$user->username);
        $templateProcessor->saveAs($temp_file);
        return $temp_file;
    }

    public function file_download($tipe_file, $proposal_id = null) {
        if($tipe_file == 'single-cek-format') {
            return response()->download($this->cek_format(), 'Borang Cek Format '.\Auth::user()->username.'.docx');
        } else if ($tipe_file == 'verifi-proposal') {
            return response()->download($this->verifi_proposal_doc(), 'Form PTA-02 '.\Auth::user()->username.'.docx');
        } else if($tipe_file == 'verifi-kemajuan') {
            return response()->download($this->verifi_kemajuan_doc(), 'Form KTA-01 '.\Auth::user()->username.'.docx');
        } else if($tipe_file == 'kegiatan-asistensi') {
            return response()->download($this->kegiatan_asisten_doc(), 'Form FTA-03 '.\Auth::user()->username.'.docx');
        } else if($tipe_file == 'perbaikan-laporan') {
            return response()->download($this->perbaikan_laporan_doc(), 'Form FTA-04 '.\Auth::user()->username.'.docx');
        } else 
            $path = $this->getPath($tipe_file, $proposal_id);

        $filename   = $tipe_file.'_'.basename($path);
        return response()->download($path, $filename);
    }

    private function cek_format() {
        $koordinator    = User::whereHas('roles', function($q) {
                                    $q->where('name', '=', 'koordinator');
                            })->first();

        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor('uploads/berkas_sistem/template/cek_format.docx');
        $templateProcessor->setValue('nama_mhs', \Auth::user()->full_name);
        $templateProcessor->setValue('nama_dosen', \Auth::user()->tugas_akhir->dosen_pembimbing->full_name);
        $templateProcessor->setValue('nama_koor', $koordinator->full_name);

        $temp_file = tempnam(sys_get_temp_dir(), 'cek_format');

        $templateProcessor->saveAs($temp_file);
        return $temp_file;
    }

    //PTA02
    private function verifi_proposal_doc() {
        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor('uploads/berkas_sistem/template/PTA02.docx');
        
        $templateProcessor->setValue('dosen_pembimbing', \Auth::user()->tugas_akhir->dosen_pembimbing->full_name);
        $templateProcessor->setValue('nama', \Auth::user()->full_name);
        $templateProcessor->setValue('nrp', \Auth::user()->username);
        $temp_file = tempnam(sys_get_temp_dir(), 'verifi_proposal');

        $templateProcessor->saveAs($temp_file);
        return $temp_file;
    }

    //KTA01
    private function verifi_kemajuan_doc() {
        $koordinator    = User::whereHas('roles', function($q) {
                                    $q->where('name', '=', 'koordinator');
                            })->first();

        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor('uploads/berkas_sistem/template/KTA01.docx');
        
        $templateProcessor->setValue('nama', \Auth::user()->full_name);
        $templateProcessor->setValue('nrp', \Auth::user()->username);
        $templateProcessor->setValue('lab', \Auth::user()->tugas_akhir->lab_ta->description);
        $templateProcessor->setValue('kategori', \Auth::user()->tugas_akhir->kategori_ta->description);
        $templateProcessor->setValue('koor', $koordinator->full_name);

        $judul          = \Auth::user()->tugas_akhir->judul;
        $judul          = str_replace(array('<strong>', '</strong>'), array('<b>', '</b>'), $judul);
        $judulDoc       = HTMLtoOpenXML::getInstance()->fromHTML($judul);
        
        $templateProcessor->setValue('judul', $judulDoc);

        $templateProcessor->setValue('dosen_pembimbing', \Auth::user()->tugas_akhir->dosen_pembimbing->full_name);
        $temp_file = tempnam(sys_get_temp_dir(), 'verifi_proposal');

        $templateProcessor->saveAs($temp_file);
        return $temp_file;
    }

    //FTA03
    private function kegiatan_asisten_doc() {
        $user = \Auth::user();
        $kegiatan       = KegiatanAsistensi::where('tugas_akhir_id', '=', $user->tugas_akhir->id)->get();
        //dd($kegiatan[0]);
        $waktu          = new Waktu;

        $judul          = $user->tugas_akhir->judul;
        $judul          = str_replace(array('<strong>', '</strong>'), array('<b>', '</b>'), $judul);
        $judulDoc       = HTMLtoOpenXML::getInstance()->fromHTML($judul);

        $tgl = date(' d ') . $waktu->bulan[date('n')] . date(' Y');

        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor('uploads/berkas_sistem/template/FTA03.docx');

        $templateProcessor->setValue('dosen_pembimbing',$user->tugas_akhir->dosen_pembimbing->full_name);
        $templateProcessor->setValue('nama', $user->full_name);
        $templateProcessor->setValue('nrp', $user->username);
        $templateProcessor->setValue('judul', $judulDoc);

        $templateProcessor->cloneRow('rowNumber', $kegiatan->count());

        for($i = 1; $i <= $kegiatan->count(); $i++) {
            $value = $kegiatan[$i-1];
            $templateProcessor->setValue('rowNumber#'.$i, $i);
            $templateProcessor->setValue('value_tanggal#'.$i, date("d-m-Y",strtotime($value->tanggal)));
            $templateProcessor->setValue('value_description#'.$i, $value->description);
        }

        $templateProcessor->setValue('tgl', $tgl);

        $temp_file = tempnam(sys_get_temp_dir(), 'kegiatan_asisten');
        $templateProcessor->saveAs($temp_file);
        return $temp_file;
    }

    //FTA04
    private function perbaikan_laporan_doc() {
        $user = \Auth::user();
        $waktu          = new Waktu;

        $judul          = $user->tugas_akhir->judul;
        $judul          = str_replace(array('<strong>', '</strong>'), array('<b>', '</b>'), $judul);
        $judulDoc       = HTMLtoOpenXML::getInstance()->fromHTML($judul);

        $tgl = date(' d ') . $waktu->bulan[date('n')] . date(' Y');

        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor('uploads/berkas_sistem/template/FTA04.docx');
        
        $templateProcessor->setValue('dosen_pembimbing', $user->tugas_akhir->dosen_pembimbing->full_name);
        $templateProcessor->setValue('nama', $user->full_name);
        $templateProcessor->setValue('nrp', $user->username);
        $templateProcessor->setValue('judul', $judulDoc);
        $templateProcessor->setValue('tgl', $tgl);

        $temp_file = tempnam(sys_get_temp_dir(), 'perbaikan_laporan');
        $templateProcessor->saveAs($temp_file);
        return $temp_file;
    }
}

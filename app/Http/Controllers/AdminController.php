<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\User;
use App\Models\Role;
use App\Models\Dosen;
use App\Models\JadwalJam;
use App\Models\JadwalKategori;

use Validator;

class AdminController extends Controller
{
    protected $notif_color = 'grey-dark';
    protected $notes_color = 'blue-dark';
    protected $error_color = 'red-dark';

    public function validator_dosen(array $data) {
        return Validator::make($data, [
                'nip'         => 'unique:dosen'
            ]
        );
    }

    public function akunReset($id) {

    }

    public function showAllAkunPegawai() {
        $users      = User::with('roles')
                        
                        ->get();
        return view('admin.akun-pegawai.showAll')->with([
            'users'         => $users
        ]);
    }

    public function showAllAkunMahasiswa() {
        $users      = User::with('roles')
                        
                        ->get();
        return view('admin.akun-mahasiswa.showAll')->with([
            'users'         => $users
        ]);
    }

    public function showAllDataDosen() {
        $dosen      = Dosen::get();
        return view('admin.data-dosen.showAll')->with([
            'users'         => $dosen
        ]);
    }

    public function showAllDataJam() {
        $jam      = JadwalJam::join('jadwal_kategori', 'jadwal_jam.kategori_id', '=', 'jadwal_kategori.id')
                        ->select('jadwal_jam.*','jadwal_kategori.description as kategori')
                        ->get();
        return view('admin.data-jam.showAll')->with([
            'jam'         => $jam
        ]);
    }

    public function showAkun($id) {
        
        $role       = Role::lists('name', 'id');
        $user       = User::find($id);
        $currrole   = $user->roles[0]->id;
        
        return view('admin.show-akun')->with([
            'user'      => $user,
            'role'      => $role,
            'currrole'  => $currrole
        ]);
    }

    public function showDosen($id) {
        $user       = Dosen::find($id);
        
        return view('admin.data-dosen.show')->with([
            'user'      => $user,
        ]);
    }

    public function getAkun() {
        $roles = Role::where('name', '!=', 'koordinator')
                        ->where('name', '!=', 'mahasiswa')
                        ->lists('name', 'id');
        return view('admin.create-akun')->with([
            'roles'         => $roles
        ]);
    }

    public function postAkun(Request $request) {
        // dd($request->input());
        if(User::where('username', '=', $request->input('username'))->first() == null) {
            $user = User::create([ 
                'username'      => $request->input('username'),
                'password'      => bcrypt($request->input('password_baru')),
                'full_name'     => $request->input('full_name'),
                'email'         => '',
                'nohp'          => '',
                'active'        => 1,
            ]);

            if($request->input('role'))
                $user->roles()->attach($request->input('role'));
            else {
                $user->roles()->attach(2);
                $user->active = 2;
                $user->save();
            }

            return redirect()->back()->with('status', [
                    'color'     => $this->notes_color,
                    'content'   => 'Berhasil membuat Akun!', 
                    ]
            );
        } else {
            return redirect()->back()->with('status', [
                    'color'     => $this->error_color,
                    'content'   => 'Akun dengan NRP '.$request->input('username').' sudah ada!', 
                    ]
            );
        }
        
    }

    public function update(Request $request, $id) {
        $user = User::find($id);
        
        $full_name  = $request->input('nama_lengkap');
        $username   = $request->input('username');
        $password   = $request->input('password_baru');
        $active     = $request->input('active');
        
        if($active == 'on' || $user->hasRole('administrator')) {
            $user->active = 1;
        } else {
            $user->active = 0;
        }

        if($full_name) {
            $user->full_name    = $full_name;
        }
        
        if($username) {
            $user->username     = $username;
        }

        if($password) {
            $user->password     = bcrypt($password);
        }
        
        $user->save();

        return redirect()->back()->with('status', [
                'color'     => $this->notif_color,
                'content'   => 'Berhasil update data!', 
                ]
        );
    }

    public function createDosen() {
        return view('admin.data-dosen.create');
    }

    public function createJam() {
        $kategori = JadwalKategori::lists('description', 'id');
        return view('admin.data-jam.create')->with([
            'kategori'      => $kategori
        ]);
    }

    public function storeJam(Request $request) {
        $jam = new JadwalJam;

        $jam->kategori_id   = $request->input('kategori');
        $jam->mulai         = $request->input('mulai');
        $jam->selesai       = $request->input('selesai');

        if($jam->save()) {
            return redirect()->back()->with('status', [
                    'color'     => $this->notif_color,
                    'content'   => 'Berhasil menambah data!', 
                    ]
            );
        } else {
            return redirect()->back()->with('status', [
                    'color'     => $this->error_color,
                    'content'   => 'GAGAL menambah data!', 
                    ]
            );
        }
    }

    public function storeDosen(Request $request) {

        $validator = $this->validator_dosen($request->all());

        if ($validator->fails()) {
            return redirect()->back()->with('status', [
                    'color'     => $this->error_color,
                    'content'   => 'NIP sudah terdaftar!', 
                    ]
            );
        }

        // Dosen::create([
        //     'nip'			=> $request->input('nip'),
        //     'full_name'	    => $request->input('nama_lengkap'),
        //     'phone'	        => '',
        //     'email'	        => '',
        //     'lab_ta_id'	    => 2,
        // ]);

        $dosen = new Dosen;
        $dosen->nip         = $request->input('nip');
        $dosen->full_name	= $request->input('nama_lengkap');

        if($dosen->save()) {
            return redirect()->back()->with('status', [
                    'color'     => $this->notif_color,
                    'content'   => 'Berhasil tambah data!', 
                    ]
            );
        } else {
            return redirect()->back()->with('status', [
                    'color'     => $this->error_color,
                    'content'   => 'GAGAL tambah data!', 
                    ]
            );
        }
    }

    public function updateDosen(Request $request, $id) {
        $dosen      = Dosen::find($id);
        $full_name  = $request->input('nama_lengkap');
        $nip        = $request->input('nip');

        if($full_name) {
            $dosen->full_name = $full_name;
        }

        if($nip) {
            $dosen->nip = $nip;
        }

        $dosen->save();
        return redirect()->back()->with('status', [
                'color'     => $this->notif_color,
                'content'   => 'Berhasil update data!', 
                ]
        );
    }

    public function destroyDosen($id) {
        $dosen = Dosen::find($id);
        $dosen->delete();

        return redirect()->route('data-dosen.showAll')->with('status', [
                'color'     => $this->notif_color,
                'content'   => 'Berhasil hapus data!', 
                ]
        );
    }

    public function destroyJam($id) {
        $jam = JadwalJam::find($id);
        $jam->delete();

        return redirect()->route('data-jam.showAll')->with('status', [
                'color'     => $this->notif_color,
                'content'   => 'Berhasil hapus data!', 
                ]
        );
    }

    public function destroyAkun($id) {
        $user = User::find($id);
        $user->delete();

        return redirect()->back()->with('status', [
                'color'     => $this->notif_color,
                'content'   => 'Berhasil hapus data!', 
                ]
        );
    }

    //Berkas
    public function berkas() {
        return view('admin.berkas.index')->with([
            'users'         => "TES"
        ]);
    }

    public function berkas_download($tipe) {
        $dir = 'uploads/berkas_sistem/template/';

        if($tipe == 'PTA02' || $tipe == 'KTA01' || $tipe == 'FTA03' || $tipe == 'FTA04') {
            
        } else {
            $dir = $dir.'berita_acara/';
        }

        $filename = file_exists($dir.$tipe.'.docx') ? $tipe.'.docx' : $tipe.'.doc';
        $filepath = $dir.$filename;
        
        return response()->download($filepath, $filename);
    }

    public function berkas_update(Request $request) {
        //PRE
        $dir = 'uploads/berkas_sistem/template/';
        if (!file_exists($dir)) {
            mkdir($dir, 0775);
        }
        chmod($dir, 0775);
        $tipe_berkas = $request->input('tipe_berkas');
        $file_berkas = $request->file('file_berkas');

        //REMOVE EXISTING FILE
        if($tipe_berkas == 'PTA02' || $tipe_berkas == 'KTA01' || $tipe_berkas == 'FTA03' || $tipe_berkas == 'FTA04') {
            
        } else {
            $dir = $dir.'berita_acara/';
        }

        $filename = file_exists($dir.$tipe_berkas.'.docx') ? $tipe_berkas.'.docx' : $tipe_berkas.'.doc';
        $filepath = $dir.$filename;
        unlink($filepath);

        $extension          = $file_berkas->clientExtension();
        $filename           = $tipe_berkas.'.'.$extension;
        $file_berkas->move($dir, $filename);

        return redirect('admin/berkas')->with([
            'status'    => [
                    'color'     => 'blue-dark',
                    'content'   => 'Berhasil mengupdate FILE: '. $tipe_berkas.'. Pastikan semua variable telah ada pada file '.$tipe_berkas
                ]
        ]);
    }
}

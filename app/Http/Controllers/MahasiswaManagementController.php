<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\User;
use Excel;
use Validator;
class MahasiswaManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $notif_color = 'grey-dark';
    protected $notes_color = 'blue-dark';
    protected $error_color = 'red-dark';
    protected $tahunAjaran;

    public function validator_reset(array $data) {
        return Validator::make($data, [
                'password_baru'         => 'required|min:6|max:20'
            ], [
                'password_baru.min'     => 'Password minimal 6 karakter.',
                'password_baru.max'     => 'Password maksimal 20 karakter.',
            ]
        );
    }

    public function getReset() {
        if(\Auth::guest()) {
            return redirect()->back();
        }
        return view('auth.reset-password');
    }

    public function postReset(Request $request) {
        if($request->input('id'))
            $user = User::find($request->input('id'));
        else
            $user = \Auth::user();

        $validator = $this->validator_reset($request->all());
        // dd($validator);
        if ($validator->fails()) {
            
            return redirect('reset')->with('status', [
                    'color'     => $this->error_color,
                    'content'   => 'PASSWORD minimal 6 karakter dan maksimal 20 karakter!', 
                    ]
            );
        }
        
        $user->password = bcrypt($request->input('password_baru'));
        $temp = $user->active;
        $user->active   = 1;

        if($user->save()) {
            if($temp == 2) {
                return redirect('auth/logout')->with('status', [
                        'color'     => $this->error_color,
                        'content'   => 'PASSWORD berhasil diperbarui!. Silahkan masuk kembali', 
                        ]
                );
            }

            return redirect('reset')->with('status', [
                    'color'     => $this->notif_color,
                    'content'   => 'PASSWORD berhasil diperbarui!', 
                    ]
            );
        } else {
            return redirect('reset')->with('status', [
                    'color'     => $this->error_color,
                    'content'   => 'PASSWORD gagal diperbarui!', 
                    ]
            );
        }
    }

    public function create_daftar_mahasiswa() {
        return view('form.modal.upload-daftar-mhs');
    }

    public function mhs_akun_create() {
        return view('admin.akun-mahasiswa.create');
    }

    public function store_daftar_mahasiswa(Request $request) {
        if($request->file('daftar_mhs')){

			$path = $request->file('daftar_mhs')->getRealPath();
            
			$data = Excel::load($path, function($reader) {
			})->get();
			if(!empty($data) && $data->count()){
                $jumlahMhsBaru = 0;
				foreach ($data as $key => $value) {
                    // dd(User::where('username', '=', $value->username)->first());
                    if(User::where('username', '=', $value->username)->first() == null) {
                        // dd($value);
                        $user = User::create([
                            'username'  => (string)$value->username, 
                            'full_name' => $value->name,
                            'password'  => bcrypt($value->username),
                            'active'    => 2
                        ]);
                        $user->roles()->attach(2);

                        $jumlahMhsBaru++;
                    }
				}
                if($jumlahMhsBaru) {
                    $arrayWith = [
                        'status'    => [
                            'color'     => $this->notes_color,
                            'content'   => 'Berhasil menambah Data Mahasiswa sebanyak = '. $jumlahMhsBaru
                        ]
                    ];
                } else {
                    $arrayWith = [
                        'status'    => [
                            'color'     => $this->notif_color,
                            'content'   => 'Data Mahasiswa sudah update.'
                        ],
                    ];
                }
			} else {
                $arrayWith = [
                    'status'    => [
                        'color'     => $this->notif_color,
                        'content'   => 'Tidak ada pada file yang diupload.'
                    ],
                ];
            }
		}

       

        return redirect()->back()->with($arrayWith);

    }

    public function index()
    {

        $mahasiswa      = User::
                            whereHas('roles', function($q) {
                                $q->where('name', '=', 'mahasiswa');
                            })
                            ->orderBy('created_at', 'desc')
                            ->get();
        $dataView       = [
            'mahasiswaMenu'    => true,
            'mahasiswa'         => $mahasiswa
        ];
        return view('koordinator.index-mahasiswa')->with($dataView);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if($user->delete()) {
            return redirect()->back()->with('status', [
                    'color'     => $this->notif_color,
                    'content'   => 'Berhasil menghapus Data Mahasiswa!', 
                    ]
            );
        }
    }
}

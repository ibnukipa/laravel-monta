<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Dosen;
use App\Models\KategoriProposal;
use App\Models\KategoriTA;
use App\Models\MataKuliahKeahlian;
use App\Models\TugasAkhir;
use App\Models\Proposal;
use App\Models\User;
use App\Models\Jadwal;
use App\Models\PengaturanSeminar;
use App\Logic\TahunAjaran;
use App\Models\Catatan;

use File;

class SidangController extends Controller
{

    protected $notif_color = 'grey-dark';
    protected $notes_color = 'blue-dark';

    protected $tahunAjaran;

    public function __construct() {
        $this->tahunAjaran = new TahunAjaran;

        if(\Auth::user()->hasRole('mahasiswa')) {
            if(!\Auth::user()->tugas_akhir || (\Auth::user()->tugas_akhir->status_id <= 9 || \Auth::user()->tugas_akhir->status_id != 8)) {
                return redirect()->back();
            }
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function change($id, $status) {
        $user = User::find($id);
        $tugasAkhir = $user->tugas_akhir;
        $proposal   = $user->proposal;
        $jadwal     = Jadwal::where('tugas_akhir_id', '=', $tugasAkhir->id)
                                ->where('jadwal_kategori_id', '=', 3)
                                ->get();
        
        if(!$jadwal->isEmpty()) {
            $jadwal = $jadwal[0];
        } else {
            $jadwal = null;
        }
            
        if($status == 'ulang'){
            $tugasAkhir->status_id = 16;
            $tugasAkhir->save();
            
            if($jadwal) {    
                $jadwal->delete();
            }
            
        } else if($status == 'lolos') {
            $tugasAkhir->status_id = 17;
            $tugasAkhir->save();
        } else if($status == 'batal') {
            $tugasAkhir->status_id = 18;
            $tugasAkhir->save();
            if($jadwal) {    
                $jadwal->delete();
            }

        } else if($status == 'gagal') {
            $tugasAkhir->delete();
            $proposal->delete();
            $jadwal     = Jadwal::where('tugas_akhir_id', '=', $tugasAkhir->id)
                                ->get();
            foreach($jadwal as $jad) {
                $jad->delete();
            }
        }

        return redirect()->route('sidang.on')->with('status', [
                'color'     => $this->notif_color,
                'content'   => 'Berhasil merubah data Sidang Ujian Lisan TA!', 
                ]
        );
    }

    public function showAllSubmited() {
        $users              = User::whereHas('tugas_akhir', function($q) {
                                        $q->where('tugas_akhir.status_id', '=', 11);
                                    })
                                ->with('tugas_akhir', 'proposal')
                                ->get();
        // dd($users);
        return view('sidang.showAllSubmited')->with([
            'm_sidang_ta' => true,
            'users'             => $users,
        ]);
    }

    public function showAllOn() {
            $users = User::
                        with([
                                        'tugas_akhir' => function($q) {
                                            $q->with([
                                                    'jadwal' => function($q) {
                                                        $q->
                                                        with(['jadwal_kategori', 'jadwal_ruang', 'jadwal_jam'])
                                                        ->where('jadwal.jadwal_kategori_id', '=', 3);
                                                    },
                                                ]);
                                        },
                                    ])
                        ->join('tugas_akhir', 'users.id', '=', 'tugas_akhir.user_id')
                        ->where('tugas_akhir.status_id', '=', 14)
                        ->orWhere('tugas_akhir.status_id', '=', 15)
                        ->orWhere('tugas_akhir.status_id', '=', 16)
                        ->select('users.*')
                        ->get();
            return view('sidang.showAllOn')->with([
                'm_sidang_ta' => true,
                'users'             => $users,
            ]);
    }


    public function index()
    {
        $pengaturanSeminar      = PengaturanSeminar::
                                        where('tahun_ajaran', '=', $this->tahunAjaran->getTahunAjaran())
                                        ->where('semester', '=', $this->tahunAjaran->getSemester())
                                        ->where('tipe_seminar', '=', 'sidang')
                                        ->first();

        if($pengaturanSeminar && \Auth::user()->tugas_akhir->status_id == 8 || \Auth::user()->tugas_akhir->status_id == 18) {
            if($pengaturanSeminar->due_to <= date("d-m-Y")) {
                return view('sidang.index')->with([
                    'm_sidang_ta' => true,
                    'lewat'         => true,
                    'tipeSeminar'  => 'Sidang Ujian Lisan TA'
                ]);
            }
        } else if(\Auth::user()->tugas_akhir->status_id == 8) {
            return view('sidang.index')->with([
                'm_sidang_ta' => true,
                'belum'         => true,
                'tipeSeminar'  => 'Sidang Ujian Lisan TA'
            ]);
        }
        

        $arrayWith      = [];
        if(\Auth::user()->tugas_akhir->status_id == 8) {
            $arrayWith = [
                'm_sidang_ta' => true,
                'status'    => [
                    'color'     => $this->notes_color,
                    'content'   => 'Silahkan membuat Pengajuan Sidang Ujian Lisan TA.'
                ],
            ];
            
        } else if(\Auth::user()->tugas_akhir->status_id == 10 || \Auth::user()->tugas_akhir->status_id == 12 || \Auth::user()->tugas_akhir->status_id == 18) {
            $kategoriTA                 = KategoriTA::lists('description', 'id');
            
            $path_laporan_ta             = FileController::getPath('laporan_ta');
            $path_berita_acara           = FileController::getPath('berita_acara');
            $path_lembar_asistensi       = FileController::getPath('lembar_asistensi');
            $path_form_perbaikan         = FileController::getPath('form_perbaikan');

            $path_borang_cek_format      = FileController::getPath('borang_cek_format');

            $tugas_akhir                 = TugasAkhir::find(\Auth::user()->tugas_akhir->id);
            $file_laporan_ta             = new \Symfony\Component\HttpFoundation\File\File($path_laporan_ta);        
            $file_berita_acara           = new \Symfony\Component\HttpFoundation\File\File($path_berita_acara);
            $file_lembar_asistensi       = new \Symfony\Component\HttpFoundation\File\File($path_lembar_asistensi);
            $file_form_perbaikan         = new \Symfony\Component\HttpFoundation\File\File($path_form_perbaikan);

            $file_borang_cek_format         = new \Symfony\Component\HttpFoundation\File\File($path_borang_cek_format);
            
            $arrayWith = [
                'm_sidang_ta' => true,
                'status'    => [
                    'color'     => $this->notes_color,
                    'content'   => 'Submit untuk segera ditindak lanjuti oleh Koordinator TA.'
                ],
                'kategoriTA'    => $kategoriTA,
                'tugas_akhir'   => $tugas_akhir,
                'file_laporan_ta'    => [
                        'name'      => basename($path_laporan_ta),
                        'type'      => $file_laporan_ta->getMimeType(),
                        'size'      => $file_laporan_ta->getSize()
                ],
                'file_berita_acara'    => [
                        'name'      => basename($path_berita_acara),
                        'type'      => $file_berita_acara->getMimeType(),
                        'size'      => $file_berita_acara->getSize()
                ],
                'file_lembar_asistensi'    => [
                        'name'      => basename($path_lembar_asistensi),
                        'type'      => $file_lembar_asistensi->getMimeType(),
                        'size'      => $file_lembar_asistensi->getSize()
                ],
                'file_form_perbaikan'    => [
                        'name'      => basename($path_form_perbaikan),
                        'type'      => $file_form_perbaikan->getMimeType(),
                        'size'      => $file_form_perbaikan->getSize()
                ],
                'file_borang_cek_format'    => [
                        'name'      => basename($path_borang_cek_format),
                        'type'      => $file_borang_cek_format->getMimeType(),
                        'size'      => $file_borang_cek_format->getSize()
                ]
            ];
        } else if(\Auth::user()->tugas_akhir->status_id == 11) {
            $arrayWith = [
                'm_sidang_ta' => true,
                'status'    => [
                    'color'     => $this->notes_color,
                    'content'   => 'Pengajuan Sidang Ujian Lisan TA Anda sedang diproses.'
                ]
            ];
        } else if(\Auth::user()->tugas_akhir->status_id >= 14) {
            $path_laporan_ta             = FileController::getPath('laporan_ta');
            $path_berita_acara           = FileController::getPath('berita_acara');
            $path_lembar_asistensi       = FileController::getPath('lembar_asistensi');
            $path_form_perbaikan         = FileController::getPath('form_perbaikan');

            $path_borang_cek_format      = FileController::getPath('borang_cek_format');

            $tugas_akhir                 = TugasAkhir::find(\Auth::user()->tugas_akhir->id);
            $file_laporan_ta             = new \Symfony\Component\HttpFoundation\File\File($path_laporan_ta);        
            $file_berita_acara           = new \Symfony\Component\HttpFoundation\File\File($path_berita_acara);
            $file_lembar_asistensi       = new \Symfony\Component\HttpFoundation\File\File($path_lembar_asistensi);
            $file_form_perbaikan         = new \Symfony\Component\HttpFoundation\File\File($path_form_perbaikan);

            $file_borang_cek_format         = new \Symfony\Component\HttpFoundation\File\File($path_borang_cek_format);

            $arrayWith = [
                'm_sidang_ta' => true,
                'status'    => [
                    'color'     => $this->notes_color,
                    'content'   => 'Selamat! Pengajuan Sidang Ujian Lisan TA Anda telah disetujui.'
                ],
                'user'          => User::where('users.id', '=', \Auth::user()->id)->get(),
                'file_laporan_ta'    => [
                        'name'      => basename($path_laporan_ta),
                        'type'      => $file_laporan_ta->getMimeType(),
                        'size'      => $file_laporan_ta->getSize()
                ],
                'file_berita_acara'    => [
                        'name'      => basename($path_berita_acara),
                        'type'      => $file_berita_acara->getMimeType(),
                        'size'      => $file_berita_acara->getSize()
                ],
                'file_lembar_asistensi'    => [
                        'name'      => basename($path_lembar_asistensi),
                        'type'      => $file_lembar_asistensi->getMimeType(),
                        'size'      => $file_lembar_asistensi->getSize()
                ],
                'file_form_perbaikan'    => [
                        'name'      => basename($path_form_perbaikan),
                        'type'      => $file_form_perbaikan->getMimeType(),
                        'size'      => $file_form_perbaikan->getSize()
                ],
                'file_borang_cek_format'    => [
                        'name'      => basename($path_borang_cek_format),
                        'type'      => $file_borang_cek_format->getMimeType(),
                        'size'      => $file_borang_cek_format->getSize()
                ]
            ];

        } else {
            return redirect()->back();
        }

        return view('sidang.index')->with($arrayWith);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sidang.create')->with([
            'judul'             => 'Formulir Sidang Ujian Lisan TA'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->file());
        $user                       = \Auth::user();
        $tugasAkhir                 = $user->tugas_akhir;

        $path_laporan_ta            = FileController::getPath('laporan_ta');
        $path_berita_acara          = FileController::getPath('berita_acara');
        $path_lembar_asistensi      = FileController::getPath('lembar_asistensi');
        $path_form_perbaikan        = FileController::getPath('form_perbaikan');

        $path_borang_cek_format     = FileController::getPath('borang_cek_format');
        
        $file_laporan_ta            = $request->file('laporan_ta');
        $file_berita_acara          = $request->file('berita_acara');
        $file_lembar_asistensi      = $request->file('lembar_asistensi');
        $file_form_perbaikan        = $request->file('form_perbaikan');

        $file_borang_cek_format     = $request->file('borang_cek_format');

        if($file_laporan_ta)
        {
            $destinationPath    = 'uploads/tugas-akhir/'. $this->tahunAjaran->getDirTahunAjaran();
            $extension          = $file_laporan_ta->clientExtension();
            $filename           = $user->username.'.'.$extension;
            if (!file_exists($destinationPath)) {
                mkdir($destinationPath, 0775);
            }
            chmod($destinationPath, 0775);
            $destinationPath    = 'uploads/tugas-akhir/'. $this->tahunAjaran->getDirTahunAjaran() .'/laporan_ta';
            if (!file_exists($destinationPath)) {
                mkdir($destinationPath, 0775);
            }
            chmod($destinationPath, 0775);
            $file_laporan_ta->move($destinationPath, $filename);

            $tugasAkhir->path_laporan_ta = base_path().'/'.$destinationPath.'/'.$filename;
        }

        if($file_berita_acara)
        {
            $destinationPath    = 'uploads/tugas-akhir/'. $this->tahunAjaran->getDirTahunAjaran();
            $extension          = $file_berita_acara->clientExtension();
            $filename           = $user->username.'.'.$extension;
            if (!file_exists($destinationPath)) {
                mkdir($destinationPath, 0775);
            }
            chmod($destinationPath, 0775);
            $destinationPath    = 'uploads/tugas-akhir/'. $this->tahunAjaran->getDirTahunAjaran() .'/berita_acara';
            if (!file_exists($destinationPath)) {
                mkdir($destinationPath, 0775);
            }
            chmod($destinationPath, 0775);
            $file_berita_acara->move($destinationPath, $filename);

            $tugasAkhir->path_berita_acara_kta = base_path().'/'.$destinationPath.'/'.$filename;
        }

        if($file_lembar_asistensi)
        {
            $destinationPath    = 'uploads/tugas-akhir/'. $this->tahunAjaran->getDirTahunAjaran();
            $extension          = $file_lembar_asistensi->clientExtension();
            $filename           = $user->username.'.'.$extension;
            if (!file_exists($destinationPath)) {
                mkdir($destinationPath, 0775);
            }
            chmod($destinationPath, 0775);
            $destinationPath    = 'uploads/tugas-akhir/'. $this->tahunAjaran->getDirTahunAjaran() .'/lembar_asistensi';
            if (!file_exists($destinationPath)) {
                mkdir($destinationPath, 0775);
            }
            chmod($destinationPath, 0775);
            $file_lembar_asistensi->move($destinationPath, $filename);

            $tugasAkhir->path_lembar_kegiatan_asistensi = base_path().'/'.$destinationPath.'/'.$filename;
        }

        if($file_form_perbaikan)
        {
            $destinationPath    = 'uploads/tugas-akhir/'. $this->tahunAjaran->getDirTahunAjaran();
            $extension          = $file_form_perbaikan->clientExtension();
            $filename           = $user->username.'.'.$extension;
            if (!file_exists($destinationPath)) {
                mkdir($destinationPath, 0775);
            }
            chmod($destinationPath, 0775);
            $destinationPath    = 'uploads/tugas-akhir/'. $this->tahunAjaran->getDirTahunAjaran() .'/form_perbaikan';
            if (!file_exists($destinationPath)) {
                mkdir($destinationPath, 0775);
            }
            chmod($destinationPath, 0775);
            $file_form_perbaikan->move($destinationPath, $filename);

            $tugasAkhir->path_form_perbaikan = base_path().'/'.$destinationPath.'/'.$filename;
        }

        if($file_borang_cek_format)
        {
            $destinationPath    = 'uploads/tugas-akhir/'. $this->tahunAjaran->getDirTahunAjaran();
            $extension          = $file_borang_cek_format->clientExtension();
            $filename           = $user->username.'.'.$extension;
            if (!file_exists($destinationPath)) {
                mkdir($destinationPath, 0775);
            }
            chmod($destinationPath, 0775);
            $destinationPath    = 'uploads/tugas-akhir/'. $this->tahunAjaran->getDirTahunAjaran() .'/borang_cek_format';
            if (!file_exists($destinationPath)) {
                mkdir($destinationPath, 0775);
            }
            chmod($destinationPath, 0775);
            $file_borang_cek_format->move($destinationPath, $filename);

            $tugasAkhir->path_borang_cek_format = base_path().'/'.$destinationPath.'/'.$filename;
        }
        $tugasAkhir->judul              = $request->input('judul_ta');

        $tugasAkhir->status_id = 10;
        $tugasAkhir->save();

        return redirect('sidang')->with('status', [
                'color'     => $this->notif_color,
                'content'   => 'Berhasil mengajukan Sidang Ujian Lisan Tugas Akhir!', 
                ]
        );
    }

    public function submit() {
        
        $user           = \Auth::user();
        $user->tugas_akhir->status_id = 11;
        $user->tugas_akhir->save();

        return redirect('sidang')->with('status', [
                'color'     => $this->notif_color,
                'content'   => 'Submit Pengajuan Sidang Ujian Lisan Tugas Akhir berhasil!', 
                ]
        );
    }

    public function tolak($id, Request $request) {

        TugasAkhir::where('id', '=', $id)->update(['status_id' => 12]);

        $tugasAkhir = TugasAkhir::find($id);
        $user       = User::find($tugasAkhir->user_id);
        $catatan    = new Catatan;

        $catatan->tipe          = 'sidang';
        $catatan->name          = 'ditolak';
        $catatan->description   = $request->input('catatan');
        $user->catatan()->save($catatan);

        return redirect('sidang/pendaftar')->with('status', [
                'color'     => $this->notif_color,
                'content'   => 'Berhasil menolak/membatalkan Pengajuan Sidang Ujian Lisan TA '.$user->full_name.' ('.$user->username.')' 
        ]);
    }

    public function terima($id, Request $request) {
        $tugasAkhir     = TugasAkhir::find($id);
        $user           = User::find($tugasAkhir->user_id);
        
        $tugasAkhir->status_id = 14;
        $tugasAkhir->save();

        $catatan = new Catatan;
        $catatan->tipe          = 'sidang';
        $catatan->name          = 'diterima';
        $catatan->description   = $request->input('catatan');
        $user->catatan()->save($catatan);

        return redirect('sidang/pendaftar')->with('status', [
                'color'     => $this->notif_color,
                'content'   => 'Berhasil! Pengajuan Sidang Ujian Lisan TA ('.$user->username.') telah diterima.', 
                ]
        );
    }

    public function jadwal() {
        $user = \Auth::user();
        $jadwalSidang = null;
        if(!$user->tugas_akhir->jadwal->isEmpty()) {
            foreach ($user->tugas_akhir->jadwal as $key => $jadwal) {
                if($jadwal->jadwal_kategori->name === 'sidang-lisan')
                    $jadwalSidang = $jadwal;
            }
        }

        
        $arrayWith = [
                'm_sidang_ta' => true,
                'user'          => User::where('users.id', '=', \Auth::user()->id)->get(),
                'jadwal'        => $jadwalSidang
            ];
        return view('sidang.jadwal')->with($arrayWith);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);

        return view('form.sidang.detail')->with([
            'user'      => $user
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user                       = \Auth::user();
        $tugasAkhir                 = $user->tugas_akhir;

        $path_laporan_ta             = FileController::getPath('laporan_ta');
        $path_berita_acara                = FileController::getPath('berita_acara');
        $path_lembar_asistensi                 = FileController::getPath('lembar_asistensi');
        $path_form_perbaikan          = FileController::getPath('form_perbaikan');

        $path_borang_cek_format     = FileController::getPath('borang_cek_format');
        
        $file_laporan_ta               = $request->file('laporan_ta');
        $file_berita_acara               = $request->file('berita_acara');
        $file_lembar_asistensi               = $request->file('lembar_asistensi');
        $file_form_perbaikan               = $request->file('form_perbaikan');

        $file_borang_cek_format     = $request->file('borang_cek_format');
        
        if($file_laporan_ta)
        {
            $destinationPath    = 'uploads/tugas-akhir/'. $this->tahunAjaran->getDirTahunAjaran() .'/laporan_ta';
            $extension          = $file_laporan_ta->clientExtension();
            $filename           = $user->username.'.'.$extension;
            $file_laporan_ta->move($destinationPath, $filename);

            $tugasAkhir->path_laporan_ta = base_path().'/'.$destinationPath.'/'.$filename;
        }

        if($file_berita_acara)
        {
            $destinationPath    = 'uploads/tugas-akhir/'. $this->tahunAjaran->getDirTahunAjaran() .'/berita_acara';
            $extension          = $file_berita_acara->clientExtension();
            $filename           = $user->username.'.'.$extension;
            $file_berita_acara->move($destinationPath, $filename);

            $tugasAkhir->path_berita_acara_kta = base_path().'/'.$destinationPath.'/'.$filename;
        }

        if($file_lembar_asistensi)
        {
            $destinationPath    = 'uploads/tugas-akhir/'. $this->tahunAjaran->getDirTahunAjaran() .'/lembar_asistensi';
            $extension          = $file_lembar_asistensi->clientExtension();
            $filename           = $user->username.'.'.$extension;
            $file_lembar_asistensi->move($destinationPath, $filename);

            $tugasAkhir->path_lembar_kegiatan_asistensi = base_path().'/'.$destinationPath.'/'.$filename;
        }

        if($file_form_perbaikan)
        {
            $destinationPath    = 'uploads/tugas-akhir/'. $this->tahunAjaran->getDirTahunAjaran() .'/form_perbaikan';
            $extension          = $file_form_perbaikan->clientExtension();
            $filename           = $user->username.'.'.$extension;
            $file_form_perbaikan->move($destinationPath, $filename);

            $tugasAkhir->path_form_perbaikan = base_path().'/'.$destinationPath.'/'.$filename;
        }

        if($file_borang_cek_format)
        {
            $destinationPath    = 'uploads/tugas-akhir/'. $this->tahunAjaran->getDirTahunAjaran() .'/borang_cek_format';
            $extension          = $file_borang_cek_format->clientExtension();
            $filename           = $user->username.'.'.$extension;
            $file_borang_cek_format->move($destinationPath, $filename);

            $tugasAkhir->path_borang_cek_format = base_path().'/'.$destinationPath.'/'.$filename;
        }

        $tugasAkhir->judul              = $request->input('judul_ta');

        $tugasAkhir->save();

        return redirect('sidang')->with('status', [
                'color'     => $this->notif_color,
                'content'   => 'Berhasil mengupdate data Pengajuan Sidang Ujian Lisan Tugas Akhir!', 
                ]
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user                       = \Auth::user();
        $tugasAkhir                 = $user->tugas_akhir;
        $proposal                   = $user->proposal;

        File::delete($tugasAkhir->path_laporan_ta);
        File::delete($tugasAkhir->path_berita_acara_kta);
        File::delete($tugasAkhir->path_lembar_kegiatan_asistensi);
        File::delete($tugasAkhir->path_form_perbaikan);
        File::delete($tugasAkhir->path_borang_cek_format);

        $tugasAkhir->path_laporan_ta  = null;
        $tugasAkhir->path_berita_acara_kta  = null;
        $tugasAkhir->path_lembar_kegiatan_asistensi  = null;
        $tugasAkhir->path_form_perbaikan  = null;
        $tugasAkhir->path_borang_cek_format  = null;

        $tugasAkhir->status_id = 8;

        $tugasAkhir->save();
        $proposal->save();

        return redirect('sidang')->with('status', [
                'color'     => $this->notif_color,
                'content'   => 'Berhasil menghapus Pengajuan Sidang Ujian Lisan TA!', 
                ]
        );
    }
}

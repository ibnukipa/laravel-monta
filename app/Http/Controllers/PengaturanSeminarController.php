<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Logic\TahunAjaran;
use App\Models\PengaturanSeminar;

class PengaturanSeminarController extends Controller
{
    protected $notif_color = 'grey-dark';
    protected $notes_color = 'blue-dark';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tahunAjaran       = new TahunAjaran;
        $data              = PengaturanSeminar::get();
        // dd($data);
        return view('pengaturan-seminar.index')->with([
            'data'  =>    $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tahunAjaran        = new TahunAjaran;
        
        $tahunAjaranExist   = PengaturanSeminar::distinct()->select('tahun_ajaran')->get()->toArray();
        $seminarExist       = PengaturanSeminar::select('tipe_seminar')->where('tahun_ajaran', '=', $tahunAjaran->getTahunAjaran())->where('semester', '=', $tahunAjaran->getSemester())->get();
        $tipeSeminar        = [
            'proposal'      => 'Seminar Proposal TA',
            'kemajuan'      => 'Seminar Kemajuan TA',
            'sidang'        => 'Sidang Ujian Lisan TA'
        ]; 

        foreach ($seminarExist as $value) {
            if($value->tipe_seminar == 'proposal') {
                $data = 'proposal';
            } else if($value->tipe_seminar == 'kemajuan') {
                $data = 'kemajuan';
            } else if($value->tipe_seminar == 'sidang') {
                $data = 'sidang';
            }
            unset($tipeSeminar[$data]);
        }
        
        if(count($tipeSeminar) > 0) {
            return view('pengaturan-seminar.create')->with([
                'tahunAjaran'       => $tahunAjaran->getTahunAjaran(),
                'semester'          => $tahunAjaran->getSemester(),
                'tipeSeminar'       => $tipeSeminar
            ]);
        } else {
            return ['status' => 'Mohon maaf! Pengaturan pada semester ini telah dibuat semua. Silahkan gunakan fitur edit untuk mengubah Tanggal Berakhir'];
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        if(PengaturanSeminar::create($request->input())) {
            return redirect('pengaturan-seminar')->with('status', [
                    'color'     => $this->notif_color,
                    'content'   => 'Berhasil menambah pengaturan Seminar baru!', 
                    ]
            );
        } else {
            return redirect('pengaturan-seminar')->with('status', [
                    'color'     => $this->notif_color,
                    'content'   => 'Gagal menambah data!', 
                    ]
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function detail($id) {
        $pengaturanSeminar        = PengaturanSeminar::find($id);
        return view('pengaturan-seminar.edit')->with([
            'tahunAjaran'       => $pengaturanSeminar->tahun_ajaran,
            'tipeSeminar'       => $pengaturanSeminar->tipe_seminar,
            'semester'          => $pengaturanSeminar->semester,
            'due_to'            => $pengaturanSeminar->due_to,
            'id'                => $id
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pengaturanSeminar = PengaturanSeminar::find($id);
        $pengaturanSeminar->due_to = $request->input('due_to');

        if($pengaturanSeminar->save()) {
            return redirect('pengaturan-seminar')->with('status', [
                    'color'     => $this->notif_color,
                    'content'   => 'Berhasil mengupdate pengaturan Seminar!', 
                    ]
            );
        } else {
            return redirect('pengaturan-seminar')->with('status', [
                    'color'     => $this->notif_color,
                    'content'   => 'Gagal mengupdate data!', 
                    ]
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pengaturanSeminar = PengaturanSeminar::find($id);
        if($pengaturanSeminar->delete()) {
            return redirect('pengaturan-seminar')->with('status', [
                    'color'     => $this->notif_color,
                    'content'   => 'Berhasil menghapus Data!', 
                    ]
            );
        } else {
            return redirect('pengaturan-seminar')->with('status', [
                    'color'     => $this->notif_color,
                    'content'   => 'Gagal menghapus Data!', 
                    ]
            );
        }
    }
}

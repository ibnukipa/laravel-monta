<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\User;
use App\Models\Proposal;
use App\Models\LabTA;
use App\Models\Jadwal;
use App\Models\KegiatanAsistensi;

use App\Logic\TahunAjaran;
use App\Logic\Waktu;

use Barryvdh\DomPDF\Facade as PDF;
use Excel;
class UmumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $waktuID;

    public function __construct() {
        $this->waktuID = new Waktu;
    }

    public function berkas($id, $name) {
        $viewName;
        $tahunAjaran    = new TahunAjaran;
        $user           = User::find($id);
        $koordinator    = User::whereHas('roles', function($q) {
                                    $q->where('name', '=', 'koordinator');
                            })->first();
                            
        if($name == 'pta03' || $name == 'pta04') {
            $jadwal     = Jadwal::where('tugas_akhir_id', '=', $user->tugas_akhir->id)
                                ->where('jadwal.jadwal_kategori_id', '=', 1)
                                ->first()
                                ;
        } else if($name == 'kta02' || $name == 'kta03') {
            $jadwal     = Jadwal::where('tugas_akhir_id', '=', $user->tugas_akhir->id)
                                ->where('jadwal.jadwal_kategori_id', '=', 2)
                                ->first()
                                ;

        } else if($name == 'uta02' || $name == 'uta03') {
            
            $jadwal     = Jadwal::where('tugas_akhir_id', '=', $user->tugas_akhir->id)
                                ->where('jadwal.jadwal_kategori_id', '=', 3)
                                ->first()
                                ;
        } else if($name == 'fta03') {
            $data   = KegiatanAsistensi::where('tugas_akhir_id', '=', \Auth::user()->tugas_akhir->id)->get();
            return view('pdf.'.$name)->with([
                'user'      => \Auth::user(),
                'waktu'     => $this->waktuID,
                'kegiatan'  => $data

            ]);
        } else if($name == 'fta04' || $name == 'fta05') {
            
            return view('pdf.'.$name)->with([
                'user'      => \Auth::user(),
                'waktu'     => $this->waktuID,
            ]);

        } else if($name =='pta02') {
            return view('pdf.'.$name)->with([
                'user'      => \Auth::user(),
                'waktu'     => $this->waktuID,
            ]);
        }

        return view('pdf.'.$name)->with([
            'waktu'     => $this->waktuID,
            'user'      => $user,
            'jadwal'    => $jadwal,
            'judul'     => $user->username .' - '. $name,
            'semester'  => 'Semester '.$tahunAjaran->getSemester().' '.$tahunAjaran->getTahunAjaran(),
            'koorta'    => $koordinator

        ]);

    }
    
    public function berkas_jadwal($kategori) {
        $labta          = LabTA::get();
        $tipeSeminar;
        $tahunAjaran    = new TahunAjaran;
        $semester       = $tahunAjaran->getSemester();
        $tahunAjar      = $tahunAjaran->getTahunAjaran();
        if($kategori == 'pendaftar-proposal') {
            $proposal           = Proposal::where('proposal_ta.status_id', '=', 2)
                                ->orderBy('created_at', 'ASC')
                                ->with('user')
                                ->get();
            $tipeSeminar = 'Pendaftar Proposal TA';
            Excel::create($tipeSeminar.' '.$tahunAjar.' '.$semester, function($excel) use($proposal, $tahunAjar, $semester, $tipeSeminar) {
                $excel->sheet('List Mahasiswa', function($sheet) use($proposal, $tahunAjar, $semester, $tipeSeminar)  {
                    
                    $sheet->setPageMargin(0.25);
                    $sheet->loadView('pdf.pendaftar-proposal', [
                        'judul' => $tipeSeminar.' '.$tahunAjar.' '.$semester,
                        'proposal' => $proposal
                    ]);
                    
                });
            })->export('xls');

        } else if($kategori == 'proposal-diterima') {
            $users           = User::
                                whereHas('proposal', function ($q) {
                                    $q->where('proposal_ta.status_id', '=', 5);    
                                })
                                ->orderBy('username', 'ASC')
                                ->get();
 
            Excel::create('Pendaftar Proposal TA DITERIMA '.$tahunAjar.' '.$semester, function($excel) use($users, $tahunAjar, $semester) {
                $excel->sheet('List Mahasiswa', function($sheet) use($users, $tahunAjar, $semester)  {
                    
                    $sheet->setPageMargin(0.25);
                    $sheet->loadView('pdf.proposal-diterima', [
                        'judul' => "Pendaftar Proposal TA DITERIMA ".$tahunAjar.' '.$semester,
                        'users' => $users
                    ]);
                    
                });
            })->export('xls');


        } else if($kategori == 'seminar-proposal') {
            $users              = User::whereHas('proposal', function($q) {
                                        $q->where('proposal_ta.status_id', '>=', 10)
                                        ->where('proposal_ta.status_id', '!=', 12)
                                        ->where('proposal_ta.status_id', '<=', 13);
                                    })
                                ->with('proposal')
                                ->get();
            $tipeSeminar = 'Seminar Proposal TA';
            
        } else if($kategori == 'seminar-proposal-down') {
            $users           = User::
                                whereHas('proposal', function ($q) {
                                    $q->where('proposal_ta.status_id', '>=', 10)
                                    ->where('proposal_ta.status_id', '!=', 12)
                                    ->where('proposal_ta.status_id', '<=', 13); 
                                })
                                ->get();

            Excel::create('Daftar Mahasiswa Seminar Proposal TA '.$tahunAjar.' '.$semester, function($excel) use($users, $tahunAjar, $semester, $labta) {
                $excel->sheet('List Mahasiswa', function($sheet) use($users, $tahunAjar, $semester, $labta)  {
                    
                    $sheet->setPageMargin(0.25);
                    $sheet->loadView('pdf.down-daftar-mahasiswa', [
                        'judul' => "Daftar Mahasiswa Seminar Proposal TA ".$tahunAjar.' '.$semester,
                        'users' => $users,
                        'labTA' => $labta,
                    ]);
                    
                });
            })->export('xls');

        } else if($kategori == 'seminar-kemajuan') {
            $users              = User::whereHas('tugas_akhir', function($q) {
                                        $q->where('tugas_akhir.status_id', '>=', 5)
                                        ->where('tugas_akhir.status_id', '<=', 7);
                                    })
                                ->with('tugas_akhir', 'proposal')
                                ->get();

            $tipeSeminar = 'Seminar Kemajuan TA';
        } else if($kategori == 'seminar-kemajuan-down') {
            $users              = User::whereHas('tugas_akhir', function($q) {
                                        $q->where('tugas_akhir.status_id', '>=', 5)
                                        ->where('tugas_akhir.status_id', '<=', 7);
                                    })
                                ->with('tugas_akhir', 'proposal')
                                ->get();

            Excel::create('Daftar Mahasiswa Seminar Kemajuan TA '.$tahunAjar.' '.$semester, function($excel) use($users, $tahunAjar, $semester, $labta) {
                $excel->sheet('List Mahasiswa', function($sheet) use($users, $tahunAjar, $semester, $labta)  {
                    
                    $sheet->setPageMargin(0.25);
                    $sheet->loadView('pdf.down-daftar-mahasiswa', [
                        'judul' => "Daftar Mahasiswa Seminar Kemajuan TA ".$tahunAjar.' '.$semester,
                        'users' => $users,
                        'labTA' => $labta,
                    ]);
                    
                });
            })->export('xls');

        } else if($kategori == 'sidang') {
            $users              = User::whereHas('tugas_akhir', function($q) {
                                        $q->where('tugas_akhir.status_id', '>=', 14)
                                        ->where('tugas_akhir.status_id', '<=', 16);
                                    })
                                ->with('tugas_akhir', 'proposal')
                                ->get();

            $tipeSeminar = 'Sidang Ujian Lisan TA';
        } else if($kategori == 'sidang-down') {
            $users              = User::whereHas('tugas_akhir', function($q) {
                                        $q->where('tugas_akhir.status_id', '>=', 14)
                                        ->where('tugas_akhir.status_id', '<=', 16);
                                    })
                                ->with('tugas_akhir', 'proposal')
                                ->get();

            Excel::create('Daftar Mahasiswa Sidang Ujian Lisan TA '.$tahunAjar.' '.$semester, function($excel) use($users, $tahunAjar, $semester, $labta) {
                $excel->sheet('List Mahasiswa', function($sheet) use($users, $tahunAjar, $semester, $labta)  {
                    
                    $sheet->setPageMargin(0.25);
                    $sheet->loadView('pdf.down-daftar-mahasiswa', [
                        'judul' => "Daftar Mahasiswa Sidang Ujian Lisan TA ".$tahunAjar.' '.$semester,
                        'users' => $users,
                        'labTA' => $labta,
                    ]);
                    
                });
            })->export('xls');
        }
        
        $data = [
            'users'         => $users,
            'labTA'         => $labta,
            'tipeSeminar'   => $tipeSeminar,
            'semester'      => $semester,
            'tahunAjaran'   => $tahunAjar,
        ];

        return view('pdf.daftar-mahasiswa')->with([
            'users'         => $users,
            'labTA'         => $labta,
            'tipeSeminar'   => $tipeSeminar,
            'semester'      => $semester,
            'tahunAjaran'   => $tahunAjar
        ]);
    }

    public function index()
    {
        $users = User::has('proposal')
                        ->get();
        return view('umum.index')->with([
            'users'     => $users
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('umum.show')->with([
            'user'      => $user
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Dosen;
use App\Models\KategoriProposal;
use App\Models\KategoriTA;
use App\Models\MataKuliahKeahlian;
use App\Models\TugasAkhir;
use App\Models\Proposal;
use App\Models\User;
use App\Models\Catatan;
use App\Models\PengaturanSeminar;
use App\Models\JadwalFile;
use App\Models\Jadwal;
use App\Logic\TahunAjaran;

use File;

class SeminarKemajuanController extends Controller
{
    protected $notif_color = 'grey-dark';
    protected $notes_color = 'blue-dark';

    protected $tahunAjaran;

    public function __construct() {
        $this->tahunAjaran = new TahunAjaran;

        if(\Auth::user()->hasRole('mahasiswa')) {
            if(!\Auth::user()->proposal || \Auth::user()->proposal->status_id < 12 || \Auth::user()->proposal->status_id != 15 ) {
                return redirect()->back();
            }
        }

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function change($id, $status) {
        $user = User::find($id);
        $tugasAkhir = $user->tugas_akhir;
        $proposal   = $user->proposal;
        $jadwal     = Jadwal::where('tugas_akhir_id', '=', $tugasAkhir->id)
                                ->where('jadwal_kategori_id', '=', 2)
                                ->get();
        
        if(!$jadwal->isEmpty()) {
            $jadwal = $jadwal[0];
        } else {
            $jadwal = null;
        }

        // dd($jadwal);
            
        if($status == 'ulang'){
            $tugasAkhir->status_id = 7;
            $tugasAkhir->save();
            
            if($jadwal) {    
                $jadwal->delete();
            }
            
        } else if($status == 'lolos') {
            $tugasAkhir->status_id = 8;
            $tugasAkhir->save();
        } else if($status == 'batal') {
            // $proposal->status_id = 15;
            // $proposal->save();
            $tugasAkhir->status_id = 9;
            // $tugasAkhir->delete();

            // $tugasAkhir->status_id = 9;
            $tugasAkhir->save();
            if($jadwal) {    
                $jadwal->delete();
            }
            // $tugasAkhir->delete();
        }

        return redirect()->route('seminar-kemajuan.on')->with('status', [
                'color'     => $this->notif_color,
                'content'   => 'Berhasil merubah data Seminar Kemajaun TA!', 
                ]
        );
    }

    public function showAllOn() {
            $users = User::
                        with([
                                        'tugas_akhir' => function($q) {
                                            $q->with([
                                                    'jadwal' => function($q) {
                                                        $q->
                                                        with(['jadwal_kategori', 'jadwal_ruang', 'jadwal_jam'])
                                                        ->where('jadwal.jadwal_kategori_id', '=', 2);
                                                    },
                                                ]);
                                        },
                                    ])
                        ->join('tugas_akhir', 'users.id', '=', 'tugas_akhir.user_id')
                        ->where('tugas_akhir.status_id', '=', 5)
                        ->orWhere('tugas_akhir.status_id', '=', 6)
                        ->orWhere('tugas_akhir.status_id', '=', 7)
                        ->select('users.*')
                        ->get();
            return view('seminar-kemajuan.showAllOn')->with([
                'm_kemajuan_ta'     => true,
                'users'             => $users,
            ]);
    }

    public function showAllSubmited() {
        $users              = User::whereHas('tugas_akhir', function($q) {
                                        $q->where('tugas_akhir.status_id', '=', 2);
                                    })
                                ->with('tugas_akhir', 'proposal')
                                ->get();
        // dd($users);
        return view('seminar-kemajuan.showAllSubmited')->with([
            'm_kemajuan_ta'     => true,
            'users'             => $users,
        ]);
    }
    
    public function index()
    {
        $pengaturanSeminar      = PengaturanSeminar::
                                        where('tahun_ajaran', '=', $this->tahunAjaran->getTahunAjaran())
                                        ->where('semester', '=', $this->tahunAjaran->getSemester())
                                        ->where('tipe_seminar', '=', 'kemajuan')
                                        ->first();
        if($pengaturanSeminar && (\Auth::user()->proposal->status_id == 12 || \Auth::user()->proposal->status_id == 14 || \Auth::user()->tugas_akhir->status_id == 9)) {
            if($pengaturanSeminar->due_to <= date("d-m-Y")) {
                return view('seminar-kemajuan.index')->with([
                    'm_kemajuan_ta'     => true,
                    'lewat'         => true,
                    'tipeSeminar'  => 'Seminar Kemajuan TA'
                ]);
            }
        } else if(\Auth::user()->proposal->status_id == 12 || \Auth::user()->proposal->status_id == 14 || \Auth::user()->tugas_akhir->status_id == 9) {
            return view('seminar-kemajuan.index')->with([
                'm_kemajuan_ta'     => true,
                'belum'         => true,
                'tipeSeminar'  => 'Seminar Kemajuan TA'
            ]);
        }
        

        $arrayWith      = [];
        if(\Auth::user()->proposal->status_id == 12 || \Auth::user()->proposal->status_id == 14 ) {
            $arrayWith = [
                'm_kemajuan_ta'     => true,
                'status'    => [
                    'color'     => $this->notes_color,
                    'content'   => 'Silahkan membuat Pengajuan Seminar Kemajuan TA.'
                ],
            ];
        } else if((\Auth::user()->tugas_akhir->status_id == 1 && \Auth::user()->proposal->status_id == 16) ||  \Auth::user()->tugas_akhir->status_id == 3) {
            $kategoriTA                 = KategoriTA::lists('description', 'id');
            
            $path_proposal              = FileController::getPath('proposal_ta');
            $path_laporan_ta             = FileController::getPath('laporan_ta');
            $path_verifi_kemajuan        = FileController::getPath('verifi_kemajuan');

            $file_proposal          = new \Symfony\Component\HttpFoundation\File\File($path_proposal);
            $file_laporan_ta             = new \Symfony\Component\HttpFoundation\File\File($path_laporan_ta);
            $file_verifi_kemajuan        = new \Symfony\Component\HttpFoundation\File\File($path_verifi_kemajuan);
            
            $arrayWith = [
                'm_kemajuan_ta'     => true,
                'status'    => [
                    'color'     => $this->notes_color,
                    'content'   => 'Submit segera Formulir Seminar Kemajuan TA Anda agar bisa doproses oleh Koordinator TA.'
                ],
                'kategoriTA'    => $kategoriTA,
                'file_laporan_ta'    => [
                        'name'      => basename($path_laporan_ta),
                        'type'      => $file_laporan_ta->getMimeType(),
                        'size'      => $file_laporan_ta->getSize()
                ],
                'file_proposal'    => [
                        'name'      => basename($path_proposal),
                        'type'      => $file_proposal->getMimeType(),
                        'size'      => $file_proposal->getSize()
                ],
                'file_verifi_kemajuan'    => [
                        'name'      => basename($path_verifi_kemajuan),
                        'type'      => $file_verifi_kemajuan->getMimeType(),
                        'size'      => $file_verifi_kemajuan->getSize()
                ],
            ];
        } else if(\Auth::user()->tugas_akhir->status_id == 2) {
            $arrayWith = [
                'm_kemajuan_ta'     => true,
                'status'    => [
                    'color'     => $this->notes_color,
                    'content'   => 'Pengajuan Seminar Kemajuan TA Anda sedang diproses.'
                ]
            ];
        } else if(\Auth::user()->tugas_akhir->status_id >= 5) {
            if(\Auth::user()->tugas_akhir->status_id == 9)
                $content = '';
            else 
                $content = 'Selamat! Pengajuan Seminar Kemajuan TA Anda telah disetujui.';
            
            $path_proposal              = FileController::getPath('proposal_ta');
            $path_laporan_ta             = FileController::getPath('laporan_ta');
            $path_verifi_kemajuan        = FileController::getPath('verifi_kemajuan');

            $file_proposal          = new \Symfony\Component\HttpFoundation\File\File($path_proposal);
            $file_laporan_ta             = new \Symfony\Component\HttpFoundation\File\File($path_laporan_ta);
            $file_verifi_kemajuan        = new \Symfony\Component\HttpFoundation\File\File($path_verifi_kemajuan);
            
            $arrayWith = [
                'm_kemajuan_ta'     => true,
                'status'    => [
                    'color'     => $this->notes_color,
                    'content'   => $content
                ],
                'user'          => User::where('users.id', '=', \Auth::user()->id)->get(),
                'file_laporan_ta'    => [
                        'name'      => basename($path_laporan_ta),
                        'type'      => $file_laporan_ta->getMimeType(),
                        'size'      => $file_laporan_ta->getSize()
                ],
                'file_proposal'    => [
                        'name'      => basename($path_proposal),
                        'type'      => $file_proposal->getMimeType(),
                        'size'      => $file_proposal->getSize()
                ],
                'file_verifi_kemajuan'    => [
                        'name'      => basename($path_verifi_kemajuan),
                        'type'      => $file_verifi_kemajuan->getMimeType(),
                        'size'      => $file_verifi_kemajuan->getSize()
                ],
            ];
        } else {
            return redirect()->back();
        }

        return view('seminar-kemajuan.index')->with($arrayWith);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        return view('seminar-kemajuan.create')->with([
            'judul'             => 'Formulir Seminar Kemajuan TA'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user                       = \Auth::user();
        $tugasAkhir                 = $user->tugas_akhir;
        $proposal                   = $user->proposal;
        
        $fileLaporanTA              = $request->file('laporan_ta');
        $fileVerifiKemajuan         = $request->file('verifi_kemajuan');
        $fileProposalTA             = $request->file('proposal_ta');
        
        if($fileProposalTA)
        {
            $destinationPath    = 'uploads/tugas-akhir/'. $this->tahunAjaran->getDirTahunAjaran();
            $extension          = $fileProposalTA->clientExtension();
            $filename           = $user->username.'.'.$extension;
            if (!file_exists($destinationPath)) {
                mkdir($destinationPath, 0775);
            }
            chmod($destinationPath, 0775);
            $destinationPath    = 'uploads/tugas-akhir/'. $this->tahunAjaran->getDirTahunAjaran() .'/proposal';
            if (!file_exists($destinationPath)) {
                mkdir($destinationPath, 0775);
            }
            chmod($destinationPath, 0775);
            $fileProposalTA->move($destinationPath, $filename);

            $tugasAkhir->path_proposal = base_path().'/'.$destinationPath.'/'.$filename;
        }

        if($fileLaporanTA)
        {
            $destinationPath    = 'uploads/tugas-akhir/'. $this->tahunAjaran->getDirTahunAjaran();
            $extension          = $fileLaporanTA->clientExtension();
            $filename           = $user->username.'.'.$extension;
            if (!file_exists($destinationPath)) {
                mkdir($destinationPath, 0775);
            }
            chmod($destinationPath, 0775);
            $destinationPath    = 'uploads/tugas-akhir/'. $this->tahunAjaran->getDirTahunAjaran() .'/laporan_ta';
            if (!file_exists($destinationPath)) {
                mkdir($destinationPath, 0775);
            }
            chmod($destinationPath, 0775);
            $fileLaporanTA->move($destinationPath, $filename);

            $tugasAkhir->path_laporan_ta = base_path().'/'.$destinationPath.'/'.$filename;
        }

        if($fileVerifiKemajuan) {
            $destinationPath    = 'uploads/tugas-akhir/'. $this->tahunAjaran->getDirTahunAjaran();
            $extension          = $fileVerifiKemajuan->clientExtension();
            $filename           = $user->username.'.'.$extension;
            if (!file_exists($destinationPath)) {
                mkdir($destinationPath, 0775);
            }
            chmod($destinationPath, 0775);
            $destinationPath    = 'uploads/tugas-akhir/'. $this->tahunAjaran->getDirTahunAjaran() .'/verifi_kemajuan';
            if (!file_exists($destinationPath)) {
                mkdir($destinationPath, 0775);
            }
            chmod($destinationPath, 0775);
            $fileVerifiKemajuan->move($destinationPath, $filename);

            $tugasAkhir->path_verifi_kemajuan = base_path().'/'.$destinationPath.'/'.$filename;
        }

        $tugasAkhir->progres_penulisan = $request->input('progres_kemajuan');
        $tugasAkhir->judul = $request->input('judul_ta');
        $tugasAkhir->status_id  = 1;
        $proposal->status_id    = 16;
        $tugasAkhir->save();
        $proposal->save();

        return redirect('seminar-kemajuan')->with('status', [
                'color'     => $this->notif_color,
                'content'   => 'Berhasil membuat Pengajuan Seminar Kemajuan TA!', 
                ]
        );
    }

    public function submit() {
        $user           = \Auth::user();
        $user->tugas_akhir->status_id = 2;
        $user->tugas_akhir->save();

        return redirect('seminar-kemajuan')->with('status', [
                'color'     => $this->notif_color,
                'content'   => 'Submit Pengajuan Semianar Kemajuan TA berhasil!', 
                ]
        );
    }

    public function tolak($id, Request $request) {
        TugasAkhir::where('id', '=', $id)->update(['status_id' => 3]);

        $tugasAkhir = TugasAkhir::find($id);
        $user     = User::find($tugasAkhir->user_id);
        $catatan = new Catatan;
        $catatan->tipe          = 'seminar-kemajuan';
        $catatan->name          = 'ditolak';
        $catatan->description   = $request->input('catatan');
        $user->catatan()->save($catatan);

        return redirect('seminar-kemajuan/pendaftar')->with('status', [
                'color'     => $this->notif_color,
                'content'   => 'Berhasil menolak/membatalkan Pengajuan Seminar Kemajuan TA '.$user->full_name.' ('.$user->username.')' 
        ]);
    }

    public function terima($id, Request $request) {
        $tugasAkhir   = TugasAkhir::find($id);
        $user       = User::find($tugasAkhir->user_id);
        
        $tugasAkhir->status_id = 5;
        $tugasAkhir->save();

        $catatan = new Catatan;
        $catatan->tipe          = 'seminar-kemajuan';
        $catatan->name          = 'diterima';
        $catatan->description   = $request->input('catatan');
        $user->catatan()->save($catatan);

        return redirect('seminar-kemajuan/pendaftar')->with('status', [
                'color'     => $this->notif_color,
                'content'   => 'Berhasil! Pengajuan Seminar Kemajuan TA ('.$user->username.') telah diterima.', 
                ]
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);

        return view('form.seminar-kemajuan.detail')->with([
            'user'      => $user
        ]);
    }

    public function jadwal() {
        $user = \Auth::user();
        $jadwalKemajuan = null;
        if(!$user->tugas_akhir->jadwal->isEmpty()) {
            foreach ($user->tugas_akhir->jadwal as $key => $jadwal) {
                if($jadwal->jadwal_kategori->name === 'seminar-kemajuan')
                    $jadwalKemajuan = $jadwal;
            }
        }

        
        $arrayWith = [
                'm_kemajuan_ta'     => true,
                'user'          => User::where('users.id', '=', \Auth::user()->id)->get(),
                'jadwal'        => $jadwalKemajuan
            ];
        return view('seminar-kemajuan.jadwal')->with($arrayWith);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user                       = \Auth::user();
        $tugasAkhir                 = $user->tugas_akhir;
        $fileProposalTA             = $request->file('proposal_ta');
        $fileLaporanTA             = $request->file('laporan_ta');
        $fileVerifiKemajuan         = $request->file('verifi_kemajuan');
        
        if($fileProposalTA)
        {
            $destinationPath    = 'uploads/tugas-akhir/'. $this->tahunAjaran->getDirTahunAjaran() .'/proposal';
            $extension          = $fileProposalTA->clientExtension();
            $filename           = $user->username.'.'.$extension;
            if (!file_exists($destinationPath)) {
                mkdir($destinationPath, 0775);
            } 
            chmod($destinationPath, 0775);
            $fileProposalTA->move($destinationPath, $filename);

            $tugasAkhir->path_proposal = base_path().'/'.$destinationPath.'/'.$filename;
        }

        if($fileLaporanTA)
        {
            $destinationPath    = 'uploads/tugas-akhir/'. $this->tahunAjaran->getDirTahunAjaran() .'/laporan_ta';
            $extension          = $fileLaporanTA->clientExtension();
            $filename           = $user->username.'.'.$extension;
            if (!file_exists($destinationPath)) {
                mkdir($destinationPath, 0775);
            } 
            chmod($destinationPath, 0775);
            $fileLaporanTA->move($destinationPath, $filename);

            $tugasAkhir->path_laporan_ta = base_path().'/'.$destinationPath.'/'.$filename;
        }

        if($fileVerifiKemajuan) {
            $destinationPath    = 'uploads/tugas-akhir/'. $this->tahunAjaran->getDirTahunAjaran() .'/verifi_kemajuan';
            $extension          = $fileVerifiKemajuan->clientExtension();
            $filename           = $user->username.'.'.$extension;
            if (!file_exists($destinationPath)) {
                mkdir($destinationPath, 0775);
            } 
            chmod($destinationPath, 0775);
            $fileVerifiKemajuan->move($destinationPath, $filename);

            $tugasAkhir->path_verifi_kemajuan = base_path().'/'.$destinationPath.'/'.$filename;
        }

        $tugasAkhir->progres_penulisan = $request->input('progres_kemajuan');
        $tugasAkhir->judul = $request->input('judul_ta');
        $tugasAkhir->save();

        return redirect('seminar-kemajuan')->with('status', [
                'color'     => $this->notif_color,
                'content'   => 'Berhasil mengupdate Pengajuan Seminar Kemajuan TA!', 
                ]
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user                       = \Auth::user();
        $tugasAkhir                 = $user->tugas_akhir;
        $proposal                   = $user->proposal;

        File::delete($tugasAkhir->path_laporan_ta);
        $tugasAkhir->path_laporan_ta  = null;

        $proposal->status_id        = 14;

        $tugasAkhir->save();
        $proposal->save();

        return redirect('seminar-kemajuan')->with('status', [
                'color'     => $this->notif_color,
                'content'   => 'Berhasil menghapus Pengajuan Seminar Kemajauan TA!', 
                ]
        );
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Dosen;
use App\Models\User;
use App\Models\TugasAkhir;
use App\Models\Proposal;
use App\Models\StatusTugasAkhir;
use App\Models\Catatan;

use App\Logic\TahunAjaran;

use Input;
use File;

class TugasAkhirController extends Controller
{
    protected $notif_color = 'grey-dark';
    protected $notes_color = 'blue-dark';
    protected $tahunAjaran;

    public function __construct() {
        $this->tahunAjaran = new TahunAjaran;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function catatan($id, $tipe) {
        if($tipe === 'pendaftaran-proposal')
            $kategori = 'Pendaftaran Proposal TA';
        else if($tipe === 'seminar-proposal')
            $kategori = 'Seminar Proposal TA';
        else if($tipe === 'seminar-kemajuan')
            $kategori = 'Seminar Kemajuan TA';
        else if($tipe === 'sidang')
            $kategori = 'Sidang Ujian Lisan TA';
        
        $listCatatan        = Catatan::where('tipe', '=', $tipe)
                                    ->where('source_id', '=', $id)
                                    ->get();
        $arrayData = [
            'kategori'      => $kategori,
            'listCatatan'   => $listCatatan
        ];
        return view('catatan.show')->with($arrayData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tugas_akhir    = new TugasAkhir;

        // $dosen_usulan                       = Dosen::find($request->input('usulan_dosen'));
        $tugas_akhir->judul                 = $request->input('usulan_judul');
        $tugas_akhir->dosen_pembimbing_id   = $request->input('usulan_dosen');
        $tugas_akhir->lab_ta_id             = $request->input('usulan_lab');
        $tugas_akhir->status_id             = 1;
        $tugas_akhir->tahun_ajaran          = $this->tahunAjaran->getTahunAjaran();
        $tugas_akhir->semester              = $this->tahunAjaran->getSemester();
        $user                           = User::where('username', '=', $request->input('username'))->first();
        
        $proposal                       = Proposal::where('user_id', '=', $user->id)->first();
        $proposal->status_id            = 5;
        $proposal->accepted_at          = \Carbon\Carbon::now();
        $proposal->save();

        $catatan = new Catatan;
        $catatan->tipe          = 'pendaftaran-proposal';
        $catatan->name          = 'diterima';
        $catatan->description   = $request->input('catatan');
        $user->catatan()->save($catatan);

        if($user->tugas_akhir()->save($tugas_akhir)) {
            return redirect('proposal/pendaftar')->with('status', [
                'color'     => $this->notif_color,
                'content'   => 'Berhasil! Pendaftaran Proposal TA ('.$user->username.') telah diterima.', 
            ]);
        }

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class JadwalOnlineMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next)
    {
        if ($this->auth->guest()) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('auth/login');
            }
        } else {
            if ($this->auth->user()->hasRole('operator') || $this->auth->user()->hasRole('koordinator') || $this->auth->user()->hasRole('kaprodi') || $this->auth->user()->hasRole('administrator') ) {
                return $next($request);
            } else {
                return redirect()->guest('home');
            }
        }
    }
}

<?php

Route::get('home', function() {
	return redirect('/');
});

Route::get('umum/tugas-akhir', [
	'as' 		=> 'umum.tugas-akhir',
	'uses' 		=> 'UmumController@index'
]);

Route::get('umum/tugas-akhir/{id}', [
	'as' 		=> 'umum.tugas-akhir.detail',
	'uses' 		=> 'UmumController@show'
]);

// Berkas Berita Acara dan Hasil Evaluasi
Route::get('berkas/{id}/{name}', [
	'as' 		=> 'berkas',
	'uses' 		=> 'UmumController@berkas'
]);

Route::get('berkas/jadwal/mahasiswa/{seminar}', [
	'as' 		=> 'berkas.jadwal.mahasiswa',
	'uses' 		=> 'UmumController@berkas_jadwal'
]);

//

Route::controllers([
	'auth' 		=> 'Auth\AuthController',
]);

Route::group(['middleware' => 'auth'], function () {

	Route::get('/', [
	    'as' 		=> 'user',
	    'uses' 		=> 'UserController@index'
	]);

	Route::get('jadwal/{seminar}/file', [
		'as' 		=> 'jadwal.file',
		'uses' 		=> 'JadwalController@file'
	]);

	Route::get('/file/{tipe}/{id}', [
		'as' 		=> 'file.preview',
		'uses' 		=> 'FileController@file_preview'
	]);

	Route::get('/file/{tipe}/{id}/download', [
		'as' 		=> 'file.download',
		'uses' 		=> 'FileController@file_download'
	]);

	Route::get('/file_zip/{tipe}/{id}/download', [
		'as' 		=> 'filezip.download',
		'uses' 		=> 'FileController@file_download_zip'
	]);

});

//tes

Route::get('reset', [
	'as' 		=> 'reset',
	'uses' 		=> 'MahasiswaManagementController@getReset'
]);

Route::post('reset', [
	'as' 		=> 'reset',
	'uses' 		=> 'MahasiswaManagementController@postReset'
]);

Route::group(['middleware' => 'mahasiswa'], function() {
	Route::resource('proposal', 'PendaftaranProposalController', ['except' => ['show', 'edit']]);
	Route::resource('asistensi', 'AsistensiController');
	
	Route::resource('seminar-proposal', 'SeminarProposalController', ['except' => ['show', 'edit']]);
	Route::resource('seminar-kemajuan', 'SeminarKemajuanController', ['except' => ['show', 'edit']]);
	Route::resource('sidang', 'SidangController', ['except' => ['show', 'edit']]);

	Route::get('catatan/{id}/{tipe}', [
		'as' 		=> 'catatan.mahasiswa',
		'uses' 		=> 'TugasAkhirController@catatan'
	]);

	Route::get('proposal/file/{file}', [
		'as'		=> 'proposal.file',
		'uses'		=> 'FileController@file_preview'
	]);

	Route::get('proposal/file/{file}/download', [
		'as'		=> 'proposal.file.download',
		'uses'		=> 'FileController@file_download'
	]);

	//Pendaftaran Proposal TA
	Route::get('proposal/submit', [
		'as'		=> 'proposal.submit',
		'uses'		=> 'PendaftaranProposalController@submit'
	]);

	//Seminar Proposal TA
	Route::get('seminar-proposal/submit', [
		'as'		=> 'seminar-proposal.submit',
		'uses'		=> 'SeminarProposalController@submit'
	]);

	Route::get('seminar-proposal/jadwal', [
		'as' 		=> 'seminar-proposal.jadwal',
		'uses' 		=> 'SeminarProposalController@jadwal'
	]);

	//Seminar Kemajuan TA
	Route::get('seminar-kemajuan/submit', [
		'as'		=> 'seminar-kemajuan.submit',
		'uses'		=> 'SeminarKemajuanController@submit'
	]);

	Route::get('seminar-kemajuan/jadwal', [
		'as' 		=> 'seminar-kemajuan.jadwal',
		'uses' 		=> 'SeminarKemajuanController@jadwal'
	]);

	//Sidang Ujian Lisan TA
	Route::get('sidang/submit', [
		'as'		=> 'sidang.submit',
		'uses'		=> 'SidangController@submit'
	]);

	Route::get('sidang/jadwal', [
		'as' 		=> 'sidang.jadwal',
		'uses' 		=> 'SidangController@jadwal'
	]);
});

Route::group(['middleware' => 'koordinator'], function() {

	//Pengaturan Seminar
	Route::resource('pengaturan-seminar', 'PengaturanSeminarController');
	Route::get('pengaturan-seminar/{id}/detail', [
		'as' 	=> 'pengaturan-seminar.detail',
		'uses' 	=> 'PengaturanSeminarController@detail'
	]);

	// pendaftaran proposal
	Route::get('proposal/pendaftar', [
		'as' 		=> 'proposal.list.pendaftar',
		'uses'		=> 'PendaftaranProposalController@showAllSubmited'
	]);

	Route::post('proposal/pendaftar/{id}/tolak', [
		'as'		=> 'proposal.pendaftar.tolak',
		'uses'		=> 'PendaftaranProposalController@tolak'
	]);

	Route::post('proposal/pendaftar/{id}/terima', [
		'as'		=> 'proposal.pendaftar.terima',
		'uses'		=> 'TugasAkhirController@store'
	]);

	Route::get('proposal/pendaftar/{id}/detail', [
		'as'		=> 'proposal.pendaftar.detail',
		'uses' 		=> 'PendaftaranProposalController@show'
	]);

	// pendaftaran seminar proposal
	Route::get('seminar-proposal/pendaftar', [
		'as' 		=> 'seminar-proposal.list.pendaftar',
		'uses'		=> 'SeminarProposalController@showAllSubmited'
	]);

	Route::get('seminar-proposal/pendaftar/{id}/detail', [
		'as'		=> 'seminar-proposal.pendaftar.detail',
		'uses' 		=> 'SeminarProposalController@show'
	]);

	Route::post('seminar-proposal/pendaftar/{id}/tolak', [
		'as'		=> 'seminar-proposal.pendaftar.tolak',
		'uses'		=> 'SeminarProposalController@tolak'
	]);

	Route::post('seminar-proposal/pendaftar/{id}/terima', [
		'as'		=> 'seminar-proposal.pendaftar.terima',
		'uses'		=> 'SeminarProposalController@terima'
	]);

	Route::get('seminar-proposal/on', [
		'as' 		=> 'seminar-proposal.on',
		'uses' 		=> 'SeminarProposalController@showAllOn'
	]);

	Route::get('seminar-proposal/{id}/{status}', [
		'as' 		=> 'seminar-proposal.on.change',
		'uses' 		=> 'SeminarProposalController@change'
	]);

	//Seminar Kemajuan TA
	Route::get('seminar-kemajuan/pendaftar', [
		'as' 		=> 'seminar-kemajuan.list.pendaftar',
		'uses'		=> 'SeminarKemajuanController@showAllSubmited'
	]);

	Route::get('seminar-kemajuan/pendaftar/{id}/detail', [
		'as'		=> 'seminar-kemajuan.pendaftar.detail',
		'uses' 		=> 'SeminarKemajuanController@show'
	]);

	Route::post('seminar-kemajuan/pendaftar/{id}/tolak', [
		'as'		=> 'seminar-kemajuan.pendaftar.tolak',
		'uses'		=> 'SeminarKemajuanController@tolak'
	]);

	Route::post('seminar-kemajuan/pendaftar/{id}/terima', [
		'as'		=> 'seminar-kemajuan.pendaftar.terima',
		'uses'		=> 'SeminarKemajuanController@terima'
	]);

	Route::get('seminar-kemajuan/on', [
		'as' 		=> 'seminar-kemajuan.on',
		'uses' 		=> 'SeminarKemajuanController@showAllOn'
	]);

	Route::get('seminar-kemajuan/{id}/{status}', [
		'as' 		=> 'seminar-kemajuan.on.change',
		'uses' 		=> 'SeminarKemajuanController@change'
	]);

	//Sidang TA
	Route::get('sidang/pendaftar', [
		'as' 		=> 'sidang.list.pendaftar',
		'uses'		=> 'SidangController@showAllSubmited'
	]);

	Route::get('sidang/pendaftar/{id}/detail', [
		'as'		=> 'sidang.pendaftar.detail',
		'uses' 		=> 'SidangController@show'
	]);

	Route::post('sidang/pendaftar/{id}/tolak', [
		'as'		=> 'sidang.pendaftar.tolak',
		'uses'		=> 'SidangController@tolak'
	]);

	Route::post('sidang/pendaftar/{id}/terima', [
		'as'		=> 'sidang.pendaftar.terima',
		'uses'		=> 'SidangController@terima'
	]);

	Route::get('sidang/on', [
		'as' 		=> 'sidang.on',
		'uses' 		=> 'SidangController@showAllOn'
	]);

	Route::get('sidang/{id}/{status}', [
		'as' 		=> 'sidang.on.change',
		'uses' 		=> 'SidangController@change'
	]);
	
	// file
	Route::get('proposal/file/pendaftar/{id}/{file}', [
		'as'		=> 'proposal.pendaftar.file',
		'uses'		=> 'FileController@file_preview'
	]);

	Route::get('proposal/file/pendaftar/{id}/{file}/download', [
		'as'		=> 'proposal.pendaftar.file.download',
		'uses'		=> 'FileController@file_download'
	]);

	Route::get('seminar-proposal/file/pendaftar/{id}/{file}', [
		'as'		=> 'seminar-proposal.pendaftar.file',
		'uses'		=> 'FileController@file_preview'
	]);

	Route::get('seminar-proposal/file/pendaftar/{id}/{file}/download', [
		'as'		=> 'seminar-proposal.pendaftar.file.download',
		'uses'		=> 'FileController@file_download'
	]);

	//File Jadwal
	Route::get('jadwal/file/upload/{seminar}', [
		'as' 		=> 'jadwal.file.upload',
		'uses' 		=> 'JadwalController@form_upload'
	]);

	Route::post('jadwal/file/upload', [
		'as' 		=> 'jadwal.file.upload.store',
		'uses' 		=> 'JadwalController@form_upload_store'
	]);

	Route::delete('jadwal/file/upload/{id}', [
		'as' 		=> 'jadwal.file.upload.destroy',
		'uses' 		=> 'JadwalController@file_destroy'
	]);

	

});

Route::group(['middleware' => 'jadwalonline'], function() {
	//Jadwal Online
	Route::get('jadwal/{seminar}', [
		'as' 		=> 'jadwal.index',
		'uses' 		=> 'JadwalController@index'
	]);

	Route::get('jadwal/{seminar}/create', [
		'as' 		=> 'jadwal.create',
		'uses' 		=> 'JadwalController@create'
	]);

	Route::post('jadwal/{seminar}/store', [
		'as' 		=> 'jadwal.store',
		'uses' 		=> 'JadwalController@store'
	]);

	Route::get('jadwal/{id}/detail', [
		'as' 		=> 'jadwal.detail',
		'uses' 		=> 'JadwalController@show'
	]);

	Route::delete('jadwal/{id}/destroy', [
		'as' 		=> 'jadwal.destroy',
		'uses' 		=> 'JadwalController@destroy'
	]);

	//Ajax Route
	Route::post('ajax/dosen-pembimbing', [
		'as' 		=> 'ajax.getDosenPembimbing',
		'uses' 		=> 'AjaxController@getDosenPembimbing'
	]);

	Route::post('ajax/dosen-penguji', [
		'as' 		=> 'ajax.getDosenPenguji',
		'uses' 		=> 'AjaxController@getDosenPenguji'
	]);

	Route::post('ajax/ruangan', [
		'as' 		=> 'ajax.getRuangan',
		'uses' 		=> 'AjaxController@getRuangan'
	]);

	//Daftar Mahasiswa
	Route::resource('mahasiswa', 'MahasiswaManagementController');
	Route::get('mahasiswa/upload/daftar', [
		'as' 	=> 'mahasiswa.upload.daftar.create',
		'uses' 	=> 'MahasiswaManagementController@create_daftar_mahasiswa'
	]);

	Route::post('mahasiswa/upload/daftar', [
		'as' 	=> 'mahasiswa.upload.daftar.store',
		'uses' 	=> 'MahasiswaManagementController@store_daftar_mahasiswa'
	]);

	Route::get('mahasiswa/akun/create', [
		'as' 	=> 'mahasiswa.akun.create',
		'uses' 	=> 'MahasiswaManagementController@mhs_akun_create'
	]);

	Route::post('mahasiswa/akun/store', [
		'as' 	=> 'mahasiswa.akun.store',
		'uses' 	=> 'MahasiswaManagementController@mhs_akun_store'
	]);

});

Route::group(['prefix' => 'admin', 'middleware' => 'administrator'], function () {

	Route::get('akun-pegawai', [
		'as' 		=> 'akun-pegawai.showAll',
		'uses' 		=> 'AdminController@showAllAkunPegawai'
	]);

	Route::get('akun-mahasiswa', [
		'as' 		=> 'akun-mahasiswa.showAll',
		'uses' 		=> 'AdminController@showAllAkunMahasiswa'
	]);

	//Akun
	Route::post('akun/reset/{id}', [
		'as' 		=> 'akun.reset',
		'uses' 		=> 'AdminController@akunReset'
	]);

	Route::get('akun/create', [
		'as' 		=> 'akun.create',
		'uses' 		=> 'AdminController@getAkun'
	]);

	Route::get('akun/{id}', [
		'as' 		=> 'akun.showAkun',
		'uses' 		=> 'AdminController@showAkun'
	]);

	Route::patch('akun/{id}', [
		'as' 		=> 'akun.update',
		'uses' 		=> 'AdminController@update'
	]);

	Route::delete('akun/{id}', [
		'as' 		=> 'akun.destroy',
		'uses' 		=> 'AdminController@destroyAkun'
	]);

	//Data Jam
	Route::get('data-jam', [
		'as' 		=> 'data-jam.showAll',
		'uses' 		=> 'AdminController@showAllDataJam'
	]);

	Route::get('data-jam/create', [
		'as' 		=> 'data-jam.create',
		'uses' 		=> 'AdminController@createJam'
	]);

	Route::post('data-jam/store', [
		'as' 		=> 'data-jam.store',
		'uses' 		=> 'AdminController@storeJam'
	]);

	Route::delete('data-jam/{id}', [
		'as' 		=> 'data-jam.destroy',
		'uses' 		=> 'AdminController@destroyJam'
	]);

	//Data Dosen
	Route::get('data-dosen', [
		'as' 		=> 'data-dosen.showAll',
		'uses' 		=> 'AdminController@showAllDataDosen'
	]);

	Route::get('data-dosen/create', [
		'as' 		=> 'data-dosen.create',
		'uses' 		=> 'AdminController@createDosen'
	]);

	Route::post('data-dosen/store', [
		'as' 		=> 'data-dosen.store',
		'uses' 		=> 'AdminController@storeDosen'
	]);

	Route::get('data-dosen/{id}', [
		'as' 		=> 'data-dosen.showDosen',
		'uses' 		=> 'AdminController@showDosen'
	]);
	
	Route::patch('data-dosen/{id}', [
		'as' 		=> 'data-dosen.update',
		'uses' 		=> 'AdminController@updateDosen'
	]);

	Route::delete('data-dosen/{id}', [
		'as' 		=> 'data-dosen.destroy',
		'uses' 		=> 'AdminController@destroyDosen'
	]);

	Route::get('berkas', [
		'as' 		=> 'admin.berkas',
		'uses' 		=> 'AdminController@berkas'
	]);

	Route::get('berkas/download/{tipe}', [
		'as' 		=> 'admin.berkas.download',
		'uses' 		=> 'AdminController@berkas_download'
	]);

	Route::post('berkas/update', [
		'as' 		=> 'admin.berkas.update',
		'uses' 		=> 'AdminController@berkas_update'
	]);
});

Route::post('akun/store', [
	'as' 		=> 'akun.store',
	'uses' 		=> 'AdminController@postAkun'
]);